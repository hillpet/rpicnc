/*  (c) Copyright:  2013..2016  Patrn, Confidential Data
**
**  $Workfile:   globals.h
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Define the mmap variables, common over all threads. If
**                      the structure needs to change, roll the revision number to make sure a new mmap file is created !
**                      
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       27 May 2013
**
 *  Revisions:
 *    $Log:   $
 *
 **/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <semaphore.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <hidapi/hidapi.h>

#define  ONEMILLION           1000000L
#define  ONEBILLION           1000000000L
#define  MAX_PARM_LEN         31
#define  MAX_PARM_LENZ        (MAX_PARM_LEN+1)
//
#define  MAX_PATH_LEN         255
#define  MAX_PATH_LENZ        (MAX_PATH_LEN+1)
//
#define  MAX_URL_LEN          1024
#define  MAX_CMD_LEN          200
#define  MAX_ARG_LEN          1000
#define  MAX_CMDLINE_LEN      (MAX_CMD_LEN + MAX_ARG_LEN)
//
//
// Virtual LCD Size : 25 x 80 chars
//
#define  LCD_VIRTUAL_ROWS     25
#define  LCD_VIRTUAL_COLS     80
#define  LCD_VIRTUAL_LOG      14
#define  LCD_VIRTUAL_LOG_LINES 5
//
#define  LCD_BACKLIGHT_ON     10
#define  LCD_SCROLL_COL       16
//
// Buttons and RCU Key defines
//
#define  MAX_NUM_BUTKEYS      8

#include "echotype.h"
#include "config.h"
#include "netfiles.h"
#include "httppage.h"

#ifdef   FEATURE_SHOW_COMMANDS
#define  GLOBAL_DUMPVARS(x)            GLOBAL_DumpVars(x)
#define  GLOBAL_DUMPCOMMAND(x)         GLOBAL_DumpCommand(x)
#else    //FEATURE_SHOW_COMMANDS
#define  GLOBAL_DUMPVARS(x)
#define  GLOBAL_DUMPCOMMAND(x)
#endif   //FEATURE_SHOW_COMMANDS

typedef u_int8 (*PFKEY)(u_int8);
typedef bool   (*PFCMD)();

typedef struct GLOCMD
{
   int      iStatus;
   int      iCommand;
   int      iData1;
   int      iData2;
   int      iData3;
   char     pcExec[MAX_CMD_LEN];
   char     pcArgs[MAX_ARG_LEN];
   PFCMD    pfCallback;
   NETCL   *pstCl;
}  GLOCMD;

typedef struct GLOBALS
{
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iStorageOffset;
   int         iStorageSize;
}  GLOBALS;

typedef struct KEYLUT
{
   u_int8   ubKey;
   PFKEY    pfCallback;
}  KEYLUT;

typedef struct GLOBUT
{
   u_int8      ubId;             // Physical Key ID: BUTTON_1...BUTTON_8 or 0
   u_int8      ubKey;            // Logical  Key ID: BUT_KEY1...
   bool        fChanged;         // Key changed yesno
   int         iStatus;          // Key status CNC_STATUS_KEYPRESS, ...
   PFKEY       pFuncButton;      // Key handler
   bool        fRepeat;          // Key pressed repeated
   u_int32     ulImage;          // Key pressed/depressed shift image
   u_int32     ulLastImage;      // Key last pressed/depressed shift image
   u_int32     ulDebounce;       // Key debounce mask
}  GLOBUT;

typedef struct VLCD
{
   bool     fLcdCursor;
   int      iLcdMode;
   int      tLcdHandle;
   u_int8   ubVirtRow, ubVirtCol;
   u_int8   ubCursRow, ubCursCol;
   //
   char     pcVirtLcd[LCD_VIRTUAL_ROWS*LCD_VIRTUAL_COLS];
}  VLCD;

typedef enum MAPSTATE
{
  MAP_CLEAR = 0,           // Clear all the mapped memory
  MAP_SYNC,                // Synchronize the map
  MAP_RESTART,             // Restart the counters using the last stored data

  NUM_MAPSTATES
} MAPSTATE;

//
// The USB Comms stucture
//
#define  GEN_USB_SIZE         2048        // USB generic buffer size
#define CMD_XFER_SIZE         64          // USB Command xfer buffer size
#define DBG_XFER_SIZE         1024        // USB debug buffer size
//

typedef struct _rpiusb_
{
   int               iIndex;              // Device Index
   int               iVendorId;           // Vendor ID
   int               iProductId;          // Product ID
   int               iInterfaceNr;        // Device Interface Number
   char             *pcDevicePath;        // Pathname
   char             *pcManufacturer;      // Manuf string
   char             *pcProduct;           // Product string
   char             *pcSerialNr;          // Serial number string
   hid_device       *ptHandle;            // Device handle
   struct _rpiusb_  *pstNext;             // Next device
}  RPIUSB;

//
typedef struct _comms_
{
   u_int8   ubStatus;
   u_int8   ubCommand;                    // Current CNC command
   long     lLineNr;                      //             line number
   //
   u_int8   pubUsb[GEN_USB_SIZE];         // USB command xfer buffer
}  COMMS;

//
// Main CNC parameter struct
//
typedef struct _rpicnc_
{
   RPIUSB  *pstRawHid;                    // CNC HID handle
   int      iSeq;                         // Sequence number
   int      iCncMotion;                   // CNC motion status
   int      iCncDelay;                    // CNC motion delay
   int      iCncPulse;                    // CNC stepper pulse delay
   int      iDrawMode;                    // CNC Pattern/drawing mode
   int      iMoveSteps;                   // CNC Pattern/moving steps
   int      iDrawSteps;                   // CNC Pattern/drawing steps
   // Settings
   float    fCanvW;                       // Total canvas  size  (mm)
   float    fCanvH;                       //
   float    fDrawW;                       // Total drawing size  (mm)
   float    fDrawH;                       //
   float    fSrev;                        // Steps per mm 
   float    fRad;                         // Radius of cogwheel   (mm)
   // Variables
   float    fXc, fYc, fZc;                // Current coords       (mm)
   float    fXn, fYn, fZn;                // New     coords       (mm)
   float    fXs, fYs, fZs;                // Corner lo left       (mm)
   float    fXe, fYe, fZe;                // Corner Hi right      (mm)
   float    fResXm;                       // Movement res X       (mm)
   float    fResYm;                       // Movement res Y       (mm)
   float    fResZm;                       // Movement res Z       (mm)
   float    fResXd;                       // Drawing  res X       (mm)
   float    fResYd;                       // Drawing  res Y       (mm)
   float    fResZd;                       // Drawing  res Z       (mm)
   float    fVar1, fVar2;                 // Misc variables
   //
   float    fAccSet[NUM_AXIS];            // steppers Acceleration
   float    fSpdMin[NUM_AXIS];            // steppers Minimum speed
   float    fSpdMax[NUM_AXIS];            // steppers Maximum speed
   long     lCurPos[NUM_AXIS];            // steppers Current position
   long     lNewPos[NUM_AXIS];            // Steppers New position
}  RPICNC;


typedef struct CNCMAP
{
   int         G_iSignatureStart;
   int         G_iStatus;
   u_int32     G_ulStartTimestamp;
   char        G_iVersionMajor;
   char        G_iVersionMinor;
   //
   pid_t       G_tHostPid;
   pid_t       G_tRootPid;
   pid_t       G_tSrvrPid;
   pid_t       G_tUdpPid;
   pid_t       G_tCncWrPid;
   pid_t       G_tCncDbPid;
   //
   sem_t       G_tCncSem;
   sem_t       G_tUdpSem;
   //
   bool        G_Connected;
   bool        G_HttpGetRequest;
   bool        G_HttpPostRequest;
   u_int32     G_ulSecondsCounter;
   //
   // User settings
   //
   char        G_pcCncDelay         [MAX_PARM_LENZ];
   char        G_pcCncPulse         [MAX_PARM_LENZ];
   char        G_pcDrawMode         [MAX_PARM_LENZ];
   char        G_pcCurPos[NUM_AXIS] [MAX_PARM_LENZ];
   char        G_pcNewPos[NUM_AXIS] [MAX_PARM_LENZ];
   char        G_pcStaPos[NUM_AXIS] [MAX_PARM_LENZ];
   char        G_pcEndPos[NUM_AXIS] [MAX_PARM_LENZ];
   char        G_pcAccSet[NUM_AXIS] [MAX_PARM_LENZ];
   char        G_pcSpdMin[NUM_AXIS] [MAX_PARM_LENZ];
   char        G_pcSpdMax[NUM_AXIS] [MAX_PARM_LENZ];
   //
   char        G_pcCanvasWidth      [MAX_PARM_LENZ];
   char        G_pcCanvasHeight     [MAX_PARM_LENZ];
   char        G_pcDrawWidth        [MAX_PARM_LENZ];
   char        G_pcDrawHeight       [MAX_PARM_LENZ];
   char        G_pcRadius           [MAX_PARM_LENZ];
   char        G_pcStepRevs         [MAX_PARM_LENZ];
   char        G_pcMoveSteps        [MAX_PARM_LENZ];
   char        G_pcDrawSteps        [MAX_PARM_LENZ];
   //
   char        G_pcNumFiles         [MAX_PARM_LENZ];
   char        G_pcFilter           [MAX_PARM_LENZ];
   char        G_pcPixWidth         [MAX_PARM_LENZ];
   char        G_pcPixHeight        [MAX_PARM_LENZ];
   char        G_pcPixFileExt       [MAX_PARM_LENZ];
   char        G_pcDetectPixel      [MAX_PARM_LENZ];
   char        G_pcDetectLevel      [MAX_PARM_LENZ];
   char        G_pcDetectSeq        [MAX_PARM_LENZ];
   char        G_pcDetectRed        [MAX_PARM_LENZ];
   char        G_pcDetectGreen      [MAX_PARM_LENZ];
   char        G_pcDetectBlue       [MAX_PARM_LENZ];
   char        G_pcDetectZoomX      [MAX_PARM_LENZ];
   char        G_pcDetectZoomY      [MAX_PARM_LENZ];
   char        G_pcDetectZoomW      [MAX_PARM_LENZ];
   char        G_pcDetectZoomH      [MAX_PARM_LENZ];
   char        G_pcDetectAvg1       [MAX_PARM_LENZ];
   char        G_pcDetectAvg2       [MAX_PARM_LENZ];
   //
   //
   // System settings
   //
   char        G_pcVersionSw        [MAX_PARM_LENZ];
   char        G_pcLastFileSize     [MAX_PARM_LENZ];
   char        G_pcTimeDrawing      [MAX_PARM_LENZ];
   char        G_pcStatus           [MAX_PARM_LENZ];
   char        G_pcDelete           [MAX_PARM_LENZ];
   //
   char        G_pcOutputFile       [MAX_PATH_LENZ];
   char        G_pcLastFile         [MAX_PATH_LENZ];
   char        G_pcPixPath          [MAX_PATH_LENZ];
   //
   char        G_pcTcpIfce          [MAX_PARM_LENZ];
   char        G_pcTcpAddr          [INET_ADDRSTRLEN+1];
   char        G_pcTcpPort          [MAX_PARM_LENZ];
   char        G_pcUdpPort          [MAX_PARM_LENZ];
   char        G_pcUdpAddr          [INET_ADDRSTRLEN+1];
   //
   // Change markers
   //
   u_int8      G_ubWidthChanged;
   u_int8      G_ubHeightChanged;
   u_int8      G_ubPixFileExtChanged;
   u_int8      G_ubIpAddrChanged;
   u_int8      G_ubOutputFileChanged;
   u_int8      G_ubPixPathChanged;
   //
   SOCKADDR_IN G_stUdpSocket;
   GLOBUT      G_stButtons          [MAX_NUM_KEYS];
   GLOCMD      G_stCmd;
   VLCD        G_stVirtLcd;
   //
   RPICNC      G_stCnc;
   COMMS       G_stUsbRcv;
   COMMS       G_stUsbXmt;
   //
   int         G_iSignatureEnd;
}  CNCMAP;

//
// Global functions
//
int      GLOBAL_AddPid           (char *);
bool     GLOBAL_Close            (void);
void     GLOBAL_DumpCommand      (char *);
void     GLOBAL_DumpVars         (char *);
CNCMAP  *GLOBAL_Init             (void);
int      GLOBAL_Status           (int);
CNCMAP  *GLOBAL_GetMapping       (void);
u_int32  GLOBAL_ReadSecs         (void);
int      GLOBAL_RemovePid        (char *);
void     GLOBAL_RestoreDefaults  (void);
bool     GLOBAL_Share            (void);
bool     GLOBAL_Sync             (void);
void     GLOBAL_UpdateSecs       (void);

#endif /* _GLOBALS_H_ */
