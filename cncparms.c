/*  (c) Copyright:  2016  Patrn, Confidential Data 
 *
 *  $Workfile:          cncparms.cpp
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            CNC parameter handling functions and data
 *
 *
 *  Entry Points:       
 *
 *
 *
 *  Compiler/Assembler: gnu gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       19 May 2016
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>

#include "echotype.h"
#include "misc_api.h"
#include "dynpage.h"
#include "httppage.h"
#include "rtc.h"
#include "log.h"
#include "raspjson.h"
#include "globals.h"
#include "graphics.h"
#include "fileinfo.h"
#include "rpicnc.h"
#include "cncparms.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local data
//
static bool    cnc_BuildMessageBody          (NETCL *, RPIPAGE *, int, bool);
static bool    cnc_BuildMessageBodyArray     (NETCL *, RPIPAGE *, int, bool);
static bool    cnc_BuildMessageBodyModes     (NETCL *, RPIPAGE *, int);
//
static bool    cnc_RpiJsonDrawModes          (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonMotionParms        (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonMotionNormalize    (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonMotionDelta        (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonMotionZoom         (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonFotos              (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonParms              (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonRun                (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonDefaults           (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonDeleteFotos        (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiJsonGetFileSize        (char *, char *, int);
static bool    cnc_RpiJsonStop               (NETCL *, RPIPAGE *, FTYPE);
//
// CncParms: CNC HTML commands:
//
static bool    cnc_RpiHtmlFotos              (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiHtmlDeleteFotos        (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiHtmlParms              (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiHtmlDefaults           (NETCL *, RPIPAGE *, FTYPE);
static bool    cnc_RpiHtmlShowFiles          (NETCL *, char *, char *, char *);
//
static int     cnc_RpiCollectParms           (NETCL *, FTYPE, int);
static bool    cnc_RpiHandleParm             (const char *, FTYPE, int);
//
static bool    cnc_ParmHasChanged            (const CNCPARMS *);
static bool    cnc_ParmSetChanged            (const CNCPARMS *, bool);
static void    cnc_UpdateCncRealtime         (void);
static void    cnc_MotionDetectDefaults      (void);
static bool    cnc_MotionNormalize           (void);
static bool    cnc_MotionDelta               (void);
static bool    cnc_MotionZoom                (void);
//
static bool    cnc_ValueToStorageFloat       (char *, void *, int, int);
static bool    cnc_ValueToStorageText        (char *, void *, int, int);
static bool    cnc_ValueToStorageLong        (char *, void *, int, int);
static bool    cnc_ValueToStorageInt         (char *, void *, int, int);
//
static bool    cnc_StorageToValueFloat       (void *, char *);
static bool    cnc_StorageToValueText        (void *, char *, int);
static bool    cnc_StorageToValueLong        (void *, char *);
static bool    cnc_StorageToValueInt         (void *, char *);
//
static RPIJSON    *cnc_RpiJsonShowFiles      (NETCL *, RPIJSON *, char *, char *, bool);
static const char *cnc_FindDelimiter         (const char *, char *);
//
// These are the supported dynamic HTML pages: GET /cam.html?parms
//
static RPIPAGE stHtmlHttpPages[] =
{
// iFunction    pcFunction        pfHandler
   { CNC_ALL,  "parms",           cnc_RpiHtmlParms        },      // On top to show correct parms object
   //
   { CNC_PIX,  "fotos",           cnc_RpiHtmlFotos        },
   { CNC_RMP,  "rm-fotos",        cnc_RpiHtmlDeleteFotos  },
   { CNC_DEF,  "defaults",        cnc_RpiHtmlDefaults     },
   { 0,        0,                 0                       }
};

//
// These are the supported dynamic JSON pages: GET /cam.json?parms
//
static RPIPAGE stJsonHttpPages[] =
{
// iFunction    pcFunction        pfHandler
   { CNC_PRM,  "parms",           cnc_RpiJsonParms          },      // On top to show correct parms object
   //
   { CNC_PRM,  "run",             cnc_RpiJsonRun            },
   { CNC_PRM,  "pause",           cnc_RpiJsonParms          },
   { CNC_PRM,  "stop",            cnc_RpiJsonStop           },
   { CNC_DRW,  "drawmodes",       cnc_RpiJsonDrawModes      },
   { CNC_PIX,  "mparms",          cnc_RpiJsonMotionParms    },
   { CNC_PIX,  "mnorm",           cnc_RpiJsonMotionNormalize},
   { CNC_PIX,  "mdelta",          cnc_RpiJsonMotionDelta    },
   { CNC_PIX,  "mzoom",           cnc_RpiJsonMotionZoom     },
   { CNC_PIX,  "fotos",           cnc_RpiJsonFotos          },
   { CNC_RMP,  "rm-fotos",        cnc_RpiJsonDeleteFotos    },
   { CNC_DEF,  "defaults",        cnc_RpiJsonDefaults       },
   { 0,        0,                 0                         }
};

//
// CNC Draw modes
//
#define  EXTRACT_DRW(a,b,c,d,e,f,g)   {b,c,d,e,f,g},
static const CNCDRW stCncDrawModes[] =
{
   #include "cncdraw.h"
   {  -1,   -1,   -1,   -1,   -1,   ""    }
};
#undef   EXTRACT_DRW

//
// CNC parameter list:
//
// iFunction:        Define if a parameter affects certain CNC function
//                      JSN_TXT : For JSON API: parameter is formatted as JSON text    ("value" = "xxxxx")
//                      JSN_INT :               parameter is formatted as JSON number  ("value" = xxxxx)
//
//                      CNC_PIX : Paramater concerns Picture file retrieval
//                      CNC_PRM :                    Parameter definition
//                      CNC_STP :                    Stopping command-in-progress
//                      CNC_ALL :                    All HTTP commands
//                      CNC____ :                    None of the commands
//
// pcJson:           JSON API parameter name
// pcParm:           HTML API parameter name
// iStorageOffset:   Global mmap structure offset of the parameter
// iStorageSize:     Global mmap parameter array size
// iChangedOffset:   Global mmap structure offset of the parameter has changed-marker
//
// CNC Stoare/Vars xref list
//
#define  EXTRACT_CNC(a,b,c,d,e,f,g,h,i)   {b,c,d,e,f,g,h,i},
static const CNCPARMS stCncParameters[] =
{
   #include "cncxref.h"
   {0, 0, NULL, NULL, 0, 0, 0, 0  }
};
#undef   EXTRACT_CNC

//
// Motion detect definitions
//
static u_int32       ulDrawingStart = -1;
//
static const char   *pcFormat = "RGB";
static               MDET stMotionDetect;
static const int     iMotionDetectDefaults[GRF_GRID_COUNT][GRF_GRID_COUNT] =
                     {  //  1    2    3    4    5    6    7    8    9   10
                        {   0,   0,   0,   0,   0,   0,   0,   0,   0,   0 },    // 1
                        {   0, 100, 100, 100, 100, 100, 100, 100, 100,   0 },    // 2
                        {   0, 100, 100, 100, 100, 100, 100, 100, 100,   0 },    // 3
                        {   0, 100, 100, 100, 100, 100, 100, 100, 100,   0 },    // 4
                        {   0, 100, 100, 100, 100, 100, 100, 100, 100,   0 },    // 5
                        {   0, 100, 100, 100, 100, 100, 100, 100, 100,   0 },    // 6
                        {   0, 100, 100, 100, 100, 100, 100, 100, 100,   0 },    // 7
                        {   0, 100, 100, 100, 100, 100, 100, 100, 100,   0 },    // 8
                        {   0, 100, 100, 100, 100, 100, 100, 100, 100,   0 },    // 9
                        {   0,   0,   0,   0,   0,   0,   0,   0,   0,   0 }     // 10
                     };
//
// Misc http header strings 
//
static char pcWebPageStopBad[]         = "<p>CncParms: No ongoing cam action</p>" NEWLINE;
static char pcWebPageParmsChanged[]    = "<p>CncParms: Parms Changed</p></br>Cam Status=%s" NEWLINE;
static char pcWebPageDefaultsChanged[] = "<p>CncParms: Parms back to default</p>" NEWLINE;
//
static const char *pcPublicWwwDir      = RPI_PUBLIC_WWW;
static const char *pcMotionDir         = RPI_MOTION_DIR;

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : CNC_InitParms
 * Description : Init Cnc Parms
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 *
 */
bool CNC_InitParms()
{
   ulDrawingStart = GLOBAL_ReadSecs();
   cnc_MotionDetectDefaults();
   return(TRUE);
}

/*
 * Function    : CNC_GetParms
 * Description : Handle the CncParms commands
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 */
bool CNC_GetParms(NETCL *pstCl, FTYPE tType)
{
   bool     fCc = FALSE;
   PFRPI    pfDefaultUrl;
   PFRPI    pfDynamicUrl;
   RPIPAGE *pstPages=NULL;
   char    *pcRpiPage;
   char    *pcUrlPage=NULL;

   //
   // Generic CncParms entry for the dynamic cnc page:
   //    HTTP-API:
   //          GET /http://RPI_IP_ADDRESS:8081/cnc.html?draw&parm_1&parm_2....&parm_n
   //                                                   ^
   //                                                   Index in pstCl receive buffer
   //
   //    JSON-API:
   //          GET /http://RPI_IP_ADDRESS:8081/cam.json?draw&parm_1=xx&parm_2=yy....&parm_n
   //                                                   ^
   //                                                   Index in pstCl receive buffer
   // Lookup command
   //
   switch(tType)
   {
      case HTTP_HTML:
         // Use HTML/JS API
         pfDefaultUrl  = cnc_RpiHtmlParms;
         pstPages      = stHtmlHttpPages;
         break;
      
      case HTTP_JSON:
         // Use JSON API
         pfDefaultUrl  = cnc_RpiJsonParms;
         pstPages      = stJsonHttpPages;
         break;
      
      default:
         return(FALSE);
   }
   pcUrlPage = HTTP_CollectGetParameterValue(pstCl);
   //
   while(pcUrlPage)
   {
      pcRpiPage = pstPages->pcFunction;
      if(pcRpiPage == NULL) break;
      //
      if(strcasecmp(pcRpiPage, pcUrlPage) == 0)
      {
         PRINTF1("CNC_GetParms(): %s" CRLF, pcUrlPage);
         //
         // This request is a dynamic HTTP command
         //
         pfDynamicUrl = (PFRPI) pstPages->pfHandler;
         if(pfDynamicUrl)
         {
            //
            // Execute CNC command
            //
            PRINTF1("CNC_GetParms():%s" CRLF, pstPages->pcFunction);
            fCc = pfDynamicUrl(pstCl, pstPages, tType);
         }
         else
         {
            //
            // No page supplied: take default
            //
            PRINTF("CNC_GetParms():Dyn URL not in list: use default function" CRLF);
            fCc = pfDefaultUrl(pstCl, pstPages, tType);
         }
         return(fCc);
      }
      else
      {
         pstPages++;
      }
   }
   //
   // The dynamic page was found, but no sub-page: take default one
   //
   PRINTF("CNC_GetParms():No URL: use default function" CRLF);
   fCc = pfDefaultUrl(pstCl, pstPages, tType);
   return(fCc);
}

/*
 * Function    : CNC_GetFromValue
 * Description : Get correct data from the Values struct
 *
 * Parameters  : Parms struct, buffer^, size
 * Returns     : Buffer^
 *
 */
char *CNC_GetFromValue(const CNCPARMS *pstParm, char *pcDst, int iSize)
{
   int      iValueOffset;
   void    *pvSrc;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   RPICNC  *pstCnc=&pstMap->G_stCnc;

   iValueOffset = pstParm->iValueOffset;
   if(iValueOffset != -1)
   {
      pvSrc = (void *) pstCnc + iValueOffset; 
      //
           if(pstParm->iFunction & CNC_FLT) cnc_ValueToStorageFloat(pcDst, pvSrc, iSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_TXT) cnc_ValueToStorageText (pcDst, pvSrc, iSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_LNG) cnc_ValueToStorageLong (pcDst, pvSrc, iSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_INT) cnc_ValueToStorageInt  (pcDst, pvSrc, iSize, pstParm->iPrec);
   }
   return(pcDst);
}

/*
 * Function    : CNC_ValueToStorage
 * Description : Update entries in the G_ storage with actual values
 *
 * Parameters  : value enum, value
 * Returns     : TRUE if OKee
 * Note        : 
 *                Src : RPICNC *pstCnc->iDrawMode)
 *                Dest: CNCMAP *pstMap->G_pcDrawMode 
 *
 */
bool CNC_ValueToStorage(int iVal, RPICNC *pstCnc)
{
   bool     bCc=FALSE;
   int      iValueOffset, iStorageOffset;
   char    *pcDst;
   void    *pvSrc;
   //
   const CNCPARMS *pstParm;
   CNCMAP         *pstMap = GLOBAL_GetMapping();

   if( (iVal < 0) || (iVal > NUM_CNC_VARS) )
   {
      PRINTF1("CNC_ValueToStorage(): Error: index out of range (%d)" CRLF, iVal);
      LOG_Report(0, "CNC", "Error: index out of range (%d)", iVal);
      return(bCc);
   }
   //
   pstParm        = &stCncParameters[iVal];
   iValueOffset   = pstParm->iValueOffset;
   iStorageOffset = pstParm->iStorageOffset;

   //PRINTF4("CNC_ValueToStorage(%d):%s: VO=%d, GO=%d" CRLF, iVal, pstParm->pcJson, iValueOffset, iStorageOffset);
   
   if( (iValueOffset != -1) && (iStorageOffset != -1) )
   {
      pvSrc = (void *) pstCnc + iValueOffset; 
      pcDst = (char *) pstMap + iStorageOffset;

           if(pstParm->iFunction & CNC_FLT) bCc = cnc_ValueToStorageFloat(pcDst, pvSrc, pstParm->iStorageSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_TXT) bCc = cnc_ValueToStorageText (pcDst, pvSrc, pstParm->iStorageSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_LNG) bCc = cnc_ValueToStorageLong (pcDst, pvSrc, pstParm->iStorageSize, pstParm->iPrec);
      else if(pstParm->iFunction & CNC_INT) bCc = cnc_ValueToStorageInt  (pcDst, pvSrc, pstParm->iStorageSize, pstParm->iPrec);
   }
   else bCc = TRUE;
   //PRINTF2("CNC_ValueToStorage(): %d Done (%s)" CRLF, iVal, pstParm->pcJson);
   return(bCc);
}

/*
 * Function    : CNC_StorageToValue
 * Description : Update entries in the CNC sytruct with data from global G_ storage
 *
 * Parameters  : value enum, value
 * Returns     : TRUE if OKee
 * Note        :
 *                Src : CNCMAP *pstMap->G_pcDrawMode
 *                Dest: RPICNC *pstCnc->iDrawMode)
 *
 */
bool CNC_StorageToValue(int iVal, RPICNC *pstCnc)
{
   bool     bCc=FALSE;
   int      iValueOffset, iStorageOffset;
   char    *pcSrc;
   void    *pvDst;
   //
   const CNCPARMS *pstParm;
   CNCMAP         *pstMap = GLOBAL_GetMapping();

   if( (iVal < 0) || (iVal > NUM_CNC_VARS) )
   {
      PRINTF1("CNC_ValueToStorage(): Error: index out of range (%d)" CRLF, iVal);
      LOG_Report(0, "CNC", "Error: index out of range (%d)", iVal);
      return(bCc);
   }
   //
   pstParm        = &stCncParameters[iVal];
   iValueOffset   = pstParm->iValueOffset;
   iStorageOffset = pstParm->iStorageOffset;

   //PRINTF4("CNC_StorageToValue(%d):%s: VO=%d, GO=%d" CRLF, iVal, pstParm->pcJson, iValueOffset, iStorageOffset);

   if( (iValueOffset != -1) && (iStorageOffset != -1) )
   {
      pvDst = (void *) pstCnc + iValueOffset; 
      pcSrc = (char *) pstMap + iStorageOffset;
   
           if(pstParm->iFunction & CNC_FLT) bCc = cnc_StorageToValueFloat(pvDst, pcSrc);
      else if(pstParm->iFunction & CNC_TXT) bCc = cnc_StorageToValueText (pvDst, pcSrc, pstParm->iStorageSize);
      else if(pstParm->iFunction & CNC_LNG) bCc = cnc_StorageToValueLong (pvDst, pcSrc);
      else if(pstParm->iFunction & CNC_INT) bCc = cnc_StorageToValueInt  (pvDst, pcSrc);
   }
   else bCc = TRUE;
   //PRINTF2("CNC_StorageToValue(): %d Done (%s)" CRLF, iVal, pstParm->pcJson);
   return(bCc);
}


/*
 * Function    : CNC_SetupDrawMode
 * Description : Setup the correct draw mode STH state
 *
 * Parameters  : Drawing Mode (DRW_xxx)
 * Returns     : STH mode for this drawing pattern
 *
 */
int CNC_SetupDrawMode(int iMode)
{
   int   iState=STH_CNC_ERROR;

   if(iMode < NUM_CNC_DRAWS)
   {
      iState = stCncDrawModes[iMode].iState;
   }
   return(iState);
}

/*
 * Function    : CNC_UpdateDrawMode
 * Description : Update the correct draw mode
 *
 * Parameters  : 
 * Returns     : STH mode for this drawing pattern
 *
 */
int CNC_UpdateDrawMode(int iMode)
{
   int      iState=STH_CNC_ERROR;
   char     cMode[3];
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   if(iMode < NUM_CNC_DRAWS)
   {
      cMode[0] = '0'+ (iMode / 10);
      cMode[1] = '0'+ (iMode % 10);
      cMode[2] = 0;
      //
      strcpy(pstMap->G_pcDrawMode, cMode);
      iState = stCncDrawModes[iMode].iState;
   }
   return(iState);
}


/*------  Local functions separator ------------------------------------
__HTML_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : cnc_BuildMessageBody
 * Description : Build applicable JSON objects
 *
 * Parameters  : Client, Page, item function, changed parms only
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_BuildMessageBody(NETCL *pstCl, RPIPAGE *pstPage, int iFunction, bool fChangedOnly)
{
   bool            fCc=FALSE;
   int             iStorageOffset;
   RPIJSON        *pstJsn=NULL;
   char           *pcValue;
   char           *pcTemp;
   CNCMAP         *pstMap=GLOBAL_GetMapping();
   const CNCPARMS *pstParm=stCncParameters;

   pcTemp = (char *) safemalloc(MAX_PATH_LEN);
   //
   // Update activity seconds
   //
   cnc_UpdateCncRealtime();
   //
   //PRINTF1("cnc_BuildMessageBody():Function %s" CRLF, pstPage->pcFunction);
   //
   // Build JSON object
   //
   pstJsn = JSON_InsertParameter(pstJsn, "Command", pstPage->pcFunction, JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
   while(pstParm->iFunction)
   {
      if(pstParm->iFunction & iFunction)
      {
         if( (iStorageOffset = pstParm->iStorageOffset) == -1)
         {
            pcValue = CNC_GetFromValue(pstParm, pcTemp, MAX_PATH_LEN);
         }
         else
         {
            pcValue = (char *)pstMap + iStorageOffset;
         }
         if(fChangedOnly)
         {
            if(cnc_ParmHasChanged(pstParm))
            {
               //
               // Add only changed parameters to JSON object
               //
               if(pstParm->iFunction & JSN_TXT) pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
               else                             pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue,         JE_COMMA|JE_CRLF);
            }
         }
         else
         {
            //
            // Add all parameters to JSON object
            //
            if(pstParm->iFunction & JSN_TXT) pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue,         JE_COMMA|JE_CRLF);
         }
      }
      pstParm++;
   }
   pstJsn = JSON_TerminateObject(pstJsn);
   fCc    = JSON_RespondObject(pstCl, pstJsn);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstJsn);
   safefree(pcTemp);
   return(fCc);
}

/*
 * Function    : cnc_BuildMessageBodyArray
 * Description : Build applicable JSON array objects
 *
 * Parameters  : Client, page, item function, fullpath-YesNo
 * Returns     : TRUE if OKee
 * Note        : JSON object returned:
 *                {
 *                   "Command": "fotos",
 *                   "fotos":
 *                   [
 *                      { "filename/pathname", filesize },
 *                      { "filename/pathname", filesize },
 *                      { ....                          },
 *                      { "filename/pathname", filesize }
 *                   ],
 *                   "NumFiles": "XXX", 
 *                   "Filter", "201306"
 *                }
 *
 */
static bool cnc_BuildMessageBodyArray(NETCL *pstCl, RPIPAGE *pstPage, int iFunction, bool fFullpath)
{
   bool            fCc=FALSE;
   int             iStorageOffset;
   char           *pcFilter;
   char           *pcFile;
   RPIJSON        *pstJsn=NULL;
   char           *pcFullPath;
   char           *pcValue;
   char           *pcTemp;
   CNCMAP         *pstMap=GLOBAL_GetMapping();
   const CNCPARMS *pstParm=stCncParameters;

   pcFullPath = (char *) safemalloc(MAX_PATH_LEN);
   pcTemp     = (char *) safemalloc(MAX_PATH_LEN);
   //
   // Update activity seconds
   //
   cnc_UpdateCncRealtime();
   //
   //PRINTF("cnc_BuildMessageBodyArray()" CRLF);
   switch(iFunction)
   {
      default:
         pcFile = "";
         break;

      case CNC_PIX:
         pcFile = pstMap->G_pcPixPath;
         break;

      case CNC_RMP:
         pcFile = pstMap->G_pcPixPath;
         break;
   }
   pcFilter = HTTP_CollectGetParameter(pstCl);
   if(pcFilter == NULL)
   {
      //
      // No filter parameter specified: take global spec
      //
      pcFilter = pstMap->G_pcFilter;
   }
   //
   // JSON command found: collect all related parameters
   // Build JSON object:
   // {
   //    "Command":"xxxxxxxx",
   //
   pstJsn = JSON_InsertParameter(pstJsn, "Command", pstPage->pcFunction, JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
   //
   // Insert global JSON info:
   //    "Version":"xxxxxxxx",
   //    "Pathname":"xxxxxxxx",
   //
   pstJsn = JSON_InsertParameter(pstJsn, "Version", SWVERSION, JE_TEXT|JE_COMMA|JE_CRLF);
   ES_sprintf(pcFullPath, "%s/", pcFile);
   pstJsn = JSON_InsertParameter(pstJsn, "Pathname", pcFullPath, JE_TEXT|JE_COMMA|JE_CRLF);
   pstJsn = JSON_InsertParameter(pstJsn, (const char *)pstPage->pcFunction, "", JE_SQRB);
   //
   // Add all array elements to JSON object
   //
   pstJsn = cnc_RpiJsonShowFiles(pstCl, pstJsn, pcFile, pcFilter, fFullpath);
   pstJsn = JSON_TerminateArray(pstJsn);
   //
   while(pstParm->iFunction)
   {
      if(pstParm->iFunction & iFunction)
      {
         if( (iStorageOffset = pstParm->iStorageOffset) == -1)
         {
            pcValue = CNC_GetFromValue(pstParm, pcTemp, MAX_PATH_LEN);
         }
         else
         {
            pcValue = (char *)pstMap + iStorageOffset;
         }
         if(pstParm->iFunction & JSN_TXT) pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
         else                             pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue,         JE_COMMA|JE_CRLF);
      }
      pstParm++;
   }
   pstJsn = JSON_TerminateObject(pstJsn);
   fCc    = JSON_RespondObject(pstCl, pstJsn);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstJsn);
   //
   safefree(pcFullPath);
   safefree(pcTemp);
   return(fCc);
}

/*
 * Function    : cnc_BuildMessageBodyModes
 * Description : Build applicable JSON array objects
 *
 * Parameters  : Client, Page, command
 * Returns     : TRUE if OKee
 * Note        : JSON object returns:
 *                {
 *                   "Command": "modes",
 *                   "...": "....",
 *                   "DrawModes":
 *                   [
 *                      { 
 *                         "Index", 1, 
 *                         "Command", "Square", 
 *                            ....
 *                      },
 *                      {
 *                         ....
 *                      }
 *                   ],
 *                   "Number": "XXX"
 *                }
 *
 */
static bool cnc_BuildMessageBodyModes(NETCL *pstCl, RPIPAGE *pstPage, int iFunction)
{
   bool           fCc=FALSE;
   int            iStorageOffset;
   int            iIdx;
   char           *pcValue;
   char           *pcTemp;
   RPIJSON        *pstJsn=NULL;
   CNCMAP         *pstMap=GLOBAL_GetMapping();
   const CNCPARMS *pstParm=stCncParameters;

   pcTemp = (char *) safemalloc(MAX_PATH_LEN);
   //
   // Update activity seconds
   //
   cnc_UpdateCncRealtime();
   //
   //PRINTF("cnc_BuildMessageBodyModes()" CRLF);
   //
   // JSON command found: collect all related parameters
   // Build JSON object:
   // {
   //    "Command":"xxxxxxxx",
   //
   pstJsn = JSON_InsertParameter(pstJsn, "Command", pstPage->pcFunction, JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
   //
   // Insert global JSON info:
   //    "xxxx":"xxxxxxxx",
   //
   while(pstParm->iFunction)
   {
      if(pstParm->iFunction & iFunction)
      {
         if( (iStorageOffset = pstParm->iStorageOffset) == -1)
         {
            pcValue = CNC_GetFromValue(pstParm, pcTemp, MAX_PATH_LEN);
         }
         else
         {
            pcValue = (char *)pstMap + iStorageOffset;
         }
         if(pstParm->iFunction & JSN_TXT) pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
         else                             pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue,         JE_COMMA|JE_CRLF);
      }
      pstParm++;
   }
   pstJsn = JSON_InsertParameter(pstJsn, "DrawModes", "", JE_SQRB);
   //
   // Add all elements to JSON object
   //
   for(iIdx=0; iIdx<NUM_CNC_DRAWS; iIdx++)
   {
      //
      // Index : Index of draw mode (0..?)
      // Mode  : Function to be called 
      //
      ES_sprintf(pcTemp, "%d", iIdx);
      pstJsn = JSON_InsertParameter(pstJsn, "Index", pcTemp, JE_CURLB|JE_COMMA);
      pstJsn = JSON_InsertParameter(pstJsn, "Mode", (char *)stCncDrawModes[iIdx].pcHelper, JE_TEXT|JE_CURLE);
   }
   pstJsn = JSON_TerminateArray(pstJsn);
   ES_sprintf(pcTemp, "%d", NUM_CNC_DRAWS);
   pstJsn = JSON_InsertParameter(pstJsn, "NumberDrawModes", pcTemp, JE_NONE);
   pstJsn = JSON_TerminateObject(pstJsn);
   fCc    = JSON_RespondObject(pstCl, pstJsn);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstJsn);
   //
   safefree(pcTemp);
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__JSON_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : cnc_RpiJsonDrawModes
 * Description : Report draw modes
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note:       : http://ip-address:port/cnc.json?modes
 *
 */
static bool cnc_RpiJsonDrawModes(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   //
   // 1- Parse the header for parameters: store them in the G_ parameter array
   //
   cnc_RpiCollectParms(pstCl, tType, pstPage->iFunction);
   //
   // 2- Build the response header and body
   //    Put out response
   //
   cnc_BuildMessageBodyModes(pstCl, pstPage, pstPage->iFunction);
   return(TRUE);
}

/*
 * Function    : cnc_RpiJsonMotionParms
 * Description : CncParms motion detect: reply motion detect parameters
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://ip-address:port/cnc.json?MotionRed=300&....
 *                                               ^
 *                                               Index in pstCl receive buffer
 *
 * Reply       : JSON result data
 *
 */
static bool cnc_RpiJsonMotionParms(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   bool  fCc=TRUE;
   int   iSeq;
   //
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   //
   // 1- Parse the header for parameters: store them in the G_ parameter array
   //
   cnc_RpiCollectParms(pstCl, tType, CNC_PIX);
   //
   // Build foto output filename and size
   //
   iSeq = atoi(pstMap->G_pcDetectSeq) & 1;
   //
   ES_sprintf(pstMap->G_pcOutputFile, "%smotion_%d.bmp", pcMotionDir, iSeq);
   ES_sprintf(pstMap->G_pcLastFile,   "%smotion_%d.bmp", pcMotionDir, iSeq);
   cnc_RpiJsonGetFileSize(pstMap->G_pcOutputFile, pstMap->G_pcLastFileSize, MAX_PARM_LEN);
   //pet8 GLOBAL_Status(CNC_STATUS_MOTION);
   fCc = cnc_BuildMessageBody(pstCl, pstPage, pstPage->iFunction, FALSE);
   return(fCc);
}

/*
 * Function    : cnc_RpiJsonMotionNormalize
 * Description : CncParms motion detect: create normalized picture
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://ip-address:port/cnc.json?MotionRed=300&....
 *                                               ^
 *                                               Index in pstCl receive buffer
 *
 * Reply       : JSON result data
 *
 *
 *
 */
static bool cnc_RpiJsonMotionNormalize(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   bool  fCc=TRUE;
   //
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   cnc_RpiCollectParms(pstCl, tType, CNC_PIX);
   cnc_MotionDetectDefaults();
   //
   if( cnc_MotionNormalize() )
   {
      //
      // Build foto output filename RPI_MOTION_DIR/motion_?_norm.bmp
      //
      ES_sprintf(pstMap->G_pcOutputFile, "%smotion_%d_norm.bmp", pcMotionDir, stMotionDetect.iSeq & 1);
      ES_sprintf(pstMap->G_pcLastFile,   "%smotion_%d_norm.bmp", pcMotionDir, stMotionDetect.iSeq & 1);
      cnc_RpiJsonGetFileSize(pstMap->G_pcOutputFile, pstMap->G_pcLastFileSize, MAX_PARM_LEN);
      //pet8 GLOBAL_Status(CNC_STATUS_MOTION);
      fCc = cnc_BuildMessageBody(pstCl, pstPage, pstPage->iFunction, FALSE);
   }
   else
   {
      //pet8 GLOBAL_Status(CNC_STATUS_ERROR);
      fCc = HTTP_BuildRespondHeaderBad(pstCl);
      PRINTF("cnc_RpiJsonMotionNormalize(): Error" CRLF);
      LOG_Report(0, "CNC", "Motion Normalize error");
   }
   return(fCc);
}

/*
 * Function    : cnc_RpiJsonMotionDelta
 * Description : CncParms motion detect: Calculate delta picture
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://ip-address:port/cnc.json?mdelta&MotionLev=...
 *                                                      ^
 *                                                      Index in pstCl receive buffer
 *
 * Reply       : JSON result data
 *
 *
 *
 */
static bool cnc_RpiJsonMotionDelta(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   bool  fCc=TRUE;
   //
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   
   cnc_RpiCollectParms(pstCl, tType, CNC_PIX);
   cnc_MotionDetectDefaults();
   //
   if( cnc_MotionDelta() )
   {
      //
      // Build foto output filename RPI_MOTION_DIR/motion_delta.bmp
      //
      ES_sprintf(pstMap->G_pcOutputFile, "%smotion_marked.bmp", pcMotionDir);
      ES_sprintf(pstMap->G_pcLastFile,   "%smotion_delta.bmp",  pcMotionDir);
      //pet8 GLOBAL_Status(CNC_STATUS_MOTION);
      fCc = cnc_BuildMessageBody(pstCl, pstPage, pstPage->iFunction, FALSE);
   }
   else
   {
      //pet8 GLOBAL_Status(CNC_STATUS_ERROR);
      fCc = HTTP_BuildRespondHeaderBad(pstCl);
      PRINTF("cnc_RpiJsonMotionDelta(): Error" CRLF);
      LOG_Report(0, "CNC", "Motion Delta error");
   }
   return(fCc);
}

/*
 * Function    : cnc_RpiJsonMotionZoom
 * Description : CncParms motion detect: Zoom in/out
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://ip-address:port/cnc.json?mzoom&MotionX=...&MotionY=..
 *                                                     ^
 *                                                     Index in pstCl receive buffer
 *
 * Reply       : JSON result data
 *
 *
 *
 */
static bool cnc_RpiJsonMotionZoom(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   bool  fCc=TRUE;
   //
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   
   cnc_RpiCollectParms(pstCl, tType, CNC_PIX);
   cnc_MotionDetectDefaults();
   //
   if( cnc_MotionZoom() )
   {
      //
      // Build foto output filename RPI_MOTION_DIR/motion_zoom.bmp
      //
      ES_sprintf(pstMap->G_pcOutputFile, "%smotion_%d_zoom.bmp", pcMotionDir, stMotionDetect.iSeq & 1);
      ES_sprintf(pstMap->G_pcLastFile,   "%smotion_%d_zoom.bmp", pcMotionDir, stMotionDetect.iSeq & 1);
      //pet8 GLOBAL_Status(CNC_STATUS_MOTION);
      fCc = cnc_BuildMessageBody(pstCl, pstPage, pstPage->iFunction, FALSE);
   }
   else
   {
      //pet8 GLOBAL_Status(CNC_STATUS_ERROR);
      fCc = HTTP_BuildRespondHeaderBad(pstCl);
      PRINTF("cnc_RpiJsonMotionZoom(): Error" CRLF);
      LOG_Report(0, "CNC", "Motion Zoom error");
   }
   return(fCc);
}

/*
 * Function    : cnc_RpiJsonParms
 * Description : CncParms Respond with the JSON objects
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://ip-address:port/cnc.json?parms&filter=201306
 *                                                    ^
 *                                                    Index in pstCl receive buffer
 */
static bool cnc_RpiJsonParms(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   RPICNC  *pstCnc=&pstMap->G_stCnc;

   PRINTF("cnc_RpiJsonParms()" CRLF);
   //
   // 1- Parse the header for parameters: store them in the G_ parameter array
   //
   cnc_RpiCollectParms(pstCl, tType, pstPage->iFunction);
   //
   // 2- Build the response header and body
   //    Put out response
   //
   //pet8 GLOBAL_Status(CNC_STATUS_IDLE);
   cnc_BuildMessageBody(pstCl, pstPage, pstPage->iFunction, FALSE);
   //
   // 3- Kickstart the Mode (G_pcDrawMode --> iDrawMode)
   //
   CNC_StorageToValue(CNC_VAR_MODE, pstCnc);
   PRINTF2("cnc_RpiJsonParms(): Run Mode=%s(%d)" CRLF, pstMap->G_pcDrawMode, pstCnc->iDrawMode);
   //
   // Send SIGUSR2 to CNC thread to kickstart the STH to refresh all variables
   //
   if(pstMap->G_tCncWrPid) kill(pstMap->G_tCncWrPid, SIGUSR2);
   return(TRUE);
}

/*
 * Function    : cnc_RpiJsonRun
 * Description : Cnc Run
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note:       : ip-address:port/cam.json?run&Mode=01
 *
 */
static bool cnc_RpiJsonRun(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   RPICNC  *pstCnc=&pstMap->G_stCnc;

   //PRINTF("cnc_RpiJsonRun()" CRLF);
   //
   // 1- Parse the header for parameters: store them in the G_ parameter array
   //
   cnc_RpiCollectParms(pstCl, tType, CNC_DRW);
   //
   // 2- Build the response header and body
   //    Put out response
   //
   //pet8 GLOBAL_Status(CNC_STATUS_RUN);
   cnc_BuildMessageBody(pstCl, pstPage, CNC_DRW, FALSE);
   //
   // 3- Kickstart the Mode (G_pcDrawMode --> iDrawMode)
   //
   CNC_StorageToValue(CNC_VAR_MODE, pstCnc);
   PRINTF2("cnc_RpiJsonRun(): Mode=%s(%d)" CRLF, pstMap->G_pcDrawMode, pstCnc->iDrawMode);
   //
   // Send SIGUSR1 to CNC thread to kickstart the STH
   //
   if(pstMap->G_tCncWrPid) kill(pstMap->G_tCncWrPid,  SIGUSR1);
   return(TRUE);
}

/*
 * Function    : cnc_RpiJsonDefaults
 * Description : CncParms parameter back to defaults
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note:       : http://ip-address:port/cnc.json?defaults
 *
 */
static bool cnc_RpiJsonDefaults(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   GLOBAL_RestoreDefaults();
   cnc_BuildMessageBody(pstCl, pstPage, pstPage->iFunction, FALSE);
   return(TRUE);
}

/*
 * Function    : cnc_RpiJsonFotos
 * Description : CncParms Respond with the JSON objects
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://ip-address:port/cnc.json?Fotos
 *                                               ^
 *                                               Index in pstCl receive buffer
 */
static bool cnc_RpiJsonFotos(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   //
   // 1- Parse the header for parameters: store them in the G_ parameter array
   //
   cnc_RpiCollectParms(pstCl, tType, CNC_PIX);
   //
   // 2- Build the response header and body
   //    Put out response
   //
   //    List available fotos through a JSON array:
   //
   //    { 
   //       fotos": 
   //       [
   //          "foto-1.jpg",
   //          "foto-2.jpg",
   //             ...
   //          "foto-n.jpg",
   //       ]
   //    }
   //
   cnc_BuildMessageBodyArray(pstCl, pstPage, pstPage->iFunction, FALSE);
   PRINTF("cnc_RpiJsonFotos():Done" CRLF);
   return(TRUE);
}

/*
 * Function    : cnc_RpiJsonDeleteFotos
 * Description : CncParms json delete Fotos from disk
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://ip-address:port/cnc.json?rm-fotos&foto.ext
 *
 */
static bool cnc_RpiJsonDeleteFotos(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   char    *pcParm;
   char    *pcShell;
   char    *pcDeleteExec = "rm";
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   pcShell = (char *) safemalloc(MAX_CMD_LEN);
   pcParm  = HTTP_CollectGetParameter(pstCl);
   if(pcParm)
   {
      ES_sprintf(pcShell, "%s %s%s/%s*", pcDeleteExec, pcPublicWwwDir, pstMap->G_pcPixPath, pcParm);
      //
      // Run execution
      //
      LOG_Report(0, "CNC", "cnc_RpiJsonDeleteFotos(): system(%s)", pcShell);
      PRINTF1("cnc_RpiJsonDeleteFotos():System(%s)" CRLF, pcShell);
      system(pcShell);
   }
   safefree(pcShell);
   cnc_BuildMessageBodyArray(pstCl, pstPage, pstPage->iFunction, FALSE);
   return(TRUE);
}

/*
 * Function    : cnc_RpiJsonShowFiles
 * Description : CncParms json show files in directory from disk
 *
 * Parameters  : Client, Json Obj, www dir name, filter, YesNo:fullpath
 * Returns     : Json object
 *
 */
static RPIJSON *cnc_RpiJsonShowFiles(NETCL *pstCl, RPIJSON *pstJsn, char *pcWww, char *pcFilter, bool fFullpath)
{
   int      x, iNum=0, iNumFiles=0;
   char     cForD;
   char    *pcFileName;
   char    *pcFullPath;
   char    *pcFilePath;
   char    *pcTemp;
   void    *pvData;
   CNCMAP  *pstMap = GLOBAL_GetMapping();

   pcFileName = (char *) safemalloc(MAX_PATH_LEN);
   pcFilePath = (char *) safemalloc(MAX_PATH_LEN);
   pcFullPath = (char *) safemalloc(MAX_PATH_LEN);
   pcTemp     = (char *) safemalloc(MAX_PATH_LEN);
   //
   ES_sprintf(pcFullPath, "%s%s", pcPublicWwwDir, pcWww);
   //PRINTF1("cnc_RpiJsonShowFiles(): Path = %s" CRLF, pcFullPath);

   pvData = FINFO_GetDirEntries(pcFullPath, pcFilter, &iNum);

   if(iNum > 0)
   {
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         ES_sprintf(pcFilePath, "%s/%s", pcFullPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, &cForD, 1);
         //PRINTF3("cnc_RpiJsonShowFiles(): Filter(%s)-File(%c):[%s]" CRLF, pcFilter, cForD, pcFilePath);
         if( cForD == 'F')
         {
            // { "name":"20130712xxx",
            if(fFullpath)  pstJsn = JSON_InsertParameter(pstJsn, "Name", pcFilePath, JE_CURLB|JE_TEXT|JE_COMMA);
            else           pstJsn = JSON_InsertParameter(pstJsn, "Name", pcFileName, JE_CURLB|JE_TEXT|JE_COMMA);
            // "Size":12345,
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, MAX_PATH_LEN);
            //PRINTF1("cnc_RpiJsonShowFiles(): Size=[%s]" CRLF, pcTemp);
            pstJsn = JSON_InsertParameter(pstJsn, "Size", pcTemp, JE_COMMA);
            // "Date":"xxxxx xx"},
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, pcTemp, MAX_PATH_LEN);
            //PRINTF1("cnc_RpiJsonShowFiles(): Date=[%s]" CRLF, pcTemp);
            pstJsn = JSON_InsertParameter(pstJsn, "Date", pcTemp, JE_TEXT|JE_CURLE);
            iNumFiles++;
         }
      }
      FINFO_ReleaseDirEntries(pvData, iNum);
   }
   else
   {
      PRINTF("cnc_RpiJsonShowFiles(): No file/dir entries" CRLF);
   }
   ES_sprintf(pstMap->G_pcNumFiles, "%d", iNumFiles);
   safefree(pcFileName);
   safefree(pcFilePath);
   safefree(pcFullPath);
   safefree(pcTemp);
   return(pstJsn);
}

/*
 * Function    : cnc_RpiJsonGetFileSize
 * Description : Get the filesize
 *
 * Parameters  : Pathname, dest ptr, dest buffer size
 * Returns     : OK if found
 *
 */
static bool cnc_RpiJsonGetFileSize(char *pcFullpath, char *pcDest, int iSize)
{
   bool  fCc;
   char  cForD;

   FINFO_GetFileInfo(pcFullpath, FI_FILEDIR, &cForD, 1);
   if( cForD == 'F')
   {
      FINFO_GetFileInfo(pcFullpath, FI_SIZE, pcDest, iSize);
      PRINTF1("cnc_RpiJsonGetFileSize(): Size=[%s]" CRLF, pcDest);
      fCc = TRUE;
   }
   else
   {
      PRINTF("cnc_RpiJsonGetFileSize(): No file/dir entries" CRLF);
      fCc = FALSE;
   }
   return(fCc);
}

/*
 * Function    : cnc_RpiJsonDtop
 * Description : Cnc STOP command: Respond with the JSON objects
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_RpiJsonStop(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("cnc_RpiJsonStop()" CRLF CRLF CRLF);
   //pet8 GLOBAL_Status(CNC_STATUS_STOPPING);
   cnc_BuildMessageBody(pstCl, pstPage, pstPage->iFunction, FALSE);
   //
   // Send SIGTERM to CNC thread to STOP CNC movements
   //
   if(pstMap->G_tCncWrPid) kill(pstMap->G_tCncWrPid, SIGTERM);
   return(TRUE);
}


/*------  Local functions separator ------------------------------------
__HTML_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : cnc_RpiHtmlFotos
 * Description : CncParms http xfer Fotos from disk
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : cam?fotos
 */
static bool cnc_RpiHtmlFotos(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   char    *pcFilter;
   char    *pcHeader = "RaspBerry Pi Camera Foto's";
   CNCMAP  *pstMap = GLOBAL_GetMapping();

   PRINTF("cnc_RpiHtmlFotos()" CRLF);
   pcFilter  = HTTP_CollectGetParameter(pstCl);
   return( cnc_RpiHtmlShowFiles(pstCl, pstMap->G_pcPixPath, pcFilter, pcHeader) );
}

/*
 * Function    : cnc_RpiHtmlDeleteFotos
 * Description : CncParms http delete Fotos from disk
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://RPI_IP_ADDRESS:8080/cnc.json?rm-fotos&foto.ext
 *
 */
static bool cnc_RpiHtmlDeleteFotos(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   char    *pcParm;
   char    *pcShell;
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   pcShell = (char *) safemalloc(MAX_CMD_LEN);
   pcParm  = HTTP_CollectGetParameter(pstCl);
   if(pcParm)
   {
      ES_sprintf(pcShell, "rm %s%s/%s*", pcPublicWwwDir, pstMap->G_pcPixPath, pcParm);
      //
      // Run execution
      //
      LOG_Report(0, "CNC", "cnc_RpiHtmlDeleteFotos(): system(%s)", pcShell);
      PRINTF1("cnc_RpiHtmlDeleteFotos():System(%s)" CRLF, pcShell);
      system(pcShell);
   }
   safefree(pcShell);
   return(cnc_RpiHtmlFotos(pstCl, pstPage, tType));
}

/*
 * Function    : cnc_RpiHtmlParms
 * Description : CncParms parameter passing
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note        : http://ip-address:port/cnc?parms&parm_1&parm_2....&parm_n
 *                                                ^
 *                                                Index in pstCl receive buffer
 *
 */
static bool cnc_RpiHtmlParms(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   bool     fCc=TRUE;
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   //PRINTF("cnc_RpiHtmlParms(): Handle Options" CRLF);
   cnc_RpiCollectParms(pstCl, tType, pstPage->iFunction);
   //
   HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
   HTTP_BuildStart(pstCl);                // Title - eyecandy
   HTTP_BuildGeneric(pstCl, pcWebPageParmsChanged, pstMap->G_pcStatus);
   HTTP_BuildLineBreaks(pstCl, 1);
   HTTP_BuildEnd(pstCl);
   return(fCc);
}

/*
 * Function    : cnc_RpiHtmlDefaults
 * Description : CncParms parameter back to defaults
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|...
 * Returns     : TRUE if OKee
 *
 * Note:       : ip-address:port/cam?defaults
 */
static bool cnc_RpiHtmlDefaults(NETCL *pstCl, RPIPAGE *pstPage, FTYPE tType)
{
   bool  fCc=TRUE;

   GLOBAL_RestoreDefaults();
   //
   HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
   HTTP_BuildStart(pstCl);                // Title - eyecandy
   HTTP_BuildGeneric(pstCl, pcWebPageDefaultsChanged);
   HTTP_BuildEnd(pstCl);
   return(fCc);
}

/*
 * Function    : cnc_RpiHtmlShowFiles
 * Description : CncParms http show files in directory from disk
 *
 * Parameters  : Client, www dir name, filter, header
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_RpiHtmlShowFiles(NETCL *pstCl, char *pcWww, char *pcFilter, char *pcHeader)
{
   bool  fCc=FALSE;
   int   x, iNum=0;
   char  cForD;
   char *pcFileName;
   char *pcFullPath;
   char *pcFilePath;
   char *pcTemp;
   void *pvData;

   pcFileName = (char *) safemalloc(MAX_PATH_LEN);
   pcFilePath = (char *) safemalloc(MAX_PATH_LEN);
   pcFullPath = (char *) safemalloc(MAX_PATH_LEN);
   pcTemp     = (char *) safemalloc(MAX_PATH_LEN);
   //
   ES_sprintf(pcFullPath, "%s%s", pcPublicWwwDir, pcWww);
   //PRINTF1("cnc_RpiHtmlShowFiles(): Path = %s" CRLF, pcFullPath);

   pvData = FINFO_GetDirEntries(pcFullPath, pcFilter, &iNum);
   if(iNum > 0)
   {
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      // Build table
      HTTP_BuildTableStart(pstCl, pcHeader, 1, 70, 3);
      //
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         ES_sprintf(pcFilePath, "%s/%s", pcFullPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, &cForD, 1);
         //PRINTF3("cnc_RpiHtmlShowFiles(): Filter(%s)-File(%c):[%s]" CRLF, pcFilter, cForD, pcFileName);
         if( cForD == 'F')
         {
            ES_sprintf(pcTemp, "%s/%s", pcWww, pcFileName);
            HTTP_BuildTableRowStart(pstCl, 6);
            HTTP_BuildTableColumnLink(pstCl, pcFileName, 25, pcTemp);
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 15);
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 30);
            HTTP_BuildTableRowEnd(pstCl);
         }
      }
      FINFO_ReleaseDirEntries(pvData, iNum);
      HTTP_BuildTableEnd(pstCl);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
      fCc = TRUE;
   }
   else
   {
      //PRINTF("cnc_RpiHtmlShowFiles(): No file/dir entries" CRLF);
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      HTTP_BuildGeneric(pstCl, pcWebPageStopBad);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
   }
   safefree(pcFileName);
   safefree(pcFilePath);
   safefree(pcFullPath);
   safefree(pcTemp);
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__MISC_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : cnc_RpiCollectParms
 * Description : CncParms parameter passing from HTTP record into the MMAP global area
 *
 * Parameters  : Client, HTTP_HTML|HTTP_JSON|..., CNC_xxx
 * Returns     : Nr of parms found
 *
 * Note        : http://ip-address:port/cnc?xxxx&parm_1&parm_2....&parm_n
 *                                               ^
 *                                               Index in pstCl receive buffer
 *
 */
static int cnc_RpiCollectParms(NETCL *pstCl, FTYPE tType, int iFunction)
{
   int         iNr=0;
   const char *pcParm;

   do
   {
      pcParm = HTTP_CollectGetParameter(pstCl);
      if(pcParm)
      {
         iNr++;
         cnc_RpiHandleParm(pcParm, tType, iFunction);
      }
   }
   while(pcParm);
   //
   //PRINTF1("cnc_RpiCollectParms(): %d arguments found" CRLF, iNr);
   //
   return(iNr);
}

/*
 * Function    : cnc_RpiHandleParm
 * Description : Lookup an argement in the main Args list and copy its value (if any) into the Global mmap area
 *
 * Parameters  : Ptr tp parm "XXX=500", HTTP_HTML|HTTP_JSON|..., CNC_xxx
 * Returns     : TRUE if parm OKee
 *
 */
static bool cnc_RpiHandleParm(const char *pcParm, FTYPE tType, int iFunction)
{
   bool        fCc=FALSE;
   int         x;
   const char *pcParmText;
   char       *pcValue;
   //
   CNCMAP         *pstMap=GLOBAL_GetMapping();
   const CNCPARMS *pstParm=stCncParameters;

   if(pcParm)
   {
      //PRINTF1("cnc_RpiHandleParm(): Parm=<%s>" CRLF, pcParm);
      //
      while(pstParm->iFunction)
      {
         if(pstParm->iFunction & iFunction)
         {
            switch(tType)
            {
               case HTTP_JSON:   pcParmText = pstParm->pcJson; break;
               case HTTP_HTML:
               default:          pcParmText = pstParm->pcHttp; break;
            }
            x = strlen(pcParmText);
            if( (x>0) && (strncasecmp(pcParmText, pcParm, x) == 0) )
            {
               //PRINTF3("cnc_RpiHandleParm(): cmp(%s,%s,%d) match" CRLF, pcParmText, pcParm, x);
               //
               // Option found: copy value
               //
               pcParmText = cnc_FindDelimiter(pcParm, "=:");
               if(pcParmText)
               {
                  pcValue = (char *)pstMap + pstParm->iStorageOffset;
                  strncpy(pcValue, pcParmText, pstParm->iStorageSize);
                  cnc_ParmSetChanged(pstParm, 1);
                  fCc = TRUE;
                  break;
               }
               else
               {
                  //PRINTF1("cnc_RpiHandleParm(): Opt <%s> has no value" CRLF, pcParm);
               }
            }
            else
            {
               //PRINTF3("cnc_RpiHandleParm(): cmp(%s,%s,%d) NO match" CRLF, pcParmText, pcParm, x);
            }
         }
         pstParm++;
      }
   }
   else
   {
      //PRINTF("cnc_RpiHandleParm(): No more Parms" CRLF);
   }
   return(fCc);
}

/*
 * Function    : cnc_FindDelimiter
 * Description : Find parameter value delimiter
 *
 * Parameters  : Parameter, delimiter(s)
 * Returns     : Value ptr
 *
 * Note        : pcParm  -> "Filter = 20130723"
 *               returns -> "20130723"
 */
static const char *cnc_FindDelimiter(const char *pcParm, char *pcDelim)
{
   int   x, iNumDelims=strlen(pcDelim);

   while(*pcParm)
   {
      for(x=0; x<iNumDelims; x++)
      {
         if(*pcParm == pcDelim[x])
         {
            return(++pcParm);
         }
      }
      pcParm++;
   }
   return(NULL);
}

/*
 * Function    : cnc_UpdateCncRealtime
 * Description : Update the real-time string
 *
 * Parameters  : 
 * Returns     : 
 *
 */
static void cnc_UpdateCncRealtime(void)
{
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   u_int32  ulSecs;

   if(ulDrawingStart != -1)
   {
      ulSecs = GLOBAL_ReadSecs() - ulDrawingStart;
      RTC_ConvertDateTime(TIME_FORMAT_HH_MM_SS, pstMap->G_pcTimeDrawing, ulSecs);
   }
   else
   {
      ES_sprintf(pstMap->G_pcTimeDrawing, "--:--:--");
   }
}

/*
 * Function    : cnc_MotionDetectDefaults
 * Description : Setup defaults for motion detect
 *
 * Parameters  : 
 * Returns     : 
 *
 */
static void cnc_MotionDetectDefaults(void)
{
   int      iX, iY;
   CNCMAP  *pstMap = GLOBAL_GetMapping();

   for(iY=0; iY<GRF_GRID_COUNT; iY++)
   {
      for(iX=0; iX<GRF_GRID_COUNT; iX++)
      {
         stMotionDetect.iGridMotion[iY][iX] = 0;
         stMotionDetect.iGridScales[iY][iX] = (int)iMotionDetectDefaults[iY][iX];
      }
   }
   stMotionDetect.iGridWidth  = GRF_GRID_COUNT;
   stMotionDetect.iGridHeight = GRF_GRID_COUNT;
   //
   stMotionDetect.iSeq        = atoi(pstMap->G_pcDetectSeq);
   stMotionDetect.iZoomX      = atoi(pstMap->G_pcDetectZoomX);
   stMotionDetect.iZoomY      = atoi(pstMap->G_pcDetectZoomY);
   stMotionDetect.iZoomW      = atoi(pstMap->G_pcDetectZoomW);
   stMotionDetect.iZoomH      = atoi(pstMap->G_pcDetectZoomH);
   stMotionDetect.iThldPixel  = atoi(pstMap->G_pcDetectPixel);
   stMotionDetect.iThldLevel  = atoi(pstMap->G_pcDetectLevel);
   stMotionDetect.iThldRed    = atoi(pstMap->G_pcDetectRed);
   stMotionDetect.iThldGreen  = atoi(pstMap->G_pcDetectGreen);
   stMotionDetect.iThldBlue   = atoi(pstMap->G_pcDetectBlue);
}

/*
 * Function    : cnc_MotionNormalize
 * Description : Normalize and do magic(k) on previous motion snapshots
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_MotionNormalize()
{
   bool     fCc=FALSE;
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Motion detect:
   //    Take 2 snapshots
   //    Convert to grayscale
   //    Calculate delta (threshold)
   //    perform action on changes (threshold)
   //

   if( GRF_MotionNormalize(RPI_MOTION_DIR, pcFormat, &stMotionDetect) )
   {
      //
      // Normalise OK: pass average values
      //
      ES_sprintf(pstMap->G_pcDetectAvg1, "%d", stMotionDetect.iAverage1);
      ES_sprintf(pstMap->G_pcDetectAvg2, "%d", stMotionDetect.iAverage2);
      fCc = TRUE;
   }
   //
   // int      iX, iY;
   //for(iY=0; iY<stMotionDetect.iGridHeight; iY++)
   //{
   //   for(iX=0; iX<stMotionDetect.iGridWidth; iX++)
   //   {
   //      PRINTF1("%3d ", stMotionDetect.iGridMotion[iY][iX]);
   //   }
   //   PRINTF(CRLF);
   //}

   return(fCc);
}

/*
 * Function    : cnc_MotionDelta
 * Description : Calculate and build delta of previous motion snapshots
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_MotionDelta()
{
   bool     fCc=FALSE;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   
   //
   // Motion detect:
   //    Take 2 snapshots
   //    Convert to grayscale
   //    Calculate delta (threshold)
   //    perform action on changes (threshold)
   //

   if( GRF_MotionDelta(RPI_MOTION_DIR, pcFormat, &stMotionDetect) >= 0)
   {
      //
      // Delta OK: pass average values
      //
      ES_sprintf(pstMap->G_pcDetectAvg1, "%d", stMotionDetect.iAverage1);
      ES_sprintf(pstMap->G_pcDetectAvg2, "%d", stMotionDetect.iAverage2);
      fCc = TRUE;
   }
   //
   // int      iX, iY;
   //for(iY=0; iY<stMotionDetect.iGridHeight; iY++)
   //{
   //   for(iX=0; iX<stMotionDetect.iGridWidth; iX++)
   //   {
   //      PRINTF1("%3d ", stMotionDetect.iGridMotion[iY][iX]);
   //   }
   //   PRINTF(CRLF);
   //}

   return(fCc);
}

/*
 * Function    : cnc_MotionZoom
 * Description : Zoom in/out
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_MotionZoom()
{
   bool     fCc=FALSE;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   
   if( GRF_MotionZoom(RPI_MOTION_DIR, pcFormat, &stMotionDetect) >= 0)
   {
      //
      // Delta OK: pass average values
      //
      ES_sprintf(pstMap->G_pcDetectAvg1, "%d", stMotionDetect.iAverage1);
      ES_sprintf(pstMap->G_pcDetectAvg2, "%d", stMotionDetect.iAverage2);
      fCc = TRUE;
   }
   return(fCc);
}

/*
 * Function    : cnc_ValueToStorageFloat
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size, precision
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_ValueToStorageFloat(char *pcDest, void *pvSrc, int iSize, int iPrec)
{
   float fVal;

   fVal = *(float *)pvSrc;
   //
   switch(iPrec)
   {
      default:
         snprintf(pcDest, iSize, "%f", fVal);
         break;

      case 0:
         snprintf(pcDest, iSize, "%.0f", fVal);
         break;

      case 1:
         snprintf(pcDest, iSize, "%.1f", fVal);
         break;

      case 2:
         snprintf(pcDest, iSize, "%.2f", fVal);
         break;

      case 3:
         snprintf(pcDest, iSize, "%.3f", fVal);
         break;

      case 4:
         snprintf(pcDest, iSize, "%.4f", fVal);
         break;

      case 5:
         snprintf(pcDest, iSize, "%.5f", fVal);
         break;
   }
   //PRINTF2("cnc_ValueToStorageFloat():V=%f, S=%s" CRLF, fVal, pcDest);
   return(TRUE);
}

/*
 * Function    : cnc_ValueToStorageLong
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size, precision
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_ValueToStorageLong(char *pcDest, void *pvSrc, int iSize, int iPrec)
{
   long  lVal;

   lVal = *(long *)pvSrc;
   snprintf(pcDest, iSize, "%ld", lVal);
   //PRINTF2("cnc_ValueToStorageLong():V=%ld, S=%s" CRLF, lVal, pcDest);
   return(TRUE);
}

/*
 * Function    : cnc_ValueToStorageInt
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size, precision
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_ValueToStorageInt(char *pcDest, void *pvSrc, int iSize, int iPrec)
{
   int iVal;

   iVal = *(int *)pvSrc;
   snprintf(pcDest, iSize, "%d", iVal);
   //PRINTF2("cnc_ValueToStorageInt():V=%d, S=%s" CRLF, iVal, pcDest);
   return(TRUE);
}

/*
 * Function    : cnc_ValueToStorageText
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size, precision
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_ValueToStorageText(char *pcDest, void *pvSrc, int iSize, int iPrec)
{
   char  *pcVal;

   pcVal = (char *)pvSrc;
   strncpy(pcDest, pcVal, iSize);
   //PRINTF2("cnc_ValueToStorageText():V=%s, S=%s" CRLF, pcVal, pcDest);
   return(TRUE);
}

/*
 * Function    : cnc_StorageToValueFloat
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_StorageToValueFloat(void *pvDst, char *pcSrc)
{
   float fVal;

   //PRINTF1("cnc_StorageToValueFloat():S=%s" CRLF, pcSrc);
   fVal = MISC_AsciiToFloat(pcSrc);
   //PRINTF1("cnc_StorageToValueFloat():D=%f" CRLF, fVal);
   *(float *)pvDst = fVal;
   return(TRUE);
}

/*
 * Function    : cnc_StorageToValueLong
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_StorageToValueLong(void *pvDst, char *pcSrc)
{
   long  lVal;

   //PRINTF1("cnc_StorageToValueLong():S=%s" CRLF, pcSrc);
   lVal = MISC_AsciiToLong(pcSrc);
   //PRINTF1("cnc_StorageToValueLong():D=%ld" CRLF, lVal);
   *(long *)pvDst = lVal;
   return(TRUE);
}

/*
 * Function    : cnc_StorageToValueInt
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_StorageToValueInt(void *pvDst, char *pcSrc)
{
   int iVal;

   //PRINTF1("cnc_StorageToValueInt():S=%s" CRLF, pcSrc);
   iVal = MISC_AsciiToInt(pcSrc);
   //PRINTF1("cnc_StorageToValueInt():D=%d" CRLF, iVal);
   *(int *)pvDst = iVal;
   return(TRUE);
}

/*
 * Function    : cnc_StorageToValueText
 * Description : Update the storage with actual data
 *
 * Parameters  : Dest, Src, size
 * Returns     : TRUE if OKee
 *
 */
static bool cnc_StorageToValueText(void *pvDst, char *pcSrc, int iSize)
{
   //PRINTF1("cnc_StorageToValueText(NOTSTORED):S=%s" CRLF, pcSrc);
   //strncpy((char *)pvDst, pcSrc, iSize);
   //PRINTF1("cnc_StorageToValueText():D=%s" CRLF, (char *)pvDst);
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
__HTTP_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : cnc_ParmHasChanged
 * Description : Check if a parm has been altered from defaults
 *
 * Parameters  : Parm ^
 * Returns     : TRUE if parm is marked as changed
 *
 */
static bool cnc_ParmHasChanged(const CNCPARMS *pstParm)
{
   bool        fChanged=FALSE;
   u_int8     *pubChanged;
   int         iChangedOffset=pstParm->iChangedOffset;
   CNCMAP     *pstMap = GLOBAL_GetMapping();

   switch(iChangedOffset)
   {
      case ALWAYS_CHANGED:
         fChanged = TRUE;
         break;

      case NEVER_CHANGED:
         break;

      default:
         pubChanged = (u_int8 *)pstMap + iChangedOffset;
         fChanged   = *pubChanged;
         break;
   }
   return(fChanged);
}

/*
 * Function    : cnc_ParmSetChanged
 * Description : Set the parm changed marker
 *
 * Parameters  : Parm ^, set/unset
 * Returns     : TRUE if parm has changed successfully
 *
 * Note        : Some parms cannot be changed (NEVER_CHANGED)
 *               Some are always volatile (ALWAYS_CHANGED)
 */
static bool cnc_ParmSetChanged(const CNCPARMS *pstParm, bool fChanged)
{
   u_int8     *pubChanged;
   int         iChangedOffset=pstParm->iChangedOffset;
   CNCMAP     *pstMap = GLOBAL_GetMapping();

   switch(iChangedOffset)
   {
      case ALWAYS_CHANGED:
         // Do not alter the change marker
         break;

      case NEVER_CHANGED:
         // Do not alter the change marker
         if(fChanged)  fChanged = FALSE;
         break;

      default:
         pubChanged = (u_int8 *)pstMap + iChangedOffset;
        *pubChanged = fChanged;
         fChanged   = TRUE;
         break;
   }
   return(fChanged);
}

#ifdef BINARY_FILE_XFER
//
// Function    : cnc_SendBinFile
// Description : Send a binary file from fs to socket
// 
// Parameters  : Client, filename
// Returns     : TRUE if OKee
//
static bool cnc_SendBinFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = malloc(READ_BUFFER_SIZE);
      //
      PRINTF1("cnc_SendBinFile(): File=<%s>" CRLF, pcFile);
      do
      {
         iRead = fread(pcBuffer, 1, READ_BUFFER_SIZE, ptFile);
         if(iRead)
         {
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            PRINTF("cnc_SendBinFile(): EOF" CRLF);
         }
      }
      while(iRead);
      PRINTF1("cnc_SendBinFile(): %d bytes sent." CRLF, iTotal);
      fclose(ptFile);
      free(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF1("cnc_SendBinFile(): Error open file <%s>" CRLF, pcFile);
   }
   return(fCc);
}
#endif   //BINARY_FILE_XFER


