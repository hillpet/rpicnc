/*  (c) Copyright:  2012  Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:
**                     Offer the API as an abstraction layer for EOS calls
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
 *
 *
 *
 *
 **/


#define ES_sprintf           sprintf
#define ES_memset            memset
#define ES_memcpy            memcpy
#define ES_memcmp            memcmp

int ES_printf                (char *, ...);
int ES_timeprintf            (char *, ...);
int REPORT_String            (char *, ...);

