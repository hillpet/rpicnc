/*  (c) Copyright:  2016  Patrn, Confidential Data
 *
 *  $Workfile:          cncparms.h
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Header file for CNC parameter handler
 *
 *
 *  Entry Points:       
 *
 *
 *
 *
 *
 *  Compiler/Assembler: 
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       19 May 2016
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CNCPARMS_H_
#define _CNCPARMS_H_

//
//
// Global prototypes
//
bool     CNC_InitParms              (void);
char    *CNC_GetFromValue           (const CNCPARMS *, char *, int);
bool     CNC_GetParms               (NETCL *, FTYPE);
bool     CNC_ValueToStorage         (int, RPICNC *);
bool     CNC_StorageToValue         (int, RPICNC *);
int      CNC_SetupDrawMode          (int);
int      CNC_UpdateDrawMode         (int);

#endif   //_CNCPARMS_H_
