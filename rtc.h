/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            rtc.c header file
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 *    20120717: Initial revision.
 *    20130721: Add TIME_FORMAT_YYYY_MM_DD_HH_MM_SS
 *              Add RTC_ConvertDateTimeSize()
 *
**/

#ifndef _RTC_H_
#define _RTC_H_

#define  DATE_TIME_SIZE             32          //"Mo 31-12-2003 15:59"
#define  DST_YES                    TRUE        // Handle daylight saving time
#define  DST_NO                     FALSE       // No     daylight saving time

#define  ES_SECONDS_PER_MINUTE      60          // seconds per minute
#define  ES_SECONDS_PER_HOUR        3600        // seconds per hour
#define  ES_SECONDS_PER_DAY         (24*3600)   // seconds per day
#define  ES_SECONDS_PER_WEEK        (604800L)   // seconds per week

#define  ES_SECONDS_TIMEZONE        3600        // Timezone UTC/GMT + 1 Hr
#define  ES_MJD_1970_OFFSET         40587
#define  ES_SECS_TO_1970            (2208988800LL)

#define  ES_SECS_DST_2004_START     (1080442800LL) // 1970
#define  ES_SECS_DST_2004_END       (1099191600LL) // 1970

//
// TIME formats
//
typedef enum
{
   TIME_FORMAT_WW_DD_MM_YYYY_HH_MM = 0,
   TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS,
   TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM,
   TIME_FORMAT_WW_DD_MMM_YYYY,
   TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM,
   TIME_FORMAT_WW_DD_MM_YYYY,
   TIME_FORMAT_DD_MM_YYYY_HH_MM,
   TIME_FORMAT_DD_MM_YYYY,
   TIME_FORMAT_HH_MM_SS,
   TIME_FORMAT_WW_YYYY_MM_DD,
   TIME_FORMAT_YYYY_MM_DD_WW,
   TIME_FORMAT_YYYY_MM_DD_HH_MM_WW,
   TIME_FORMAT_YYYY_MM_DD_HH_MM_SS,

   NUM_TIME_FORMATS
}  TIMEFORMAT;

//
// Structures and enum type definitions
//
typedef struct CLOCK_DAY_TIME
{
   u_int8 ubHour;
   u_int8 ubMin;
   u_int8 ubSec;
   u_int8 ubDay;
   u_int8 ubMonth;
   u_int8 ubYear;       // Year since 1900 !!!
   u_int8 ubWeekDay;
}  CLOCK_DAY_TIME;

u_int32 RTC_GetSystemSecs       (void);
void    RTC_SetTimeZone         (int iGmtOffset);
u_int32 RTC_GetCurrentQuarter   (u_int32);
u_int32 RTC_GetDateTime         (char *pcDateTime);
u_int32 RTC_GetDateTimeSecs     (char *pcDateTime);
void    RTC_ConvertDateTime     (TIMEFORMAT tFormat, char *pcDateTime, u_int32 ulSecs);
int     RTC_ConvertDateTimeSize (TIMEFORMAT tFormat);

#endif  /*_RTC_H_ */

