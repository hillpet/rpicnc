/*  (c) Copyright:  2012...2016  Patrn, Confidential Data  
 *
 *  $Workfile:          dynpage.cpp
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Dynamic HTTP page for Raspberry pi webserver
 *
 *
 *  Entry Points:       HTTP_Page*()
 *                      HTTP_Build*()
 *                      HTTP_Send*()
 *
 *  Compiler/Assembler: gnu gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Jul 2012
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "echotype.h"
#include "dynpage.h"
#include "httppage.h"
#include "raspjson.h"
#include "log.h"
#include "misc_api.h"
#include "globals.h"
#include "rpicnc.h"
#include "cncparms.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//

static bool    http_DynPageDefault           (NETCL *);
static bool    http_DynPageEmpty             (NETCL *);
static bool    http_DynPageExit              (NETCL *);
static bool    http_DynPageLog               (NETCL *);
static bool    http_DynPagePids              (NETCL *);
static bool    http_DynPageScreen            (NETCL *);
static bool    http_DynPageShell             (NETCL *);
//
static bool    http_DynPageCncJson           (NETCL *);
static bool    http_DynPageShellJson         (NETCL *);
static bool    http_DynPageScreenJson        (NETCL *);
//             
static bool    http_ExPageGeneric            (NETCL *, char *);
static bool    http_ExPageStart              (NETCL *, char *);
//
static char   *http_ExtractPageName          (NETCL *);
static bool    http_SendTextFile             (NETCL *, char *);
static int     http_ReadTextFile             (FILE *, char *, int);
static bool    http_Xmt                      (NETCL *, char);
static void    http_CheckVirtualData         (char *);
static void    http_BuildPidRow              (NETCL *, pid_t, char *, char *);
static int     http_KillProcess              (pid_t, bool);

//
// These are the supported dynamic HTTP pages
//
static const DYNPAGE stDynamicPages[] =
{
   { "",                http_DynPageEmpty          },
   { "cmd.html",        http_DynPageShell          },
   { "exit.html",       http_DynPageExit           },
   { "info.html",       http_DynPageDefault        },
   { "log.html",        http_DynPageLog            },
   { "pids.html",       http_DynPagePids           },
   { "screen.html",     http_DynPageScreen         },
   //
   { "cnc.json",        http_DynPageCncJson        },
   { "cmd.json",        http_DynPageShellJson      },
   { "screen.json",     http_DynPageScreenJson     },
   //
   {  0,                0                          }
};

//
//
//
static const RPINFO stVirtualInfo[] = 
{  // trigger              title                row   col   offs  length
   { "top -",              "",                   2,    0,    6,   80    },
   { "MemTotal",           NULL,                 3,    0,    0,   80    },
   { "MemFree",            NULL,                 4,    0,    0,   80    },
   //
   { NULL,                 NULL,                 0,    0,    0,   0     }
};
//
// Misc http header strings 
//
static char pcWebPageTitle[]           = "PATRN Embedded Software Services";
static char pcWebPageShell[]           = "RaspberryPi$ %s";
//
static char pcWebPageLineBreak[]       = "<br/>";
static char pcWebPagePreStart[]        = "<div align=\"left\"><pre>";
static char pcWebPagePreEnd[]          = "</pre></div> ";
static char pcWebPageShellBad[]        = "<p>Cmd shell: bad request (errno=%d)</p>" NEWLINE;
//
static char pcWebPageFontStart[]       = "<font face=\"%s\">";
static char pcWebPageFontEnd[]         = "</font>";
//
// HTTP Response:
//=============================
//    HTTP Response header
//    NEWLINE
//    Optional Message body
//    NEWLINE NEWLINE
//
static char pcHttpResponseHeader[]     = "HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
static char pcHttpResponseHeaderJson[] = "HTTP/1.0 200 OK" NEWLINE "Content-Type=application/json" NEWLINE NEWLINE;
static char pcHttpResponseHeaderBad[]  = "HTTP/1.0 400 Bad Request" NEWLINE NEWLINE;

//
// Generic HTTP header
//
static char pcWebPageStart[] =
{  
   "<!doctype html public \"-//w3c//dtd html 4.01//en\"" NEWLINE \
   "<html>" NEWLINE \
      "<head>" NEWLINE \
         "<style type=\"text/css\">" NEWLINE \
            "thead {font-family:\"Tahoma\";font-size:\"100%%\";color:green}" NEWLINE \
            "tbody {font-family:\"Courier new\";font-size:\"25%%\";color:blue}" NEWLINE \
         "</style>" NEWLINE \
         "<meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1252\">" NEWLINE \
         "<title>%s</title>" NEWLINE \
      "</head>" NEWLINE \
      "<body style=\"text-align:center\">" NEWLINE
};

static char pcWebPageDefault[] =
{
   "<hr/>" NEWLINE \
   "<h2>PATRN ESS</h2>" NEWLINE \
   "<h3>Raspberry Pi HTTP server</h3>" NEWLINE \
   "<hr/>" NEWLINE
};

static char pcWebPageEmpty[] =
{
   "<br/>" NEWLINE
   "Welcome to another PATRN mini HTTP server." NEWLINE \
   "<a href=\"http://www.patrn.nl\" target=\"_blank\">Patrn.nl</a>" NEWLINE \
   "<br/>" NEWLINE
};


static char pcWebExitPage[] =
{
   "<br/>" NEWLINE \
   "<br/>" NEWLINE \
   "Exiting PATRN HTTP server. Good bye !" NEWLINE \
   "<br/>" NEWLINE \
   "<br/>" NEWLINE
};

//
// HTTP table start: Needs border, width(%), colspan, headerstring
//
static char pcWebPageTableStart[] =
{
   "<table align=center border=%d width=\"%d%%\">" NEWLINE \
      "<body style=\"text-align:left\">" NEWLINE
      "<thead>" NEWLINE \
      "<th colspan=%d>%s</th>" NEWLINE
};

//
// HTTP table end
//
static char pcWebPageTableEnd[] =
{
   "</tbody></table>" NEWLINE
};

//
// HTTP table row: Needs row height
//
static char pcWebPageTableRowStart[] =
{
   "<tr height=%d>" NEWLINE
};

//
// HTTP table row end
//
static char pcWebPageTableRowEnd[] =
{
   "</tr>" NEWLINE
};

//
// HTTP table cel data: Needs [cell width,] data
//
static char pcWebPageCellDataSimple[] = 
{
   "<td>%s</td>" NEWLINE
};
static char pcWebPageCellDataExtended[] =
{
   "<td width=\"%d%%\">%s</td>" NEWLINE NEWLINE
};

static char* pcWebPageCellData[] =
{
   pcWebPageCellDataSimple,
   pcWebPageCellDataExtended
};

//
// HTTP table cel link data : Needs [cell width,] link, data
//
static char pcWebPageCellDataLinkSimple[] =
{
   "<td><a href=\"%s\">%s</td>" NEWLINE
};
static char pcWebPageCellDataLinkExtended[] =
{
   "<td width=\"%d%%\"><a href=\"%s\">%s</td>" NEWLINE
};

static char * pcWebPageCellDataLink[] =
{
   pcWebPageCellDataLinkSimple,
   pcWebPageCellDataLinkExtended
};

//
// HTTP table cel numeric data : Needs [cell width,] data
//
static char pcWebPageCellDataNumberSimple[] =
{
   "<td>%d</td>" NEWLINE
};
static char pcWebPageCellDataNumberExtended[] =
{
   "<td width=\"%d%%\">%d</td>" NEWLINE
};

static char * pcWebPageCellDataNumber[] =
{
   pcWebPageCellDataNumberSimple,
   pcWebPageCellDataNumberExtended
};

//
// Generic HTTP trailer
//
static char pcWebPageEnd[] =
{
   "</body></html>" NEWLINE NEWLINE 
};

//
// HTTP table cel link data : Needs [cell width,] link, data
//
static char pcWebPageText[] =
{
   "[%s]" NEWLINE
};

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function    : HTTP_PageIsDynamic
// Description : Check if the requested page is dynamic or (file)static
//
// Parameters  : Net client struct
// Returns     : TRUE if requested page is dynamic (and needs construction)
//
bool HTTP_PageIsDynamic(NETCL *pstCl)
{
   bool           fCc=FALSE;
   const DYNPAGE *pstDynPage=stDynamicPages;
   char          *pcFilename;
   char          *pcPagename;

   pcPagename = http_ExtractPageName(pstCl);

   if ( pcPagename == NULL)   return(FALSE);
   if (*pcPagename == '/')    pcPagename++;
   if (*pcPagename == 0)      return(TRUE);

   while(TRUE)
   {
      pcFilename  = pstDynPage->pcFilename;
      if(pcFilename == NULL) break;
      //
      if(strcmp(pcFilename, pcPagename) == 0)
      {
         //
         // This request is a dynamic HTTP page
         //
         fCc = TRUE;
         break;
      }
      else
      {
         pstDynPage++;
      }
   }
   return(fCc);
}

/*
 * Function    : HTTP_BuildGeneric
 * Description : Build a generic http page
 *
 * Parameters  : Client, string^, parameters
 * Returns     : TRUE if OKee
 *
 */
bool HTTP_BuildGeneric(NETCL *pstCl, char *pcPage, ...)
{
   va_list  tParms;
   bool     fParsing=1;
   bool     fCc=TRUE;
   int      i=0, iVal;
   int32    lVal;
   char    *pcData, cChar, cBuffer[20];
   
   va_start(tParms, pcPage);
   //
   while(fParsing)
   {
      cChar = pcPage[i];
      switch(cChar)
      {
         case 0:
            fParsing = 0;
            break;
            
         case '%':
            i++;
            cChar = pcPage[i];
            switch(cChar)
            {
               case '%':
                  http_Xmt(pstCl, cChar);
                  break;
               
               case 'd':
                  iVal = va_arg(tParms, int);
                  ES_sprintf (cBuffer, "%d", iVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'x':
                  iVal = va_arg(tParms, int);
                  ES_sprintf (cBuffer, "%x", iVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'l':
                  lVal = va_arg(tParms, int32);
                  ES_sprintf (cBuffer, "%ld", lVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 's':
                  pcData = va_arg(tParms, char *);
                  HTTP_SendString(pstCl, pcData);
                  break;
               
               case 'S':
                  pcData = va_arg(tParms, char *);
                  HTTP_SendString(pstCl, pcData);
                  break;
               
               default:
                  fCc = FALSE;
                  break;
            }
            break;
            
         default:
            http_Xmt(pstCl, cChar);
            break;
      }
      i++;
   }
   va_end(tParms);
   
   return(fCc);
}

// 
// Function    : HTTP_PageSend
// Description : Dynamically create and send a http web page
// 
// Parameters  : NETCL *, Page name
// Returns     : TRUE if OKee
// 
bool HTTP_PageSend(NETCL *pstCl, char *pcPagename)
{
   bool           fCC=FALSE;
   const DYNPAGE *pstDynPage=stDynamicPages;
   char          *pcFilename;
   PFNCL          pfAddr;

   if ( pcPagename == NULL)   return(FALSE);
   if (*pcPagename == '/')    pcPagename++;

   while(TRUE)
   {
      pcFilename = pstDynPage->pcFilename;
      if(pcFilename == 0) break;
      if(strcmp(pcFilename, pcPagename) == 0)
      {
         //
         // This request is a dynamic HTTP page: go create it
         // 
         pfAddr = (PFNCL) pstDynPage->pfHandler;
         if(pfAddr)
         {
            fCC = pfAddr(pstCl);
         }
         break;
      }
      else
      {
         pstDynPage++;
      }
   }
   return(fCC);
}

// 
// Function    : HTTP_SendDynamicPage
// Description : Dynamically create and send a http web page
// 
// Parameters  : Client
// Returns     : TRUE if OKee
// 
bool HTTP_SendDynamicPage(NETCL *pstCl)
{
   bool        fCC;
   char       *pcPagename;

   pcPagename = http_ExtractPageName(pstCl);
   fCC        = HTTP_PageSend(pstCl, pcPagename);

   return(fCC);
}

//
// Function    : HTTP_SendData
// Description : Put out Client buffer
//
// Parameters  : Client
// Returns     : 
//
void HTTP_SendData(NETCL *pstCl, char *pcData, int iLen)
{
   int   iIdx=0;

   while(iIdx<iLen)
   {
      http_Xmt(pstCl, pcData[iIdx]);
      iIdx++;
   }
}

/*
 * Function    : HTTP_SendString
 * Description : Display http string
 *
 * Parameters  : Client, string^
 * Returns     : 
 *
 */
void HTTP_SendString(NETCL *pstCl, char *pcData)
{
   char    cChar;
   int     i=0;

   while(1)
   {
      cChar = pcData[i];
      //
      if(cChar)
      {
         http_Xmt(pstCl, cChar);
         i++;
      }
      else break;
   }
}

/*------  Local functions separator ------------------------------------
__BUILD_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function    : HTTP_BuildStart
// Description : Put out HTTP page start 
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildStart(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML header, start tag, title etc
   //
   fOKee &= http_ExPageStart(pstCl,   pcWebPageTitle);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageDefault);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function    : HTTP_BuildRespondHeader
// Description : Put out HTTP response header
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildRespondHeader(NETCL *pstCl)
{
   return( http_ExPageGeneric(pstCl, pcHttpResponseHeader) );
}

//
// Function    : HTTP_BuildRespondHeaderBad
// Description : Put out HTTP response header Bad Request
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildRespondHeaderBad(NETCL *pstCl)
{
   return( http_ExPageGeneric(pstCl, pcHttpResponseHeaderBad) );
}

//
// Function    : HTTP_BuildRespondHeaderJson
// Description : Put out HTTP page start Json
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildRespondHeaderJson(NETCL *pstCl)
{
   return( http_ExPageGeneric(pstCl, pcHttpResponseHeaderJson) );
}

//
// Function    : HTTP_BuildLineBreaks
// Description : Put out HTTP linebreaks
//
// Parameters  : Client, number of newlines
// Returns     : True if okee
//
bool HTTP_BuildLineBreaks(NETCL *pstCl, int iNr)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML header, start tag, title etc
   //
   while(iNr--)   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function    : HTTP_BuildDivStart
// Description : Put out HTTP page div/section start 
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildDivStart(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML div start tag
   //
   fOKee &= http_ExPageGeneric(pstCl, pcWebPagePreStart);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function    : HTTP_BuildDivEnd
// Description : Put out HTTP page div/section end
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildDivEnd(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML div end tag
   //
   fOKee &= http_ExPageGeneric(pstCl, pcWebPagePreEnd);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function    : HTTP_BuildEnd
// Description : Put out HTTP page end
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildEnd(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML end
   //
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageEnd);
   return(fOKee);
}

/*------  Local functions separator ------------------------------------
__TABLE_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : HTTP_BuildTableStart
 * Description : Start a http web page table
 *
 * Parameters  : Client, page header, border with, table width, number of columns
 * Returns     :
 *
 */
bool  HTTP_BuildTableStart(NETCL *pstCl, char *pcPage, u_int16 usBorder, u_int16 usWidth, u_int16 usCols)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageTableStart, usBorder, usWidth, usCols, pcPage);

   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableRowStart
 * Description : Starts a row in a table
 *
 * Parameters  : Client, row height
 * Returns     :
 *
 */
bool  HTTP_BuildTableRowStart(NETCL *pstCl,  u_int16 usHeight)
{
   bool  fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageTableRowStart, usHeight);
   
   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableRowEnd
 * Description : Ends a row
 *
 * Parameters  : Client
 * Returns     :
 *
 */
bool  HTTP_BuildTableRowEnd(NETCL *pstCl)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageTableRowEnd);
   
   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableColumnText
 * Description : Start a generic column
 *
 * Parameters  : Client, page struct, column text, column width
 * Returns     :
 *
 */
bool  HTTP_BuildTableColumnText(NETCL *pstCl, char *pcText, u_int16 usWidth)
{
   bool     fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellData[1], usWidth, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellData[0], pcText);

   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableColumnLink
 * Description : Start a link with a hyperlink
 *
 * Parameters  : Client, page struct, column text, width, link
 * Returns     :
 *
 */
bool  HTTP_BuildTableColumnLink(NETCL *pstCl, char *pcText, u_int16 usWidth, char *pcLink)
{
   bool  fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellDataLink[1], usWidth, pcLink, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellDataLink[0], pcLink, pcText);

   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableColumnNumber
 * Description : Start a column with a number
 *
 * Parameters  : Client, page struct, number, width
 * Returns     :
 *
 */
bool  HTTP_BuildTableColumnNumber(NETCL *pstCl, u_int16 usNr, u_int16 usWidth)
{
   bool     fOKee;
   
   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellDataNumber[1], usWidth, usNr);
   else        fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellDataNumber[0], usNr);

   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableEnd
 * Description : End a http web page table
 *
 * Parameters  : Client
 * Returns     :
 *
 */
bool  HTTP_BuildTableEnd(NETCL *pstCl)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageTableEnd);

   return(fOKee);
}

/*------  Local functions separator ------------------------------------
__HTML_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : http_DynPageEmpty
 * Description : Dynamically created empty page
 *
 * Parameters  : Client
 * Returns     : 1 if OKee
 *
 */
static bool http_DynPageEmpty(NETCL *pstCl)
{
   if(!http_SendTextFile(pstCl, RPI_PUBLIC_DEFAULT) )
   {
      //
      // Put out a default reply: the HTML header, start tag and the rest
      //         
      http_ExPageGeneric(pstCl, pcHttpResponseHeader);
      //
      http_ExPageStart(pstCl,   pcWebPageTitle);
      http_ExPageGeneric(pstCl, pcWebPageDefault);
      http_ExPageGeneric(pstCl, pcWebPageEmpty);
      http_ExPageGeneric(pstCl, pcWebPageEnd);
   }
   return(1);
}

/*
 * Function    : http_DynPageDefault
 * Description : Dynamically created default page
 *
 * Parameters  : Client
 * Returns     : 1 if OKee
 *
 */
static bool http_DynPageDefault(NETCL *pstCl)
{
   //
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   //
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   
   return(1);
}

/*
 * Function    : http_DynPagePids
 * Description : Dynamically created system page to show all threads
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 */
static bool http_DynPagePids(NETCL *pstCl)
{
   int      iCc;
   bool     fFriendly=TRUE, fDoKill=FALSE;
   pid_t   *ptPid=NULL;
   char    *pcParm1;
   char    *pcParm2;
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   // 
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   //
   http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   HTTP_BuildTableStart(pstCl, "RPi Thread PIDs", 1, 80, 3);
   HTTP_BuildTableRowStart(pstCl, 20);
   HTTP_BuildTableColumnText(pstCl, "Thread Id", 10);
   HTTP_BuildTableColumnText(pstCl, "Thread Pid", 10);
   HTTP_BuildTableColumnText(pstCl, "Description", 60);
   HTTP_BuildTableRowEnd(pstCl);
   //
   http_BuildPidRow(pstCl, pstMap->G_tHostPid,  "HOST", "Main thread");
   http_BuildPidRow(pstCl, pstMap->G_tRootPid,  "ROOT", "Main deamon");
   http_BuildPidRow(pstCl, pstMap->G_tSrvrPid,  "SRV",  "HTTP server");
   http_BuildPidRow(pstCl, pstMap->G_tCncWrPid, "CNC",  "CNC USB Write");
   http_BuildPidRow(pstCl, pstMap->G_tCncDbPid, "CNC",  "CNC USB Debug");
   http_BuildPidRow(pstCl, pstMap->G_tUdpPid,   "UDP",  "CNC UDP");
   //
   pcParm1 = HTTP_CollectGetParameter(pstCl);
   pcParm2 = HTTP_CollectGetParameter(pstCl);
   //
   if(pcParm1 && pcParm2) 
   {
      if(strcasecmp(pcParm1, "kill") == 0) { fDoKill=TRUE; fFriendly=FALSE; }
      if(strcasecmp(pcParm1, "term") == 0) { fDoKill=TRUE; fFriendly=TRUE;  }
      //
      if(fDoKill)
      {
         switch(*pcParm2)
         {
            default:
               iCc = -1;
               break;

            case '1':
               ptPid = &pstMap->G_tHostPid;
               break;

            case '2': 
               ptPid = &pstMap->G_tRootPid;
               break;

            case '3': 
               ptPid = &pstMap->G_tSrvrPid;
               break;

            case '4': 
               ptPid = &pstMap->G_tCncWrPid;
               break;

            case '5': 
               ptPid = &pstMap->G_tCncDbPid;
               break;

            case '6': 
               ptPid = &pstMap->G_tUdpPid;
               break;
         }
         if(ptPid && *ptPid)
         {
            http_BuildPidRow(pstCl, *ptPid, pcParm1, "---");
            http_BuildPidRow(pstCl, *ptPid, pcParm2, "---");
            //
            iCc = http_KillProcess(*ptPid, fFriendly);
            if(iCc == 0) 
            {
               *ptPid = 0;
               http_BuildPidRow(pstCl, iCc, " OKee", "Action completed");
            }
            else http_BuildPidRow(pstCl, iCc, " Error", "Action completed");
         }
         else http_BuildPidRow(pstCl, 0, " Error", "Action not completed");
      }
   }
   //
   HTTP_BuildTableEnd(pstCl);
   //
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   PRINTF("http_DynPagePids():OKee" CRLF);
   return(1);
}

/*
 * Function    : http_DynPageShell
 * Description : Dynamically created system page
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 * Note        : This call acts as a router to Linux command shell
 *                GET /cmd?ps&-a&-x blahblah
 *
 *                 --> system("ps -a -x ><dir>/<file>")
 *                     return <file> to caller as text output from the command
 *
 */
static bool http_DynPageShell(NETCL *pstCl)
{
   bool  fCc=TRUE;
   int   iCc;
   char *pcShell;
   char *pcCmd;
   char *pcTemp;

   pcShell = safemalloc(MAX_CMD_LEN);
   pcCmd   = safemalloc(MAX_CMD_LEN);
   //
   // Collect command and parameters, execute command and return its reply 
   //
   pcTemp = HTTP_CollectGetParameter(pstCl);
   //if(pcTemp) PRINTF1("http_DynPageShell():P=<%s>" CRLF, pcTemp);
   //else       PRINTF("http_DynPageShell():No Parms" CRLF);
   if(pcTemp)
   {
      //
      // We have a Shell command: collect the whole string
      //
      ES_sprintf(pcShell, "%s", pcTemp);
      do
      {
         ES_sprintf(pcCmd, "%s", pcShell);
         pcTemp = HTTP_CollectGetParameter(pstCl);
         if(pcTemp) ES_sprintf(pcShell, "%s %s", pcCmd, pcTemp);
      }
      while(pcTemp);
      //
      ES_sprintf(pcCmd, "%s", pcShell);
      ES_sprintf(pcShell, "%s >%s%s", pcCmd, RPI_SHELL_DIR, RPI_SHELL_FILE);
      PRINTF1("http_DynPageShell():%s" CRLF, pcShell);
      //
      // Execute the Shell command
      //
      LOG_Report(0, "DYN", "Run system call [%s]", pcShell);
      iCc = system(pcShell);
      // 
      //
      // Put out the HTML header, start tag and the rest
      //         
      http_ExPageGeneric(pstCl, pcHttpResponseHeader);
      http_ExPageStart(pstCl,   pcWebPageTitle);
      http_ExPageGeneric(pstCl, pcWebPageLineBreak);
      http_ExPageGeneric(pstCl, pcWebPageDefault);
      //
      //
      if(iCc < 0) 
      {
         LOG_Report(errno, "DYN", "ERROR: Running system call [%s]", pcShell);
         PRINTF2("http_DynPageShell(): Error completion %d (errno=%d)" CRLF, iCc, errno);
         HTTP_BuildGeneric(pstCl, (char *)pcWebPageShellBad, errno);
         fCc=FALSE;
      }
      http_ExPageGeneric(pstCl, pcWebPagePreStart);
      //
      // Send the shell results to the client pcWebPageShell
      //
      HTTP_BuildGeneric(pstCl, (char *)pcWebPageShell, pcShell);
      http_ExPageGeneric(pstCl, pcWebPageLineBreak);
      http_ExPageGeneric(pstCl, pcWebPageLineBreak);
      ES_sprintf(pcCmd, "%s%s", RPI_SHELL_DIR, RPI_SHELL_FILE);
      http_SendTextFile(pstCl, pcCmd);
      http_ExPageGeneric(pstCl, pcWebPagePreEnd);
      http_ExPageGeneric(pstCl, pcWebPageEnd);
   }
   else
   {
      http_ExPageGeneric(pstCl, pcHttpResponseHeaderBad);
      http_ExPageStart(pstCl,   pcWebPageTitle);
      http_ExPageGeneric(pstCl, pcWebPageEnd);
      fCc=FALSE;
   }
   //
   safefree(pcShell);
   safefree(pcCmd);
   PRINTF("http_DynPageShell():Done" CRLF);
   return(fCc);
}

/*
 * Function    : http_DynPageScreen
 * Description : Dynamically created system page
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 * Note        : This call acts as a router to Linux command shell
 *                GET /cmd?ps&-a&-x blahblah
 *
 *                 --> system("ps -a -x ><dir>/<file>")
 *                     return <file> to caller as text output from the command
 *
 */
static bool http_DynPageScreen(NETCL *pstCl)
{
   bool     fCc=TRUE;
   int      iRow;
   char    *pcTemp;
   char    *pcVirtual;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;

   pcTemp = safemalloc(LCD_VIRTUAL_COLS+1);
   //
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   //
   HTTP_BuildGeneric(pstCl,  pcWebPageFontStart, "Courier New");
   http_ExPageGeneric(pstCl, pcWebPagePreStart);
   //
   pcVirtual = pstLcd->pcVirtLcd;
   for(iRow=0; iRow<LCD_VIRTUAL_ROWS; iRow++)
   {
      memcpy(pcTemp, pcVirtual, LCD_VIRTUAL_COLS);
      pcTemp[LCD_VIRTUAL_COLS] = 0;
      HTTP_BuildGeneric(pstCl, pcWebPageText, pcTemp);
      pcVirtual += LCD_VIRTUAL_COLS;
   }
   http_ExPageGeneric(pstCl, pcWebPagePreEnd);
   http_ExPageGeneric(pstCl, pcWebPageFontEnd);
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   safefree(pcTemp);
   PRINTF("http_DynPageScreen():Done" CRLF);
   return(fCc);
}

/*
 * Function    : http_DynPageLog
 * Description : Dynamically created system page to show the log file
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 */
static bool http_DynPageLog(NETCL *pstCl)
{
   // 
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   //
   http_ExPageGeneric(pstCl, pcWebPagePreStart);
   http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   http_SendTextFile(pstCl,  RPI_PUBLIC_LOG_FILE);
   http_ExPageGeneric(pstCl, pcWebPagePreEnd);
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   PRINTF("http_DynPageLog():OKee" CRLF);
   return(1);
}

/*
 * Function    : http_DynPageExit
 * Description : Dynamically created system page to exit the server
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 * Note        : This call acts as a gracefull exit from the server to the Linux command shell
 *
 */
static bool http_DynPageExit(NETCL *pstCl)
{
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   //
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   http_ExPageGeneric(pstCl, pcWebExitPage);
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   PRINTF("http_DynPageExit():OKee" CRLF);
   LOG_Report(0, "DYN", "Exit request from browser");
   //
   // Request our own exit:
   //   signal the host (CMD line mode) or the root deamon (service mode)
   //
   if(pstMap->G_tHostPid)       kill(pstMap->G_tHostPid, SIGINT);
   else if(pstMap->G_tRootPid)  kill(pstMap->G_tRootPid, SIGINT);
   else                         LOG_Report(0, "DYN", "Exit request from browser failed: no Host or Root found !");
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
__JSON_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : http_DynPageShellJson
 * Description : Dynamically created system page using JSON reply
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 * Note        : This call acts as a router to Linux command shell
 *                GET /cmd.json?ps&-a&-x blahblah
 *
 *                 --> system("ps -a -x ><dir>/<file>")
 *
 */
static bool http_DynPageShellJson(NETCL *pstCl)
{
   bool        fCc=TRUE;
   int         iCc, iRead=0, iLines=0;
   RPIJSON    *pstJsn=NULL;
   char       *pcShell;
   char       *pcCmd;
   char       *pcTemp;
   FILE       *ptFile;

   pcShell = safemalloc(MAX_CMD_LEN);
   pcCmd   = safemalloc(MAX_CMD_LEN);
   //
   // Collect command and parameters, execute command and return its reply 
   //
   pcTemp = HTTP_CollectGetParameter(pstCl);
   //if(pcTemp) PRINTF1("http_DynPageShellJson():P=<%s>" CRLF, pcTemp);
   //else       PRINTF("http_DynPageShellJson():No Parms" CRLF);
   if(pcTemp)
   {
      //
      // We have a Shell command: collect the whole string
      //
      ES_sprintf(pcShell, "%s", pcTemp);
      do
      {
         ES_sprintf(pcCmd, "%s", pcShell);
         pcTemp = HTTP_CollectGetParameter(pstCl);
         if(pcTemp) ES_sprintf(pcShell, "%s %s", pcCmd, pcTemp);
      }
      while(pcTemp);
      //
      ES_sprintf(pcCmd, "%s", pcShell);
      ES_sprintf(pcShell, "%s >%s%s", pcCmd, RPI_SHELL_DIR, RPI_SHELL_FILE);
      PRINTF1("http_DynPageShellJson():%s" CRLF, pcShell);
      //
      // Execute the Shell command
      //
      //LOG_Report(0, "DYN", "Run [%s]", pcShell);
      iCc = system(pcShell);
      //
      if(iCc < 0) 
      {
         LOG_Report(errno, "DYN", "ERROR: Running system call [%s]", pcShell);
         PRINTF2("http_DynPageShellJson(): Error completion %d (errno=%d)" CRLF, iCc, errno);
         fCc=FALSE;
      }
      else
      {
         ES_sprintf(pcCmd, "%s%s", RPI_SHELL_DIR, RPI_SHELL_FILE);
         PRINTF1("http_DynPageShellJson(): Split file in JSON elements : %s" CRLF, pcCmd);
         ptFile = fopen(pcCmd, "r");
         //
         pstJsn = JSON_InsertParameter(pstJsn, "Command", "shell", JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
         pstJsn = JSON_InsertParameter(pstJsn, "Filename", pcCmd, JE_TEXT|JE_COMMA|JE_CRLF);
         pstJsn = JSON_InsertParameter(pstJsn, "Output", "", JE_SQRB|JE_CRLF);
         do
         {
            iRead = http_ReadTextFile(ptFile, pcShell, MAX_CMD_LEN-1);
            if(iRead)
            {
               pstJsn = JSON_InsertValue(pstJsn, pcShell, JE_TEXT|JE_COMMA|JE_CRLF);
               http_CheckVirtualData(pcShell);
               iLines++;
            }
         }
         while(iRead);
         pstJsn = JSON_TerminateArray(pstJsn);
         ES_sprintf(pcCmd, "%d", iLines);
         pstJsn = JSON_InsertParameter(pstJsn, "Lines", pcCmd, JE_COMMA|JE_CRLF);
         pstJsn = JSON_TerminateObject(pstJsn);
         fCc    = JSON_RespondObject(pstCl, pstJsn);
         //
         // Done with this function: exit
         //
         JSON_ReleaseObject(pstJsn);
         fclose(ptFile);
      }
   }
   else
   {
      fCc=FALSE;
   }
   //
   safefree(pcShell);
   safefree(pcCmd);
   PRINTF("http_DynPageShellJson():Done" CRLF);
   return(fCc);
}

/*
 * Function    : http_DynPageCncJson
 * Description : Dynamically created CNC page using JSON reply
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 */
static bool http_DynPageCncJson(NETCL *pstCl)
{
   return( CNC_GetParms(pstCl, HTTP_JSON) );
}

/*
 * Function    : http_DynPageScreenJson
 * Description : Dynamically created virtual screen using JSON reply
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 */
static bool http_DynPageScreenJson(NETCL *pstCl)
{
   bool        fCc=TRUE;
   int         iRow;
   RPIJSON    *pstJsn=NULL;
   char       *pcTemp;
   char       *pcVirtual;
   CNCMAP     *pstMap=GLOBAL_GetMapping();
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   pcTemp = safemalloc(LCD_VIRTUAL_COLS+1);
   //
   // Put out the contents of the virtual screen 
   //
   pstJsn = JSON_InsertParameter(pstJsn, "Command", "VirtualScreen", JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
   pstJsn = JSON_InsertParameter(pstJsn, "Output", "", JE_SQRB|JE_CRLF);
   //
   pcVirtual = pstLcd->pcVirtLcd;
   for(iRow=0; iRow<LCD_VIRTUAL_ROWS; iRow++)
   {
      memcpy(pcTemp, pcVirtual, LCD_VIRTUAL_COLS);
      pcTemp[LCD_VIRTUAL_COLS] = 0;
      pstJsn = JSON_InsertValue(pstJsn, pcTemp, JE_TEXT|JE_COMMA|JE_CRLF);
      pcVirtual += LCD_VIRTUAL_COLS;
   }
   pstJsn = JSON_TerminateArray(pstJsn);
   pstJsn = JSON_TerminateObject(pstJsn);
   fCc    = JSON_RespondObject(pstCl, pstJsn);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstJsn);
   //
   PRINTF("http_DynPageScreenJson():Done" CRLF);
   safefree(pcTemp);
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__HTTP_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : http_BuildPidRow
 * Description : Build a singe row with text
 *
 * Parameters  : Client, pid, tex2, text3
 * Returns     : 
 *
 */
static void http_BuildPidRow(NETCL *pstCl, pid_t tPid, char *pcText1, char *pcText2)
{
   char pcValue[16];

   ES_sprintf(pcValue, "%d", tPid);
   //
   HTTP_BuildTableRowStart(pstCl, 10);
   HTTP_BuildTableColumnText(pstCl, pcText1, 10);
   HTTP_BuildTableColumnText(pstCl, pcValue, 10);
   HTTP_BuildTableColumnText(pstCl, pcText2, 60);
   HTTP_BuildTableRowEnd(pstCl);
}

/*
 * Function    : http_CheckVirtualData
 * Description : Check if this data contains info to put out to the virtual display
 *
 * Parameters  : Text
 * Returns     :
 *
 */
static void http_CheckVirtualData(char *pcData)
{
   const RPINFO  *pstInfo=stVirtualInfo;
   int            iOffset, iLength;
   u_int8         ubCol;
   char          *pcResult;
   char          *pcTitle;

   PRINTF1("http_CheckVirtualData(): New virtual data <%s>" CRLF, pcData);
   while(pstInfo->pcTrigger)
   {
      PRINTF1("http_CheckVirtualData():Check <%s>" CRLF, pstInfo->pcTrigger);
      if( (pcResult = strstr(pcData, pstInfo->pcTrigger)) != NULL )
      {
         ubCol = pstInfo->ubCol;
         PRINTF3("http_CheckVirtualData():Found: (%2d,%2d) <%s>" CRLF, pstInfo->ubRow, ubCol, pcResult);
         pcTitle = pstInfo->pcTitle;
         if(pcTitle)
         {
            PRINTF1("http_CheckVirtualData():Title <%s>" CRLF, pcTitle);
            //
            // Put title first
            //
            LCD_TEXT(pstInfo->ubRow, ubCol, pcTitle);
            ubCol += (u_int8)strlen(pcTitle);
         }
         iOffset = pstInfo->iOffset;
         iLength = pstInfo->iLength;
         if( iLength < strlen(&pcResult[iOffset]) )
         {
            //
            // Result string needs to be truncated !
            //
            PRINTF3("http_CheckVirtualData():Truncate L=%d, O=%d <%s>" CRLF, iLength, iOffset, pcResult);
            pcResult[iOffset+iLength] = 0;
         }
         PRINTF3("http_CheckVirtualData():(%2d,%2d) <%s>" CRLF, pstInfo->ubRow, ubCol, &pcResult[iOffset]);
         LCD_TEXT(pstInfo->ubRow, ubCol, &pcResult[iOffset]);
      }
      pstInfo++;
   }
}

/*
 * Function    : http_ExPageGeneric
 * Description : Send out generic data to the http client
 *
 * Parameters  : Client
 * Returns     :
 *
 */
static bool  http_ExPageGeneric(NETCL *pstCl, char *pcData)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcData);

   return(fOKee);
}

/*
 * Function    : http_ExPageStart
 * Description : Starts a web page
 *
 * Parameters  : Client
 * Returns     :
 *
 */
static bool  http_ExPageStart(NETCL *pstCl, char *pcTitle)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageStart, pcTitle);

   return(fOKee);
}

/*
 * Function    : http_ExtractPageName
 * Description : Remove HTTP specific chars 
 *
 * Parameters  : Net client struct
 * Returns     : Filename ptr
 *
 */
static char *http_ExtractPageName(NETCL *pstCl)
{
   char *pcFilename = NULL;

   //
   //   "GET /index.html HTTP/1.1"
   //   "Host: 10.0.0.231
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5
   //   "
   //   "Accept-Encoding: gzip,deflate
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
   //   "Keep-Alive: 115
   //   "Connection: keep-alive
   //   CR,CR,LF,CR,CR,LF
   //
   //    pcPathname
   //       iFileIdx : index to filename
   //       iExtnIdx : index to extension
   //
   PRINTF2("http_ExtractPageName(%d)=%s" CRLF, pstCl->iFileIdx, pstCl->pcUrl);
   if(pstCl->iFileIdx != -1)
   {
      pcFilename = &pstCl->pcUrl[pstCl->iFileIdx];
   }
   return(pcFilename);
}

//
// Function    : http_SendTextFile
// Description : Send a textfile from fs to socket
// 
// Parameters  : Client, filename
// Returns     : TRUE if OKee
//
static bool http_SendTextFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = malloc(READ_BUFFER_SIZE);
      //
      PRINTF1("http_SendTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, READ_BUFFER_SIZE, ptFile);
         if(pcRead)
         {
            iRead = strlen(pcBuffer);
            //PRINTF2("http_SendTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            //PRINTF("http_SendTextFile(): EOF" CRLF);
         }
      }
      while(pcRead);
      //PRINTF1("http_SendTextFile(): %d bytes written."CRLF, iTotal);
      fclose(ptFile);
      free(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF1("http_SendTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(fCc);
}

//
// Function    : http_ReadTextFile
// Description : Read a textfile from fs
// 
// Parameters  : Buffer, length
// Returns     : Number read, 0 for EOF
//
static int http_ReadTextFile(FILE *ptFile, char *pcBuffer, int iLength)
{
   int      iRem, iRead=0;
   char    *pcRead;

   if(ptFile)
   {
      pcRead = fgets(pcBuffer, iLength, ptFile);
      if(pcRead)
      {
         iRem  = MISC_RemoveChar(pcRead, 0x0a);
         iRem += MISC_RemoveChar(pcRead, 0x0d);
         iRead = strlen(pcRead);
         //PRINTF3("http_ReadTextFile():(%2d-%2d) %s" CRLF, iRead, iRem, pcRead);
      }
      else
      {
         PRINTF("http_ReadTextFile(): EOF" CRLF);
      }
   }
   else
   {
      PRINTF("http_ReadTextFile(): Error: No open file" CRLF);
   }
   return(iRead);
}

/*
 * Function    : http_KillProcess
 * Description : Kill an ongoing process (if any)
 *
 * Parameters  : pid to kill, frendly request yesno
 * Returns     : completion code (0=ok)
 *
 */
static int http_KillProcess(pid_t tPid, bool fFriendly)
{
   int      iCc=0;
   pid_t    tPidAct;
   int      iStatus;
   char     cShell[32];

   if(tPid)
   {
      if(fFriendly)
      {
         kill(tPid, SIGINT);
         //
         LOG_Report(0, "DYN", "http_KillProcess(): Friendly ask background thread pid=%d to exit", tPid);
         PRINTF1("http_KillProcess(): Friendly ask background thread pid=%d to exit" CRLF, tPid);
      }
      else
      {
         //
         // Ongoing action: terminate
         //
         LOG_Report(0, "DYN", "http_KillProcess(): HTTP request to kill thread (pid=%d)", tPid);
         ES_sprintf(cShell, "kill -kill %d", tPid);
         iCc  = system(cShell);
         while( ((tPidAct = waitpid(tPid, &iStatus, 0)) == -1) && (errno == EINTR) )
           continue;       /* Restart if interrupted by handler */ 
         //
         PRINTF3("http_KillProcess(): Background (pid=%d, act=%d) killed(iCc=%d)" CRLF, tPid, tPidAct, iCc);
      }
   }
   return(iCc);
}
/*
 * Function    : http_Xmt 
 * Description : Send char to transmit buffer
 *
 * Parameters  : Client data, char
 * Returns     : TRUE if OKee
 *
 */
static bool http_Xmt(NETCL *pstCl, char cChar)
{
   bool  fCc=TRUE;

#ifdef   FEATURE_ECHO_HTTP_OUT
   //
   // Echo all HTTP request data from the client
   //
   ES_printf("%c", cChar);
#endif   //FEATURE_ECHO_HTTP_OUT
   //
   // Write the char into the XMT buffer. If the buffer is above the threshold put it out to the socket.
   // NET_WriteBuffer() does not return until all chars have been xmitted or an error has occurred.
   //
   pstCl->pcXmtBuffer[pstCl->iXmtBufferPut++] = cChar;
   if(pstCl->iXmtBufferPut >= WRITE_BUFFER_MAX)
   {
      if(NET_FlushCache(pstCl) < 0) fCc = FALSE;
   }
   return(fCc);
}



