/*  (c) Copyright:  2016 Patrn ESS, Confidential Data
**
**  $Workfile:          cncdraw.h
**  $Revision:  
**  $Modtime:   
**
**  Purpose:            Simple CNC STH states
**
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       4 Jun 2016
**
 *  Revisions:
 *  Note:               The DRW_xxx enum will be transferred through the HTTP link as
 *                      cnc.json?Mode=xx
 *
 *                      Mode=xx --> G_pcDrawMode -->iDrawMode
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// CNC draw modes
//
//          Enum                 iState                  iParm1                           iParm2      iParm3      iParm4       pcHelper
EXTRACT_DRW(DRW_IDLE,            STH_CNC_READY,          -1,                              -1,         -1,         -1,         "CNC Idle"             )
EXTRACT_DRW(DRW_HOME,            STH_CNC_HOME,           -1,                              -1,         -1,         -1,         "CNC Home"             )
EXTRACT_DRW(DRW_SETTINGS,        STH_CNC_SETTING1,       -1,                              -1,         -1,         -1,         "CNC Settings"         )
EXTRACT_DRW(DRW_GOTO_MM,         STH_CNC_GOTO_MM,        -1,                              -1,         -1,         -1,         "CNC Position (mm)"    )
EXTRACT_DRW(DRW_GOTO_STEPS,      STH_CNC_GOTO_STEPS,     -1,                              -1,         -1,         -1,         "CNC Position (steps)" )
EXTRACT_DRW(DRW_CROSSPLOT,       STH_CNC_PAT1_START,     offsetof(CNCMAP, G_pcDrawMode),  -1,         -1,         -1,         "CNC Crosshatch"       )
EXTRACT_DRW(DRW_ARCPLOT,         STH_CNC_READY,          offsetof(CNCMAP, G_pcDrawMode),  -1,         -1,         -1,         "CNC Arc"              )
EXTRACT_DRW(DRW_SQUARE,          STH_CNC_SQUARE,         offsetof(CNCMAP, G_pcDrawMode),  -1,         -1,         -1,         "CNC Square"           )
EXTRACT_DRW(DRW_CIRCLE,          STH_CNC_CIRCLE1,        offsetof(CNCMAP, G_pcDrawMode),  -1,         -1,         -1,         "CNC Circle"           )
