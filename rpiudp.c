/*  (c) Copyright:  2016 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            rpiudp is a simple UDP client to communicate with the CNC
**
**  Entry Points:       UDP_Deamon()
**
**
**
**
**
**
**
**
**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 May 2016
**                                   
 *  Revisions:
 *
 *
 *
 **/

#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <resolv.h>

#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "echotype.h"
#include "log.h"
#include "globals.h"
#include "raspjson.h"
#include "rpicnc.h"
#include "rpiudp.h"

//#define USE_PRINTF
#include "printf.h"


#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

//
static int     udp_Deamon              (void);
//
static bool    udp_SignalRegister      (sigset_t *);
static void    udp_ReceiveSignalUser1  (int);
static void    udp_ReceiveSignalUser2  (int);
static void    udp_ReceiveSignalInt    (int);
static void    udp_ReceiveSignalTerm   (int);
//
static void    udp_InitSemaphore       (void);
static void    udp_PostSemaphore       (void);
static void    udp_WaitSemaphore       (void);
//
volatile static bool bCncRunning = TRUE;
static int           iSocket     = 0;

/* ======   Local Functions separator ===========================================
___Global_functions(){}
==============================================================================*/

//
//  Function:  UDP_Deamon
//  Purpose:   Run the UDP client deamon
//
//  Parms:     
//  Returns:   The deamon PID for the parent only
//
pid_t UDP_Deamon(void)
{
   int      iCc;
   pid_t    tPid;
   CNCMAP  *pstMap;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent returns with the deamon PID
         PRINTF1("UDP_Deamon(): UDP deamon pid=%d" CRLF, tPid);
         break;

      case 0:
         // UDP Deamon
         PRINTF("UDP_Deamon(): UDP Deamon running" CRLF);
         iCc = udp_Deamon();
         pstMap = GLOBAL_GetMapping();
         pstMap->G_tUdpPid = 0;
         PRINTF("UDP_Deamon(): UDP Deamon EXIT" CRLF);
         exit(iCc);
         break;

      case -1:
         // Error
         ES_printf("UDP_Deamon(): UDP deamon Error!"CRLF);
         exit(255);
         break;
   }
   return(tPid);
}


/* ======   Local Functions separator ===========================================
___Local_functions(){}
==============================================================================*/

//
//  Function:   udp_Deamon
//  Purpose:    Main entry point for UDP client deamon
//
//  Parms:      
//  Returns:    Exit codes
//
static int udp_Deamon(void)
{
   int         iIdx;
   int         iXmtSize, iNewSize, iDgrSize=MAX_DATAGRAM_SIZE;
   sigset_t    tBlockset;
   CNCMAP     *pstMap=GLOBAL_GetMapping();
   RPICNC     *pstCnc=&pstMap->G_stCnc;
   char       *pcJson;

   //
   // Init Datagram seq nr
   //
   pstCnc->iSeq = 0;
   if(udp_SignalRegister(&tBlockset) == FALSE) 
   {
      pstMap->G_tUdpPid = 0;
      exit(254);
   }
   udp_InitSemaphore();
   //
   // Set up socket 
   //
   while( bCncRunning && (pstMap->G_Connected == FALSE) )
   {
      //
      // Wait for a connection
      //
      sleep(1);
   }
   //
   // Someone has connected to the CNC: setup an UDP connection to signal a-synchronous events
   //
   if(bCncRunning)
   {
      PRINTF("udp_Deamon(): UDP start socket..." CRLF);
      iSocket = NET_ClientConnect(pstMap->G_pcUdpAddr, pstMap->G_pcUdpPort, UDP_PROTOCOL);
      if(iSocket == -1)
      {
         PRINTF("udp_Deamon(): UDP connect failed: exiting" CRLF);
         LOG_Report(errno, "UDP", "udp_Deamon(): Server connect failed: exiting.");
         pstMap->G_tUdpPid = 0;
         exit(254);
      }
      LOG_Report(0, "UDP", "udp_Deamon(): connected to %s:%s", pstMap->G_pcUdpAddr, pstMap->G_pcUdpPort);
      //
      // Wait for signal to put out data
      //
      pcJson = (char *) safemalloc(iDgrSize);
      //
      while(bCncRunning)
      {
         // PRINTF("udp_Deamon():wait SIGUSR1........." CRLF);
         udp_WaitSemaphore();
         //
         // Data has changed: notify theApp.
         // Update Datagram seq nr
         //
         pstCnc->iSeq++;
         //
         while(1)
         {
            // PRINTF("udp_Deamon():Goooooooo" CRLF);
            //
            iNewSize = udp_BuildMessageBodyVars(pcJson, iDgrSize, CNC_DGM);
            if(iNewSize > iDgrSize)
            {
               //
               // JSON buffer too small: enlarge and retry
               //
               PRINTF3("udp_Deamon():UDP resize datagram[%d], have[%d], need[%d]" CRLF, pstCnc->iSeq, iDgrSize, iNewSize);
               iDgrSize += iNewSize;
               pcJson = (char *) saferemalloc(pcJson, iDgrSize);
            }
            else break;
         }
         PRINTF3("udp_Deamon(): datagram[%d] size=%d data=[%s]" CRLF, pstCnc->iSeq, iNewSize, pcJson);
         //
         // Send datagram(s)
         //
         iIdx = 0;
         while(iNewSize > 0)
         {
            if(iNewSize > MAX_DATAGRAM_SIZE) iXmtSize = MAX_DATAGRAM_SIZE;
            else                             iXmtSize = iNewSize;
            //
            iXmtSize = NET_SendData(iSocket, (u_int8 *)&pcJson[iIdx], iXmtSize);
            if(iXmtSize < 0)
            {
               LOG_Report(errno, "UDP", "udp_Deamon(): UDP send ERROR");
               PRINTF("udp_Deamon():UDP send ERROR" CRLF);
               break;
            }
            else
            {
               PRINTF1("udp_Deamon():UDP send datagram, chunk=[%d]" CRLF, iXmtSize);
               iIdx      += iXmtSize;
               iNewSize  -= iXmtSize;
            }
         }
         PRINTF("udp_Deamon():Done." CRLF);
      }
      //
      // Exit UDP thread: close socket
      //
      NET_ClientDisconnect(iSocket); 
      safefree(pcJson);
   }
   exit(0);
}

/* ======   Local Functions separator ===========================================
___Local_functions(){}
==============================================================================*/

/*
 * Function    : udp_BuildMessageBodyVars
 * Description : Build applicable JSON array objects
 *
 * Parameters  : Buffer, size, Parameter set
 * Returns     : Actual JSON object size
 * Note        : JSON object returns:
 *                {
 *                   "Command":"xxxxxxxx",
 *                   "...": "....",
 *                   "Number": "XXX"
 *                }
 *
 */
static int udp_BuildMessageBodyVars(char *pcBuffer, int iLength, int iFunction)
{
   int            iValueOffset, iStorageOffset;
   int            iSize=0;
   char           *pcValue;
   char           *pcTemp;
   RPIJSON        *pstJsn=NULL;
   CNCMAP         *pstMap=GLOBAL_GetMapping();
   const CNCPARMS *pstParm=stCncParameters;

   pcTemp = (char *) safemalloc(MAX_PATH_LEN);
   //
   // PRINTF("udp_BuildMessageBodyVars()" CRLF);
   // PRINTF6("udp_BuildMessageBodyVars(): (%s, %s, %s) --> (%s, %s, %s)" CRLF, 
   //          pstMap->G_pcCurPos[0], pstMap->G_pcCurPos[1], pstMap->G_pcCurPos[2], 
   //          pstMap->G_pcNewPos[0], pstMap->G_pcNewPos[1], pstMap->G_pcNewPos[2]);
   //
   //
   // Build JSON object:
   // {
   //    "Command":"xxxxxxxx",
   //
   pstJsn = JSON_InsertParameter(pstJsn, "Command", "variables", JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
   //
   // Insert global JSON info:
   //    "xxxx":"xxxxxxxx",
   //
   while(pstParm->iFunction)
   {
      if(pstParm->iFunction & iFunction)
      {
         if( (iValueOffset = pstParm->iValueOffset) != -1)
         {
            pcValue = CNC_GetFromValue(pstParm, pcTemp, MAX_PATH_LEN);
            //PRINTF2("udp_BuildMessageBodyVars(VALUE): %10s=[%s]" CRLF, pstParm->pcJson, pcValue);
         }
         else
         {
            if( (iStorageOffset = pstParm->iStorageOffset) != -1)
            {
               pcValue = (char *)pstMap + iStorageOffset;
               //PRINTF2("udp_BuildMessageBodyVars(STORE): %10s=[%s]" CRLF, pstParm->pcJson, pcValue);
            }
            else pcValue = NULL;
         }
         if(pcValue)
         {
            if(pstParm->iFunction & JSN_TXT) pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstJsn = JSON_InsertParameter(pstJsn, pstParm->pcJson, pcValue,         JE_COMMA|JE_CRLF);
         }
         else
         {
            PRINTF1("udp_BuildMessageBodyVars(-----): %10s=Not found !!" CRLF, pstParm->pcJson);
         }
      }
      pstParm++;
   }
   pstJsn = JSON_TerminateObject(pstJsn);
   //
   // return the resulting JSON object, if it fits !
   //
   iSize = JSON_GetObjectSize(pstJsn);
   if(iSize < iLength)
   {
      strcpy(pcBuffer, pstJsn->pcObject);
   }
   JSON_ReleaseObject(pstJsn);
   //
   safefree(pcTemp);
   return(iSize);
}

//
//  Function:   udp_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool udp_SignalRegister(sigset_t *ptBlockset)
{
  bool bCC = TRUE;

  // Local signals :
  if( signal(SIGUSR1, &udp_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp_SignalRegister(): SIGUSR1 ERROR");
     bCC = FALSE;
  }
  if( signal(SIGUSR2, &udp_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp_SignalRegister(): SIGUSR2 ERROR");
     bCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &udp_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp_SignalRegister(): SIGTERM ERROR");
     bCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &udp_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "UDP", "udp_SignalRegister(): SIGINT ERROR");
     bCC = FALSE;
  }
  //
  if(bCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGTERM);
  }
  return(bCC);
}

//
//  Function:   udp_ReceiveSignalUser1
//  Purpose:    SIGUSR1 (10) signal from a thread
//
//  Parms:
//  Returns:    
//
static void udp_ReceiveSignalUser1(int iSignal)
{
   //LOG_Report(0, "UDP", "udp_ReceiveSignalUser1()");
   udp_PostSemaphore();
}
//
//  Function:   udp_ReceiveSignalUser2
//  Purpose:    SIGUSR2 (12) signal from a thread
//
//  Parms:
//  Returns:    
//
static void udp_ReceiveSignalUser2(int iSignal)
{
   LOG_Report(0, "UDP", "udp_ReceiveSignalUser2(): Not used");
}

//
//  Function:   udp_ReceiveSignalInt
//  Purpose:    Handle the SIGINT command (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void udp_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "UDP", "udp_ReceiveSignalInt()");
   bCncRunning = FALSE;
   udp_PostSemaphore();
}

//
//  Function:   udp_ReceiveSignalTerm
//  Purpose:    Handle the SIGTERM command (Ctl-X)
//              LOG and ignore
//  Parms:
//  Returns:    
//
static void udp_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "UDP", "udp_ReceiveSignalTerm()");
}

/*------  Local functions separator ------------------------------------
__Semaphore_Functions_(){};
----------------------------------------------------------------------------*/

//
//  Function:  udp_InitSemaphore
//  Purpose:   Init the UDP Semaphore
//
//  Parms:      
//  Returns:    
//  Note:      The sem value is always >= 0:
//                               0 means LOCKED
//                              >0 means UNLOCKED
//             Init(sem 1, VAL)  VAL=1 (unlocked)
//             Wait(sem)         LOCK:    if VAL>0:  VAL--
//                                        if(VAL==0) Wait
//             Post(sem)         UNLOCK:  VAL++
//
static void udp_InitSemaphore(void)
{
   CNCMAP  *pstMap = GLOBAL_GetMapping();

   //
   // Init the semaphore as LOCKED
   //
   if(sem_init(&pstMap->G_tUdpSem, 1, 0) == -1)
   {
      LOG_Report(errno, "UDP", "udp_InitSemaphore(): sem_init Error");
   }
}

//
//  Function:   udp_PostSemaphore
//  Purpose:    Unlock the UDP Semaphore
//
//  Parms:      
//  Returns:    
//
static void udp_PostSemaphore(void)
{
   int      iCc;
   CNCMAP  *pstMap = GLOBAL_GetMapping();

   //
   // sem++ (UNLOCK)   
   //
   iCc = sem_post(&pstMap->G_tUdpSem);
   if(iCc == -1) LOG_Report(errno, "UDP", "udp_PostSemaphore(): sem_post error");
}

//
//  Function:   udp_WaitSemaphore
//  Purpose:    Wait till UDP Semaphore has been unlocked, then LOCK and proceed
//
//  Parms:      
//  Returns:    
//
static void udp_WaitSemaphore(void)
{
   CNCMAP  *pstMap = GLOBAL_GetMapping();

   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   while( (sem_wait(&pstMap->G_tUdpSem) == -1) && errno == EINTR)
   {
      continue;
   }
}


