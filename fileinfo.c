/*  (c) Copyright:  2013..2016 Patrn, Confidential Data
**
**  $Workfile:   $      fileinfo.c
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Get directory and file info
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       2 Jun 2013
**
 *  Revisions:
 *    $Log:   $
 * v100: Initial revision.
 *
 *
 *
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>

#include "echotype.h"
#include "log.h"
#include "fileinfo.h"

//#define USE_PRINTF
#include "printf.h"

#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

//
// Static functions
//
static char *pcFileFilter = NULL;  
static int   finfo_FileFilter    (const struct dirent *);
static void  finfo_RemoveCrLf    (char *);

/* ======   Local Functions separator ===========================================
void ___global_functions(){}
==============================================================================*/

/*
 * Function    : FINFO_GetDirEntries
 * Description : Retrieve all info inside a single directory
 *
 * Parameters  : Dir path, filter string, result ptr
 * Returns     : dirent struct prt
 *
 */
void *FINFO_GetDirEntries(char *pcPath, char *pcFilter, int *piNum)
{
   struct dirent **pstDir=NULL;

   pcFileFilter = pcFilter;
   //
   *piNum = scandir(pcPath, &pstDir, finfo_FileFilter, alphasort);
   return(pstDir);
}

/*
 * Function    : FINFO_ReleaseDirEntries
 * Description : Release the dir info data struct
 *
 * Parameters  : Dir info prt, num entries
 * Returns     : 
 *
 */
void FINFO_ReleaseDirEntries(void *pvData, int iNr)
{
   struct dirent **pstDir = pvData;

   while(iNr--)
   {
      free(pstDir[iNr]);
   }
   free(pstDir);
}

/*
 * Function    : FINFO_GetFilename
 * Description : Get filename from current dir/file
 *
 * Parameters  : dirent dir info, entry nr, buffer and size
 * Returns     : TRUE if OKee
 *
 */
bool FINFO_GetFilename(void *pvData, int iIdx, char *pcBuffer, int iLen)
{
   struct dirent **pstDir = pvData;

   strncpy(pcBuffer, pstDir[iIdx]->d_name, iLen);
   return(TRUE);
}

/*
 * Function    : FINFO_GetFileInfo
 * Description : Retrieve info about a file in this dir
 *
 * Parameters  : Dir path, info type, buffer, len
 * Returns     : TRUE if OKee
 *
 */
bool FINFO_GetFileInfo(char *pcPath, FINFO iFinfo, char *pcBuffer, int iLen)
{
   bool        fCc=TRUE;
   struct stat stStat;

   PRINTF1("FINFO_GetFileInfo(): File=[%s]" CRLF, pcPath);

   if(stat(pcPath, &stStat) == -1) return(FALSE);

   switch(iFinfo)
   {
      case FI_FILEDIR:
         switch(stStat.st_mode & S_IFMT)
         {
            case S_IFDIR: 
               *pcBuffer = 'D';
               break;

            case S_IFREG:
               *pcBuffer = 'F';
               break;

            default:
               *pcBuffer = '?';
               break;
         }
         break;

      case FI_SIZE:
         ES_sprintf(pcBuffer, "%lld", (long long) stStat.st_size);
         break;

      case FI_MDATE:
         ES_sprintf(pcBuffer, "%s", ctime(&stStat.st_mtime));
         finfo_RemoveCrLf(pcBuffer);
         break;

      case FI_UID:
         ES_sprintf(pcBuffer, "%ld", (long)stStat.st_uid);
         break;

      case FI_GUI:
         ES_sprintf(pcBuffer, "%ld", (long)stStat.st_gid);
         break;

      default:
         fCc = FALSE;
      // Fallthrough
      case FI_NONE:
         *pcBuffer = 0;
         break;
   }
   return(fCc);
}

/* ======   Local Functions separator ===========================================
void ___static_functions(){}
==============================================================================*/

/*
 * Function    : finfo_FileFilter
 * Description : Filefilterfunction
 *
 * Parameters  : Dirent struct
 * Returns     : 0 to discard file entry
 *
 */
static int finfo_FileFilter(const struct dirent *pstDirent)
{
   int   iCc=0;
   int   iFilterLen;

   if(pcFileFilter)
   {
      //
      // We have a filter: check if there is a match
      //
      iFilterLen = strlen(pcFileFilter);
      if(strncmp(pcFileFilter, pstDirent->d_name, iFilterLen) == 0) 
      {
         PRINTF2("finfo_FileFilter(): Pass: Filter=[%s]-File=[%s]" CRLF, pcFileFilter, pstDirent->d_name);
         iCc = 1;
      }
      else
      {
         PRINTF2("finfo_FileFilter(): Fail: Filter=[%s]-File=[%s]" CRLF, pcFileFilter, pstDirent->d_name);
      }
   }
   else
   {
      //
      // No filter
      //
      iCc = 1;
      PRINTF1("finfo_FileFilter(): Pass: Filter=[None]-File=[%s]" CRLF, pstDirent->d_name);
   }
   return(iCc);
}

/*
 * Function    : finfo_RemoveCrLf
 * Description : Remove any line-ends from the buffer
 *
 * Parameters  : Buffer
 * Returns     : 
 *
 */
static void finfo_RemoveCrLf(char *pcBuffer)
{
   while(*pcBuffer)
   {
      if( (*pcBuffer == '\r') || (*pcBuffer == '\n') ) 
      {
         *pcBuffer = '\0';
         break;
      }
      pcBuffer++;
   }
}
