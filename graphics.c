/*  (c) Copyright:  2014..2016 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Add graphics capabilities for still pix analysis
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Raspbian linux gcc
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       29 Apr 2014
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <magick/api.h>
#include <wand/magick_wand.h>

#include "config.h"
#include "echotype.h"
#include "netfiles.h"
#include "rtc.h"
#include "log.h"
#include "globals.h"
#include "graphics.h"

//#define USE_PRINTF
#include "printf.h"


#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

//
// Local prototypes
//
static void    grf_DrawPicture         (u_int16 *, u_int32);
static int     grf_MarkDeltaGrids      (MagickWand *, char *, char *, const char *, MDET *, int, int);
static void    grf_ImageDrawLine       (MagickWand *, const char *, int, int, int, int);
static int     grf_ImageGetDelta       (char *, char *, int, const char *, int, int, int, int, int, int);
static int     grf_ImageSetDelta       (char *, char *, int, const char *, int, int, int, int, int, int);
static int     grf_ImageBufferGrayscale(char *, const char *, MDET *, int, int, int, int, int, int);
static int     grf_ImageBufferNormalize(char *, const char *, MDET *, int, int, int, int, int, int);
static char   *grf_MagickToImageBuffer (MagickWand *, const char *, int, int, int, int, int *);
static int     grf_ImageBufferToMagick (MagickWand *, char *, const char *, int, int, int, int);
static void    grf_ImageWriteDisplay   (char *, int, int, int);

#ifdef FEATURE_DRAW_BASICS
static void    grf_ImageDrawText       (MagickWand *, const char *, char *, int, int);
static void    grf_ImageDrawRect       (MagickWand *, const char *, int, int, int, int);
#endif   //FEATURE_DRAW_BASICS

//#define FEATURE_DUMP_RGB_LEVELS
#ifdef FEATURE_DUMP_RGB_LEVELS
//
// Dump RGB levels
//
static void grf_DumpRgbLevels(int, int, int);
//
#define GRF_DUMP_RGB_LEVELS(g,t,m)     grf_DumpRgbLevels(g,t,m)

#else
//
// No dump
//
#define GRF_DUMP_RGB_LEVELS(g,t,m)
//
#endif   //FEATURE_DUMP_RGB_LEVELS


static const char *pcPublicWwwDir = RPI_PUBLIC_WWW;
static const char *pcRgb          = "RGB";

/* ======   Local Functions separator ===========================================
void ___global_functions(){} 
==============================================================================*/

// 
//  Function:   GRF_Init
//  Purpose:    Graphics init
//
//  Parms:      
//  Returns:    -1 if error
//
int GRF_Init()
{
   return(0);
}

//
//  Function:   GRF_MotionDisplay
//  Purpose:    Graphics motion display on TFT
//
//  Parms:      Pix dir path
//  Returns:    
//
void GRF_MotionDisplay(char *pcFile)
{
   int            iSize, iHi, iWi;
   char          *pcImage=NULL;
   char          *pcFrmt;
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWand;

   //
   // Initialize GraphicsMagick API
   //
   InitializeMagick(NULL);
   //
   // Allocate Wand handles
   //
   pstWand = NewMagickWand();

   #ifdef COMMENT
   // Read the input image: default frog logo
	MagickReadImage(pstWand,"logo:");
	// (over)write image
	MagickWriteImage(pstWand, pcFile);
   MagickSetFormat(pstWand, "JPEG");
   #endif //COMMENT

   tStatus = MagickReadImage(pstWand, pcFile);
   pcFrmt  = MagickGetImageFormat(pstWand);
   if(pcFrmt)
   {
      PRINTF1("GRF_MotionDisplay(): Image Format = [%s]" CRLF, pcFrmt);
      free(pcFrmt);
   }
   else
   {
      PRINTF1("GRF_MotionDisplay(): BAD Image Format, status = %d" CRLF, tStatus);
   } 
   //====================================================================================
   // Check if we need to scale the picture
   //====================================================================================
   if (tStatus == MagickPass)
   {
      iHi     = MagickGetImageHeight(pstWand);
      iWi     = MagickGetImageWidth(pstWand);
      pcImage = grf_MagickToImageBuffer(pstWand, pcRgb, 0, 0, iWi, iHi, &iSize);
      //
      // Check if formats are OKee
      //
      PRINTF3("GRF_MotionDisplay(): Displaying w=%d h=%d[%s]..." CRLF, iWi, iHi, pcFile);
      grf_ImageWriteDisplay(pcImage, iWi, iHi, strlen(pcRgb));
   }
   else
   {
      PRINTF1("GRF_MotionDisplay(): Problems reading image [%s]..." CRLF, pcFile);
   }
   DestroyMagickWand(pstWand);
   DestroyMagick();
   safefree(pcImage);
}

//
//  Function:   GRF_MotionDetect
//  Purpose:    Graphics motion detect 
//
//  Parms:      Pix dir path, picture image format ("RGB", "I"), MDET struct
//  Returns:    Number of cells exceeding the motion threshold or -1 if error
//
int GRF_MotionDetect(char *pcPixDir, const char *pcFormat, MDET *pstMotDet)
{
   int            iDelta=0;
   int            iHiOlde=0, iWiOlde=0, iHiNewe=0, iWiNewe=0;
   char          *pcFileOld; 
   char          *pcFileNew; 
   char          *pcFileTmp; 
   char          *pcImageOld; 
   char          *pcImageNew; 
   char          *pcTimeStp; 
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWandOld;
   MagickWand    *pstWandNew;
   CNCMAP        *pstMap=GLOBAL_GetMapping();

   PRINTF1("GRF_MotionDetect(): using format [%s]..." CRLF, pcFormat);
   //
   // Initialize GraphicsMagick API
   //
   InitializeMagick(NULL);
   //
   // Allocate Wand handles
   //
   pstWandOld = NewMagickWand();
   pstWandNew = NewMagickWand();
   //
   pcFileOld = safemalloc(MAX_PATH_LEN);
   pcFileNew = safemalloc(MAX_PATH_LEN);
   pcFileTmp = safemalloc(MAX_PATH_LEN);
   pcTimeStp = safemalloc(MAX_PATH_LEN);
   //
   if(pstMotDet->iSeq & 1)
   {
      //
      // Old image is motion_0.bmp
      // New image is motion_1.bmp
      //
      ES_sprintf(pcFileOld, "%smotion_0.bmp", pcPixDir);
      ES_sprintf(pcFileNew, "%smotion_1.bmp", pcPixDir);
   }
   else
   {
      //
      // Old image is motion_1.bmp
      // New image is motion_0.bmp
      //
      ES_sprintf(pcFileOld, "%smotion_1.bmp", pcPixDir);
      ES_sprintf(pcFileNew, "%smotion_0.bmp", pcPixDir);
   }
   //====================================================================================
   // oldest pix --> WandOld
   // newest pix --> WandNew
   //====================================================================================
   if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandOld, pcFileOld);
   if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandNew, pcFileNew);
   //====================================================================================
   // Abort if both images have different sizes
   //====================================================================================
   if (tStatus == MagickPass)
   {
      PRINTF( "GRF_MotionDetect():MagickReadImage() OKee" CRLF);
      iHiOlde = MagickGetImageHeight(pstWandOld);
      iWiOlde = MagickGetImageWidth(pstWandOld);
      iHiNewe = MagickGetImageHeight(pstWandNew);
      iWiNewe = MagickGetImageWidth(pstWandNew);
      //
      // Check if formats are non-zero and identical
      //
      if(iWiOlde && iWiNewe && iHiOlde && iHiNewe)
      {
         if( (iWiOlde != iWiNewe) || (iHiOlde != iHiNewe) )
         {
            // Nope: abort
            PRINTF( "GRF_MotionDetect():ERROR! Bad Image(s)" CRLF);
            PRINTF3("GRF_MotionDetect():    Image(%s):w=%d, h=%d" CRLF, pcFileOld, iWiOlde, iHiOlde);
            PRINTF3("GRF_MotionDetect():    Image(%s):w=%d, h=%d" CRLF, pcFileNew, iWiNewe, iHiNewe);
            tStatus = MagickFail;
         }
      }
      else
      {
         tStatus = MagickFail;
      }
   }
   if (tStatus != MagickPass)
   {
      DestroyMagickWand(pstWandOld);
      DestroyMagickWand(pstWandNew);
      DestroyMagick();
      safefree(pcFileOld);
      safefree(pcFileNew);
      safefree(pcFileTmp);
      safefree(pcTimeStp);
      PRINTF4("GRF_MotionDetect():ERROR! Images Old w=%d, h=%d, New w=%d, h=%d" CRLF, iWiOlde, iHiOlde, iWiNewe, iHiNewe);
      return(-1);
   }
   PRINTF3("GRF_MotionDetect():Image(%s) w=%d, h=%d" CRLF, pcFileOld, iWiOlde, iHiOlde);
   PRINTF3("GRF_MotionDetect():Image(%s) w=%d, h=%d" CRLF, pcFileNew, iWiNewe, iHiNewe);
   //====================================================================================
   // WandOld --> ImageOld
   // WandNew --> ImageNew
   //====================================================================================
   pcImageOld = grf_MagickToImageBuffer(pstWandOld, pcFormat, 0, 0, iWiOlde, iHiOlde, NULL);
   pcImageNew = grf_MagickToImageBuffer(pstWandNew, pcFormat, 0, 0, iWiNewe, iHiNewe, NULL);
   //====================================================================================
   // Grayscale(ImageOld) --> ImageOld
   // Grayscale(ImageNew) --> ImageNew
   //====================================================================================
   grf_ImageBufferGrayscale(pcImageOld, pcFormat, pstMotDet, iWiOlde, iHiOlde, 0, 0, iWiOlde, iHiOlde);
   grf_ImageBufferGrayscale(pcImageNew, pcFormat, pstMotDet, iWiOlde, iHiOlde, 0, 0, iWiNewe, iHiNewe);
   //====================================================================================
   // Normalize(ImageOld) --> ImageOld
   // Normalize(ImageNew) --> ImageNew
   //====================================================================================
   pstMotDet->iAverage1 = grf_ImageBufferNormalize(pcImageOld, pcFormat, pstMotDet, iWiOlde, iHiOlde, 0, 0, iWiOlde, iHiOlde);
   pstMotDet->iAverage2 = grf_ImageBufferNormalize(pcImageNew, pcFormat, pstMotDet, iWiOlde, iHiOlde, 0, 0, iWiNewe, iHiNewe);
   //====================================================================================
   // ImageNew --> WandNew
   // Save the Grayscaled/Normalized NEW ImageBuffer to file
   //====================================================================================
   if( grf_ImageBufferToMagick(pstWandNew, pcImageNew, pcFormat, 0, 0, iWiNewe, iHiNewe) )
   {
      //====================================================================================
      // Calculate number of cells exceeding the motion threshold between OLD and NEW images
      //====================================================================================
      iDelta = grf_MarkDeltaGrids(pstWandNew, pcImageNew, pcImageOld, pcFormat, pstMotDet, iWiOlde, iHiOlde);
      //====================================================================================
      // Write new image to file, all grid-cells where motion was detected have been outlined
      //====================================================================================
      ES_sprintf(pcFileTmp, "%smotion_new_bw.bmp", pcPixDir);
      tStatus = MagickWriteImage(pstWandNew, pcFileTmp);

      //====================================================================================
      // ImageOld --> WandOld
      // Save the Grayscaled/Normalized OLD ImageBuffer to file
      //====================================================================================
      if( grf_ImageBufferToMagick(pstWandOld, pcImageOld, pcFormat, 0, 0, iWiOlde, iHiOlde) )
      {
         ES_sprintf(pcFileTmp, "%smotion_old_bw.bmp", pcPixDir);
         tStatus = MagickWriteImage(pstWandOld, pcFileTmp);
      }
      else
      {
         LOG_Report(0, "GRF", "GRF_MotionDetect(): Old-MagickSetImagePixels() error");
         PRINTF("GRF_MotionDetect(): Old-MagickSetImagePixels() error" CRLF);
         tStatus = MagickFail;
      }

      //
      //====================================================================================
      // If we have grids that exceed the motion threshold:
      // Save permananetly: YYYMMDD_HHMMSS_motion.bmp (including outlined cells)  
      //====================================================================================
      if(iDelta)
      {
         RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcTimeStp, RTC_GetSystemSecs());
         ES_sprintf(pcFileTmp, "%s%s/%s_motion.bmp", pcPublicWwwDir, pstMap->G_pcPixPath, pcTimeStp);
         PRINTF1("GRF_MotionDetect():Save motion-file(%s)..." CRLF, pcFileTmp);
         tStatus = MagickWriteImage(pstWandNew, pcFileTmp);
         //
         //====================================================================================
         // Convert the DELTA image from Grayscale to Black/White
         //====================================================================================
         grf_ImageSetDelta(pcImageNew, pcImageOld, pstMotDet->iThldPixel, pcFormat, iWiOlde, iHiOlde, 0, 0, iWiOlde, iHiOlde);
         //====================================================================================
         // ImageNew --> WandNew --> File 
         // Save permananetly: YYYMMDD_HHMMSS_delta.bmp   
         //====================================================================================
         if( grf_ImageBufferToMagick(pstWandNew, pcImageNew, pcFormat, 0, 0, iWiNewe, iHiNewe) )
         {
            ES_sprintf(pcFileTmp, "%s%s/%s_delta.bmp", pcPublicWwwDir, pstMap->G_pcPixPath, pcTimeStp);
            PRINTF1("GRF_MotionDetect():Save delta-file(%s)..." CRLF, pcFileTmp);
            tStatus = MagickWriteImage(pstWandNew, pcFileTmp);
         }
         else
         {
            LOG_Report(0, "GRF", "GRF_MotionDetect():MagickSetImagePixels() error");
            PRINTF("GRF_MotionDetect():MagickSetImagePixels() error" CRLF);
            tStatus = MagickFail;
         }
         //====================================================================================
         // Reload newest image and safe also permanently
         //====================================================================================
         if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandNew, pcFileNew);
         if (tStatus == MagickPass)
         {
            ES_sprintf(pcFileTmp, "%s%s/%s.bmp", pcPublicWwwDir, pstMap->G_pcPixPath, pcTimeStp);
            PRINTF1("GRF_MotionDetect():Save delta-file(%s)..." CRLF, pcFileTmp);
            tStatus = MagickWriteImage(pstWandNew, pcFileTmp);
         }
      }
   }
   else
   {
      LOG_Report(0, "GRF", "GRF_MotionDetect(): New-MagickSetImagePixels() error");
      PRINTF("GRF_MotionDetect(): New-MagickSetImagePixels() error" CRLF);
      tStatus = MagickFail;
   }
   //
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   //
   //
   if(tStatus != MagickPass)
   {
      char          *pcDescription;
      ExceptionType  tSeverity;

      pcDescription = MagickGetException(pstWandOld, &tSeverity);
      LOG_Report(0, "GRF", "GRF_MotionDetect():Old: %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF_MotionDetect(): Old: %s (Severity=%d)" CRLF, pcDescription, tSeverity);
      //
      pcDescription = MagickGetException(pstWandNew, &tSeverity);
      LOG_Report(0, "GRF", "GRF_MotionDetect():New : %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF_MotionDetect(): Old : %s (Severity=%d)" CRLF, pcDescription, tSeverity);
   }
   else
   {
      PRINTF("GRF_MotionDetect():OKee completion" CRLF);
   }
   DestroyMagickWand(pstWandOld);
   DestroyMagickWand(pstWandNew);
   DestroyMagick();
   safefree(pcFileOld);
   safefree(pcFileNew);
   safefree(pcImageOld);
   safefree(pcImageNew);
   safefree(pcFileTmp);
   safefree(pcTimeStp);
   //
   return(iDelta);
}

//
//  Function:   GRF_MotionNormalize
//  Purpose:    Normalize snapshot
//
//  Parms:      Pix dir path, picture image format ("RGB", "I"), MDET struct
//  Returns:    TRUE if OKee
//
bool GRF_MotionNormalize(char *pcPixDir, const char *pcFormat, MDET *pstMotDet)
{
   bool           fCc=FALSE;
   int            iHiEnd, iWiEnd;
   char          *pcFile; 
   char          *pcImage; 
   MagickWand    *pstWand;
   MagickPassFail tStatus=MagickPass;

   PRINTF2("GRF_MotionNormalize():Using format[%s]:Seq=%d" CRLF, pcFormat, pstMotDet->iSeq);
   //====================================================================================
   // Initialize GraphicsMagick API
   //====================================================================================
   InitializeMagick(NULL);
   //====================================================================================
   // Allocate Wand handles
   //====================================================================================
   pstWand   = NewMagickWand();
   pcFile    = safemalloc(MAX_PATH_LEN);
   //====================================================================================
   // image is motion_?.bmp
   //====================================================================================
   ES_sprintf(pcFile, "%smotion_%d.bmp", pcPixDir, pstMotDet->iSeq & 1);
   //====================================================================================
   // pix --> Wand
   //====================================================================================
   tStatus = MagickReadImage(pstWand, pcFile);
   //
   iHiEnd = MagickGetImageHeight(pstWand);
   iWiEnd = MagickGetImageWidth(pstWand);
   PRINTF3("GRF_MotionNormalize():Image(%s) w=%d, h=%d" CRLF, pcFile, iWiEnd, iHiEnd);
   //====================================================================================
   // Wand --> Image
   //====================================================================================
   pcImage = grf_MagickToImageBuffer(pstWand, pcFormat, 0, 0, iWiEnd, iHiEnd, NULL);
   //====================================================================================
   // Grayscale(Image) --> Image
   //====================================================================================
   grf_ImageBufferGrayscale(pcImage, pcFormat, pstMotDet, iWiEnd, iHiEnd, 0, 0, iWiEnd, iHiEnd);
   //====================================================================================
   // Normalize(Image) --> Image
   //====================================================================================
   grf_ImageBufferNormalize(pcImage, pcFormat, pstMotDet, iWiEnd, iHiEnd, 0, 0, iWiEnd, iHiEnd);
   //====================================================================================
   // Image --> Wand
   // Save the Grayscaled/Normalized NEW ImageBuffer to file
   //====================================================================================
   if( grf_ImageBufferToMagick(pstWand, pcImage, pcFormat, 0, 0, iWiEnd, iHiEnd) )
   {
      fCc = TRUE;
      //====================================================================================
      // Write normalized image to file
      //====================================================================================
      ES_sprintf(pcFile, "%smotion_%d_norm.bmp", pcPixDir, pstMotDet->iSeq & 1);
      tStatus = MagickWriteImage(pstWand, pcFile);
   }
   else
   {
      LOG_Report(0, "GRF", "GRF_MotionNormalize(): MagickSetImagePixels() error");
      PRINTF("GRF_MotionNormalize(): MagickSetImagePixels() error" CRLF);
      tStatus = MagickFail;
   }
   //
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   //
   //
   if(tStatus != MagickPass)
   {
      char          *pcDescription;
      ExceptionType  tSeverity;

      pcDescription = MagickGetException(pstWand, &tSeverity);
      LOG_Report(0, "GRF", "GRF_MotionNormalize(): %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF_MotionNormalize(): %s (Severity=%d)" CRLF, pcDescription, tSeverity);
   }
   else
   {
      PRINTF("GRF_MotionNormalize():OKee completion" CRLF);
   }
   DestroyMagickWand(pstWand);
   DestroyMagick();
   safefree(pcFile);
   safefree(pcImage);
   //
   return(fCc);
}

//
//  Function:   GRF_MotionDelta
//  Purpose:    Graphics motion calculate and build delta image
//
//  Parms:      Pix dir path, picture image format ("RGB", "I"), MDET struct
//  Returns:    Number of cells exceeding the motion threshold or -1 if error
//
int GRF_MotionDelta(char *pcPixDir, const char *pcFormat, MDET *pstMotDet)
{
   int            iDelta=0;
   int            iHiOlde=0, iWiOlde=0, iHiNewe=0, iWiNewe=0;
   char          *pcFileOld; 
   char          *pcFileNew; 
   char          *pcFileTmp; 
   char          *pcImageOld; 
   char          *pcImageNew; 
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWandOld;
   MagickWand    *pstWandNew;

   PRINTF1("GRF_MotionDelta(): using format [%s]..." CRLF, pcFormat);
   //
   // Initialize GraphicsMagick API
   //
   InitializeMagick(NULL);
   //
   // Allocate Wand handles
   //
   pstWandOld = NewMagickWand();
   pstWandNew = NewMagickWand();
   //
   pcFileOld = safemalloc(MAX_PATH_LEN);
   pcFileNew = safemalloc(MAX_PATH_LEN);
   pcFileTmp = safemalloc(MAX_PATH_LEN);
   //

   if(pstMotDet->iSeq & 1)
   {
      //====================================================================================
      // Old image is smotion_0_norm.bmp
      // New image is smotion_1_norm.bmp
      //====================================================================================
      ES_sprintf(pcFileOld, "%smotion_0_norm.bmp", pcPixDir);
      ES_sprintf(pcFileNew, "%smotion_1_norm.bmp", pcPixDir);
   }
   else
   {
      //====================================================================================
      // Old image is smotion_1_norm.bmp
      // New image is smotion_0_norm.bmp
      //====================================================================================
      ES_sprintf(pcFileOld, "%smotion_1_norm.bmp", pcPixDir);
      ES_sprintf(pcFileNew, "%smotion_0_norm.bmp", pcPixDir);
   }
   //
   if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandOld, pcFileOld);
   if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandNew, pcFileNew);
   //====================================================================================
   // Abort if both images have different sizes
   //====================================================================================
   if (tStatus == MagickPass)
   {
      iHiOlde = MagickGetImageHeight(pstWandOld);
      iWiOlde = MagickGetImageWidth(pstWandOld);
      iHiNewe = MagickGetImageHeight(pstWandNew);
      iWiNewe = MagickGetImageWidth(pstWandNew);
      //
      // Check if formats are non-zero and identical
      //
      if(iWiOlde && iWiNewe && iHiOlde && iHiNewe)
      {
         if( (iWiOlde != iWiNewe) || (iHiOlde != iHiNewe) )
         {
            // Nope: abort
            PRINTF( "GRF_MotionDelta():Bad Image(s)" CRLF);
            PRINTF3("GRF_MotionDelta():    Image(%s):w=%d, h=%d" CRLF, pcFileOld, iWiOlde, iHiOlde);
            PRINTF3("GRF_MotionDelta():    Image(%s):w=%d, h=%d" CRLF, pcFileNew, iWiNewe, iHiNewe);
            tStatus = MagickFail;
         }
      }
      else
      {
         tStatus = MagickFail;
      }
   }
   if (tStatus != MagickPass)
   {
      DestroyMagickWand(pstWandOld);
      DestroyMagickWand(pstWandNew);
      DestroyMagick();
      safefree(pcFileOld);
      safefree(pcFileNew);
      safefree(pcFileTmp);
      return(-1);
   }
   PRINTF3("GRF_MotionDelta():Old Image(%s) w=%d, h=%d" CRLF, pcFileOld, iWiOlde, iHiOlde);
   PRINTF3("GRF_MotionDelta():New Image(%s) w=%d, h=%d" CRLF, pcFileNew, iWiNewe, iHiNewe);
   //====================================================================================
   // FileOld --> WandOld --> ImageOld
   // FileNew --> WandNew --> ImageNew
   //====================================================================================
   pcImageOld = grf_MagickToImageBuffer(pstWandOld, pcFormat, 0, 0, iWiOlde, iHiOlde, NULL);
   pcImageNew = grf_MagickToImageBuffer(pstWandNew, pcFormat, 0, 0, iWiNewe, iHiNewe, NULL);
   //====================================================================================
   // Calculate number of cells exceeding the motion threshold between OLD and NEW images
   //====================================================================================
   iDelta = grf_MarkDeltaGrids(pstWandNew, pcImageNew, pcImageOld, pcFormat, pstMotDet, iWiOlde, iHiOlde);
   //====================================================================================
   // Write new image to file, all grid-cells where motion was detected have been outlined
   //====================================================================================
   ES_sprintf(pcFileTmp, "%smotion_marked.bmp", pcPixDir);
   tStatus = MagickWriteImage(pstWandNew, pcFileTmp);
   //
   //====================================================================================
   // Create the DELTA image from Grayscale Converted to Black/White
   // ImageNew --> WandNew --> File 
   //====================================================================================
   grf_ImageSetDelta(pcImageNew, pcImageOld, pstMotDet->iThldPixel, pcFormat, iWiOlde, iHiOlde, 0, 0, iWiOlde, iHiOlde);
   if( grf_ImageBufferToMagick(pstWandNew, pcImageNew, pcFormat, 0, 0, iWiNewe, iHiNewe) )
   {
      ES_sprintf(pcFileTmp, "%smotion_delta.bmp", pcPixDir);
      // pwjh Save on WWW: 
      // ES_sprintf(pcFileTmp, "%s%s/%s_delta.bmp", pcPublicWwwDir, pstMap->G_pcPixPath, pcTimeStp);
      PRINTF1("GRF_MotionDelta():Save delta-file(%s)..." CRLF, pcFileTmp);
      tStatus = MagickWriteImage(pstWandNew, pcFileTmp);
   }
   else
   {
      LOG_Report(0, "GRF", "GRF_MotionDelta():MagickSetImagePixels() error");
      PRINTF("GRF_MotionDelta():MagickSetImagePixels() error" CRLF);
      tStatus = MagickFail;
   }
   //
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   //
   if(tStatus != MagickPass)
   {
      char          *pcDescription;
      ExceptionType  tSeverity;

      pcDescription = MagickGetException(pstWandOld, &tSeverity);
      LOG_Report(0, "GRF", "GRF_MotionDelta():Old: %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF_MotionDelta(): Old: %s (Severity=%d)" CRLF, pcDescription, tSeverity);
      //
      pcDescription = MagickGetException(pstWandNew, &tSeverity);
      LOG_Report(0, "GRF", "GRF_MotionDelta():New : %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF_MotionDelta(): Old : %s (Severity=%d)" CRLF, pcDescription, tSeverity);
   }
   else
   {
      PRINTF("GRF_MotionDelta():OKee completion" CRLF);
   }
   DestroyMagickWand(pstWandOld);
   DestroyMagickWand(pstWandNew);
   DestroyMagick();
   safefree(pcFileOld);
   safefree(pcFileNew);
   safefree(pcImageOld);
   safefree(pcImageNew);
   safefree(pcFileTmp);
   //
   return(iDelta);
}

//
//  Function:   GRF_MotionZoom
//  Purpose:    Zoom snapshot
//
//  Parms:      Pix dir path, picture image format ("RGB", "I"), MDET struct
//  Returns:    TRUE if OKee
//
bool GRF_MotionZoom(char *pcPixDir, const char *pcFormat, MDET *pstMotDet)
{
   bool           fCc=FALSE;
   int            iSize, iHi, iWi;
   char          *pcFile; 
   char          *pcImage=NULL;
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWand;

   PRINTF2("GRF_MotionZoom():Using format[%s]:Seq=%d" CRLF, pcFormat, pstMotDet->iSeq);
   //====================================================================================
   // Initialize GraphicsMagick API
   //====================================================================================
   InitializeMagick(NULL);
   //====================================================================================
   // Allocate Wand handles
   //====================================================================================
   pstWand   = NewMagickWand();
   pcFile    = safemalloc(MAX_PATH_LEN);
   //====================================================================================
   // image is motion_?.bmp
   //====================================================================================
   ES_sprintf(pcFile, "%smotion_%d.bmp", pcPixDir, pstMotDet->iSeq & 1);
   //====================================================================================
   // pix --> Wand
   //====================================================================================
   tStatus = MagickReadImage(pstWand, pcFile);
   //====================================================================================
   // Check if we need to scale the picture
   //====================================================================================
   if (tStatus == MagickPass)
   {
      // PwjH: test scale factor 2
      iHi     = MagickGetImageHeight(pstWand)/2;
      iWi     = MagickGetImageWidth(pstWand)/2;
      pcImage = grf_MagickToImageBuffer(pstWand, pcRgb, 0, 0, iWi, iHi, &iSize);
      //
      // Check if formats are OKee
      //
      PRINTF4("GRF_MotionZoom(): Displaying w=%d h=%d[%s]=%d bytes..." CRLF, iWi, iHi, pcFile, iSize);
      //====================================================================================
      // Image --> Wand
      // Save the ZOOMed ImageBuffer to file
      //====================================================================================
      if( grf_ImageBufferToMagick(pstWand, pcImage, pcFormat, 0, 0, iWi, iHi) )
      {
         //====================================================================================
         // Write normalized image to file
         //====================================================================================
         ES_sprintf(pcFile, "%smotion_%d_zoom.bmp", pcPixDir, pstMotDet->iSeq & 1);
         tStatus = MagickWriteImage(pstWand, pcFile);
         grf_ImageWriteDisplay(pcImage, iWi, iHi, strlen(pcRgb));
         fCc = TRUE;
      }
      else
      {
         LOG_Report(0, "GRF", "GRF_MotionZoom(): grf_ImageBufferToMagick() error");
         PRINTF("GRF_MotionZoom(): grf_ImageBufferToMagick() error" CRLF);
         tStatus = MagickFail;
      }
   }
   //
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   //
   //
   if(tStatus != MagickPass)
   {
      char          *pcDescription;
      ExceptionType  tSeverity;

      pcDescription = MagickGetException(pstWand, &tSeverity);
      LOG_Report(0, "GRF", "GRF_MotionZoom(): %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF_MotionZoom(): %s (Severity=%d)" CRLF, pcDescription, tSeverity);
   }
   else
   {
      PRINTF("GRF_MotionZoom():OKee completion" CRLF);
   }
   DestroyMagickWand(pstWand);
   DestroyMagick();
   safefree(pcFile);
   safefree(pcImage);
   return(fCc);
}


/* ======   Local Functions separator ===========================================
void ___local_functions(){} 
==============================================================================*/

//
//  Function:  grf_MarkDeltaGrids
//  Purpose:   Calcutate the delta's for the whole grid
//
//  Parms:     pstWand, pcImageNew, pcImageOld, pcFormatReq, pstMotDet, iWi, iHi
//  Returns:   Number of grid-cells exceeding the threshold value   
//
static int grf_MarkDeltaGrids(MagickWand *pstWand, char *pcImageNew, char *pcImageOld, const char *pcFormatReq, MDET *pstMotDet, int iWi, int iHi)
{
   int   iResult=0, iDelta, iX, iY;
   int   iGridWi, iGridHi;
   int   iXs, iXe, iYs, iYe;

   iGridWi = pstMotDet->iGridWidth;
   iGridHi = pstMotDet->iGridHeight;
   //
   // Setup grid-element coords:
   //
   //       iXs         iXe
   //  iYs  +-----------+
   //       |           |
   //       |           |
   //       |           |
   //       |           |
   //  iYe  +-----------+
   //
   //

   //
   for(iY=0; iY<iGridHi; iY++)
   {
      iYs = (iY+0)*iHi/iGridHi;
      iYe = (iY+1)*iHi/iGridHi;
      //
      for(iX=0; iX<iGridWi; iX++)
      {
         iXs = (iX+0)*iWi/iGridWi;
         iXe = (iX+1)*iWi/iGridWi;
         //
         // o Divide whole picture into separate grids
         // o Calculate percentage of pixels that deviate above threshold (iThldPixel) within each grid
         // o Scale each value according to the grid-scales (iGridScales 0..100%)
         // o If deviation exceeds level (iThldLevel): mark grid with yellow paint
         //
         iDelta  = grf_ImageGetDelta(pcImageNew, pcImageOld, pstMotDet->iThldPixel, pcFormatReq, iWi, iHi, iX*iWi/iGridWi, iY*iHi/iGridHi, (iX+1)*iWi/iGridWi, (iY+1)*iHi/iGridHi);
         iDelta *= pstMotDet->iGridScales[iY][iX];
         iDelta /= 100;
         //
         pstMotDet->iGridMotion[iY][iX] = iDelta;
         if(iDelta > pstMotDet->iThldLevel)
         {
            iResult++;
            grf_ImageDrawLine(pstWand, "yellow", iXs, iYs, iXe, iYs);
            grf_ImageDrawLine(pstWand, "yellow", iXe, iYs, iXe, iYe);
            grf_ImageDrawLine(pstWand, "yellow", iXe, iYe, iXs, iYe);
            grf_ImageDrawLine(pstWand, "yellow", iXs, iYe, iXs, iYs);
            //
            // Or block (fill color/opacity ?)
            // grf_ImageDrawRect(pstWand, "yellow", iXs, iYs, iXe, iYe);
            //
            PRINTF4("grf_MarkDeltaGrids():Block[%d,%d]=%2d,%d%%" CRLF, iX, iY, iDelta/10, iDelta%10);
         }
      }
   }
   return(iResult);
}

//
//  Function:   grf_ImageDrawLine
//  Purpose:    Draw line onto imagewand
//
//  Parms:      Wand, linecolor, start w/h, end w/h
//  Returns:    
//
static void grf_ImageDrawLine(MagickWand *pstWand, const char *pcColor, int iXs, int iYs, int iXe, int iYe)
{
   DrawingWand   *pstDraw;
   PixelWand     *pstColor;
   
   pstDraw  = NewDrawingWand();
   pstColor = NewPixelWand();
   //
   PixelSetColor(pstColor, pcColor);
   DrawSetStrokeColor(pstDraw, pstColor);
   DrawSetStrokeOpacity(pstDraw, 1.0);
   DrawLine(pstDraw, (double)iXs, (double)iYs, (double)iXe, (double)iYe);
   MagickDrawImage(pstWand, pstDraw);
   //
   DestroyPixelWand(pstColor);
   DestroyDrawingWand(pstDraw);
}

#ifdef FEATURE_DRAW_BASICS
//
//  Function:   grf_ImageDrawRect
//  Purpose:    Draw rectangle onto imagewand
//
//  Parms:      Wand, linecolor, start w/h, end w/h
//  Returns:    
//
static void grf_ImageDrawRect(MagickWand *pstWand, const char *pcColor, int iXs, int iYs, int iXe, int iYe)
{
   DrawingWand   *pstDraw;
   PixelWand     *pstColor;
   
   pstDraw  = NewDrawingWand();
   pstColor = NewPixelWand();
   //
   PixelSetColor(pstColor, pcColor);
   DrawSetStrokeColor(pstDraw, pstColor);
   DrawSetStrokeOpacity(pstDraw, 0.5);
   //
   PixelSetColor(pstColor, "white");
   DrawSetFillColor(pstDraw, pstColor);
   DrawSetFillOpacity(pstDraw, 0.5);
   //DrawLine(pstDraw, (double)iXs, (double)iYs, (double)iXe, (double)iYe);
   DrawRectangle(pstDraw, (double)iXs, (double)iYs, (double)iXe, (double)iYe);
   MagickDrawImage(pstWand, pstDraw);
   //
   DestroyPixelWand(pstColor);
   DestroyDrawingWand(pstDraw);
}

//
//  Function:   grf_ImageDrawText
//  Purpose:    Draw text onto imagewand
//
//  Parms:      Wand, linecolor, text, start w/h
//  Returns:    
//
static void grf_ImageDrawText(MagickWand *pstWand, const char *pcColor, char *pcText, int iXs, int iYs)
{
   DrawingWand   *pstDraw;
   PixelWand     *pstColor;
   
   pstDraw  = NewDrawingWand();
   pstColor = NewPixelWand();
   //
   PixelSetColor(pstColor, pcColor);
   DrawSetStrokeColor(pstDraw, pstColor);
   //
   MagickAnnotateImage(pstWand, pstDraw, (double)iXs, (double)iYs, 0.0, pcText);
   MagickDrawImage(pstWand, pstDraw);
   //
   DestroyPixelWand(pstColor);
   DestroyDrawingWand(pstDraw);
}
#endif   //FEATURE_DRAW_BASICS

//
//  Function:   grf_ImageGetDelta
//  Purpose:    Calculate the delta between two images
//
//  Parms:      Image Dest, Image Src, Threshold, Total w/h, start w/h, end w/h
//  Returns:    Delta 0-100.0%   
//
static int grf_ImageGetDelta(char *pcImgDest, char *pcImgSrc, int iThreshold, const char *pcFormat, int iWt, int iHt, int iWs, int iHs, int iWe, int iHe)
{
   int            i, iTemp, iDelta=0;
   int            iPixSrc, iPixDest;
   int            iImageSize=0, iX=0, iY=0;
   char          *pcSrc, *pcDest; 
   const int      iFormatLength = strlen(pcFormat);

   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc  = &pcImgSrc[iFormatLength*((iY*iWt)+iWs)];
      pcDest = &pcImgDest[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         for(i=0; i<iFormatLength; i++)
         {
            iPixSrc  = (int) *pcSrc++;
            iPixDest = (int) *pcDest++;
            iTemp    = abs(iPixSrc - iPixDest);
            if(iTemp > iThreshold)
            {
               //
               // Significant change in grayscale pixel value : count it
               //
               iDelta++;
               //PRINTF3("grf_ImageGetDelta(): Delta=%5d (%3d=%3d)" CRLF, iDelta, iPixSrc, iPixDest);
            }
            else
            {
               //
               // Grayscale pixels are almost identical: clear them
               //
            }
            iImageSize++;
         }
      }
   }
   iTemp = (iDelta*1000)/iImageSize;
   //PRINTF6("grf_ImageGetDelta(): Image[%3d,%3d]-[%3d,%3d]:%3d,%d%%" CRLF, iWs, iHs, iWe, iHe, iTemp/10, iTemp%10);
   return(iTemp);
}

//
//  Function:   grf_ImageSetDelta
//  Purpose:    Calculate the delta between two images and set all values accordingly
//
//  Parms:      Image Dest, Image Src, Threshold, Total w/h, start w/h, end w/h
//  Returns:    Delta 0-100.0%   
//
static int grf_ImageSetDelta(char *pcImgDest, char *pcImgSrc, int iThreshold, const char *pcFormat, int iWt, int iHt, int iWs, int iHs, int iWe, int iHe)
{
   int            i, iTemp, iDelta=0;
   int            iPixSrc, iPixDest;
   int            iImageSize=0, iX=0, iY=0;
   char          *pcSrc, *pcDest; 
   const int      iFormatLength = strlen(pcFormat);

   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc  = &pcImgSrc[iFormatLength*((iY*iWt)+iWs)];
      pcDest = &pcImgDest[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         for(i=0; i<iFormatLength; i++)
         {
            iPixSrc  = (int) *pcSrc;
            iPixDest = (int) *pcDest;
            iTemp    = abs(iPixSrc - iPixDest);
            if(iTemp > iThreshold)
            {
               //
               // Significant change in grayscale pixel value : count it
               //
               iDelta++;
               *pcDest = 255;
               //PRINTF3("grf_ImageSetDelta(): Delta=%5d (%3d=%3d)" CRLF, iDelta, iPixSrc, iPixDest);
            }
            else
            {
               //
               // Grayscale pixels are almost identical: clear them
               //
               *pcDest = 0;
            }
            pcSrc++;
            pcDest++;
            iImageSize++;
         }
      }
   }
   iTemp = (iDelta*1000)/iImageSize;
   //PRINTF6("grf_ImageSetDelta(): Image[%3d,%3d]-[%3d,%3d]:%3d,%d%%" CRLF, iWs, iHs, iWe, iHe, iTemp/10, iTemp%10);
   return(iTemp);
}

//
//  Function:   grf_ImageBufferNormalize
//  Purpose:    Normalize an image: find MAX and recalculate all to MAX
//
//  Parms:      Image, Image format, MDET struct, image sizes
//  Returns:    New average
//
static int grf_ImageBufferNormalize(char *pcImage, const char *pcFormat, MDET *pstMotDet, int iWt, int iHt, int iWs, int iHs, int iWe, int iHe)
{
   int         i, iMin=255, iMax=0;
   int         iPix, iAvg=0, iImageSize=0;
   int         iX, iY;
   long        lSum=0;
   char       *pcSrc;
   const int   iFormatLength = strlen(pcFormat);
   
   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc = &pcImage[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         for(i=0; i<iFormatLength; i++)
         {
            iPix  = (int) *pcSrc++;
            // Keep track of min-max
            if(iPix > iMax) iMax = iPix;
            if(iPix < iMin) iMin = iPix;
         }
      }
   }
   //
   // Recalculate all value to normalize(max)
   //
   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc = &pcImage[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         for(i=0; i<iFormatLength; i++)
         {
            iPix        = (int)*pcSrc;
            iPix       -= iMin;
            iPix        = (iPix * 255) / (iMax-iMin);
            lSum       += (long) iPix;
            *pcSrc++    = (char) iPix;
            iImageSize++;
         }
      }
   }
   if(iImageSize) iAvg = (int)(lSum/iImageSize);
   PRINTF3("grf_ImageBufferNormalize(): Min=%d, Max=%d, Avg=%d" CRLF, iMin, iMax, iAvg);
   return(iAvg);
}

//
//  Function:   grf_ImageBufferGrayscale
//  Purpose:    Transfer selected pixels into grayscale
//
//  Parms:      Pixelbuffer, wandformat (RGB,..), MDET struct, total w/h, start w/h, end w/h
//  Returns:    1=OKee
//
static int grf_ImageBufferGrayscale(char *pcImage, const char *pcFormat, MDET *pstMotDet, int iWt, int iHt, int iWs, int iHs, int iWe, int iHe)
{
   int            i;
   int            iX, iY, iGray, iAverageSum;
   int            iRd, iGn, iBl;
   char          *pcSrc, *pcDest; 
   const int      iFormatLength = strlen(pcFormat);

   iRd = pstMotDet->iThldRed;
   iGn = pstMotDet->iThldGreen;
   iBl = pstMotDet->iThldBlue;
   //
   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc  = &pcImage[iFormatLength*((iY*iWt)+iWs)];
      pcDest = &pcImage[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         iGray       = 0;
         iAverageSum = 0;
         //
         for(i=0; i<iFormatLength; i++)
         {
            switch(pcFormat[i])
            {
               case 'R':
                  iGray       += (iRd * (*pcSrc++));
                  iAverageSum += iRd;
                  break;

               case 'G':
                  iGray       += (iGn * (*pcSrc++));
                  iAverageSum += iGn;
                  break;

               case 'B':
                  iGray       += (iBl * (*pcSrc++));
                  iAverageSum += iBl;
                  break;

               default:
               //case 'Y':
               //case 'U':
               //case 'V':
               //case 'I':
                  iGray       += (1000 * (*pcSrc++));
                  iAverageSum += 1000;
                  break;

            }
         }
         //
         // Scale back the average
         //
         iGray /= iAverageSum;
         //
         // Store the result
         //
         for(i=0; i<iFormatLength; i++)
         {
            *pcDest++ = (char) iGray;
         }
         //
         // Display result :
         //   pix, threshold, mode (0=CRLF, 1=value)
         //
         //GRF_DUMP_RGB_LEVELS(iGray, 100, 1);
         GRF_DUMP_RGB_LEVELS(iGray, 0, 1);
      }
      GRF_DUMP_RGB_LEVELS(0,0,0);
   }
   return(1);
}

//
//  Function:   grf_MagickToImageBuffer
//  Purpose:    Transfer selected pixels from Magick cache into a new buffer
//
//  Parms:      Wand-struct, coordinates start/end, result buffer size
//  Returns:    Buffer^ or NULL
//
static char *grf_MagickToImageBuffer(MagickWand *pstMagickWand, const char *pcFormat, int iWs, int iHs, int iWe, int iHe, int *piSize)
{
   int            iTotalSize;
   char          *pcImage; 
   const int      iFormatLength = strlen(pcFormat);

   iTotalSize = (iWe-iWs) * (iHe-iHs) * iFormatLength * sizeof(char);
   pcImage = safemalloc(iTotalSize);
   if(piSize) *piSize = iTotalSize;
   //
   // Read image RGB pixels
   //
   if( MagickGetImagePixels(pstMagickWand, iWs, iHs, iWe, iHe, pcFormat, CharPixel, (void *)pcImage) == 0)
   {
      LOG_Report(0, "GRF", "grf_MagickToImageBuffer():MagickGetImagePixels() error");
      PRINTF("grf_MagickToImageBuffer():MagickGetImagePixels() error" CRLF);
   }
   return(pcImage);
}

//
//  Function:   grf_ImageBufferToMagick
//  Purpose:    Transfer selected pixels from pixel buffer to Magick cache
//
//  Parms:      Wand-struct, coordinates
//  Returns:    1=OKee
//
static int grf_ImageBufferToMagick(MagickWand *pstMagickWand, char *pcPixels, const char *pcFormat, int iWs, int iHs, int iWe, int iHe)
{
   int   iCc;

   iCc = MagickSetImagePixels(pstMagickWand, iWs, iHs, iWe, iHe, pcFormat, CharPixel, (void *)pcPixels);
   return(iCc);
}

//
//  Function:  grf_ImageWriteDisplay
//  Purpose:   Convert and write RGB pixels to a (virtual) display
//
//  Parms:     RGB Buffer, widt, heigth, number of subpixels (RGB, I)
//  Returns:    
//
static void grf_ImageWriteDisplay(char *pcImage, int iWi, int iHi, int iRgb)
{
   int      iW, iWiS, iH, iHiS;
   int      iR, iG, iB;
   int      iRow, iCol;
   int      iDisRow, iDisCol;
   u_int16  usImage;
   u_int8   ubR, ubG, ubB;
   char    *pcTemp;
   char    *pcPix;
   u_int16 *pusImage;
   u_int16 *pusTemp;
   u_int16 *pusImageEnd;

   PRINTF3("grf_ImageWriteDisplay(): Bmp w=%d h=%d (%d bpp) " CRLF, iWi, iHi, iRgb);
   //
   // Allocate memory for TFT image
   //
   pusImage = (u_int16 *) safemalloc(GRF_ACTUAL_ROWS * GRF_ACTUAL_COLS * sizeof(u_int16));
   pusTemp  = pusImage;
   //
   pusImageEnd  = &pusImage[GRF_ACTUAL_ROWS * GRF_ACTUAL_COLS];
   PRINTF2("grf_ImageWriteDisplay(): Display Start=%p End=%p" CRLF, pusTemp, pusImageEnd);
   //
   memset(pusImage, 0, GRF_ACTUAL_ROWS * GRF_ACTUAL_COLS * sizeof(u_int16));
   //
   // Check if we need to scale the picture down or not
   //
   iWiS = iWi/GRF_ACTUAL_COLS;
   iHiS = iHi/GRF_ACTUAL_ROWS;
   //
   // If remainder: always round up !
   //
   if(iWi%GRF_ACTUAL_COLS) iWiS++;
   if(iHi%GRF_ACTUAL_ROWS) iHiS++;
   //
   // Scale down: we need to average the RGB values
   //
   PRINTF2("grf_ImageWriteDisplay(): TFT Scale WiS=%d HiS=%d" CRLF, iWiS, iHiS);
   //
   // Center scaled TFT screen
   //
   iDisRow = (GRF_ACTUAL_ROWS-(iHi/iHiS))/2;
   iDisCol = (GRF_ACTUAL_COLS-(iWi/iWiS))/2;
   PRINTF2("grf_ImageWriteDisplay(): TFT Center row=%d col=%d" CRLF, iDisRow, iDisCol);
   //
   for(iRow=0; iRow<iHi-iHiS; iRow+=iHiS)
   {
      pusTemp = &pusImage[(iDisRow*GRF_ACTUAL_COLS)+iDisCol];
      for(iCol=0; iCol<iWi-iWiS; iCol+=iWiS)
      {
         pcTemp = &pcImage[((iRow*iWi) + iCol) * iRgb];
         iR = iG = iB = 0;
         for(iH=0; iH<iHiS; iH++)
         {
            pcPix = &pcTemp[(iH*iWi)*iRgb];
            for(iW=0; iW<iWiS; iW++)
            {
               switch(iRgb)
               {
                  default:
                     // Unknown profile
                     break;

                  case 1:
                     // "I"
                     iR += *pcPix;
                     iG += *pcPix;
                     iB += *pcPix++;
                     break;

                  case 3:
                     // "RGB"
                     iR += *pcPix++;
                     iG += *pcPix++;
                     iB += *pcPix++;
                     break;
               }
            }
         }
         //
         // Convert each 24 bits RGB value into a 16 bits RGB value:
         //
         // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         // |B|B|B|B|B|B|B|B|G|G|G|G|G|G|G|G|R|R|R|R|R|R|R|R|
         // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         //
         // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         // |R|R|R|R|R|G|G|G|G|G|G|B|B|B|B|B|
         // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         //
         ubR = (u_int8)(iR/(iHiS * iWiS));
         ubG = (u_int8)(iG/(iHiS * iWiS));
         ubB = (u_int8)(iB/(iHiS * iWiS));
         //
         usImage = (ubR >> 3);
         usImage = usImage << 6;
         usImage = usImage | (ubG >> 2);
         usImage = usImage << 5;
         usImage = usImage | (ubB >> 3);     
         //
         *pusTemp++ = usImage;
         if(pusTemp > pusImageEnd)
         {
            PRINTF5("grf_ImageWriteDisplay(): pusImage overflow ! row=%d col=%d disp=%d: %p %p" CRLF, iRow, iCol, iDisRow, pusTemp, pusImageEnd);
            pusTemp--;
         }
      }
      if(++iDisRow>=GRF_ACTUAL_ROWS)
      {
         PRINTF3("grf_ImageWriteDisplay(): Display row overflow! disp=%d: %p %p" CRLF, iDisRow, pusTemp, pusImageEnd);
         iDisRow--;
      }
   }
   PRINTF4("grf_ImageWriteDisplay(): Display filled (%d rows): Sta=%p Cur=%p End=%p" CRLF, iDisRow+1, pusImage, pusTemp, pusImageEnd);
   grf_DrawPicture(pusImage, (u_int32)(GRF_ACTUAL_ROWS * GRF_ACTUAL_COLS));
   PRINTF("grf_ImageWriteDisplay(): Called GRF_DrawPicture()" CRLF);
   safefree(pusImage);
}

/* ======   Local Functions separator ===========================================
void ___example_functions(){} 
==============================================================================*/

//
//  Function:   grf_DrawPicture
//  Purpose:    Write the 5-6-5-RGB image to the framebuffer
//
//  Parms:      
//  Returns:    
//
static void grf_DrawPicture(u_int16 *pusImage, u_int32 ulSize)
{
}

#ifdef FEATURE_DUMP_RGB_LEVELS
//
//  Function:   grf_DumpRgbLevels
//  Purpose:    Dump RGB levels to stdout
//
//  Parms:      
//  Returns:    
//
static void grf_DumpRgbLevels(int iGray, int iThres, int iMode)
{
   switch(iMode)
   {
      case 0:
         //
         // One line done
         //
         ES_printf(CRLF);
         break;

      default:
      case 1:
         //
         // Busy assembling one line with either bcd values or ascii
         //
         if(iThres)
         {
            //
            // Dump as ascii
            //
            if(iGray > iThres) ES_printf("*");
            else               ES_printf(" ");
         }
         else
         {
            //
            // Dump raw level data
            //
            ES_printf("%03d ", iGray);
         }
         break;
   }
}
#endif   //FEATURE_DUMP_RGB_LEVELS


//
// Trials:
//

// int            iCc=0;
// double         dParm;
// Image         *pstImage1;
// Image         *pstImage2;
// ImageInfo     *pstImageInfo1;
// ImageInfo     *pstImageInfo2;
// ExceptionInfo  stException;
// 
// ES_printf("GRF_Init():Magick on (%s, %d)..." CRLF, pcFoto, iParm);
// dParm = (double)iParm;
// dParm /= 1000;
// //
// GetExceptionInfo(&stException);
// InitializeMagick(NULL);
// pstImageInfo1 = CloneImageInfo((ImageInfo *) NULL);
// strcpy(pstImageInfo1->filename, pcFoto);
// pstImage1 = ReadImage(pstImageInfo1, &stException);
// //
// if (stException.severity != UndefinedException)
// {
//    ES_printf("GRF_Init():ReadImage(%s) ERROR %s:%s" CRLF, pstImageInfo1->filename, stException.reason, stException.description);
//    DestroyImageInfo(pstImageInfo1);
//    CatchException(&stException);
// }
// else
// {
//    ES_printf("GRF_Init():ReadImage(%s) OK: r=%d, c=%d (%s)" CRLF, pstImage1->filename, pstImage1->rows, pstImage1->columns, pstImage1->magick);
//    pstImageInfo2 = CloneImageInfo((ImageInfo *) NULL);
//    //pwjh pstImageInfo2 = CloneImageInfo(pstImageInfo1);
//    DestroyImageInfo(pstImageInfo1);
//    //
//    // Do something with the BMP
//    //
//    pstImage2 = EmbossImage(pstImage1, (double)0, dParm, &stException );
//    //
//    ES_sprintf(pstImageInfo2->filename, "%s_%s", pcFoto, "new.bmp");
//    SetImageInfo(pstImageInfo2, 0, &stException);
//    iCc = WriteImage(pstImageInfo2, pstImage2);
//    if(iCc)  ES_printf("GRF_Init():WriteImage(%s) OK" CRLF,    pstImageInfo2->filename);
//    else     ES_printf("GRF_Init():WriteImage(%s) ERROR" CRLF, pstImageInfo2->filename);
//    DestroyImageInfo(pstImageInfo2);
// }
// DestroyExceptionInfo(&stException);
// DestroyMagick();

   // Dump RGB levels of each pixel
   //
   // if(iParm < 175)
   // {
   //    // R
   //    for(iX=iParm; iX<iParm+25; iX++)
   //    {
   //       ES_printf("%03d ", pcPixels[(iY*200*3)+(iX+0)]);
   //    }
   //    ES_printf(CRLF);
   //    // G
   //    for(iX=iParm; iX<iParm+25; iX++)
   //    {
   //       ES_printf("%03d ", pcPixels[(iY*200*3)+(iX+1)]);
   //    }
   //    // B
   //    ES_printf(CRLF);
   //    for(iX=iParm; iX<iParm+25; iX++)
   //    {
   //       ES_printf("%03d ", pcPixels[(iY*200*3)+(iX+2)]);
   //    }
   //    ES_printf(CRLF);
   // }
   // else
   // {
   //    ES_printf("GRF_Filter():MagickGetImagePixels():Column too large" CRLF);
   // }

   //
   // Threshold image 
   //
   // if (tStatus == MagickPass)
   // {
   //    PixelWand *pstColor;
   //    pstColor=NewPixelWand();
   //    ES_printf("GRF_Filter():Filter image (%s)..." CRLF, pcFile);
   //    //PixelSetColor(pstColor, "#ffffff");
   //    PixelSetColorCount(pstColor, iParm);
   //    MagickBlackThresholdImage(pstMagickWand, pstColor);
   //    DestroyPixelWand(pstColor);
   // }

   //
   // Rotate image clockwise 30 degrees with black background
   // if (tStatus == MagickPass)
   // {
   //    PixelWand *pstBackground;
   //    ES_printf("GRF_Filter():Rotate image (%s)..." CRLF, pcFile);
   //    pstBackground=NewPixelWand();
   //    PixelSetColor(pstBackground, "#000000");
   //    tStatus = MagickRotateImage(pstMagickWand, pstBackground, iParm);
   //    DestroyPixelWand(pstBackground);
   // }

   // MagickPassFail tCc;
   // tCc = ImageToFile(pstImage, pcFile, &stException);
   // switch(tCc)
   // {
   //    case MagickPass:
   //       LOG_Report(0, "GRF", "Magick test edit OK : %s", pcFile);
   //       break;
   //
   //    case MagickFail:
   //       LOG_Report(0, "GRF", "Magick test edit BAD: %s", pcFile);
   //       break;
   //
   //    default:
   //       LOG_Report(0, "GRF", "Magick test edit UNKNOWN: %s", pcFile);
   //       break;
   // }


   // double         dAvg, dStDev;
   //
   // //====================================================================================
   // // WandOld : Calculate Avg, StDev
   // //====================================================================================
   // MagickGetImageChannelMean(pstWandOld, RedChannel, &dAvg, &dStDev);
   // PRINTF2("GRF_MotionDetect(): OLD:Original  RED: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //
   // MagickGetImageChannelMean(pstWandOld, GreenChannel, &dAvg, &dStDev);
   // PRINTF2("GRF_MotionDetect(): OLD:Original  GRN: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //
   // MagickGetImageChannelMean(pstWandOld, BlueChannel, &dAvg, &dStDev);
   // PRINTF2("GRF_MotionDetect(): OLD:Original  BLU: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //====================================================================================
   // // WandNew : Calculate Avg, StDev
   // //====================================================================================
   // MagickGetImageChannelMean(pstWandNew, RedChannel, &dAvg, &dStDev);
   // PRINTF2("GRF_MotionDetect(): NEW:Original  RED: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //
   // MagickGetImageChannelMean(pstWandNew, GreenChannel, &dAvg, &dStDev);
   // PRINTF2("GRF_MotionDetect(): NEW:Original  GRN: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //
   // MagickGetImageChannelMean(pstWandNew, BlueChannel, &dAvg, &dStDev);
   // PRINTF2("GRF_MotionDetect(): NEW:Original  BLU: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);

   // //====================================================================================
   // // Draw result into image
   // //
   // //====================================================================================
   // {
   //    DrawingWand *pstDraw;
   //    Image      *pstImage;
   // 
   //    pstImage = AllocateImage(NULL);
   //    pstDraw  = DrawAllocateWand(NULL, pstImage);
   // 
   //    DrawAnnotation(pstDraw, 0, 0, "Peter Hillen");
   // }

#ifdef COMMENT
//
//  Function:   grf_ImageBufferNormalize
//  Purpose:    Normalize an image: find MAX and recalculate all to MAX
//
//  Parms:      Image, Image size
//  Returns:    New average
//
static int grf_ImageBufferNormalize(char *pcImage, int iImageSize)
{
   int   i, iMin=255, iMax=0;
   int   iPix, iAvg;
   long  lSum=0;
   char *pcImage2 = pcImage;
   
   for(i=0; i<iImageSize; i++)
   {
      iPix  = (int) *pcImage2++;
      // Keep track of min-max
      if(iPix > iMax) iMax = iPix;
      if(iPix < iMin) iMin = iPix;
   }
   //
   // Recalculate all value to normalize(max)
   //
   for(i=0; i<iImageSize; i++)
   {
      //iPix        = ((int)*pcImage * 255)/ iMax;
      //*pcImage++  = (char) iPix;
      //
      iPix        = (int)*pcImage;
      iPix       -= iMin;
      iPix        = (iPix * 255) / (iMax-iMin);
      lSum       += (long) iPix;
      *pcImage++  = (char) iPix;
   }
   iAvg = (int)(lSum/iImageSize);
   PRINTF3("grf_ImageBufferNormalize(): Min=%d, Max=%d, Avg=%d" CRLF, iMin, iMax, iAvg);
   return(iAvg);
}

//
//  Function:   grf_ImageDelta
//  Purpose:    Calculate the delta between two images
//
//  Parms:      Image Dest, Image Src, Image size, threshold
//  Returns:    Delta 0-100.0%   
//
static int grf_ImageDelta(char *pcImgDest, char *pcImgSrc, int iImageSize, int iThreshold)
{
   int   i, iTemp, iDelta=0;
   int   iPixSrc, iPixDest;
   
   for(i=0; i<iImageSize; i++)
   {
      iPixSrc  = (int) *pcImgSrc;
      iPixDest = (int) *pcImgDest;
      iTemp    = abs(iPixSrc - iPixDest);
      if(iTemp > iThreshold)
      {
         //
         // Significant change in grayscale pixel value : count it
         //
         iDelta++;
         *pcImgDest = 255;
         //PRINTF3("grf_ImageDelta(): Delta=%5d (%3d=%3d)" CRLF, iDelta, iPixSrc, iPixDest);
      }
      else
      {
         //
         // Grayscale pixels are almost identical: clear them
         //
         *pcImgDest = 0;
      }
      pcImgSrc++;
      pcImgDest++;
   }
   iTemp = (iDelta*1000)/iImageSize;
   PRINTF4("grf_ImageDelta(): Image=%5d, Delta=%5d, Percentage=%3d,%d%%" CRLF, iImageSize, iDelta, iTemp/10, iTemp%10);
   return(iTemp);
}

//
//  Function:   grf_ImageBufferGrayscale
//  Purpose:    Transfer selected pixels into grayscale
//
//  Parms:      Wand-struct, coordinates
//  Returns:    1=OKee
//
static int grf_ImageBufferGrayscale(char *pcImage, const char *pcFormat, int iWs, int iHs, int iWe, int iHe)
{
   int            i;
   int            iX, iY, iGray, iAverageSum;
   char          *pcSrc, *pcDest; 
   const int      iFormatLength = strlen(pcFormat);

   pcSrc  = pcImage;
   pcDest = pcImage;
   //
   for(iY=iHs; iY<iHe;iY++)
   {
      for(iX=iWs; iX<iWe; iX++)
      {
         iGray       = 0;
         iAverageSum = 0;
         //
         for(i=0; i<iFormatLength; i++)
         {
            switch(pcFormat[i])
            {
               case 'R':
                  iGray       += (213 * (*pcSrc++));
                  iAverageSum += 213;
                  break;

               case 'G':
                  iGray       += (715 * (*pcSrc++));
                  iAverageSum += 715;
                  break;

               case 'B':
                  iGray       += (72 * (*pcSrc++));
                  iAverageSum += 72;
                  break;

               default:
               case 'I':
                  iGray       += (1000 * (*pcSrc++));
                  iAverageSum += 1000;
                  break;

            }
         }
         //
         // Scale back the average
         //
         iGray /= iAverageSum;
         //
         // Store the result
         //
         for(i=0; i<iFormatLength; i++)
         {
            *pcDest++ = (char) iGray;
         }
         //
         // Display result :
         //   pix, threshold, mode (0=CRLF, 1=value)
         //
         //GRF_DUMP_RGB_LEVELS(iGray, 100, 1);
         GRF_DUMP_RGB_LEVELS(iGray, 0, 1);
      }
      GRF_DUMP_RGB_LEVELS(0,0,0);
   }
   return(1);
}
#endif //COMMENT
