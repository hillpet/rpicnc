/*  (c) Copyright:  2005  CvB, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:     This module contains wrappers around a number of system calls and
**               library functions so that a default error behavior can be defined.
**               by John Goerzen, Linux Programming Bible
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       20 Jul 2005
**
 *  Revisions:
 *    $Log:   $
 * v000:Initial revision.
 * v001:
 * v002:
 *
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <malloc.h>
#include <signal.h>
#include <errno.h>
#include <stdarg.h>

#define __SAFECALLS_C__
#include "echotype.h"
#include "log.h"
#include "safecalls.h"

//
// LOG errors
//
#define SAFECALL_TO_LOGFILE
#ifdef  SAFECALL_TO_LOGFILE
#define HANDLE_ERROR   LOG_Report
#else
#define HANDLE_ERROR   HandleError
#endif  //SAFECALL_TO_LOGFILE


/* The first two are automatically set by HandleError.  The third you can
   set to be the file handle to which error messages are written.  If
   NULL, is taken to be stderr. */

const char *SafeLibErrorLoc;
int         SafeLibErrno     = 0;
FILE       *SafeLibErrorDest = NULL;
int         iSafeMallocs     = 0;

//
//  Function:   safestrdup
//  Purpose:    safe strdup(s)
//              
//  Parms:      string ptr
//  Returns:    
//
char *safestrdup(const char *s)
{
  char *retval;

  retval = strdup(s);
  if (!retval)
  {
    HANDLE_ERROR(0, "SAF", "strdup %s failed", s);
  }
  return retval;
}

//
//  Function:   safestrncpy
//  Purpose:    safe strncpy(d, s, len)
//              
//  Parms:      
//  Returns:    
//
char *safestrncpy(char *dest, const char *src, size_t n)
{
  if (strlen(src) >= n)
  {
    HANDLE_ERROR(0, "SAF", "strncpy: attempt to copy string <%s> to buffer %d bytes long", src, (int) n);
  }
  return strncpy(dest, src, n);
}

//
//  Function:   safestrcat
//  Purpose:    safe strcat(d, s, len)
//              
//  Parms:      
//  Returns:    
//
char *safestrcat(char *dest, const char *src, size_t n)
{
  if ((strlen(src) + strlen(dest)) >= n)
  {
    HANDLE_ERROR(0, "SAF", "strcat: attempt to strcat too big a string");
  }
  return strncat(dest, src, n - 1);
}


//
//  Function:   safekill
//  Purpose:    safe kill(p, s)
//              
//  Parms:      process ID, sig
//  Returns:    
//
int safekill(pid_t pid, int sig)
{
  int retval;

  retval = kill(pid, sig);
  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "kill (pid %d, sig %d) failed", (int) pid, sig);
  }
  return retval;
}


//
//  Function:   safegetenv
//  Purpose:    safe getenv(s)
//              
//  Parms:      
//  Returns:    
//
char *safegetenv(const char *name)
{
  char *retval;

  retval = getenv(name);
  if (!retval)
  {
    HANDLE_ERROR(errno, "SAF", "getenv on %s failed", name);
  }
  return retval;
}

//
//  Function:   safechdir
//  Purpose:    safe chdir(s)
//              
//  Parms:      
//  Returns:    
//
int safechdir(const char *path)
{
  int retval;

  retval = chdir(path);
  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "chdir to %s failed", path);
  }
  return retval;
}

//
//  Function:   safemkdir
//  Purpose:    safe mkdir(s, m)
//              
//  Parms:      
//  Returns:    
//
int safemkdir(const char *path, mode_t mode)
{
  int retval;

  retval = mkdir(path, mode);
  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "mkdir %s failed", path);
  }
  return retval;
}

//
//  Function:   safestat
//  Purpose:    safe stat(s, b)
//              
//  Parms:      
//  Returns:    
//
int safestat(const char *file_name, struct stat *buf)
{
int retval;
  retval = stat(file_name, buf);
  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "Couldn't stat %s", file_name);
  }
  return retval;
}  

//
//  Function:   safeopen
//  Purpose:    safe open(s, f)
//              
//  Parms:      
//  Returns:    
//
int safeopen(const char *pathname, int flags)
{
  int retval;

  if ((retval = open(pathname, flags)) == -1) 
  {
    HANDLE_ERROR(errno, "SAF", "open %s failed", pathname);
  }
  return retval;
}

//
//  Function:   safeopen2
//  Purpose:    safe open(s, f, m)
//              
//  Parms:      
//  Returns:    
//
int safeopen2(const char *pathname, int flags, mode_t mode)
{
  int retval;

  retval = open(pathname, flags, mode);
  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "open2 %s failed", pathname);
  }
  return retval;
}

//
//  Function:   safepipe
//  Purpose:    safe pipe(d)
//              
//  Parms:      
//  Returns:    
//
int safepipe(int filedes[2])
{
  int retval;

  retval = pipe(filedes);
  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "pipe failed");
  }
  return retval;
}

//
//  Function:   safedup2
//  Purpose:    safe dup2(o, n)
//              
//  Parms:      
//  Returns:    
//
int safedup2(int oldfd, int newfd)
{
  int retval;

  retval = dup2(oldfd, newfd);
  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "dup2 failed"); 
  }
  return retval;
}

//
//  Function:   safeexecvp
//  Purpose:    safe execvp(s, s)
//              
//  Parms:      
//  Returns:    
//
int safeexecvp(const char *file, char *const argv[])
{
  int retval;

  retval = execvp(file, argv);
  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "execvp %s failed", file);
  }
  return retval;
}

//
//  Function:   saferead
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
int saferead(int fd, void *buf, size_t count)
{
  int retval;

  retval = read(fd, buf, count);
  if (retval == -1)
  {
     HANDLE_ERROR(errno, "SAF", "read %d bytes from fd %d failed", (int) count, fd);
  }
  return retval;
}

//
//  Function:   safewrite
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
int safewrite(int fd, const char *buf, size_t count)
{
  int retval;

  retval = write(fd, buf, count);
  if (retval == -1)
  {
     HANDLE_ERROR(errno, "SAF", "write %d bytes to fd %d failed", (int) count, fd);
  }
  return retval;
}

//
//  Function:   safeclose
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
int safeclose(int fd)
{
  int retval;

  retval = close(fd);

  if (fd == -1) 
  {
    HANDLE_ERROR(errno, "SAF", "Possible serious problem: close failed");
  }
  return retval;
}

//
//  Function:   safefopen
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
FILE *safefopen(char *path, char *mode)
{
  FILE *retval;

  retval = fopen(path, mode);
  if (!retval)
  {
    HANDLE_ERROR(errno, "SAF", "fopen %s failed", path);
  }
  return retval;
}

//
//  Function:   safefread
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
size_t safefread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
  size_t retval;

  retval = fread(ptr, size, nmemb, stream);
  if (ferror(stream))
  {
    HANDLE_ERROR(errno, "SAF", "fread failed");
  }
  return retval;
}

//
//  Function:   safefgets
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
char *safefgets(char *s, int size, FILE *stream) 
{
  char *retval;

  retval = fgets(s, size, stream);
  if (!retval) 
  {
    HANDLE_ERROR(errno, "SAF", "fgets failed");
  }
  return retval;
}

//
//  Function:   safefwrite
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
size_t safefwrite(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
  size_t retval;

  retval = fread(ptr, size, nmemb, stream);
  if (ferror(stream))
  {
    HANDLE_ERROR(errno, "SAF", "fwrite failed");
  }
  return retval;
}

//
//  Function:   safefclose
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
int safefclose(FILE *stream)
{
  int retval;

  retval = fclose(stream);
  if (retval != 0)
  {
    HANDLE_ERROR(errno, "SAF", "Possibly serious error: fclose failed");
  }
  return retval;
}

//
//  Function:   safefflush
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
int safefflush(FILE *stream)
{
  int retval;

  retval = fflush(stream);
  if (retval != 0)
  {
    HANDLE_ERROR(errno, "SAF", "fflush failed");
  }
  return retval;
}

//
//  Function:   safemalloccount
//  Purpose:    return the malloc/free sequences
//              
//  Parms:      void
//  Returns:    malloc/free balance
//
int safemalloccount(void)
{
  return(iSafeMallocs);
}

//
//  Function:   safemalloc
//  Purpose:    Safely allocate memory, keeping track of the malloc/free sequences
//              
//  Parms:      
//  Returns:    
//
void *safemalloc(size_t size)
{
  void *pvData;

  pvData = malloc(size);
  if (!pvData)
  {
    HANDLE_ERROR(ENOMEM, "SAF", "malloc failed");
  }
  else
  {
     //
     // Count the allocation and reset the data
     //
     iSafeMallocs++;
     bzero(pvData, size);
  }
  return(pvData);
}

//
//  Function:   saferemalloc
//  Purpose:    Re-malloc memory, copy data over in new buffer
//              
//  Parms:      old buffer, new size
//  Returns:    new buffer
//
void *saferemalloc(void *pvData, size_t size)
{
  void *pvNewData;

  pvNewData = realloc(pvData, size);
  if (!pvNewData)
  {
    HANDLE_ERROR(ENOMEM, "SAF", "realloc failed");
  }
  else
  {
     //
     // If remalloc() is actually a malloc(): count the allocation
     //pwjh130.02 if(pvData = NULL) iSafeMallocs++;
     //pwjh140.01 remalloc does not need to count the allocation !
     //pwjh140.01 if(pvData != pvNewData) iSafeMallocs++;
  }
  return(pvNewData);
}

//
//  Function:   safefree
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
void safefree(void *pvData)
{
  if(pvData == NULL)
  {
    HANDLE_ERROR(0, "SAF", "Free NULL pointer !");
  }
  else 
  {
     iSafeMallocs--;
     free(pvData);
  }
}


//
//  Function:   safefork
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
pid_t safefork(void)
{
  int retval;

  retval = fork();

  if (retval == -1)
  {
    HANDLE_ERROR(errno, "SAF", "fork failed");
  }
  return retval;
}

//
//  Function:   HandleError
//  Purpose:    Handle the error exit of these functions
//              
//  Parms:      
//  Returns:    
//
void HandleError(int ecode, const char *const caller, const char *fmt, ...) 
{

  va_list           fmtargs;
  struct sigaction  sastruct;
  FILE *of = (SafeLibErrorDest) ? SafeLibErrorDest : stderr;

  /* Safe these into global variables for any possible signal handler. */

  SafeLibErrorLoc = caller;
  SafeLibErrno    = ecode;

  /* Print the error message(s) */

  va_start(fmtargs, fmt);

  fprintf(of, "*** Error in %s: ", caller);
  vfprintf(of, fmt, fmtargs);
  va_end(fmtargs);
  fprintf(of, "\n");
  if (ecode) 
  {
    fprintf(of, "*** Error cause: %s\n", strerror(ecode));
  }

  /* Exit if no signal handler.  Otherwise, raise a signal. */

  sigaction(SIGTERM, NULL, &sastruct);
  if (sastruct.sa_handler != SIG_DFL) 
  {
    raise(SIGINT);
  } 
}
