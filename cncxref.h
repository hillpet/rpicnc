/*  (c) Copyright:  2016 Patrn ESS, Confidential Data
**
**  $Workfile:          cncxref.h
**  $Revision:  
**  $Modtime:   
**
**  Purpose:            CNC variables xref list
**
**
**
**
**
 *  Compiler/Assembler: Raspbian
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       21 May 2016
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// CNC Xref list
//
//          ___Enum____   _____________ iFunction _______________    iPrec pcJson,           pcHttp   ____ iValueOffset _____       ______ iStorageOffset ______           iStorageSize      iChangedOffset
EXTRACT_CNC(CNC_VAR_SEQU, JSN_INT|CNC_INT|CNC_DGM|CNC_PRM|CNC____,   0,    "CncSeq",         "cncs=", offsetof(RPICNC, iSeq),       -1,                                    MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CNCD, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC____,   0,    "CncDel",         "cncd=", offsetof(RPICNC, iCncDelay),  offsetof(CNCMAP, G_pcCncDelay),        MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CNCP, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC____,   0,    "CncPul",         "cncp=", offsetof(RPICNC, iCncPulse),  offsetof(CNCMAP, G_pcCncPulse),        MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_MODE, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC_ALL,   0,    "Mode",           "mode=", offsetof(RPICNC, iDrawMode),  offsetof(CNCMAP, G_pcDrawMode),        MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CURX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "CurX",           "xc=",   offsetof(RPICNC, fXc),        offsetof(CNCMAP, G_pcCurPos[0]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CURY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "CurY",           "yc=",   offsetof(RPICNC, fYc),        offsetof(CNCMAP, G_pcCurPos[1]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CURZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "CurZ",           "zc=",   offsetof(RPICNC, fZc),        offsetof(CNCMAP, G_pcCurPos[2]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_NEWX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "NewX",           "xn=",   offsetof(RPICNC, fXn),        offsetof(CNCMAP, G_pcNewPos[0]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_NEWY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "NewY",           "yn=",   offsetof(RPICNC, fYn),        offsetof(CNCMAP, G_pcNewPos[1]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_NEWZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   2,    "NewZ",           "zn=",   offsetof(RPICNC, fZn),        offsetof(CNCMAP, G_pcNewPos[2]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_STAX, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosXs",          "xs=",   offsetof(RPICNC, fXs),        offsetof(CNCMAP, G_pcStaPos[0]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_STAY, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosYs",          "ys=",   offsetof(RPICNC, fYs),        offsetof(CNCMAP, G_pcStaPos[1]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_STAZ, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosZs",          "zs=",   offsetof(RPICNC, fZs),        offsetof(CNCMAP, G_pcStaPos[2]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_ENDX, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosXe",          "xe=",   offsetof(RPICNC, fXe),        offsetof(CNCMAP, G_pcEndPos[0]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_ENDY, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosYe",          "ye=",   offsetof(RPICNC, fYe),        offsetof(CNCMAP, G_pcEndPos[1]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_ENDZ, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   2,    "PosZe",          "ze=",   offsetof(RPICNC, fZe),        offsetof(CNCMAP, G_pcEndPos[2]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CANW, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   1,    "CanvasW",        "canw=", offsetof(RPICNC, fCanvW),     offsetof(CNCMAP, G_pcCanvasWidth),     MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CANH, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   1,    "CanvasH",        "canh=", offsetof(RPICNC, fCanvH),     offsetof(CNCMAP, G_pcCanvasHeight),    MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DRAW, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   1,    "DrawingW",       "drww=", offsetof(RPICNC, fDrawW),     offsetof(CNCMAP, G_pcDrawWidth),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DRAH, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   1,    "DrawingH",       "drwh=", offsetof(RPICNC, fDrawH),     offsetof(CNCMAP, G_pcDrawHeight),      MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_RADS, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   5,    "Radius",         "rad=",  offsetof(RPICNC, fRad),       offsetof(CNCMAP, G_pcRadius),          MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DISP, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   0,    "StRev",          "srev=", offsetof(RPICNC, fSrev),      offsetof(CNCMAP, G_pcStepRevs),        MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_MOVS, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC____,   0,    "MoveSt",         "movs=", offsetof(RPICNC, iMoveSteps), offsetof(CNCMAP, G_pcMoveSteps),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DRWS, JSN_TXT|CNC_INT|CNC____|CNC_PRM|CNC____,   0,    "DrawSt",         "drws=", offsetof(RPICNC, iDrawSteps), offsetof(CNCMAP, G_pcDrawSteps),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
//
EXTRACT_CNC(CNC_VAR_CURL, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "CurL",           "curl=", offsetof(RPICNC, lCurPos[0]), -1,                                    0,                ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CURR, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "CurR",           "curr=", offsetof(RPICNC, lCurPos[1]), -1,                                    0,                ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_CURP, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "CurP",           "curp=", offsetof(RPICNC, lCurPos[2]), -1,                                    0,                ALWAYS_CHANGED                              )
//
EXTRACT_CNC(CNC_VAR_NEWL, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "NewL",           "newl=", offsetof(RPICNC, lNewPos[0]), -1,                                    0,                ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_NEWR, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "NewR",           "newr=", offsetof(RPICNC, lNewPos[1]), -1,                                    0,                ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_NEWP, JSN_TXT|CNC_LNG|CNC_DGM|CNC_PRM|CNC____,   0,    "NewP",           "newp=", offsetof(RPICNC, lNewPos[2]), -1,                                    0,                ALWAYS_CHANGED                              )
//
EXTRACT_CNC(CNC_VAR_ACCX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   0,    "AccX",           "accx=", offsetof(RPICNC, fAccSet[0]), offsetof(CNCMAP, G_pcAccSet[0]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_ACCY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   0,    "AccY",           "accy=", offsetof(RPICNC, fAccSet[1]), offsetof(CNCMAP, G_pcAccSet[1]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_ACCZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   0,    "AccZ",           "accz=", offsetof(RPICNC, fAccSet[2]), offsetof(CNCMAP, G_pcAccSet[2]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_MINX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MinX",           "minx=", offsetof(RPICNC, fSpdMin[0]), offsetof(CNCMAP, G_pcSpdMin[0]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_MINY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MinY",           "miny=", offsetof(RPICNC, fSpdMin[1]), offsetof(CNCMAP, G_pcSpdMin[1]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_MINZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MinZ",           "minz=", offsetof(RPICNC, fSpdMin[2]), offsetof(CNCMAP, G_pcSpdMin[2]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_MAXX, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MaxX",           "maxx=", offsetof(RPICNC, fSpdMax[0]), offsetof(CNCMAP, G_pcSpdMax[0]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_MAXY, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MaxY",           "maxy=", offsetof(RPICNC, fSpdMax[1]), offsetof(CNCMAP, G_pcSpdMax[1]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_MAXZ, JSN_TXT|CNC_FLT|CNC_DGM|CNC_PRM|CNC____,   1,    "MaxZ",           "maxz=", offsetof(RPICNC, fSpdMax[2]), offsetof(CNCMAP, G_pcSpdMax[2]),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
//
EXTRACT_CNC(CNC_VAR_PICW, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   0,    "PixW",           "pw=",   -1,                           offsetof(CNCMAP, G_pcPixWidth),        MAX_PARM_LEN,     offsetof(CNCMAP, G_ubWidthChanged)          )
EXTRACT_CNC(CNC_VAR_PICH, JSN_TXT|CNC_FLT|CNC____|CNC_PRM|CNC____,   0,    "PixH",           "ph=",   -1,                           offsetof(CNCMAP, G_pcPixHeight),       MAX_PARM_LEN,     offsetof(CNCMAP, G_ubHeightChanged)         )
EXTRACT_CNC(CNC_VAR_PICX, JSN_TXT|CNC_TXT|CNC____|CNC_PRM|CNC____,   0,    "PixX",           "pe=",   -1,                           offsetof(CNCMAP, G_pcPixFileExt),      MAX_PARM_LEN,     offsetof(CNCMAP, G_ubPixFileExtChanged)     )
// Misc
EXTRACT_CNC(CNC_VAR_NUMF, JSN_INT|CNC_INT|CNC_PIX|CNC_PRM|CNC____,   0,    "NumFiles",       "dir=",  -1,                           offsetof(CNCMAP, G_pcNumFiles),        MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_FLTR, JSN_TXT|CNC_TXT|CNC_PIX|CNC_PRM|CNC____,   0,    "Filter",         "ftr=",  -1,                           offsetof(CNCMAP, G_pcFilter),          MAX_PATH_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_STAT, JSN_TXT|CNC_TXT|CNC____|CNC_PRM|CNC_ALL,   0,    "Status",         "",      -1,                           offsetof(CNCMAP, G_pcStatus),          MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DELT, JSN_TXT|CNC_TXT|CNC____|CNC_PRM|CNC____,   0,    "Delete",         "del=",  -1,                           offsetof(CNCMAP, G_pcDelete),          MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_VERS, JSN_TXT|CNC_TXT|CNC____|CNC_PRM|CNC____,   0,    "Version",        "vrs=",  -1,                           offsetof(CNCMAP, G_pcVersionSw),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
// Pictures
EXTRACT_CNC(CNC_VAR_DPIX, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionPix",      "mdp=",  -1,                           offsetof(CNCMAP, G_pcDetectPixel),     MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DLEV, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionLev",      "mdl=",  -1,                           offsetof(CNCMAP, G_pcDetectLevel),     MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DSEQ, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionSeq",      "mds=",  -1,                           offsetof(CNCMAP, G_pcDetectSeq),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DRED, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionRed",      "mdr=",  -1,                           offsetof(CNCMAP, G_pcDetectRed),       MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DGRN, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionGrn",      "mdg=",  -1,                           offsetof(CNCMAP, G_pcDetectGreen),     MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DBLU, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionBlu",      "mdb=",  -1,                           offsetof(CNCMAP, G_pcDetectBlue),      MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DZMX, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionX",        "mzx=",  -1,                           offsetof(CNCMAP, G_pcDetectZoomX),     MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DZMY, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionY",        "mzy=",  -1,                           offsetof(CNCMAP, G_pcDetectZoomY),     MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DZMW, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionW",        "mzw=",  -1,                           offsetof(CNCMAP, G_pcDetectZoomW),     MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DZMH, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionH",        "mzh=",  -1,                           offsetof(CNCMAP, G_pcDetectZoomH),     MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DAV1, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionAvg1",     "ma1=",  -1,                           offsetof(CNCMAP, G_pcDetectAvg1),      MAX_PARM_LEN,     ALWAYS_CHANGED                              )
EXTRACT_CNC(CNC_VAR_DAV2, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "MotionAvg2",     "ma2=",  -1,                           offsetof(CNCMAP, G_pcDetectAvg2),      MAX_PARM_LEN,     ALWAYS_CHANGED                              )
// The output file
EXTRACT_CNC(CNC_VAR_OUTF, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "OutputFile",     "o=",    -1,                           offsetof(CNCMAP, G_pcOutputFile),      MAX_PATH_LEN,     offsetof(CNCMAP, G_ubOutputFileChanged)     )
EXTRACT_CNC(CNC_VAR_OUTL, JSN_TXT|CNC_TXT|CNC____|CNC____|CNC____,   0,    "LastFile",       "",      -1,                           offsetof(CNCMAP, G_pcLastFile),        MAX_PATH_LEN,     NEVER_CHANGED                               )
EXTRACT_CNC(CNC_VAR_SIZE, JSN_INT|CNC_LNG|CNC____|CNC____|CNC____,   0,    "LastFileSize",   "",      -1,                           offsetof(CNCMAP, G_pcLastFileSize),    MAX_PARM_LEN,     NEVER_CHANGED                               )
// Ports
EXTRACT_CNC(CNC_VAR_UDPP, JSN_TXT|CNC_TXT|CNC____|CNC_PRM|CNC____,   0,    "UdpPort",        "",      -1,                           offsetof(CNCMAP, G_pcUdpPort),         MAX_PARM_LEN,     NEVER_CHANGED                               )
