/*  (c) Copyright:  2016 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            rpicnc handles communication between RPi and a HID USB device (Like Teensy)
**
**  Entry Points:       main()
**
**
**
**
**
**
**
**
**
**
**
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       1 Apr 2016
**
 *  Revisions:
 *
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <resolv.h>
#include <semaphore.h>

#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "config.h"
#include "echotype.h"
#include "safecalls.h"
#include "rtc.h"
#include "log.h"
#include "misc_api.h"
#include "eos_api.h"
#include "globals.h"
#include "rpihttp.h"
#include "cncwrite.h"
#include "cncread.h"
#include "cncdebug.h"
#include "rpicnc.h"
#include "cncparms.h"
#include "rpiusb.h"
#include "rpiudp.h"

#define USE_PRINTF
#include "printf.h"


#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

static pid_t   rpi_Deamon              (void);
static pid_t   rpi_CncWriteDeamon      (RPIUSB *);
static pid_t   rpi_CncDebug            (RPIUSB *);
static void    rpi_Help                (void);
static void    rpi_Version             (void);
//
static bool    rpi_SignalRegister      (sigset_t *);
static void    rpi_ReceiveSignalUser1  (int);
static void    rpi_ReceiveSignalUser2  (int);
static void    rpi_ReceiveSignalInt    (int);
static void    rpi_ReceiveSignalTerm   (int);
//
static void    rpi_InitSemaphore       (void);
static void    rpi_WaitSemaphore       (void);
static void    rpi_PostSemaphore       (void);
//
volatile static bool bCncRunning = TRUE;
//
static bool    bRpiDeamonMode    = FALSE;
static sem_t   tSemRoot;

/* ======   Local Functions separator ===========================================
___Main_Functions(){}
==============================================================================*/

//
//  Function:   main
//  Purpose:    Main entry point for the Raspberry Pi monitor
//
//  Parms:      Commandline options
//  Returns:    Exit codes
//
int main(int argc, char **argv)
{
   int         iOpt, iCc=254;
   sigset_t    tBlockset;
   RPIUSB     *pstHid=NULL;
   CNCMAP     *pstMap=GLOBAL_Init();

   if(rpi_SignalRegister(&tBlockset) == FALSE) exit(iCc);
   //
   if( LOG_ReportOpenFile(RPI_PUBLIC_LOG_FILE) == FALSE)
   {
      LOG_Report(errno, "CNC", "main():Log file open error");
      ES_printf("LDG: !----------------------------!" CRLF);
      ES_printf("LDG: !! Using STDERR as log-file !!" CRLF);
      ES_printf("LDG: !----------------------------!" CRLF);
   }
   else
   {
      ES_printf("main():Logfile is %s" CRLF, RPI_PUBLIC_LOG_FILE);
   }
   //
   while ((iOpt = getopt(argc, argv, "DHPThpv")) != -1)
   {
      switch (iOpt)
      {
         case 'D':
            PRINTF("main():Option -D" CRLF);
            ES_printf("Starting rpicnc server as deamon" CRLF);
            stderr = fopen(RPI_ERROR_FILE, "a+");
            stdout = fopen(RPI_TRACE_FILE, "a+");
            LOG_Report(0, "CNC", "main():Option -D: stderr rerouted to %s", RPI_ERROR_FILE);
            LOG_Report(0, "CNC", "main():Option -D: stdout rerouted to %s", RPI_TRACE_FILE);
            rpi_Deamon();
            //
            // Deamon mode:
            //    the HOST has exited already and we are now SRVR and running in the background
            //
            break;

         case 'H':
            PRINTF("main():Option -H" CRLF);
            CNC_UpdateDrawMode(DRW_HOME);  
            break;

         case 'P':
            PRINTF("main():Option -P" CRLF);
            CNC_UpdateDrawMode(DRW_CROSSPLOT);  
            break;

         case 'T':
            PRINTF("main():Option -T" CRLF);
            CNC_UpdateDrawMode(DRW_SQUARE);
            break;

         case 'h':
            PRINTF("main():Option -h"CRLF);
            rpi_Help();
            bCncRunning = FALSE;
            break;
   
         case 'p':
            PRINTF1("main():Option -p=%s" CRLF, optarg);
            strcpy(pstMap->G_pcTcpPort, optarg);
            LOG_Report(0, "RPI", "main():Option -p:ServerPort=%s", pstMap->G_pcTcpPort);
            break;

         case 'v':
            PRINTF("main():Option -v"CRLF);
            rpi_Version() ;
            bCncRunning = FALSE;
            break;
   
         default:
            PRINTF("main(): Invalid option."CRLF);
            rpi_Help();
            bCncRunning = FALSE;
            break;
      }
   }
   //
   rpi_InitSemaphore();
   //
   // Start the deamons
   //
   if(bCncRunning)
   {
      CNC_InitParms();
      //
      pstMap->G_tSrvrPid   = HTTP_Deamon();
      pstMap->G_tUdpPid    = UDP_Deamon();
      //
      pstHid               = USB_GetInfo(0, 0);
      pstMap->G_tCncWrPid  = rpi_CncWriteDeamon(pstHid);
      //
      pstMap->G_tCncDbPid  = rpi_CncDebug(pstHid);
      //
      PRINTF ("main():Threads running:" CRLF);
      PRINTF1("main():  Host             pid=%d" CRLF, pstMap->G_tHostPid);
      PRINTF1("main():  Root      deamon pid=%d" CRLF, pstMap->G_tRootPid);
      PRINTF1("main():  Http      deamon pid=%d" CRLF, pstMap->G_tSrvrPid);
      PRINTF1("main():  Cnc Write deamon pid=%d" CRLF, pstMap->G_tCncWrPid);
      PRINTF1("main():  Debug     deamon pid=%d" CRLF, pstMap->G_tCncDbPid);
      PRINTF1("main():  UDP       deamon pid=%d" CRLF, pstMap->G_tUdpPid);
      //
      LOG_Report(0, "CNC", "main():Host            pid=%d", pstMap->G_tHostPid);
      LOG_Report(0, "CNC", "main():Root     deamon pid=%d", pstMap->G_tRootPid);
      LOG_Report(0, "CNC", "main():Http     deamon pid=%d", pstMap->G_tSrvrPid);
      LOG_Report(0, "CNC", "main():Cnc Writ deamon pid=%d", pstMap->G_tCncWrPid);
      LOG_Report(0, "CNC", "main():Debug    deamon pid=%d", pstMap->G_tCncDbPid);
      LOG_Report(0, "CNC", "main():UDP      deamon pid=%d", pstMap->G_tUdpPid);
      //
      sleep(2);
      //
      // Kickstart default pattern, if any
      //
      if(pstMap->G_tCncWrPid) kill(pstMap->G_tCncWrPid,  SIGUSR1);
   }
   //
   //  Main loop: use SIGINT to exit
   //  Active CNC using current draw sequence. This needs to come from the HTTP client
   //  at a later stage
   //
   //
   while(bCncRunning)
   {
      //
      // Wait till sem tells us to exit
      //
      rpi_WaitSemaphore();
   }
   //
   // Friendly ask threads to exit
   //
   if(pstMap->G_tSrvrPid)  kill(pstMap->G_tSrvrPid,  SIGINT);
   if(pstMap->G_tUdpPid)   kill(pstMap->G_tUdpPid,   SIGINT);
   if(pstMap->G_tCncWrPid) kill(pstMap->G_tCncWrPid, SIGINT);
   if(pstMap->G_tCncDbPid) kill(pstMap->G_tCncDbPid, SIGINT);
   if(pstMap->G_tUdpPid)   kill(pstMap->G_tUdpPid,   SIGINT);
   //
   // Wait to let threads cleanup
   //
   sleep(2);
   //
   // Cleanup host
   //
   USB_ReleaseInfo(pstHid);
   LOG_Report(0, "CNC", "main():RpiCnc-v%s-%s now EXIT", VERSION, BUILD);
   LOG_ReportCloseFile();
   //
   GLOBAL_Sync();
   GLOBAL_Close();
   //
   ES_printf("main():exit(%d)" CRLF, 0);
   exit(iCc);
}

/* ======   Local Functions separator ===========================================
___Local_Functions(){}
==============================================================================*/

//
//  Function:   rpi_Deamon
//  Purpose:    Run the Raspberry Pi CNC server as a deamon, rather than the shelled version
//
//                Foreground threads:                    Pid:
//                =================================================
//                HOST: Main thread, exits here          G_tHostPid
//
//                Background threads:                    Pid:
//                =================================================
//                SRVR: runs as HTTP server              G_tSrvrPid
//                CNC:  runs USB CNC write comms         G_tCncWrPid
//                DBG:  runs USB Debug comms             G_tCncDbPid
//                UDP:  runs UDP                         G_tUdpPid
//  Parms:
//  Returns:
//
static pid_t rpi_Deamon(void)
{
   pid_t    tPid;
   CNCMAP  *pstMap=GLOBAL_GetMapping();

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or start-up service) 
         pstMap->G_tHostPid = 0;
         exit(0);
         break;

      case 0:
         // Root deamon
         pstMap->G_tRootPid = getpid();
         PRINTF1("rpi-Deamon(): RpiCnc running background mode, Root=%d" CRLF, pstMap->G_tRootPid);
         //
         bRpiDeamonMode = TRUE;
         break;

      case -1:
         // Error
         ES_printf("rpi-Deamon(): Error!"CRLF);
         LOG_Report(errno, "CNC", "rpi-Deamon(): Error");
         exit(255);
         break;
   }
   return(tPid);
}

//
//  Function:  rpi_CncWriteDeamon
//  Purpose:   Run the CNC USB write deamon
//
//  Parms:     HID struct
//  Returns:   The deamon PID for the parent only
//
static pid_t rpi_CncWriteDeamon(RPIUSB *pstHid)
{
   int      iCc;
   pid_t    tPid;
   CNCMAP  *pstMap;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent returns with the comms deamon PID
         PRINTF1("rpi_CncWriteDeamon(): USB write deamon pid=%d" CRLF, tPid);
         break;

      case 0:
         // USB comms Deamon
         PRINTF("rpi_CncWriteDeamon(): USB write Deamon running" CRLF);
         iCc = CNC_WriteDeamon(pstHid);
         //
         // Signal host to exit
         //
         pstMap = GLOBAL_GetMapping();
         pstMap->G_tCncWrPid = 0;
         PRINTF("rpi_CncWriteDeamon(): USB write Deamon EXIT" CRLF);
         exit(iCc);
         break;

      case -1:
         // Error
         ES_printf("rpi_CncWriteDeamon(): USB write Deamon Error!"CRLF);
         exit(255);
         break;
   }
   return(tPid);
}

//
//  Function:  rpi_CncDebug
//  Purpose:   Run the CNC USB debug deamon
//
//  Parms:     HID struct
//  Returns:   The deamon PID for the parent only
//
static pid_t rpi_CncDebug(RPIUSB *pstHid)
{
   pid_t    tPid=0;
#ifdef FEATURE_USB_DEBUG_TRACES
   int      iCc;
   CNCMAP  *pstMap;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent returns with the deamon PID
         PRINTF1("rpi_CncDebug(): USB Debug deamon pid=%d" CRLF, tPid);
         break;

      case 0:
         // USB debug Deamon
         PRINTF("rpi_CncDebug(): USB Debug Deamon running" CRLF);
         iCc = CNC_Debug(pstHid);
         pstMap = GLOBAL_GetMapping();
         pstMap->G_tCncDbPid = 0;
         PRINTF("rpi_CncDebug(): USB Debug Deamon EXIT" CRLF);
         exit(iCc);
         break;

      case -1:
         // Error
         ES_printf("rpi_CncDebug(): USB Debug Deamon Error!"CRLF);
         exit(255);
         break;
   }
#endif //FEATURE_USB_DEBUG_TRACES
   return(tPid);
}

//
//  Function:   rpi_Version
//  Purpose:    Show the version string
//
//  Parms:
//  Returns:
//
static void rpi_Version(void)
{
  char  cLog[DATE_TIME_SIZE];

  RTC_GetDateTimeSecs(cLog);

  ES_printf(CRLF);
  ES_printf("Raspberry Pi: USB Hid comms RpiCnc-v%s-%s" CRLF, VERSION, BUILD);
  ES_printf("   Time  : %s" CRLF, cLog);
  ES_printf("   Built : %s" CRLF, RPI_BUILT);
  ES_printf("   Home  : http://www.patrn.nl" CRLF);
  ES_printf(CRLF);

  fflush(stdout);
}

//
//  Function:   rpi_Help
//  Purpose:
//
//  Parms:
//  Returns:
//
static void rpi_Help(void)
{
   //
   ES_printf("Usage : rpicnc -[D H P T p h v] " CRLF);
   ES_printf(CRLF);
   ES_printf("Available options:" CRLF);
   ES_printf("  -D\t\tRun rpicnc as background daemon" CRLF);
   ES_printf("  -H\t\tRun to HOME position" CRLF);
   ES_printf("  -P\t\tRun standard pattern" CRLF);
   ES_printf("  -T\t\tRun test pattern" CRLF);
   ES_printf("  -h\t\tThis help" CRLF);
   ES_printf("  -v\t\tShow version number:" CRLF);
   rpi_Version();
   ES_printf(CRLF);

   /*==================================================================
      ES_printf("Variable sizes:" CRLF);
      ES_printf("  signed char   : %d" CRLF, sizeof(signed char));
      ES_printf("  unsigned char : %d" CRLF, sizeof(unsigned char));
      ES_printf("  pointer       : %d" CRLF, sizeof(int *));
      ES_printf("  int           : %d" CRLF, sizeof(int));
      ES_printf("  signed short  : %d" CRLF, sizeof(signed short));
      ES_printf("  unsigned short: %d" CRLF, sizeof(unsigned short));
      ES_printf("  signed long   : %d" CRLF, sizeof(signed long));
      ES_printf("  unsigned long : %d" CRLF, sizeof(unsigned long));
      ES_printf("  long long     : %d" CRLF, sizeof(signed long long));
      ES_printf("  float         : %d" CRLF, sizeof(float));
      ES_printf("  double        : %d" CRLF, sizeof(double));
      ES_printf(CRLF);
   ====================================================================*/
}

//
//  Function:   rpi_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rpi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // Local signals :
  if( signal(SIGUSR1, &rpi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "rpi_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  if( signal(SIGUSR2, &rpi_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "rpi_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &rpi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "rpi_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &rpi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "CNC", "rpi_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGTERM);
  }
  return(fCC);
}

//
//  Function:   rpi_ReceiveSignalUser1
//  Purpose:    SIGUSR1 signal
//
//  Parms:
//  Returns:    
//
static void rpi_ReceiveSignalUser1(int iSignal)
{
   LOG_Report(0, "CNC", "rpi_ReceiveSignalUser1(): No action");
}

//
//  Function:   rpi_ReceiveSignalUser2
//  Purpose:    SIGUSR2 signal
//
//  Parms:
//  Returns:    
//
static void rpi_ReceiveSignalUser2(int iSignal)
{
   LOG_Report(0, "CNC", "rpi_ReceiveSignalUser2(): No action");
}

//
//  Function:   rpi_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalInt(int iSignal)
{
   PRINTF("rpi_ReceiveSignalInt()" CRLF);
   LOG_Report(0, "CNC", "rpi_ReceiveSignalInt(): Exit all");
   //
   bCncRunning = FALSE;
   rpi_PostSemaphore();
}

//
//  Function:   rpi_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalTerm(int iSignal)
{
   PRINTF("rpi_ReceiveSignalTerm()" CRLF);
   LOG_Report(0, "CNC", "rpi_ReceiveSignalTerm(): No action");
}

/*------  Local functions separator ------------------------------------
__Semaphore_Functions_(){};
----------------------------------------------------------------------------*/

//
//  Function:  rpi_InitSemaphore
//  Purpose:   Init the USB Semaphore
//
//  Parms:      
//  Returns:    
//  Note:      The sem value is always >= 0:
//                               0 means LOCKED
//                              >0 means UNLOCKED
//             Init(sem 1, VAL)  VAL=1 (unlocked)
//             Wait(sem)         LOCK:    if VAL>0:   VAL--
//                                        if(VAL==0) Wait
//             Post(sem)         UNLOCK:  VAL++
//
static void rpi_InitSemaphore(void)
{
   //
   // Init the semaphore as UNLOCKED
   //
   if(sem_init(&tSemRoot, 1, 1) == -1)
   {
      LOG_Report(errno, "CNC", "rpi_InitSemaphore(): sem_init Error");
   }
}

//
//  Function:   rpi_WaitSemaphore
//  Purpose:    Wait till USB COMM Semaphore has been unlocked, then LOCK and proceed
//
//  Parms:      
//  Returns:    
//
static void rpi_WaitSemaphore(void)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   while( (sem_wait(&tSemRoot) == -1) && errno == EINTR)
   {
      continue;
   }
}

//
//  Function:   rpi_PostSemaphore
//  Purpose:    Unlock the USB comm Semaphore
//
//  Parms:      
//  Returns:    
//
static void rpi_PostSemaphore(void)
{
   int      iCc;

   //
   // sem++ (UNLOCK)   
   //
   iCc = sem_post(&tSemRoot);
   if(iCc == -1) LOG_Report(errno, "CNC", "rpi_PostSemaphore(): sem_post error");
}

