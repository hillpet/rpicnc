/*  (c) Copyright:  2012...2016  Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            rpi dynamic HTTP server header file
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
**/

#ifndef _RPIHTTP_H_
#define _RPIHTTP_H_

#define  RPI_CONNECTION_TIMEOUT_MSECS     2000
#define  RPI_PROTOCOL                     "tcp"

pid_t HTTP_Deamon             (void);

#endif  /*_RPIHTTP_H_ */

