/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:         misc_api.h 
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            
**                      
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100: Initial revision.
 *
 **/

#ifndef _MISC_API_H_
#define _MISC_API_H_

typedef enum _delimiters_
{
    DELIM_NOSPACE = 0,          // All but space
    DELIM_SPACE,                // ' '
    DELIM_COLON,                // ':'
    DELIM_NUMERIC,              // '0'..'9'
    DELIM_COMMA,                // ','
    DELIM_PERIOD,               // '.'
    DELIM_ASTERIX,              // '*'
    DELIM_EQU,                  // '='
    DELIM_CR,                   // '\r'
    DELIM_LF,                   // '\n'
    DELIM_TAB,                  // '\t'
    DELIM_NEXTFIELD,            // 'aap     noot" skip to next space separated field
    DELIM_STATIC,               // '@'      static file marker

    NUM_DELIMS
}   DELIM;


char    *MISC_CopyToDelimiter       (DELIM, char *, char *, int);
char    *MISC_FindDelimiter         (char *, DELIM);
int      MISC_RemoveChar            (char *, char);
void     MISC_GetPublicKey          (u_int8 *, int);
void     MISC_DumpData              (char *, int, u_int8 *, int);
char    *MISC_TrimAll               (char *);
char    *MISC_TrimLeft              (char *);
char    *MISC_TrimRight             (char *);
//
int      MISC_AsciiToInt            (char *);
long     MISC_AsciiToLong           (char *);
float    MISC_AsciiToFloat          (char *);
//
u_int8   ubyte_get                  (u_int8 *, u_int8, u_int8);
u_int32  ulong_get                  (u_int8 *, u_int32, u_int8);
u_int8  *ulong_put                  (u_int8 *, u_int32);
u_int32  ulong_to_BCD               (u_int32);
u_int16  ushort_get                 (u_int8 *, u_int16, u_int8);
u_int8  *ushort_put                 (u_int8 *, u_int16);
u_int16  ushort_to_BCD              (u_int16);
void     ushort_update              (u_int8 *, u_int16);



#endif /* _MISC_API_H_ */
