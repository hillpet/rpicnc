/*  (c) Copyright:  2016 Patrn ESS, Confidential Data
**
**  $Workfile:          cncstates.h
**  $Revision:  
**  $Modtime:   
**
**  Purpose:            Simple CNC STH states
**
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       3 Jun 2016
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// CNC states
//
//          Current-State        Next-State              Error-State          State-Exec              Helper-Text
EXTRACT_STH(STH_CNC_CIRCLE1,     STH_CNC_CIRCLE2,        STH_CNC_ERROR,       cnc_SthCircle1,         "STH:Circle1"           )
EXTRACT_STH(STH_CNC_CIRCLE2,     STH_CNC_HOME,           STH_CNC_ERROR,       cnc_SthCircle2,         "STH:Circle2"           )
EXTRACT_STH(STH_CNC_ERROR,       STH_CNC_ERROR,          STH_CNC_ERROR,       cnc_SthError,           "STH:Error !!"          )
EXTRACT_STH(STH_CNC_GOTO_MM,     STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthGotoMm,          "STH:Goto mm"           )
EXTRACT_STH(STH_CNC_GOTO_STEPS,  STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthGotoSteps,       "STH:Goto steps"        )
EXTRACT_STH(STH_CNC_HOME,        STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthHome,            "STH:Home"              )
EXTRACT_STH(STH_CNC_PAT1_START,  STH_CNC_PAT1,           STH_CNC_ERROR,       cnc_SthPattern1Start,   "STH:Pat1 Start"        )
EXTRACT_STH(STH_CNC_PAT1,        STH_CNC_PAT2_START,     STH_CNC_ERROR,       cnc_SthPattern1,        "STH:Pat1"              )
EXTRACT_STH(STH_CNC_PAT2_START,  STH_CNC_PAT2,           STH_CNC_ERROR,       cnc_SthPattern2Start,   "STH:Pat2 Start"        )
EXTRACT_STH(STH_CNC_PAT2,        STH_CNC_PAT3_START,     STH_CNC_ERROR,       cnc_SthPattern2,        "STH:Pat2"              )
EXTRACT_STH(STH_CNC_PAT3_START,  STH_CNC_PAT3,           STH_CNC_ERROR,       cnc_SthPattern3Start,   "STH:Pat3 Start"        )
EXTRACT_STH(STH_CNC_PAT3,        STH_CNC_PAT4_START,     STH_CNC_ERROR,       cnc_SthPattern3,        "STH:Pat3"              )
EXTRACT_STH(STH_CNC_PAT4_START,  STH_CNC_PAT4,           STH_CNC_ERROR,       cnc_SthPattern4Start,   "STH:Pat4 Start"        )
EXTRACT_STH(STH_CNC_PAT4,        STH_CNC_PAT_END,        STH_CNC_ERROR,       cnc_SthPattern4,        "STH:Pat4"              )
EXTRACT_STH(STH_CNC_PAT_END,     STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthPatternEnd,      "STH:Pat End"           )
EXTRACT_STH(STH_CNC_READY,       -1,                     STH_CNC_ERROR,       cnc_SthReady,           "STH:Ready"             )
EXTRACT_STH(STH_CNC_SETTING1,    STH_CNC_SETTING2,       STH_CNC_ERROR,       cnc_SthSetting1,        "STH:Setting1"          )
EXTRACT_STH(STH_CNC_SETTING2,    STH_CNC_SETTING3,       STH_CNC_ERROR,       cnc_SthSetting2,        "STH:Setting2"          )
EXTRACT_STH(STH_CNC_SETTING3,    STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthSetting3,        "STH:Setting3"          )
EXTRACT_STH(STH_CNC_SQUARE,      STH_CNC_READY,          STH_CNC_ERROR,       cnc_SthPatternSquare,   "STH:Square"            )
