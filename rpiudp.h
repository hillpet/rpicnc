/*  (c) Copyright:  2016  Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            rpi udp header file
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       25 May 2016
**
 *  Revisions:
 *    $Log:   $
 *
**/

#ifndef _RPIUDP_H_
#define _RPIUDP_H_

#define  UDP_PROTOCOL            "udp"
#define  MAX_DATAGRAM_SIZE       1500

pid_t UDP_Deamon                 (void);

#endif  /*_RPIUDP_H_ */

