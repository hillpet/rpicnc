/*  (c) Copyright:  2013..2016 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            File/directory functions
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       2 Jun 2013
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
 *
**/

#ifndef _FILEINFO_H_
#define _FILEINFO_H_

typedef enum FINFO
{
   FI_NONE = 0,
   FI_FILEDIR,
   FI_SIZE,
   FI_MDATE,
   FI_UID,
   FI_GUI
}  FINFO;

//
// Global prototypes
//
void    *FINFO_GetDirEntries        (char *, char *, int *);
void     FINFO_ReleaseDirEntries    (void *, int);
bool     FINFO_GetFilename          (void *, int, char *, int);
bool     FINFO_GetFileInfo          (char *, FINFO, char *, int);
 
#endif  /*_FILEINFO_H_ */

