/*  (c) Copyright:  2016 Patrn ESS, Confidential Data
**
**  $Workfile:          sth.c
**  $Revision:  
**  $Modtime:   
**
**  Purpose:            Simple State handler
**
**
**
**
**
 *  Compiler/Assembler: Raspbian
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       3 Jun 2016
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "echotype.h"
#include "log.h"
#include "sth.h"

#define USE_PRINTF
#include "printf.h"

/*------ functions separator ------------------------------------------------
____Global_Functions()  {};
----------------------------------------------------------------------------*/


//
//  Function:  STH_Init
//  Purpose:   Statehandler init
//
//  Parms:     State handler top struct
//  Returns:   Init state
//
int STH_Init(const STH *pstTop)
{
   return(pstTop->tCurState);
}

//
//  Function:  STH_Execute
//  Purpose:   Statehandler exec
//
//  Parms:     State handler struct, state, parameter
//  Returns:   New state
//
int STH_Execute(const STH *pstTop, int iState, void *pvParm)
{
   const STH *pstSth = &pstTop[iState];

   if(iState != pstSth->tNxtState) PRINTF1("STH_Execute():[%s]" CRLF, pstSth->pcHelper);
   //
   return( pstSth->pfHandler(pstSth, iState, pvParm) );
}
