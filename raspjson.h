/*  (c) Copyright:  2013...2016  Patrn, Confidential Data
 *
 *  $Workfile:          raspjson.h
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Header file for HTTP web server helper files
 *
 *
 *  Entry Points:       
 *
 *
 *
 *
 *
 *  Compiler/Assembler: 
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       26 May 2013
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RASPJSON_H_
#define _RASPJSON_H_

#include "netfiles.h"

#define  JSON_DEFAULT_OBJECT_SIZE   1000

typedef struct RPIJSON
{
   char    *pcObject;
   int      iIdx;
   int      iSize;
   int      iFree;
   int      iLastComma;
}  RPIJSON;

//
// Define JSON Element type
//
#define  JE_NONE        0x0000         // No specials
#define  JE_CURLB       0x0001         // { for multi-element begin
#define  JE_CURLE       0x0002         // }                   end
#define  JE_COMMA       0x0004         // , for multi=element Separator
#define  JE_COLN        0x0008         // : for multi=element Separator
#define  JE_TEXT        0x0010         // "Text"
#define  JE_SQRB        0x0020         // [ for multi-element begin
#define  JE_SQRE        0x0040         // ]                   end
#define  JE_CRLF        0x0080         // CRLF

//
// Global prototypes
//
bool     JSON_RespondObject         (NETCL *, RPIJSON *);
RPIJSON *JSON_TerminateObject       (RPIJSON *);
RPIJSON *JSON_InsertParameter       (RPIJSON *, const char *, char *, int);
RPIJSON *JSON_InsertValue           (RPIJSON *, char *, int);
RPIJSON *JSON_TerminateArray        (RPIJSON *);
int      JSON_GetObjectSize         (RPIJSON *);
void     JSON_ReleaseObject         (RPIJSON *);

#endif   //_RASPJSON_H_
