/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Various echotype defines
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
 *
**/

#ifndef ECHOTYPE_H
#define ECHOTYPE_H
#include "config.h"

#ifndef TYPEDEF_INT8
   typedef signed char     int8;
   #define TYPEDEF_INT8
#endif

#ifndef TYPEDEF_U_INT8
   typedef unsigned char   u_int8;
   #define TYPEDEF_U_INT8
#endif

#ifndef TYPEDEF_BOOL
    typedef int bool;
   #define TYPEDEF_BOOL
#endif

#ifndef TYPEDEF_INT16
   typedef signed short    int16;
   #define TYPEDEF_INT16
#endif

#ifndef TYPEDEF_U_INT16
   typedef unsigned short  u_int16;
   #define TYPEDEF_U_INT16
#endif

#ifndef TYPEDEF_INT32
   typedef signed long     int32;
   #define TYPEDEF_INT32
#endif

#ifndef TYPEDEF_U_INT32
   typedef unsigned long   u_int32;
   #define TYPEDEF_U_INT32
#endif

/*----------------------------------------------------------------------------
*        The following definitions are zero, non-zero based                  *
*---------------------------------------------------------------------------*/
#ifndef  TRUE
 #define FALSE           0
 #define TRUE            (!FALSE)
#endif

/*----------------------------------------------------------------------------
*                 UNIVERSAL "C" DECLARATIONS                                 *
*---------------------------------------------------------------------------*/
#define FOREVER         for (;;)

#ifndef NULL
#define NULL            0
#endif

#endif   //_ECHOTYPE_H
