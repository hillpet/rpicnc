/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:     Library headerfile for:
**                   general networking
**                   sockets, pipes, etc.
**                   unbuffered I/O
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
 *
 *
 **/

#ifndef __NETFILES_H__
#define __NETFILES_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <malloc.h>
#include <errno.h>           /* errno global variable */
#include "safecalls.h"

#ifndef  INADDR_NONE
#define  INADDR_NONE       0xffffffff
#endif

//    struct hostent
//  
//    This data type is used to represent an entry in the hosts database. It has the following members:
//  
//    char   *h_name         This is the �official� name of the host.
//    char  **h_aliases      These are alternative names for the host, represented as a null-terminated vector of strings.
//    int     h_addrtype     This is the host address type; in practice, its value is always either AF_INET or AF_INET6, 
//                           with the latter being used for IPv6 hosts. In principle other kinds of addresses could be 
//                           represented in the database as well as Internet addresses; if this were done, you might 
//                           find a value in this field other than AF_INET or AF_INET6. See Socket Addresses.
//    int     h_length       This is the length, in bytes, of each address.
//    char  **h_addr_list    This is the vector of addresses for the host. (Recall that the host might be connected to 
//                           multiple networks and have different addresses on each one.) The vector is terminated by a 
//                           null pointer.
//    char   *h_addr         This is a synonym for h_addr_list[0]; in other words, it is the first host address. 
//
typedef struct hostent      *LPHOSTENT;
typedef const char          *LPCSTR;

//  #include <netinet/in.h>
//  
//   struct sockaddr_in 
//   {
//      short            sin_family;   // e.g. AF_INET
//      unsigned short   sin_port;     // e.g. htons(3490)
//      struct in_addr   sin_addr;     // see struct in_addr, below
//      char             sin_zero[8];  // zero this if you want to
//   };
//  
//   struct in_addr 
//   {
//      unsigned long s_addr;  // load with inet_aton()
//   };
//
typedef struct sockaddr       SOCKADDR;
typedef struct sockaddr_in    SOCKADDR_IN;
typedef struct sockaddr_in6   SOCKADDR_IN6;

#ifndef COPY_BUFSIZE
#define COPY_BUFSIZE          10*1024           // Buffer size for copies
#endif

#define  NUM_CONNECTIONS      5
#define  READ_BUFFER_SIZE     1024
#define  WRITE_BUFFER_SIZE    1024
#define  WRITE_BUFFER_MAX     (WRITE_BUFFER_SIZE-10)
//
#define  NET_DUMP_ASCII    0x0001
#define  NET_DUMP_HEX      0x0002

typedef struct NETCL
{
   int      iReqState;                          // State handler state
   bool     fReqEnd;                            // End of request 
   int      iReqHdr;                            // Index to request header
   int      iReqLen;                            // Request length
   int      iSocket;                            // Session Socket
   char    *pcRcvBuffer;                        // RCV data buffer
   int      iRcvBufferGet;                      //     get index
   int      iRcvBufferPut;                      //     put index
   char    *pcUrl;                              // GET URL buffer
   int      iPathIdx;                           //     pathname  index
   int      iParmIdx;                           //     parameter index
   int      iFileIdx;                           //     filename index
   int      iExtnIdx;                           //     extension index
   char    *pcXmtBuffer;                        // XMT data buffer
   int      iXmtBufferPut;                      //     Write index
}  NETCL;

/* Connection based API */
typedef struct NETCON
{
   int      iConnections;                       // Number of possible sessions
   int      iSocketConnect;                     // FD for connection socket
   int      iSocketMax;                         // FD highest socket number
   NETCL    stClient[NUM_CONNECTIONS];          // Session client state handler data
   //                                           
   fd_set   tFdRead;                            // FD Set Read
   fd_set   tFdWrite;                           //        Write 
   fd_set   tFdOoB;                             //        Out of Band data
   //
   struct   timeval stTimeout;                  // Time out structure
}  NETCON;


NETCON  *NET_Init                   (void);
int      NET_BuildConnectionList    (NETCON *);
int      NET_WaitForConnection      (NETCON *, int);
int      NET_HandleSessions         (NETCON *);

/* Basic reading and writing */
int      NET_ReadBuffer             (int, char *, int);
int      NET_WriteBuffer            (int, char *, int);
int      NET_SendData               (int, u_int8 *, int);

/* String/delimited reading and writing */

int      NET_WriteString            (int, char *);
int      NET_ReadString             (int, char *, int);
int      NET_ReadDelimString        (int, char *, int, char);

/* Integer reading and writing */

int      NET_ReadUlong              (int, uint32_t *);
int      NET_WriteUlong             (int, uint32_t);

/* Data copy */

int      NET_Copy                   (int, int, int);

/* Reverse DNS lookups and friends */

char    *NET_GetMyFqdn              (char *, int);
char    *NET_GetFqdn                (char *, int);
                                 
/* Network initialization */     
                                 
int      NET_ServerConnect          (NETCON *, const char *, const char *);
void     NET_ServerDisconnect       (NETCL *);
int      NET_ServerAccept           (NETCON *);
int      NET_FlushCache             (NETCL *);
//
int      NET_ClientConnect          (const char *, const char *, const char *);
void     NET_ClientDisconnect       (int);
//                                 
bool     NET_GetIpInfo              (void);

#endif   // __NETFILES_H__
