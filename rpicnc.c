/*  (c) Copyright:  2016  Patrn.nl, Confidential Data
**
**  $Workfile:          rpicnc
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            CNC Interface functions
**                      All functions are being called from the CNC-Deamon (cncwrite.c)
**
**  Entry Points:       CNC_Init()        : Init parms abd steppers
**                      CNC_Execute()     : Runs the STH statehandler
**                      CNC_GetMotion()   : Extract the motion status from the reply
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       1 Apr 2016
**
 *  Revisions:
 *       09 Apr 2016:   //resd: No more use fResD, use fResXd, fResYd
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/select.h>
#include <sys/signal.h>
#include <sys/types.h>

#include "config.h"
#include "echotype.h"
#include "safecalls.h"
#include "misc_api.h"
#include "log.h"
#include "globals.h"
#include "sth.h"
#include "rpicnc.h"
#include "cncparms.h"
#include "rpiusb.h"

#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static bool    cnc_CheckBoundaries        (RPICNC *, int, float *, float *, float *);
static int     cnc_CheckReply             (RPICNC *);
static bool    cnc_DrawAbsoluteMm         (RPICNC *, float, float, float);
static bool    cnc_DrawRelativeMm         (RPICNC *, float, float);
static bool    cnc_End                    (RPICNC *);
static void    cnc_Init                   (RPICNC *);
static bool    cnc_MakeGcommandMm         (RPICNC *, u_int8, float, float, float);
static bool    cnc_MakeGcommandSteps      (RPICNC *, u_int8, float, float, float);
static bool    cnc_MoveAbsoluteMm         (RPICNC *, float, float, float);
static bool    cnc_MoveAbsoluteSteps      (RPICNC *, float, float, float);
static bool    cnc_MoveRelativeMm         (RPICNC *, int, float, float);
static long    cnc_Round                  (float);
static bool    cnc_SetParameters          (RPICNC *);
static bool    cnc_SetHome                (RPICNC *, float, float, float);
static bool    cnc_Stop                   (RPICNC *);
static int     cnc_StorageToValue         (RPICNC *);
static void    cnc_UpdateCurrentPosition  (RPICNC *);
static int     cnc_ValueToStorage         (RPICNC *);
static bool    cnc_WriteSteppers          (RPICNC *);
//
static int     cnc_SthCircle1             (const STH *, int, void *);
static int     cnc_SthCircle2             (const STH *, int, void *);
static int     cnc_SthError               (const STH *, int, void *);
static int     cnc_SthGotoMm              (const STH *, int, void *);
static int     cnc_SthGotoSteps           (const STH *, int, void *);
static int     cnc_SthHome                (const STH *, int, void *);
static int     cnc_SthPattern1Start       (const STH *, int, void *);
static int     cnc_SthPattern1            (const STH *, int, void *);
static int     cnc_SthPattern2Start       (const STH *, int, void *);
static int     cnc_SthPattern2            (const STH *, int, void *);
static int     cnc_SthPattern3Start       (const STH *, int, void *);
static int     cnc_SthPattern3            (const STH *, int, void *);
static int     cnc_SthPattern4Start       (const STH *, int, void *);
static int     cnc_SthPattern4            (const STH *, int, void *);
static int     cnc_SthPatternEnd          (const STH *, int, void *);
static int     cnc_SthPatternSquare       (const STH *, int, void *);
static int     cnc_SthReady               (const STH *, int, void *);
static int     cnc_SthSetting1            (const STH *, int, void *);
static int     cnc_SthSetting2            (const STH *, int, void *);
static int     cnc_SthSetting3            (const STH *, int, void *);

//
// Show CNC parameters
//
#ifdef FEATURE_SHOW_PARAMETERS
 static void cnc_ShowParms             (RPICNC *, const char *);
 #define CNC_SHOW_PARMS(a,b)           cnc_ShowParms(a,b)
#else    //FEATURE_SHOW_PARAMETERS
 #define CNC_SHOW_PARMS(a,b)
#endif   //FEATURE_SHOW_PARAMETERS

extern volatile int iCncRunMode;
//
// Local vars
//
static int iSettingAxis       = 0;
//
static const char *pcOk       = "ok ";
static const char *pcRs       = "rs ";
//
static const char *pcX        = "X";
static const char *pcY        = "Y";
static const char *pcZ        = "Z";
static const char *pcS        = "S";
static const char *pcAllAxis  = "XYZ";
//
static const CNCBND stMovePosBoundary[] = 
            {
               {0.0, 1.0}, {0.0, 0.0}, {-1.0, -1.0}, {1.0, 0.0}
            };
//
static const CNCBND stMoveNegBoundary[] = 
            {
               {-1.0, 0.0}, {1.0, 1.0},  {0.0, 0.0}, {0.0, -1.0}
            };

//
// CNC Store/Vars list
//
#define  EXTRACT_CNC(a,b,c,d,e,f,g,h,i)   a,
static const int iCncParameters[] =
{
   #include "cncxref.h"
};
static const int iNumCncParameters = sizeof(iCncParameters)/sizeof(int);
#undef   EXTRACT_CNC

//
// State handlers
//
#define  EXTRACT_STH(a,b,c,d,e)   {a,b,c,d,e},
static const STH stCncStates[] =
{
   #include "cncstates.h"
   {STH_CNC_ERROR, STH_CNC_ERROR, STH_CNC_ERROR, cnc_SthError, "STH Error state !"  }
};
#undef   EXTRACT_STH


/*------ functions separator ------------------------------------------------
____Global_Functions()  {};
----------------------------------------------------------------------------*/

//
//  Function:  CNC_Init
//  Purpose:   Init the CNC settings and CNC steppers
//             Called from the CNC-Deamon (rpiusb.c)
//
//  Parms:     CNC struct
//  Returns:   
//
void CNC_Init(RPICNC *pstCnc)
{
   int   iErrors;

   STH_Init(stCncStates);
   //
   // Setup all variables, they come from the client GUI.
   //
   if( (iErrors = cnc_StorageToValue(pstCnc)) )
   {
      LOG_Report(0, "CNC", "CNC_Init(): %d errors in storage to vars", iErrors);
   }
   //
   // Take some defaults for the actual drawing :
   // Calculate drawing scale
   //
   cnc_Init(pstCnc);
   //
   // Set the home position of all 3 axis manually in the correct position. 
   // The ZERO point will be the lower left corner of the canvas, the HOME
   // position will by definition be the middle of the X-axis at the bottom 
   // of the Y-axis of the canvas.
   //
   cnc_SetHome(pstCnc, (CNC_CANVAS_W/2.0), 0.0, 0.0);
   cnc_SetParameters(pstCnc);
   //
   if( (iErrors = cnc_ValueToStorage(pstCnc)) )
   {
      GLOBAL_Status(CNC_STATUS_ERROR);
      PRINTF1("CNC_Init(): %d errors in vars to storage" CRLF, iErrors);
      LOG_Report(0, "CNC", "CNC_Init(): %d errors in vars to storage", iErrors);
   }
   else
   {
      GLOBAL_Status(CNC_STATUS_IDLE);
   }
   CNC_SHOW_PARMS(pstCnc, "Updated: Values --> Storage:"); 
}

//
//  Function:  CNC_Execute
//  Purpose:   Run the CNC machine using a simple finite state machine. 
//             Called from the CNC-Deamon (rpiusb.c)
//
//  Parms:     pstCNC, initial Runmode^
//  Returns:   Last known state
//  Note:      The exit state is -1, meaning that the CNC cycle has completed.
//
int CNC_Execute(RPICNC *pstCnc, volatile int *piRunMode)
{
   int      iState, iErrors;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;

   pstCommXmt->lLineNr = 0;
   //
   // Setup all variables, they come from the client GUI
   //
   if( (iErrors = cnc_StorageToValue(pstCnc)) )
   {
      LOG_Report(0, "CNC", "CNC_Execute(): %d errors in storage to vars", iErrors);
   }
   CNC_SHOW_PARMS(pstCnc, "CNC_Execute: Storage --> Value:"); 
   //
   // Setup the initial draw mode (aka pattern):
   //
   iState            = CNC_SetupDrawMode(pstCnc->iDrawMode);
   pstCnc->iDrawMode = DRW_IDLE;
   PRINTF1("CNC_Execute():Run STH Initial state=%d" CRLF, iState);
   //
   while(iState != -1)
   {
      switch(*piRunMode)
      {
         default:
         case CNC_RUNMODE_IDLE:
            iState = -1;
            break;

         case CNC_RUNMODE_EXEC:
            iState = STH_Execute(stCncStates, iState, pstCnc);
            break;

         case CNC_RUNMODE_PARM:
            //
            // New parameters from HTTP server: re-acquire them
            // Calculate drawing scale
            //
            if( (iErrors = cnc_StorageToValue(pstCnc)) ) LOG_Report(0, "CNC", "CNC_Execute(): %d errors in storage to vars", iErrors);
            //
            cnc_Init(pstCnc);
            //
            if( (iErrors = cnc_ValueToStorage(pstCnc)) ) LOG_Report(0, "CNC", "CNC_Execute(): %d errors in vars to storage", iErrors);
            //
            CNC_SHOW_PARMS(pstCnc, "CNC_Execute RunMode Parms: Values --> Storage:"); 
            //
            // And update the CNC
            //
            iState = STH_Execute(stCncStates, iState, pstCnc);
            break;

         case CNC_RUNMODE_STOP:
            //
            // Stop request: stop/pause CNC
            //
            cnc_Stop(pstCnc);
            iState = -1;
            break;

         case CNC_RUNMODE_EXIT:
            iState = -1;
            break;
      }
   }
   cnc_End(pstCnc);
   //
   GLOBAL_Status(CNC_STATUS_IDLE);
   *piRunMode = CNC_RUNMODE_IDLE;
   return(iState);
}

//
//  Function:  CNC_GetMotion
//  Purpose:   Retrieve the motion status from the USB reply buffer
//
//  Parms:     Reply buffer
//  Returns:   Motion of 3 steppers
//                X-Axis = 1
//                Y-Axis = 2
//                Z-Axis = 4
//                or -1 on error
//
int CNC_GetMotion(char *pcReply)
{
   char *pcTemp;
   int   iMotion=-1;

   pcTemp = strstr(pcReply, pcS);
   if(pcTemp)
   {
      pcTemp += strlen(pcS);
      iMotion = MISC_AsciiToInt(pcTemp);
      //pwjh PRINTF1("CNC_GetMotion():Motion=%x" CRLF, iMotion);
   }
   return(iMotion);
}

/*------ functions separator ------------------------------------------------
____Local_cnc_Functions()  {};
----------------------------------------------------------------------------*/

//
//  Function:  cnc_CheckBoundaries
//  Purpose:   Check if the new MoveTo coords are still withing the area
//  Parms:     CNC struct^, Pattern, Xnew^, Ynew^
//  Returns:   TRUE if we are still within the target area
//             FALSE if we had to change some coords to stay within the target area
//
static bool cnc_CheckBoundaries(RPICNC *pstCnc, int iPat, float *pfXnew, float *pfYnew, float *pfZnew)
{
   bool  bCc=TRUE;
   float fXnew=*pfXnew;
   float fYnew=*pfYnew;
   float fZnew=*pfZnew;

   if(fXnew > pstCnc->fXe)
   {
      fXnew  = pstCnc->fXe;
      fYnew += (stMoveNegBoundary[iPat].fYadj * pstCnc->fResYm);
      bCc    = FALSE;
      PRINTF3("cnc_CheckBoundaries(): X>> now %f, %f, %f" CRLF, fXnew, fYnew, fZnew);
   }
   else if(fXnew < pstCnc->fXs)
   {
      fXnew  = pstCnc->fXs;
      fYnew += (stMovePosBoundary[iPat].fYadj * pstCnc->fResYm);
      bCc    = FALSE;
      PRINTF3("cnc_CheckBoundaries(): X<< now %f, %f, %f" CRLF, fXnew, fYnew, fZnew);
   }
   //
   if(fYnew > pstCnc->fYe)
   {
      fXnew += (stMoveNegBoundary[iPat].fXadj * pstCnc->fResXm);
      fYnew  = pstCnc->fYe;
      bCc    = FALSE;
      PRINTF3("cnc_CheckBoundaries(): Y>> now %f, %f, %f" CRLF, fXnew, fYnew, fZnew);
   }
   //else if(fYnew < pstCnc->fYs)
   else if(fYnew < 0.0)
   {
      fXnew += (stMovePosBoundary[iPat].fXadj * pstCnc->fResXm);
      fYnew  = pstCnc->fYs;
      bCc    = FALSE;
      PRINTF3("cnc_CheckBoundaries(): Y<< now %f, %f, %f" CRLF, fXnew, fYnew, fZnew);
   }
   if(bCc == FALSE)
   {
      *pfXnew = fXnew;
      *pfYnew = fYnew;
      *pfZnew = fZnew;
   }
   return(bCc);
}

//
//  Function:  cnc_CheckReply
//  Purpose:   Check the reply from the USB stepper controller
//  Parms:     CNC struct^
//  Returns:   +1: Steppers still moving
//              0: Steppers ready, no movement
//             -1: NACK: resend line
//
static int cnc_CheckReply(RPICNC *pstCnc)
{
   int         iCc=0;
   char       *pcSrc;
   char       *pcTemp;
   CNCMAP     *pstMap=GLOBAL_GetMapping();
   COMMS      *pstCommRcv=&pstMap->G_stUsbRcv;

   //
   // Temp solution might be to copy the goto position and assume we have arrived:
   //       pstCnc->lCurPos[X_AXIS] = pstCnc->lNewPos[X_AXIS];
   //       pstCnc->lCurPos[Y_AXIS] = pstCnc->lNewPos[Y_AXIS];
   //       pstCnc->lCurPos[Z_AXIS] = pstCnc->lNewPos[Z_AXIS];
   //
   // Better:
   // Update current position: needs to taken from the returned data,
   // or at least verify if the steppers managed to arrive there !!!
   //
   // Check reply: 
   //    "ok 1234 S1\n                       : ACK, steppers busy
   //    "ok 1234 X=123 Y=321 z=567 S3\n"    : ACK, steppers ready, Update new position
   //    "rs 1234 S0\n"                      : NACK, resend line
   //
   //    Stepper motion status Sx : 0x01     : Stepper X-Axis
   //                               0x02     : Stepper Y-Axis
   //                               0x04     : Stepper Z-Axis
   //
   pcSrc = (char *)pstCommRcv->pubUsb;
   //
   if(strncmp(pcSrc, pcOk, 3) == 0)
   {
      //
      // ok completion: check new positions
      //
      pcTemp = strstr(pcSrc, pcX);
      if(pcTemp)
      {
         pcTemp += strlen(pcX);
         pstCnc->lCurPos[X_AXIS] = MISC_AsciiToLong(pcTemp);
      }
      pcTemp = strstr(pcSrc, pcY);
      if(pcTemp)
      {
         pcTemp += strlen(pcY);
         pstCnc->lCurPos[Y_AXIS] = MISC_AsciiToLong(pcTemp);
      }
      pcTemp = strstr(pcSrc, pcZ);
      if(pcTemp)
      {
         pcTemp += strlen(pcZ);
         pstCnc->lCurPos[Z_AXIS] = MISC_AsciiToLong(pcTemp);
      }
      pstCnc->iCncMotion = CNC_GetMotion(pcSrc);
      PRINTF1("cnc_CheckReply():Motion=%d" CRLF, pstCnc->iCncMotion);
      if(pstCnc->iCncMotion) iCc = 1;
   }
   else
   {
      if(strncmp(pcSrc, pcRs, 3) == 0)
      {
         // NACK
         iCc = -1;
      }
   }
   return(iCc);
}

//
//  Function:  cnc_DrawAbsoluteMm
//  Purpose:   Draw absolute mm to X and Y and Z
//             The CNC structure contains all relevant data
//  Parms:     CNC struct^, x, y, z
//  Returns:   TRUE if we are still within the target area
//
static bool cnc_DrawAbsoluteMm(RPICNC *pstCnc, float fX, float fY, float fZ)
{
   bool  bCc=TRUE;

   if(fX > pstCnc->fXe)
   {
      PRINTF2("cnc_DrawAbsoluteMm(X!): %f >>> %f" CRLF, fX, pstCnc->fXc);
      fX  = pstCnc->fXe;
      bCc = FALSE;
   }
   else if(fX < pstCnc->fXs)
   {
      PRINTF2("cnc_DrawAbsoluteMm(X!): %f <<< %f" CRLF, fX, pstCnc->fXc);
      fX  = pstCnc->fXs;
      bCc = FALSE;
   }
   //
   if(fY > pstCnc->fYe)
   {
      PRINTF2("cnc_DrawAbsoluteMm(Y!): %f >>> %f" CRLF, fY, pstCnc->fYc);
      fY  = pstCnc->fYe;
      bCc = FALSE;
   }
   else if(fY < 0.0)
   {
      PRINTF2("cnc_DrawAbsoluteMm(Y!): %f <<< %f" CRLF, fY, pstCnc->fYc);
      fY  = pstCnc->fYs;
      bCc = FALSE;
   }
   //
   // Setup new position ! 
   //
   pstCnc->fXn = fX;
   pstCnc->fYn = fY;
   pstCnc->fZn = fZ;
   //
   // Feed steppers to new location
   // ...step and wait here for ACK("ok #### X## Y## Z## S0\n") or NACK("rs\n")
   //
   cnc_MakeGcommandMm(pstCnc, G_CMD_FEED_LIN, fX, fY, fZ);
   CNC_SHOW_PARMS(pstCnc, "Draw Abs To");
   //
   //PRINTF6("cnc_DrawAbsoluteMm(): (%f, %f, %f) --> (%f, %f, %f)", pstCnc->fXc, pstCnc->fYc, pstCnc->fZc, fX, fY, fZ);
   //PRINTF3(" ==> M=%ld, N=%ld, P=%ld" CRLF, pstCnc->lNewPos[X_AXIS], pstCnc->lNewPos[Y_AXIS], pstCnc->lNewPos[Z_AXIS]);
   //
   if(!cnc_WriteSteppers(pstCnc) )
   {
      PRINTF("cnc_DrawAbsoluteMm(): cnc_WriteSteppers ERROR");
   }
   return(bCc);
}

//
//  Function:  cnc_DrawRelativeMm
//  Purpose:   Move X and Y
//             The CNC structure contains all relevant data
//  Parms:     CNC struct^, x-dir, y-dir
//  Returns:   TRUE if we are still within the target area
//
static bool cnc_DrawRelativeMm(RPICNC *pstCnc, float fXdir, float fYdir)
{
   float fX=pstCnc->fXc;
   float fY=pstCnc->fYc;
   float fZ=pstCnc->fZc;

   //
   // From where we are now: use the Res- increments to move into
   // the direction X-dir and Y-dir until we hit a boundary.
   //
   while(iCncRunMode != CNC_RUNMODE_STOP)
   {
      // X:
      fX += (fXdir * pstCnc->fResXd);
      if(fX > pstCnc->fXe) return(FALSE);
      if(fX < pstCnc->fXs) return(FALSE);
      // Y:
      fY += (fYdir * pstCnc->fResYd);
      if(fY > pstCnc->fYe) return(FALSE);
      if(fY < pstCnc->fYs) return(FALSE);
      //
      // Setup new position ! 
      //
      pstCnc->fXn = fX;
      pstCnc->fYn = fY;
      pstCnc->fZn = fZ;
      //
      // Move steppers to new location
      // ...step and wait here for ACK("ok #### X## Y## Z##\n") or NACK("rs\n")
      //
      //CNC_SHOW_PARMS(pstCnc, "Draw Rel To");
      cnc_MakeGcommandMm(pstCnc, G_CMD_FEED_LIN, fX, fY, fZ);
      //
      //PRINTF6("cnc_DrawRelativeMm(): (%f, %f, %f) --> (%f, %f, %f)", pstCnc->fXc, pstCnc->fYc, pstCnc->fZc, fX, fY, fZ);
      //PRINTF3(" ==> M=%ld, N=%ld, P=%ld" CRLF, pstCnc->lNewPos[X_AXIS], pstCnc->lNewPos[Y_AXIS], pstCnc->lNewPos[Z_AXIS]);
      //
      if(!cnc_WriteSteppers(pstCnc) )
      {
         PRINTF("cnc_DrawRelativeMm(): cnc_WriteSteppers ERROR");
      }
   }
   return(TRUE);
}

//
//  Function:  cnc_End
//  Purpose:   End of program
//
//  Parms:     pstCnc
//  Returns:   TRUE if OKee
//
static bool cnc_End(RPICNC *pstCnc)
{
   bool     bCc=TRUE;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;

   PRINTF("cnc_End()" CRLF);
   pstCommXmt->ubCommand = M_CMD_PGM_END;
   pstCommXmt->lLineNr++;
   //
   // Transer values to M-Codes into the xfer buffer
   //
   ES_sprintf((char *)pstCommXmt->pubUsb, "N%ld M%d\n", 
               pstCommXmt->lLineNr, pstCommXmt->ubCommand);
   //
   bCc = cnc_WriteSteppers(pstCnc);
   return(bCc);
}

//
//  Function:  cnc_Init
//  Purpose:   Init the CNC settings
//
//  Parms:     CNC struct
//  Returns:   
//
static void cnc_Init(RPICNC *pstCnc)
{
   //
   // Take some defaults for the actual drawing :
   // Calculate drawing scale
   //
   pstCnc->fXe    = pstCnc->fXs + pstCnc->fDrawW;
   pstCnc->fYe    = pstCnc->fYs + pstCnc->fDrawH;
   //
   pstCnc->fResXd = pstCnc->fXe - pstCnc->fXs;
   pstCnc->fResYd = pstCnc->fYe - pstCnc->fYs;
   //
   pstCnc->fResXm = pstCnc->fResXd / pstCnc->iMoveSteps;
   pstCnc->fResYm = pstCnc->fResYd / pstCnc->iMoveSteps;
   pstCnc->fResZm = 0.0;
   //
   pstCnc->fResXd = pstCnc->fResXd / pstCnc->iDrawSteps;
   pstCnc->fResYd = pstCnc->fResYd / pstCnc->iDrawSteps;
   pstCnc->fResZd = 0.0;
}

//
//  Function:  cnc_MakeGcommandMm
//  Purpose:   Write X, Y and Z to comms structure
//
//  Parms:     CNC struct^, command, x, y, z
//  Returns:   TRUE if moved OKee
//
static bool cnc_MakeGcommandMm(RPICNC *pstCnc, u_int8 ubCmd, float fX, float fY, float fZ)
{
   float    fT2, fRad2;
   float    fM2, fM;
   float    fN2, fN;
   float    fStepsPerMm;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;

   fStepsPerMm = pstCnc->fSrev / (2 * M_PI * pstCnc->fRad);
   //PRINTF3("cnc_MakeGcommandMm():%f steps/rev, Rad=%f mm : %f mm/step" CRLF, pstCnc->fSrev, pstCnc->fRad, fStepsPerMm);
   //
   // Rework coordinates within the drawing to absolute positions
   // of each steppermotor (M and N). Calculate according the drawing 
   // BMP the pen-force (maybe just PEN-UP or PEN-DOWN).
   //
   fRad2 = powf(pstCnc->fRad, 2);
   //
   // Left stepper (M)
   //
   //     M^2 = (H-Y)^2 + X^2           : needs to be corrected for cogwheel radius :
   //     Mc  = sqrt(M^2 - Radius^2)    :
   //
   fM2  = powf((pstCnc->fCanvH - fY), 2);
   fM2 += powf(fX, 2);
   fT2  = fM2 - fRad2;
   fM   = sqrtf(fT2);
   //pwjh PRINTF3("cnc_MakeGcommandMm():Coords(%f, %f): M=%f mm", fX, fY, fM);
   // Convert to steps (displacement is in steps/mm) 
   //  = CNC_STEPS_PER_REV / (2 * M_PI * CNC_COGWHEEL_RADIUS);
   fM  *= fStepsPerMm;
   //pwjh PRINTF1(" = %f steps" CRLF, fM);
   //
   // Right stepper (N)
   //
   // New N^2 = (W-X)^2 + (H-Y)^2       : needs to be corrected for cogwheel radius :
   //     Nc  = sqrt(N^2 - Radius^2)    :
   //
   fN2  = powf((pstCnc->fCanvH - fY), 2);
   fT2  = powf((pstCnc->fCanvW - fX), 2);
   fN2 += fT2;
   fT2  = fN2 - fRad2;
   fN   = sqrtf(fT2);
   //pwjh PRINTF3("cnc_MakeGcommandMm():Coords(%f, %f): N=%f mm", fX, fY, fN);
   // Convert to steps (displacement is in steps/mm) 
   //  = CNC_STEPS_PER_REV / (2 * M_PI * CNC_COGWHEEL_RADIUS);
   fN  *= fStepsPerMm;
   //pwjh PRINTF1(" = %f steps" CRLF, fN);
   //
   // Convert mm to long position
   //
   pstCnc->lNewPos[X_AXIS] = cnc_Round(fM);
   pstCnc->lNewPos[Y_AXIS] = cnc_Round(fN);
   pstCnc->lNewPos[Z_AXIS] = cnc_Round(fZ);
   //
   // Transer values to G-Codes into the xfer buffer
   //
   pstCommXmt->ubCommand = ubCmd;
   pstCommXmt->lLineNr++;
   //
   ES_sprintf((char *)pstCommXmt->pubUsb, "N%ld G%d X%ld Y%ld Z%ld\n", 
               pstCommXmt->lLineNr, ubCmd, pstCnc->lNewPos[X_AXIS], pstCnc->lNewPos[Y_AXIS], pstCnc->lNewPos[Z_AXIS]);
   //
   return(TRUE);
}

//
//  Function:  cnc_MakeGcommandSteps
//  Purpose:   Write X, Y and Z to comms structure
//
//  Parms:     CNC struct^, command, x, y, z
//  Returns:   TRUE if moved OKee
//
static bool cnc_MakeGcommandSteps(RPICNC *pstCnc, u_int8 ubCmd, float fX, float fY, float fZ)
{
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;

   //
   // Convert float steps to long position
   //
   pstCnc->lNewPos[X_AXIS] = cnc_Round(fX);
   pstCnc->lNewPos[Y_AXIS] = cnc_Round(fY);
   pstCnc->lNewPos[Z_AXIS] = cnc_Round(fZ);
   //
   // Transer values to G-Codes into the xfer buffer
   //
   ES_sprintf((char *)pstCommXmt->pubUsb, "N%ld G%d X%ld Y%ld Z%ld\n", 
               pstCommXmt->lLineNr, ubCmd, pstCnc->lNewPos[X_AXIS], pstCnc->lNewPos[Y_AXIS], pstCnc->lNewPos[Z_AXIS]);
   //
   return(TRUE);
}

//
//  Function:  cnc_MoveAbsoluteMm
//  Purpose:   Move absolute mm to X and Y
//             The CNC structure contains all relevant data
//  Parms:     CNC struct^, x, y, z
//  Returns:   TRUE if we are still within the target area
//
static bool cnc_MoveAbsoluteMm(RPICNC *pstCnc, float fX, float fY, float fZ)
{
   bool  bCc=TRUE;

   if(fX > pstCnc->fXe)
   {
      PRINTF2("cnc_MoveAbsoluteMm(X!): %f >>> %f" CRLF, fX, pstCnc->fXc);
      fX  = pstCnc->fXe;
      bCc = FALSE;
   }
   else if(fX < pstCnc->fXs)
   {
      PRINTF2("cnc_MoveAbsoluteMm(X!): %f <<< %f" CRLF, fX, pstCnc->fXc);
      fX  = pstCnc->fXs;
      bCc = FALSE;
   }
   //
   if(fY > pstCnc->fYe)
   {
      PRINTF2("cnc_MoveAbsoluteMm(Y!): %f >>> %f" CRLF, fY, pstCnc->fYc);
      fY  = pstCnc->fYe;
      bCc = FALSE;
   }
   else if(fY < 0.0)
   {
      PRINTF2("cnc_MoveAbsoluteMm(Y!): %f <<< %f" CRLF, fY, pstCnc->fYc);
      fY  = pstCnc->fYs;
      bCc = FALSE;
   }
   //
   // Setup new position ! 
   //
   pstCnc->fXn = fX;
   pstCnc->fYn = fY;
   pstCnc->fZn = fZ;
   //
   // Move steppers to new location
   // ...step and wait here for ACK("ok #### X## Y## Z## S0\n") or NACK("rs\n")
   //
   cnc_MakeGcommandMm(pstCnc, G_CMD_MOVE, fX, fY, fZ);
   CNC_SHOW_PARMS(pstCnc, "Move Abs To");
   //
   //PRINTF6("cnc_MoveAbsoluteMm(): (%f, %f, %f) --> (%f, %f, %f)", pstCnc->fXc, pstCnc->fYc, pstCnc->fZc, fX, fY, fZ);
   //PRINTF3(" ==> M=%ld, N=%ld, P=%ld" CRLF, pstCnc->lNewPos[X_AXIS], pstCnc->lNewPos[Y_AXIS], pstCnc->lNewPos[Z_AXIS]);
   //
   if(!cnc_WriteSteppers(pstCnc) )
   {
      PRINTF("cnc_MoveAbsoluteMm(): cnc_WriteSteppers ERROR");
   }
   return(bCc);
}

//
//  Function:  cnc_MoveAbsoluteSteps
//  Purpose:   Move absolute steps to X and Y
//             The CNC structure contains all relevant data
//  Parms:     CNC struct^, x, y, z
//  Returns:   TRUE if we are still within the target area
//
static bool cnc_MoveAbsoluteSteps(RPICNC *pstCnc, float fX, float fY, float fZ)
{
   bool  bCc=TRUE;

   //
   // Setup new position ! 
   //
   pstCnc->fXn = fX;
   pstCnc->fYn = fY;
   pstCnc->fZn = fZ;
   //
   // Move steppers to new location
   // ...step and wait here for ACK("ok #### X## Y## Z##\n") or NACK("rs\n")
   //
   cnc_MakeGcommandSteps(pstCnc, G_CMD_MOVE, fX, fY, fZ);
   CNC_SHOW_PARMS(pstCnc, "Move Abs Steps");
   //
   //PRINTF6("cnc_MoveAbsoluteSteps(): (%f, %f, %f) --> (%f, %f, %f)", pstCnc->fXc, pstCnc->fYc, pstCnc->fZc, fX, fY, fZ);
   //PRINTF3(" ==> M=%ld, N=%ld, P=%ld" CRLF, pstCnc->lNewPos[X_AXIS], pstCnc->lNewPos[Y_AXIS], pstCnc->lNewPos[Z_AXIS]);
   //
   if(!cnc_WriteSteppers(pstCnc) )
   {
      PRINTF("cnc_MoveAbsoluteSteps(): cnc_WriteSteppers ERROR");
   }
   return(bCc);
}

//
//  Function:  cnc_MoveRelativeMm
//  Purpose:   Move X and Y
//             The CNC structure contains all relevant data
//  Parms:     CNC struct^, pattern, x-dir, y-dir
//  Returns:   TRUE if we are still within the target area
//
static bool cnc_MoveRelativeMm(RPICNC *pstCnc, int iPat, float fXdir, float fYdir)
{
   bool  bCc=TRUE;
   float fX=pstCnc->fXc;
   float fY=pstCnc->fYc;
   float fZ=pstCnc->fZc;

   //
   // From where we are now: use the Res- increments to move into
   // the direction X-dir and Y-dir until we hit a boundary.
   // We need to recalculate the new position if we hit a boundary !!!
   //
   fX += (fXdir * pstCnc->fResXm);
   fY += (fYdir * pstCnc->fResYm);
   //
   cnc_CheckBoundaries(pstCnc, iPat, &fX, &fY, &fZ);
   //
   // Setup new position ! 
   //
   pstCnc->fXn = fX;
   pstCnc->fYn = fY;
   pstCnc->fZn = fZ;
   //
   // Move steppers to new location
   // ...step and wait here for ACK("ok #### X## Y## Z##\n") or NACK("rs\n")
   //
   //CNC_SHOW_PARMS(pstCnc, "Move Rel To");
   cnc_MakeGcommandMm(pstCnc, G_CMD_MOVE, fX, fY, fZ);
   //
   //PRINTF6("cnc_MoveRelativeMm(): (%f, %f, %f) --> (%f, %f, %f)", pstCnc->fXc, pstCnc->fYc, pstCnc->fZc, fX, fY, fZ);
   //PRINTF3(" ==> M=%ld, N=%ld, P=%ld" CRLF, pstCnc->lNewPos[X_AXIS], pstCnc->lNewPos[Y_AXIS], pstCnc->lNewPos[Z_AXIS]);
   //
   if(!cnc_WriteSteppers(pstCnc) )
   {
      PRINTF("cnc_MoveRelativeMm(): cnc_WriteSteppers ERROR");
   }
   return(bCc);
}

//
//  Function:  cnc_Round
//  Purpose:   Round float to long
//
//  Parms:     Float
//  Returns:   Long
//
static long cnc_Round(float fValue)
{
  return (long)(fValue + 0.5);
}

//
//  Function:  cnc_SetParameters
//  Purpose:   Send CNC parameters to steppermotors using GCode
//
//  Parms:     CNC struct^
//  Returns:   TRUE if OKee
//
static bool cnc_SetParameters(RPICNC *pstCnc)
{
   bool     bCc=TRUE;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;

   PRINTF("cnc_SetParameters()" CRLF);
   pstCommXmt->lLineNr++;
   //
   // Transer values to G-Codes into the xfer buffer
   //
   //pwjh ES_sprintf((char *)pstCommXmt->pubUsb, "N%ld O%s.%s G%d P%d\n", 
   //pwjh             pstCommXmt->lLineNr, VERSION, BUILD, G_CMD_EXACT_STOP, pstCnc->iCncDelay);
   ES_sprintf((char *)pstCommXmt->pubUsb, "N%ld G%d P%d\n", 
               pstCommXmt->lLineNr, G_CMD_EXACT_STOP, pstCnc->iCncDelay);
   //
   if(!cnc_WriteSteppers(pstCnc) )
   {
      PRINTF("cnc_SetParameters(): cnc_WriteSteppers ERROR");
   }
   return(bCc);
}

//
//  Function:  cnc_SetHome
//  Purpose:   Set current position as HOME position
//
//  Parms:     CNC struct^, x, y, z
//  Returns:   TRUE if moved OKee
//
static bool cnc_SetHome(RPICNC *pstCnc, float fX, float fY, float fZ)
{
   bool  bCc=TRUE;

   PRINTF("cnc_SetHome()" CRLF);
   //
   // Setup new position ! 
   //
   pstCnc->fXn = fX;
   pstCnc->fYn = fY;
   pstCnc->fZn = fZ;
   //
   // Build Gcode block to tell the CNC this is the known  position
   //
   cnc_MakeGcommandMm(pstCnc, G_CMD_SET_POS, fX, fY, fZ);
   //
   if(!cnc_WriteSteppers(pstCnc) )
   {
      PRINTF("cnc_SetHome(): cnc_WriteSteppers ERROR");
   }
   return(bCc);
}

//
//  Function:  cnc_Stop
//  Purpose:   Controlled stop
//
//  Parms:     pstCnc
//  Returns:   TRUE if OKee
//
static bool cnc_Stop(RPICNC *pstCnc)
{
   bool     bCc=TRUE;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;

   PRINTF("cnc_Stop()" CRLF);
   pstCommXmt->ubCommand = M_CMD_MAN_STOP;
   pstCommXmt->lLineNr++;
   //
   // Transer values to M-Codes into the xfer buffer
   //
   ES_sprintf((char *)pstCommXmt->pubUsb, "N%ld M%d\n", 
               pstCommXmt->lLineNr, pstCommXmt->ubCommand);
   //
   //
   bCc = cnc_WriteSteppers(pstCnc);
   return(bCc);
}

//
//  Function:  cnc_StorageToValue
//  Purpose:   Convert all G_ storage to their variables
//
//  Parms:     pstCnc
//  Returns:   Number of errors in converted vars
//
static int cnc_StorageToValue(RPICNC *pstCnc)
{
   int   iIdx, iErrors=0;
   
   for(iIdx=0; iIdx<iNumCncParameters; iIdx++)
   {
      if( !CNC_StorageToValue(iCncParameters[iIdx], pstCnc) ) iErrors++;
   }
   return(iErrors);
}

//
//  Function:  cnc_UpdateCurrentPosition
//  Purpose:   Update variables and convert all variables back to their G_ storage
//
//  Parms:     pstCnc
//  Returns:   
//
static void cnc_UpdateCurrentPosition(RPICNC *pstCnc)
{
   CNCMAP     *pstMap=GLOBAL_GetMapping();

   pstCnc->fXc = pstCnc->fXn;
   pstCnc->fYc = pstCnc->fYn;
   pstCnc->fZc = pstCnc->fZn;
   //
   // Update global setting which will be send back to theApp
   //
   CNC_ValueToStorage(CNC_VAR_CURX, pstCnc);
   CNC_ValueToStorage(CNC_VAR_CURY, pstCnc);
   CNC_ValueToStorage(CNC_VAR_CURZ, pstCnc);
   //
   // Notify theApp through a UDP datagram containing the JSON object
   //
   // PRINTF6("cnc_UpdateCurrentPosition(): (%s, %s, %s) --> (%s, %s, %s)" CRLF, 
   //          pstMap->G_pcCurPos[0], pstMap->G_pcCurPos[1], pstMap->G_pcCurPos[2], 
   //          pstMap->G_pcNewPos[0], pstMap->G_pcNewPos[1], pstMap->G_pcNewPos[2]);
   //
   if(pstMap->G_tUdpPid) kill(pstMap->G_tUdpPid, SIGUSR1);
}

//
//  Function:  cnc_ValueToStorage
//  Purpose:   Convert all variables back to their G_ storage
//
//  Parms:     pstCnc
//  Returns:   Number of errors in converted vars
//
static int cnc_ValueToStorage(RPICNC *pstCnc)
{
   int   iIdx, iErrors=0;
   
   for(iIdx=0; iIdx<iNumCncParameters; iIdx++)
   {
      if( !CNC_ValueToStorage(iCncParameters[iIdx], pstCnc) ) iErrors++;
   }
   return(iErrors);
}

//
//  Function:  cnc_WriteSteppers
//  Purpose:   Write X, Y and Z to steppers
//             The CNC structure contains all relevant data
//  Parms:     pstCnc
//  Returns:   TRUE if moved OKee
//             FALSE on ERROR
//
static bool cnc_WriteSteppers(RPICNC *pstCnc)
{
   bool     bCc=TRUE;
   bool     bWaitForReply=TRUE;
   bool     bSteppersMoving=TRUE;
   int      iReply, iNr, iRetry=3;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;
   COMMS   *pstCommRcv=&pstMap->G_stUsbRcv;

   USB_DUMP(pstCommXmt, CMD_XFER_SIZE, DUMP_ASCII, "WriteSteppers():Cnc-Xmt");
   // USB_DUMP(pstCommXmt, CMD_XFER_SIZE, DUMP_COMMS, "WriteSteppers():Cnc-Xmt");
   // USB_DUMP(pstCommXmt, CMD_XFER_SIZE, DUMP_HEX, "WriteSteppers():Cnc-Xmt");
   //
   while(bSteppersMoving)
   {
      iNr = USB_Write(pstCnc->pstRawHid, pstCommXmt->pubUsb, CMD_XFER_SIZE);
      if(iNr < 0)
      {
         PRINTF("cnc_WriteSteppers():Rawhid write error" CRLF);
         if(--iRetry == 0)
         {
            bSteppersMoving = FALSE;
            bCc             = FALSE;
         }
         bWaitForReply = FALSE;
      }
      //
      // Wait RawHid reply
      //
      //PRINTF1("cnc_WriteSteppers():Written %d bytes. Wait for reply..." CRLF, iNr);
      //
      while(bWaitForReply )
      {
         iNr = USB_Read(pstCnc->pstRawHid, pstCommRcv->pubUsb, CMD_XFER_SIZE, 100);
         if(iNr < 0)
         {
            PRINTF("cnc_WriteSteppers():Rawhid read error" CRLF);
            // Ignore
            //bWaitForReply = FALSE;
            //bCc           = FALSE;
         }
         else if(iNr > 0)
         {
            USB_DUMP(pstCommRcv, CMD_XFER_SIZE, DUMP_ASCII, "WriteSteppers():CNC-Rcv");
            //
            // Check reply: 
            //    "ok 1234\n"                      : ACK, steppers busy
            //    "ok 1234 X=123 Y=321 z=567\n"    : ACK, steppers ready, Update new position
            //    "rs 1234\n"                      : NACK, resend line
            //
            iReply = cnc_CheckReply(pstCnc);
            switch(iReply)
            {
               case 1:
                  // Steppers still moving
                  PRINTF("cnc_WriteSteppers():Check=Moving" CRLF);
                  break;

               case 0:
                  //
                  // We have arrived in one piece: update current position
                  //
                  PRINTF("cnc_WriteSteppers():Check=Stopped" CRLF);
                  bWaitForReply   = FALSE;
                  bSteppersMoving = FALSE;
                  cnc_UpdateCurrentPosition(pstCnc);
                  break;

               case -1:
                  // NACK respond: resend
                  PRINTF("cnc_WriteSteppers():Check=NACK" CRLF);
                  bWaitForReply = FALSE;
                  break;
            }
         }
         else
         {
            //
            // No data: keep reading or exit
            //
            if( (iCncRunMode == CNC_RUNMODE_STOP) || (iCncRunMode == CNC_RUNMODE_EXIT)) 
            {
               bSteppersMoving = FALSE;
               bWaitForReply   = FALSE;
            }
            //else PRINTF("cnc_WriteSteppers():No read data..." CRLF);
         }
      }
   }
   return(bCc);
}


/*------ functions separator ------------------------------------------------
____Local_usb_Functions()  {};
----------------------------------------------------------------------------*/

#ifdef FEATURE_SHOW_PARAMETERS
//
//  Function:  cnc_ShowParms
//  Purpose:   List all parameters
//
//  Parms:     Parms struct
//  Returns:   
//  Note:      fCanvW              Total canvas width   (mm)
//             fCanvH              Total canvas heigth  (mm)
//             fSrev               Steps per rev
//             fRad                Radius of cogwheel   (mm)
//             fXc, fYc, fZc       Current coords       (mm)
//             fXs, fYs, fZs       Corner lo left       (mm)
//             fXe, fYe, fZe       Corner Hi right      (mm)
//             fResXm              Movement res X       (mm)
//             fResYm              Movement res Y       (mm)
//             fResZm              Movement res Z       (mm)
//             fResXd              Drawing  res X       (mm)
//             fResYd              Drawing  res Y       (mm)
//             fResZd              Drawing  res Z       (mm)
//
static void cnc_ShowParms(RPICNC *pstCnc, const char *pcHdr)
{
   ES_printf("cnc_ShowParms       : %s"                         CRLF, pcHdr);
   ES_printf("     CNC Delay      : %d"                         CRLF, pstCnc->iCncDelay);
   ES_printf("     CNC pattern    : %d"                         CRLF, pstCnc->iDrawMode);
   ES_printf("     Canvas W, H    : %5.1f, %5.1f"               CRLF, pstCnc->fCanvW, pstCnc->fCanvH);
   ES_printf("     Draw   W, H    : %5.1f, %5.1f"               CRLF, pstCnc->fDrawW, pstCnc->fDrawH);
   ES_printf("     Radius         : %5.5f"                      CRLF, pstCnc->fRad);
   ES_printf("     Steps/rev      : %5.0f"                      CRLF, pstCnc->fSrev);
   ES_printf("     Xs, Xc, Xn, Xe : %5.2f, %5.2f, %5.2f, %5.2f" CRLF, pstCnc->fXs, pstCnc->fXc, pstCnc->fXn, pstCnc->fXe);
   ES_printf("     Ys, Yc, Yn, Ye : %5.2f, %5.2f, %5.2f, %5.2f" CRLF, pstCnc->fYs, pstCnc->fYc, pstCnc->fYn, pstCnc->fYe);
   ES_printf("     Zs, Zc, Zn, Ze : %5.2f, %5.2f, %5.2f, %5.2f" CRLF, pstCnc->fZs, pstCnc->fZc, pstCnc->fZn, pstCnc->fZe);
   ES_printf("     Res-Xm, Ym     : %5.2f, %5.2f"               CRLF, pstCnc->fResXm, pstCnc->fResYm);
   ES_printf("     Res-Xd, Yd     : %5.2f, %5.2f"               CRLF, pstCnc->fResXd, pstCnc->fResYd);
   //            
   ES_printf("    Acceleration    : %5.0f, %5.0f, %5.0f"        CRLF, pstCnc->fAccSet[X_AXIS], pstCnc->fAccSet[Y_AXIS], pstCnc->fAccSet[Z_AXIS]);
   ES_printf("       Min speed    : %5.1f, %5.1f, %5.1f"        CRLF, pstCnc->fSpdMin[X_AXIS], pstCnc->fSpdMin[Y_AXIS], pstCnc->fSpdMin[Z_AXIS]);
   ES_printf("       Max speed    : %5.1f, %5.1f, %5.1f"        CRLF, pstCnc->fSpdMax[X_AXIS], pstCnc->fSpdMax[Y_AXIS], pstCnc->fSpdMax[Z_AXIS]);
   ES_printf("Current position    : %4ld, %4ld, %4ld"           CRLF, pstCnc->lCurPos[X_AXIS], pstCnc->lCurPos[Y_AXIS], pstCnc->lCurPos[Z_AXIS]);
   ES_printf(" Target position    : %4ld, %4ld, %4ld"           CRLF, pstCnc->lNewPos[X_AXIS], pstCnc->lNewPos[Y_AXIS], pstCnc->lNewPos[Z_AXIS]);
}
#endif   //FEATURE_SHOW_PARAMETERS

/*------  Local functions separator ------------------------------------
__State_Handler_Functions_(){};
----------------------------------------------------------------------------*/

//
//  Function:  cnc_SthCircle1
//  Purpose:   Handle settings transfer
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      G-Code sequence:
//
static int cnc_SthCircle1(const STH *pstSth, int iState, void *pvParm)
{
   float    fXn, fYn;
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   //
   // Var1 : current degree
   // Var2 : step in degrees
   //
   pstCnc->fVar1 = 0.0;
   pstCnc->fVar2 = 360.0 / pstCnc->iDrawSteps;
   //
   fXn = pstCnc->fXs +  pstCnc->fDrawW;
   fYn = pstCnc->fYs + (pstCnc->fDrawH/2);
   //
   PRINTF4("cnc_SthCircle1(): (%f, %f) - Angle=%f, Steps=%f" CRLF, fXn, fYn, pstCnc->fVar1, pstCnc->fVar2);
   //
   cnc_MoveAbsoluteMm(pstCnc, fXn, fYn, 0.0);
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthCircle2
//  Purpose:   Handle settings transfer
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      G-Code sequence:
//
static int cnc_SthCircle2(const STH *pstSth, int iState, void *pvParm)
{
   float    fXc, fYc;
   float    fXn, fYn;
   float    fAngle;
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   pstCnc->fVar1 += pstCnc->fVar2;
   fAngle = (pstCnc->fVar1 * M_PI) / 180.0;
   //
   fXc = pstCnc->fXs + (pstCnc->fDrawW/2);
   fYc = pstCnc->fYs + (pstCnc->fDrawH/2);
   //
   fXn = fXc + ((pstCnc->fDrawW/2) * cosf(fAngle));
   fYn = fYc + ((pstCnc->fDrawH/2) * sinf(fAngle));
   //
   PRINTF3("cnc_SthCircle2(): (%f, %f) - Angle=%f" CRLF, fXn, fYn, pstCnc->fVar1);
   //
   cnc_DrawAbsoluteMm(pstCnc, fXn, fYn, 0.0);
   //
   if( pstCnc->fVar1 > 360.0)
   {
      iState = pstSth->tNxtState;
   }
   return(iState);
}

//
//  Function:  cnc_SthError
//  Purpose:   Error state (generic)
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthError(const STH *pstSth, int iState, void *pvParm)
{
   GLOBAL_Status(CNC_STATUS_ERROR);
   return(iState);
}

//
//  Function:  cnc_SthHome
//  Purpose:   Move to the HOME position (Xpos=W/2, Ypos=0)
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthHome(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   //
   pstCnc->fXn = pstCnc->fCanvW/2;
   pstCnc->fYn = 0.0;
   pstCnc->fZn = 0.0;
   //
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXn, pstCnc->fYn, pstCnc->fZn);
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthSetting1
//  Purpose:   Handle settings transfer
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      G-Code sequence:
//
//                "N1 G5  X0 P<Pulsewidth><LF>"
//                "N2 G5  Y0 P<Pulsewidth><LF>"
//                "N3 G5  Z0 P<Pulsewidth><LF>"
//                "N4 G21 X0 F<Speed> S<Accel> <LF>"
//                "N5 G21 Y0 F<Speed> S<Accel> <LF>"
//                "N6 G21 Z0 F<Speed> S<Accel> <LF>"
//
static int cnc_SthSetting1(const STH *pstSth, int iState, void *pvParm)
{
   //
   iSettingAxis = X_AXIS;
   iState       = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthSetting2
//  Purpose:   Handle Pulsewidth settings transfer
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      G-Code sequence for settings:
//
//                "N1 G5  X0 P<Pulsewidth><LF>"
//                "N2 G5  Y0 P<Pulsewidth><LF>"
//                "N3 G5  Z0 P<Pulsewidth><LF>"
//                "N4 ....
//
static int cnc_SthSetting2(const STH *pstSth, int iState, void *pvParm)
{
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   //
   // Transer values to G-Codes into the xfer buffer
   //
   pstCommXmt->lLineNr++;
   ES_sprintf((char *)pstCommXmt->pubUsb, "N%ld G5 %c0 P%d \n", 
               pstCommXmt->lLineNr, pcAllAxis[iSettingAxis], pstCnc->iCncPulse);
   //
   if(!cnc_WriteSteppers(pstCnc) )
   {
      PRINTF("cnc_SthSetting2(): cnc_WriteSteppers ERROR");
   }
   //
   if(++iSettingAxis >= NUM_AXIS)  iState = pstSth->tNxtState;
   //
   return(iState);
}

//
//  Function:  cnc_SthSetting3
//  Purpose:   Handle Speed/Accel settings transfer
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      G-Code sequence for settings:
//
//                "N3 ...
//                "N4 G21 X0 F<Speed> S<Accel> <LF>"
//                "N5 G21 Y0 F<Speed> S<Accel> <LF>"
//                "N6 G21 Z0 F<Speed> S<Accel> <LF>"
//
static int cnc_SthSetting3(const STH *pstSth, int iState, void *pvParm)
{
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   COMMS   *pstCommXmt=&pstMap->G_stUsbXmt;
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   //
   // Transer values to G-Codes into the xfer buffer
   //
   pstCommXmt->lLineNr++;
   ES_sprintf((char *)pstCommXmt->pubUsb, "N%ld G21 %c0 S%f F%f \n", 
               pstCommXmt->lLineNr, pcAllAxis[iSettingAxis], pstCnc->fSpdMax[iSettingAxis], pstCnc->fAccSet[iSettingAxis]);
   //
   if(!cnc_WriteSteppers(pstCnc) )
   {
      PRINTF("cnc_SthSetting2(): cnc_WriteSteppers ERROR");
   }
   //
   if(++iSettingAxis >= NUM_AXIS)  iState = pstSth->tNxtState;
   //
   return(iState);
}

//
//  Function:  cnc_SthGotoMm
//  Purpose:   Move to the GOTO mm position fXn, fYn
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthGotoMm(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   //
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXn, pstCnc->fYn, pstCnc->fZn);
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthGotoSteps
//  Purpose:   Move to the GOTO steps position fXn, fYn
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthGotoSteps(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   //
   cnc_MoveAbsoluteSteps(pstCnc, pstCnc->fXn, pstCnc->fYn, pstCnc->fZn);
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthPattern1Start
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthPattern1Start(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   //
   // Move to the starting point of the drawing: 
   //    o Draw from lower right corner diagonally up-left
   //
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXe, pstCnc->fYs, pstCnc->fZs);
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthPattern1
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthPattern1(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   //
   // Handle Pattern-1 drawing
   // Start at the lower right corner and zig-zag towards the
   // upper left corner.
   //
   cnc_DrawRelativeMm(pstCnc, -1.0, -1.0);      // zig
   cnc_MoveRelativeMm(pstCnc, CNC_PAT_ROLB, -1.0,  0.0);
   //
   cnc_DrawRelativeMm(pstCnc,  1.0,  1.0);      // zag
   cnc_MoveRelativeMm(pstCnc, CNC_PAT_ROLB,  0.0,  1.0);
   //
   // Keep drifting towards the upper left corner until we are there
   //
   if((pstCnc->fXc <= pstCnc->fXs)
         &&
      (pstCnc->fYc >= pstCnc->fYe) )
   {
      // Done
      PRINTF("CNC-STH():Upper left corner reached" CRLF);
      iState = pstSth->tNxtState;
   }
   return(iState);
}

//
//  Function:  cnc_SthPattern2Start
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthPattern2Start(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   //
   // Move to the starting point of the drawing: 
   //    o Draw from lower left  corner diagonally up-right
   //
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXs, pstCnc->fYs, pstCnc->fZs);
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthPattern2
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthPattern2(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   //
   // Handle Pattern-2 drawing
   // Start at the lower left corner and zig-zag towards the
   // upper right corner.
   //
   cnc_DrawRelativeMm(pstCnc, -1.0,  1.0);      // zig
   cnc_MoveRelativeMm(pstCnc, CNC_PAT_LORB,  0.0,  1.0);
   //
   cnc_DrawRelativeMm(pstCnc,  1.0, -1.0);      // zag
   cnc_MoveRelativeMm(pstCnc, CNC_PAT_LORB,  1.0,  0.0);
   //
   // Keep drifting towards the upper left corner until we are there
   //
   if((pstCnc->fXc >= pstCnc->fXs)
         &&
      (pstCnc->fYc >= pstCnc->fYe) )
   {
      // Done
      PRINTF("CNC-STH():Upper right corner reached" CRLF);
      iState = pstSth->tNxtState;
   }
   return(iState);
}

//
//  Function:  cnc_SthPattern3Start
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthPattern3Start(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   //
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fCanvW/2, 0.0, 0.0);
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthPattern3
//  Purpose:   
//
//  Parms:      
//  Returns:   New state 
//
static int cnc_SthPattern3(const STH *pstSth, int iState, void *pvParm)
{
   //
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthPattern4Start
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthPattern4Start(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   //
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fCanvW/2, 0.0, 0.0);
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthPattern4
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthPattern4(const STH *pstSth, int iState, void *pvParm)
{
   //
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthPatternEnd
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthPatternEnd(const STH *pstSth, int iState, void *pvParm)
{
   //
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthPatternSquare
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//  Note:      Draw a line accross the borders of the current drawing
//
static int cnc_SthPatternSquare(const STH *pstSth, int iState, void *pvParm)
{
   RPICNC  *pstCnc=(RPICNC *)pvParm;

   GLOBAL_Status(CNC_STATUS_RUN);
   // Move to lower right corner
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXe-5.0,  pstCnc->fYs+5.0, 0);
   // Draw square inside border
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXe-5.0,  pstCnc->fYe-5.0, 0);
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXs+5.0,  pstCnc->fYe-5.0, 0);
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXs+5.0,  pstCnc->fYs+5.0, 0);
   cnc_MoveAbsoluteMm(pstCnc, pstCnc->fXe-5.0,  pstCnc->fYs+5.0, 0);
   //
   sleep(2);
   //
   cnc_DrawAbsoluteMm(pstCnc, pstCnc->fXe-5.0,  pstCnc->fYe-5.0, 0);
   cnc_DrawAbsoluteMm(pstCnc, pstCnc->fXs+5.0,  pstCnc->fYe-5.0, 0);
   cnc_DrawAbsoluteMm(pstCnc, pstCnc->fXs+5.0,  pstCnc->fYs+5.0, 0);
   cnc_DrawAbsoluteMm(pstCnc, pstCnc->fXe-5.0,  pstCnc->fYs+5.0, 0);
   // Home
   cnc_MoveAbsoluteMm(pstCnc, (pstCnc->fCanvW/2.0), 0.0, 0.0);
   //
   iState = pstSth->tNxtState;
   return(iState);
}

//
//  Function:  cnc_SthReady
//  Purpose:   
//
//  Parms:     State^, parms^ 
//  Returns:   New state 
//
static int cnc_SthReady(const STH *pstSth, int iState, void *pvParm)
{
   if(iState != pstSth->tNxtState) 
   {
      GLOBAL_Status(CNC_STATUS_STOPPING);
   }
   return(-1);
}


