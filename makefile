#################################################################################
#
# makefile:
#	rpicnc - Raspberry Pi HTTP server for the Teensy cnc controller
#
#	Copyright (c) 2016 Peter Hillen
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),1)
#
# RPi#1 (Rev-1): Black box with PiFace CAD LCD
#
	BOARD_OK=yes	
	DEFS=-D FEATURE_IO_PIFACECAD
	INCMAGICK=/usr/local/include/GraphicsMagick
	LIBMAGICK=-lrt -lpng12 -ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -ljasper	-lX11 -lXext -lxml2 -llcms -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBLIRC=-llirc_client 
endif

ifeq ($(BOARD),3)
#
# RPi#3 (Rev-1): PiFace Digital
#
	BOARD_OK=yes	
	DEFS=-D FEATURE_IO_PIFACE
	INCMAGICK=/usr/include/GraphicsMagick
	LIBMAGICK=-lrt -lpng12 -ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -ljasper	-lX11 -lXext -lxml2 -llcms -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBLIRC=
endif

ifeq ($(BOARD),6)
#
# RPi#6 (Rev-1): Rpi TFT Camera
#
	BOARD_OK=yes	
	DEFS=-D FEATURE_IO_TFTBUTS
	INCMAGICK=/usr/local/include/GraphicsMagick
	LIBMAGICK=-lrt -lpng12 -ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -ljasper	-lX11 -lXext -lxml2 -llcms -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBLIRC=
endif

ifeq ($(BOARD),7)
#
# RPi#7 (Rev-2): PopKodi and Apache web server
#
	BOARD_OK=yes	
	DEFS=-D FEATURE_IO_NONE
	INCMAGICK=/usr/local/include/GraphicsMagick
	LIBMAGICK=-lrt -lpng12 -ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -ljasper	-lX11 -lXext -lxml2 -llcms -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBLIRC=-llirc_client 
endif

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): CNC Controller
#
	BOARD_OK=yes	
	DEFS=-D FEATURE_IO_NONE
	INCMAGICK=/usr/local/include/GraphicsMagick
	LIBMAGICK=-lrt -lpng12 -ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -ljasper	-lX11 -lXext -lxml2 -llcms -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBLIRC=
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	DEFS=-D FEATURE_IO_NONE
	INCMAGICK=/usr/local/include/GraphicsMagick
	LIBMAGICK=-lrt -lpng12 -ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -ljasper -lX11 -lXext -lxml2 -llcms -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBLIRC=
endif

CC		= gcc
CFLAGS	= $(DEFS) -O2 -Wall -g
DEFS	+= -D BOARD_RPI$(BOARD)
INCDIR	= -I$(INCMAGICK)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib
LIBS	= -lwiringPi -lwiringPiDev
LIBS    += -lm -lhidapi-hidraw -lhidapi-libusb -lbcm2835 
LIBS	+= $(LIBMAGICK) 
OBJ		= main.o rpicnc.o dynpage.o httppage.o safecalls.o netfiles.o eos_api.o log.o rtc.o misc_api.o globals.o raspjson.o	rpihttp.o \
			fileinfo.o cncparms.o graphics.o rpiudp.o sth.o rpiusb.o cncwrite.o cncdebug.o

all: modules $(DESTDIR)/rpicnc

modules:
$(DESTDIR)/rpicnc: $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LDFLGS)
	strip $(DESTDIR)/rpicnc

sync:
	@echo "RPi Board = $(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/rpicnc/*.c ./
	cp -u /mnt/rpi/softw/rpicnc/*.h ./
	cp -u /mnt/rpi/softw/rpicnc/makefile ./

forcesync:
	@echo "Force RPi Board = $(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/rpicnc/*.c ./
	cp /mnt/rpi/softw/rpicnc/*.h ./
	cp /mnt/rpi/softw/rpicnc/makefile ./

clean:
	rm -rf *.o
	rm -rf $(DESTDIR)/rpicnc

install:
	sudo /etc/init.d/rpicnc stop
	sudo cp $(DESTDIR)/rpicnc /usr/sbin/rpicnc
	sudo /etc/init.d/rpicnc restart

restart:
	sudo /etc/init.d/rpicnc stop
	sudo /etc/init.d/rpicnc restart

setup:
	sudo mkdir -p /mnt/rpicache
	sudo mkdir -p /mnt/rpipix
	sudo cp /usr/local/share/rpi/rpicnc.map /mnt/rpicache/rpicnc.map

ziplog:
	sudo gzip -c /usr/local/share/rpi/rpicnc.log 2>/dev/null >>/usr/local/share/rpi/rpicnclogs.gz
	sudo rm -f /usr/local/share/rpi/rpicnc.log
	sudo rm -f /mnt/rpicache/rpicnc.log

delmap:
	sudo rm -f /usr/local/share/rpi/rpicnc.map
	sudo rm -f /mnt/rpicache/rpicnc.map

.c.o:
	$(CC) -c $(CFLAGS) $(INCDIR) $(INOUT) $< 
