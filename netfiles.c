/*  (c) Copyright:  2012..2016 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:     Library for:
**                   general networking
**                   sockets, pipes, etc.
**                   unbuffered I/O
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 **/

#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <ifaddrs.h>
#include "echotype.h"
#include "dynpage.h"
#include "httppage.h"
#include "netfiles.h"
#include "globals.h"
#include "raspjson.h"

//#define USE_PRINTF
#include "printf.h"

#define SAFECALL_TO_LOGFILE
#ifdef  SAFECALL_TO_LOGFILE
void    LOG_Report(int iError, char *pcCaller, char *pcFormat, ...);
#define HANDLE_ERROR   LOG_Report
#else
#define HANDLE_ERROR   HandleError
#endif  //SAFECALL_TO_LOGFILE

static bool net_HandleGetRequest       (NETCL *);
static bool net_HandlePostRequest      (NETCL *);
static bool net_HandleNewSession       (NETCON *);
static bool net_HandleSession          (NETCL *);
static bool net_HasAlphanumerics       (const char *string);
static int  net_ProtocolType           (const char *protocol);
static int  net_ResolveProtocol        (const char *protocol);
static void net_SocketAddressInit      (SOCKADDR_IN *);
static int  net_SocketAddressService   (SOCKADDR_IN *, const char *service, const char *proto);
static int  net_SocketAddressHost      (SOCKADDR_IN *, const char *host);
static void net_setnonblocking         (int);
static bool net_Session                (NETCL *);
static void net_SessionClear           (NETCL *);

#ifdef  FEATURE_NET_DUMP_RECORD
static void net_DumpRecord             (int, char *, int);
#define NET_DUMPRECORD(x,y,z)          net_DumpRecord(x,y,z)
#else
#define NET_DUMPRECORD(x,y,z)
#endif  //FEATURE_NET_DUMP_RECORD



/* ======   Local Functions separator ===========================================
void ___global_functions(){}
==============================================================================*/

//
//  Function:   NET_Init
//  Purpose:    Initialize the connection based API
//              
//              
//  Parms:      
//  Returns:    NetCon struct
//
NETCON *NET_Init()
{
   NETCON  *pstNet;

   PRINTF("NET_Init()" CRLF);
   pstNet = safemalloc(sizeof(NETCON));
   memset(pstNet, 0, sizeof(NETCON));
   //
   pstNet->iConnections = NUM_CONNECTIONS;
   //
   FD_ZERO(&pstNet->tFdRead);
   FD_ZERO(&pstNet->tFdWrite);
   FD_ZERO(&pstNet->tFdOoB);
   //
   return(pstNet);
}

//
//  Function:  NET_ServerConnect
//  Purpose:   Setup server connection:
//             socket()
//             bind()
//             listen()
//              
//  Parms:     Port number ("8080"), protocol("tcp")
//  Returns:   Socket ID or -1 on error
//
int NET_ServerConnect(NETCON *pstNet, const char *pcPort, const char *pcProto) 
{
   SOCKADDR_IN  stSocket;
   int          iSocket;
   int          iTrueval = 1;

   PRINTF2("NET_ServerConnect(): Port=%s, Protocol=%s" CRLF, pcPort, pcProto);

   net_SocketAddressInit(&stSocket);
   net_SocketAddressService(&stSocket, pcPort, pcProto);
 
   iSocket = socket(AF_INET, net_ProtocolType(pcProto), net_ResolveProtocol(pcProto));
   if (iSocket < 0) 
   {
      PRINTF1("NET_ServerConnect(): Socket %d Error" CRLF, iSocket);
      HANDLE_ERROR(errno, "NET", "NET_ServerConnect():couldn't create socket");
      return(-1);
   }
   //
   setsockopt(iSocket, SOL_SOCKET, SO_REUSEADDR, &iTrueval, sizeof(iTrueval));
   net_setnonblocking(iSocket);
   //
   if (bind(iSocket, (SOCKADDR *)&stSocket, sizeof(stSocket)) < 0) 
   {
      PRINTF1("NET_ServerConnect(): Bind Error socket %d" CRLF, iSocket);
      HANDLE_ERROR(errno, "NET", "NET_ServerConnect():bind to port %s failed", pcPort);
      return(-1);
   }
   //
   net_setnonblocking(iSocket);

   if (net_ProtocolType(pcProto) == SOCK_STREAM) 
   {
      PRINTF1("NET_ServerConnect(): listen on port %s" CRLF, pcPort);
      if (listen(iSocket, 5) < 0) 
      {
         PRINTF1("NET_ServerConnect(): Listen Error socket %d" CRLF, iSocket);
         HANDLE_ERROR(errno, "NET", "NET_ServerConnect():listen on port %s failed", pcPort);
         return(-1);
      }
      PRINTF1("NET_ServerConnect(): Listening on port %s..." CRLF, pcPort);
   }
   pstNet->iSocketConnect = iSocket;
   return(iSocket);
}

//
//  Function:  NET_ServerDisconnect
//  Purpose:   Disconnect to a Server
//              
//  Parms:     
//  Returns:   
//
void NET_ServerDisconnect(NETCL *pstCl) 
{
   if(pstCl)
   {
      if(pstCl->iSocket) shutdown(pstCl->iSocket, 2);
   }
}

//
//  Function:   NET_BuildConnectionList
//  Purpose:    Build the network list of open connections
//              
//              
//  Parms:      Netcon struct
//  Returns:    Highest connection FD
//
int NET_BuildConnectionList(NETCON *pstNet)
{
   int x, iFd;

   // 
   // First put together fd_set for select(), which will
   // consist of the sock veriable in case a new connection
   // is coming in, plus all the sockets we have already
   // accepted.
   // 
   // FD_ZERO() clears out the fd_set called socks, so that
   //     it doesn't contain any file descriptors.
   // 
   FD_ZERO(&pstNet->tFdRead);

   // 
   // FD_SET() adds the file descriptor "sock" to the fd_set,
   // so that select() will return if a connection comes in
   // on that socket (which means you have to do accept(), etc.
   // 
   FD_SET(pstNet->iSocketConnect, &pstNet->tFdRead);
   pstNet->iSocketMax = pstNet->iSocketConnect;

   // 
   // Loops through all the possible connections and adds
   // those sockets to the fd_set
   // 
   for (x=0; x<pstNet->iConnections; x++) 
   {
      iFd = pstNet->stClient[x].iSocket; 
      if (iFd != 0) 
      {
         FD_SET(iFd, &pstNet->tFdRead);
         if (iFd > pstNet->iSocketMax)
         {
            pstNet->iSocketMax = iFd;
         }
       }
   }
   //PRINTF1("NET_BuildConnectionList(): %d connections" CRLF, pstNet->iSocketMax);
   return(pstNet->iSocketMax);
}

//
//  Function:   NET_WaitForConnection
//  Purpose:    
//              
//              
//  Parms:      Netcon struct, timeout in mSecs
//  Returns:    Number of sockets with data
//
int NET_WaitForConnection(NETCON *pstNet, int iTimeout)
{
   int   iNumSocks, iSecs, iMicroSecs;

   iSecs      =  iTimeout / 1000;
   iMicroSecs = (iTimeout % 1000)*1000;
   //
   pstNet->stTimeout.tv_sec  = iSecs;
   pstNet->stTimeout.tv_usec = iMicroSecs;

   // 
   // The first argument to select is the highest file
   // descriptor value plus 1. In most cases, you can
   // just pass FD_SETSIZE and you'll be fine.
   // 
   // The second argument to select() is the address of
   // the fd_set that contains sockets we're waiting
   // to be readable (including the listening socket).
   // 
   // The third parameter is an fd_set that you want to
   // know if you can write on -- this example doesn't
   // use it, so it passes 0, or NULL. The fourth parameter
   // is sockets you're waiting for out-of-band data for,
   // which usually, you're not.
   // 
   // The last parameter to select() is a time-out of how
   // long select() should block. If you want to wait forever
   // until something happens on a socket, you'll probably
   // want to pass NULL.
   // 
   iNumSocks = select(pstNet->iSocketMax+1, &pstNet->tFdRead, &pstNet->tFdWrite, &pstNet->tFdOoB, &pstNet->stTimeout);
   //
   // select() returns the number of sockets that had
   // things going on with them -- i.e. they're readable.
   //
   // Once select() returns, the original fd_set has been
   // modified so it now reflects the state of why select()
   // woke up. i.e. If file descriptor 4 was originally in
   // the fd_set, and then it became readable, the fd_set
   // contains file descriptor 4 in it.
   //
   return(iNumSocks);
}

//
//  Function:   NET_HandleSessions
//  Purpose:    There is data avilable in a socket connection
//              
//  Parms:      Netcon struct, handler function
//              
//  Returns:    Number of sockets with data
//
int NET_HandleSessions(NETCON *pstNet)
{
   int      x, iFd, iNr=0;
   NETCL   *pstCl;

   //PRINTF("NET_HandleSessions()" CRLF);
   //
   // OK, now socks will be set with whatever socket(s)
   // are ready for reading.  Lets first check our
   // connection socket, and then check the sockets
   // in session.
   // 
   // If a client is trying to connect() to our connection
   //  socket, select() will consider that as the socket
   //  being 'readable'. Thus, if the connection socket is
   //  part of the fd_set, we need to accept a new session.
   //
   if(FD_ISSET(pstNet->iSocketConnect, &pstNet->tFdRead))
   {
      net_HandleNewSession(pstNet);
   }
   // 
   // Now check connectlist for available data
   // 
   // Run through our sockets and check to see if anything
   // happened with them, if so 'service' them.
   // 
   for(x=0; x<pstNet->iConnections; x++) 
   {
      pstCl = &pstNet->stClient[x]; 
      iFd   = pstCl->iSocket; 
      if(FD_ISSET(iFd, &pstNet->tFdRead))
      {
         iNr++;
         net_HandleSession(pstCl);
      }
   }
   //PRINTF1("NET_HandleSessions():exit(%d)" CRLF, iNr);
   return(iNr);
}

//
//  Function:   NET_WriteString
//  Purpose:    Send a string, including terminating null.  NET_ReadDelimString() could be
//              perfect for reading it on the other end.  And in fact, NET_ReadString()
//              uses just that.
//  Parms:      Socket, String
//  Returns:    
//
int NET_WriteString(int iSocket, char *pcStr) 
{
  return( NET_WriteBuffer(iSocket, pcStr, strlen(pcStr)+1) );
}

//
//  Function:   NET_ReadString
//  Purpose:    Reads a string from the network, terminated by a null.
//              
//              
//  Parms:      
//  Returns:    
//
int NET_ReadString(int iSocket, char *pcBuf, int iMaxlen) 
{
  return( NET_ReadDelimString(iSocket, pcBuf, iMaxlen, 0) );
}

//
//  Function:   NET_ReadNewlineString
//  Purpose:    Reads a string terminated by a newline
//              
//              
//  Parms:      Socket, Buffer, Buffer length
//  Returns:    
//
int NET_ReadNewlineString(int iSocket, char *pcBuf, int iMaxlen) 
{
  return( NET_ReadDelimString(iSocket, pcBuf, iMaxlen, '\n') );
}

//
//  Function:  NET_ReadDelimString
//  Purpose:   Reads a string with an arbitrary ending delimiter.
//              
//              
//  Parms:     Socket, Buffer, Buffer length, delimiter 
//  Returns:   Number read
//
int NET_ReadDelimString(int iSocket, char *pcBuf, int iMaxlen, char cDelim) 
{
  int iCount = 0, iRead;

  while(iCount <= iMaxlen) 
  {
    iRead = read(iSocket, pcBuf+iCount, 1);
    if (iRead < 0) return(iRead);
    if (iRead < 1) 
    {
      HANDLE_ERROR(errno, "NET", "NET_ReadDelimString():unexpected EOF from socket");
      return(iRead);
    }
    if (pcBuf[iCount] == cDelim) 
    {            
       /* Found the delimiter */
       pcBuf[iCount] = 0;
       return(0);
    }
    iCount++;
  }
  return(0);
}

//
//  Function:   NET_Copy
//  Purpose:    Copies data from the in to the out file descriptor.  If numsize
//              is nonzero, specifies the maximum number of bytes to copy.  If
//              it is 0, data will continue being copied until in returns EOF. 
//  Parms:      Fd in and out, max size
//  Returns:    
//
int NET_Copy(int iFdIn, int iFdOut, int iMaxbytes) 
{
  char   pcBuffer[COPY_BUFSIZE];
  int    iRead, iRemaining;

  iRemaining = iMaxbytes;

  while(iRemaining || !iMaxbytes) 
  {
    iRead = read(iFdIn, pcBuffer, (!iRemaining || COPY_BUFSIZE < iRemaining) ? COPY_BUFSIZE : iRemaining);
    if (iRead < 1) return(iRead);
    NET_WriteBuffer(iFdOut, pcBuffer, iRead);
    if (iMaxbytes) iRemaining -= iRead;
  }
  return(0);
}

//
//  Function:   NET_WriteBuffer
//  Purpose:    This function will write a certain number of bytes from the buffer
//              to the descriptor fd.  The number of bytes written are returned.
//              This function will not return until all data is written or an error
//              occurs.
//  Parms:      Fd, buffer, count
//  Returns:    
//
int NET_WriteBuffer(int iFd, char *pcBuffer, int iCount) 
{
   int  iWritten=0, iResult;

   if (iCount < 0) return(-1);
 
   while(iWritten != iCount) 
   {
      iResult = write(iFd, &pcBuffer[iWritten], iCount - iWritten);
      if (iResult < 0) 
      {
         //PRINTF2("NET_WriteBuffer():Error %d=%d" CRLF, iResult, errno);
         switch(errno)
         {
            case EAGAIN:
               // send buffer is full: wait
               iResult = 0;
               break;

            default:
               return(iResult);
         }
      }
      else if(iResult != (iCount-iWritten)) 
      {
         //PRINTF2("NET_WriteBuffer():W=%d, Act=%d" CRLF, iCount, iResult);
      }
      iWritten += iResult;
   }
   return(iWritten);
}

//
//  Function:   NET_ReadBuffer
//  Purpose:    
//              This function will read a number of bytes from the descriptor fd.  The
//              number of bytes read are returned.  In the event of an error, the
//              error handler is returned.  In the event of an EOF at the first read
//              attempt, 0 is returned.  In the event of an EOF after some data has
//              been received, the count of the already-received data is returned.
//  Parms:      
//  Returns:    Neg on read error
//              else number read
//
int NET_ReadBuffer(int iFd, char *pcBuf, int iCount) 
{
   char *pcTemp=pcBuf;
   int   iRead=0, iResult;
   
   if (iCount < 0) return(-1);
   
   while(iRead != iCount) 
   {
      //PRINTF1("NET_ReadBuffer(): read %d bytes" CRLF, n);
      NET_DUMPRECORD(NET_DUMP_ASCII, pcBuf, n);
      //
      iResult = read(iFd, &pcTemp[iRead], iCount-iRead);
      if (iResult < 0)
      {
         PRINTF2("NET_ReadBuffer():Error %d=%d" CRLF, iResult, errno);
         switch(errno)
         {
            case EAGAIN:
               // receive buffer is empty: exit
               iResult = 0;
               break;

            default:
               return(iResult);
         }
      }
      if (iResult == 0) return(iRead);
      iRead += iResult;
   }
   return(iRead);
}

//
//  Function:   NET_ReadUlong
//  Purpose:    Reads a uint32 from the network in network byte order.
//              
//              A note on the implementation: because some architectures cannot
//              write to the memory of the integer except all at once, a character
//              buffer is used that is then copied into place all at once.
//  Parms:      
//  Returns:    
//
int NET_ReadUlong(int iFd, uint32_t *pulValue) 
{
  char   pcBuffer[sizeof(uint32_t)];
  int    iStatus;

  iStatus = NET_ReadBuffer(iFd, pcBuffer, sizeof(uint32_t));
  if (iStatus != sizeof(uint32_t)) 
  {
    HANDLE_ERROR(0, "NET", "NET_ReadUlong():unexpected EOF");
    return(-1);
  }
  bcopy(pcBuffer, (char *)pulValue, sizeof(uint32_t));
  *pulValue = ntohl(*pulValue);
  return(0);
}

//
//  Function:   NET_WriteUlong
//  Purpose:    Write an unsigned long in network byte order
//              
//  Parms:      
//  Returns:    
//
int NET_WriteUlong(int iFd, uint32_t ulValue) 
{
  char     pcBuffer[sizeof(uint32_t)];
  uint32_t ulTemp;
  int      iStatus;

   ulTemp = htonl(ulValue);
   bcopy((char *)&ulTemp, pcBuffer, sizeof(ulTemp));
   iStatus = NET_WriteBuffer(iFd, pcBuffer, sizeof(ulTemp));
   if (iStatus != sizeof(ulTemp)) return(-1);
   return(0);
}

//
//  Function:   NET_GetMyFqdn
//  Purpose:    Returns the fully qualified domain name of the current host.
//              
//  Parms:      Host name, max-size
//  Returns:    
//
char *NET_GetMyFqdn(char *pcHostname, int iMaxSize) 
{

  gethostname(pcHostname, iMaxSize);
  return( NET_GetFqdn(pcHostname, iMaxSize) );
}

//
//  Function:   NET_GetFqdn
//  Purpose:    Returns the fully qualified domain name of an arbitrary host.
//              
//  Parms:      Buffer, max size
//  Returns:    
//
char *NET_GetFqdn(char *pcHostname, int iMaxSize) 
{
  struct hostent *pstHp;
 
  pstHp = gethostbyname(pcHostname);
  if (pstHp == NULL) return(NULL);
  safestrncpy(pcHostname, (pstHp->h_aliases[0]) ? pstHp->h_aliases[0] : pstHp->h_name, iMaxSize);
  return(pcHostname);
}

//
//
//  Function:  NET_ClientConnect
//  Purpose:   Connect to a client
//             socket()
//             connect()
//             
//  Parms:     Host("10.0.0.123"), port("8080"), protocol("tcp")
//  Returns:   Socket ID or -1 on error
//
int NET_ClientConnect(const char *pcHost, const char *pcPort, const char *pcProto) 
{
   int          iSocket;
   CNCMAP      *pstMap=GLOBAL_GetMapping();
   

   PRINTF3("NET_ClientConnect(): Address=%s, Port=%s, Protocol=%s" CRLF, pcHost, pcPort, pcProto);
   net_SocketAddressInit(&pstMap->G_stUdpSocket);
   net_SocketAddressService(&pstMap->G_stUdpSocket, pcPort, pcProto);
   net_SocketAddressHost(&pstMap->G_stUdpSocket, pcHost);
   
   iSocket = socket(AF_INET, net_ProtocolType(pcProto), net_ResolveProtocol(pcProto));
   if (iSocket < 0) 
   {
      PRINTF1("NET_ClientConnect() Socket %d Error" CRLF, iSocket);
      HANDLE_ERROR(errno, "NET", "NET_ClientConnect():socket failed");
      return(-1);
   }
   
   #ifdef FEATURE_UDP_CONNECTED
   //
   // The UDP datagram connection based solution (uses send(....)
   // Does not need a destination:
   //    iNr = send(iSocket, pubData, iLen, 0);
   //
   // Note: causes the server to generate ICMP error messages !!!
   // ============================================================
   if (connect(iSocket, (__CONST_SOCKADDR_ARG)&pstMap->G_stUdpSocket, sizeof(pstMap->G_stUdpSocket)) < 0) 
   {
      PRINTF1("NET_ClientConnect() Connect Error socket %d" CRLF, iSocket);
      HANDLE_ERROR(errno, "NET", "NET_ClientConnect():connect failed");
      return(-1);
   }
   #endif   //FEATURE_UDP_CONNECTED

   return(iSocket);
}

//
//  Function:  NET_ClientDisconnect
//  Purpose:   Disconnect to a client
//             
//  Parms:     Socket
//  Returns:   
//
void NET_ClientDisconnect(int iSocket) 
{
   if(iSocket > 0)
   {
      close(iSocket);
   }
}

//
//  Function:  NET_ServerAccept
//  Purpose:   Accept a connection from a client
//             accept()
//             
//  Parms:     Connection 
//  Returns:   Session Socket ID or -1 on error
//
int NET_ServerAccept(NETCON *pstNet)
{
   int         iSocket, iCc;
   SOCKADDR_IN stSocket;
   socklen_t   tLength = sizeof(stSocket);
   CNCMAP     *pstMap;

   pstMap = GLOBAL_GetMapping();
   //
   iSocket = pstNet->iSocketConnect;
   PRINTF1("NET_ServerAccept() Accept FD=%d" CRLF, iSocket);
   iCc = accept(iSocket, (SOCKADDR *) &stSocket, &tLength);
   if(iCc != -1)
   {
      //
      // Save caller IP addr:
      //
      pstMap->G_Connected = TRUE;
      inet_ntop(AF_INET, &stSocket.sin_addr, pstMap->G_pcUdpAddr, INET_ADDRSTRLEN);
      PRINTF1("NET_ServerAccept(): IP=%s" CRLF, pstMap->G_pcUdpAddr);
   }
   else
   {
      pstMap->G_Connected = FALSE;
      strcpy(pstMap->G_pcUdpAddr, "");
   }
   return(iCc);
}

//
//  Function:  NET_FlushCache
//  Purpose:   Flush the cached xmt buffer
//             
//  Parms:     NETCL *
//  Returns:   Bytes flushed
//
//
int NET_FlushCache(NETCL *pstCl)
{
   int   iWr;

   NET_DUMPRECORD(NET_DUMP_HEX, pstCl->pcXmtBuffer, pstCl->iXmtBufferPut);
   //
   iWr = NET_WriteBuffer(pstCl->iSocket, pstCl->pcXmtBuffer, pstCl->iXmtBufferPut);
   //PRINTF2("NET_FlushCache():%d bytes written from %d" CRLF, iWr, pstCl->iXmtBufferPut);
   if(iWr > 0)
   {
      pstCl->iXmtBufferPut -= iWr;
   }
   //PRINTF1("NET_FlushCache():Put=%d" CRLF, pstCl->iXmtBufferPut);
   return(iWr);
}

//
//  Function:  NET_GetIpInfo
//  Purpose:   Retrieve the IP address of the local interfaces
//             
//  Parms:     
//  Returns:   TRUE if IP address found
//  Note:      struct ifaddrs 
//             {
//                 struct ifaddrs *  ifa_next;
//                 char *            ifa_name;
//                 u_int             ifa_flags;
//                 struct sockaddr * ifa_addr;
//                 struct sockaddr * ifa_netmask;
//                 struct sockaddr * ifa_dstaddr;
//                 void *            ifa_data;
//             }
//                
//
bool NET_GetIpInfo(void)
{
   int               iFamily, s, n;
   struct ifaddrs   *pstIfAddr, *pstIf;
   CNCMAP           *pstMap=GLOBAL_GetMapping();

   if(getifaddrs(&pstIfAddr) == -1) 
   {
      PRINTF("NET_GetIpInfo():getifaddrs() failed" CRLF);
      return(FALSE);
   }

   //
   // Walk through linked list, maintaining head pointer so we
   // can free list later

   for (pstIf=pstIfAddr, n=0; pstIf!=NULL; pstIf=pstIf->ifa_next, n++) 
   {
      if (pstIf->ifa_addr == NULL)
         continue;
      //
      // Ignore local Ip 127.0.0.1
      //
      if(strcmp(pstIf->ifa_name, "lo") == 0)
         continue;

      PRINTF1("NET_GetIpInfo(): Adapter %d" CRLF, n);
      PRINTF1("NET_GetIpInfo(): ifa_name  = %s" CRLF, pstIf->ifa_name);
      PRINTF1("NET_GetIpInfo(): ifa_flags = %x" CRLF, pstIf->ifa_flags);

      iFamily = pstIf->ifa_addr->sa_family;

      // For an AF_INET* interface address, display the address

      if (iFamily == AF_INET || iFamily == AF_INET6) 
      {
         s = getnameinfo(pstIf->ifa_addr,
                (iFamily == AF_INET) ? sizeof(SOCKADDR_IN) : sizeof(SOCKADDR_IN6),
                pstMap->G_pcTcpAddr, INET_ADDRSTRLEN, NULL, 0, NI_NUMERICHOST);
         //
         if (s != 0) 
         {
            HANDLE_ERROR(0, "NET", "NET_GetIpInfo():getnameinfo() failed:%s(=%d)", gai_strerror(s), s);
            PRINTF2("NET_GetIpInfo():getnameinfo() failed:%s(=%d)" CRLF, gai_strerror(s), s);
         }
         strncpy(pstMap->G_pcTcpIfce, pstIf->ifa_name, MAX_PARM_LEN);
         PRINTF2("NET_GetIpInfo(): my IP=%s (%s)" CRLF, pstMap->G_pcTcpAddr, pstMap->G_pcTcpIfce);
      } 
   }
   freeifaddrs(pstIfAddr);
   return(strlen(pstMap->G_pcTcpAddr) > 0);
}

//
//  Function:  NET_SendData
//  Purpose:   Send data to socket connection:
//              
//  Parms:     socket, data, length, 
//  Returns:   
//  Note:      Usage of UDP datagrams:
//
//             int s;
//             sockaddr_in addrDest;
//             sockaddr_in addrLocal;
//             char* msg = "Hello World";
//             //
//             // create the socket
//             //
//             s = socket(AF_INET, SOCK_DGRAM, 0); // UDP socket
//             //
//             addrLocal.sin_family = AF_INET;
//             addrLocal.sin_port = htons(9999);
//             addrLocal.sin_addr = INADDR_ANY; // zero-init sin_addr to tell it to use all available adapters on the local host
//             //
//             // associate this socket with local UDP port 9999
//             //
//             result = bind(s, (struct sockaddr*)&addrLocal, 0);
//             //
//             // send "Hello world" from local port 9999 to the host at 1.2.3.4 on its port 8888
//             //
//             addrDest.sin_family = AF_INET;
//             addrDest.sin_port = htons(8888);
//             addrDest.sin_addr.s_addr = inet_addr("1.2.3.4");
//             //
//             // strlen(msg)+1 for terminating null char
//             //
//             result = sendto(s, msg, strlen(msg)+1, 0, (struct sockaddr*)&addrDest, sizeof(addrDest));
//
int NET_SendData(int iSocket, u_int8 *pubData, int iLen) 
{
   int            iNr;

   #ifdef FEATURE_UDP_CONNECTED
   //
   // connection based UDP datagrams: destination is already known !
   //
   // Note: causes the server to generate ICMP error messages !!!
   // ============================================================
   //
   PRINTF("NET_SendData(): Connection style UDP" CRLF);
   iNr = send(iSocket, pubData, iLen, 0);

   #else //FEATURE_UDP_CONNECTED
   CNCMAP        *pstMap=GLOBAL_GetMapping();

   PRINTF("NET_SendData(): Unconnected UDP" CRLF);
   iNr = sendto(iSocket, pubData, iLen, 0, (SOCKADDR *)&pstMap->G_stUdpSocket, sizeof(pstMap->G_stUdpSocket));

   #endif //FEATURE_UDP_CONNECTED

   if(iNr < 0)
   {
      HANDLE_ERROR(errno, "NET", "NET_SendData(): Failed");
   }
   return(iNr);
}

/* ======   Local Functions separator ===========================================
void ___local_functions(){}
==============================================================================*/

//
//  Function:  net_HandleGetRequest
//  Purpose:   Handle the GET request
//             
//  Parms:     Client 
//  Returns:   TRUE if request handled completely
//
static bool net_HandleGetRequest(NETCL *pstCl)
{
   bool fCc=TRUE;

   //
   // Handle incoming HTTP GET request:
   //
   //   "GET /index.html HTTP/1.1" CR,LF
   //   "Host: 10.0.0.231" CR,LF
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)" CR,LF
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5" CR,LF
   //   "Accept-Encoding: gzip,deflate" CR,LF
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7" CR,LF
   //   "Keep-Alive: 115" CR,LF
   //   "Connection: keep-alive" CR,LF
   //   CR,LF
   //
   if(pstCl->iReqLen)
   {
      //
      // The client has additional data to send: take it
      //
      PRINTF1("net_HandleGetRequest(): Content-Length = %d bytes" CRLF, pstCl->iReqLen);
   }
   //
   // Check if the requested URL is a dynamically generated page or if it should
   // be on the filesystem.
   //
   if( HTTP_PageIsDynamic(pstCl) )
   {
      //
      // This page is an internal (dynamically build) page
      //
      PRINTF1("net_HandleGetRequest():Dynamic page:%s" CRLF, &pstCl->pcUrl[pstCl->iFileIdx]);
      //PRINTF1("net_HandleGetRequest(): F=%d" CRLF, pstCl->iFileIdx);
      //PRINTF1("net_HandleGetRequest(): E=%d" CRLF, pstCl->iExtnIdx);
      //PRINTF1("net_HandleGetRequest(): P=%d" CRLF, pstCl->iParmIdx);
      fCc = HTTP_SendDynamicPage(pstCl);
      //
      // Dynamic page returns TRUE if the current page request should complete the HTTP request
      //
      if(fCc) NET_FlushCache(pstCl);
   }
   else
   {
      //
      // This page is a static page (needs to come from the file system)
      //
      PRINTF1("net_HandleGetRequest():Static page %s" CRLF, pstCl->pcUrl);
      //PRINTF1("net_HandleGetRequest(): F=%d" CRLF, pstCl->iFileIdx);
      //PRINTF1("net_HandleGetRequest(): E=%d" CRLF, pstCl->iExtnIdx);
      //PRINTF1("net_HandleGetRequest(): P=%d" CRLF, pstCl->iParmIdx);
      HTTP_RequestStaticPage(pstCl);
      NET_FlushCache(pstCl);
   }
   return(fCc);
}

//
//  Function:  net_HandlePostRequest
//  Purpose:   Handle the POST request
//             
//  Parms:     Client 
//  Returns:   
//
static bool net_HandlePostRequest(NETCL *pstCl)
{
   bool fCc=FALSE;

   if(pstCl->iReqLen)
   {
      //
      // The client has additional data to send: take it
      //
      PRINTF1("net_HandlePostRequest(): Content-Length = %d bytes" CRLF, pstCl->iReqLen);
   }
   //
   // POST request
   // ToDo.....
   NET_FlushCache(pstCl);
   return(fCc);
}

//
//  Function:  net_HasAlphanumerics
//  Purpose:   a private function used only by this library.  It checks
//             the passed string.  
//  Parms:      
//  Returns:   TRUE if string contains non-numerics
//             FALSE if only 0..9
//
static bool net_HasAlphanumerics(const char *pcString) 
{
  int x;

  for (x=0; x<strlen(pcString); x++)
  {
    if (!(isdigit(pcString[x])))  return(TRUE);
  }
  return(FALSE);
}

//
//  Function:  net_HandleSession
//  Purpose:   Handle this session
//              
//  Parms:     Netcl struct, Handler fnct
//              
//  Returns:   TRUE if OKee
//
static bool net_HandleSession(NETCL *pstCl)
{
   bool     fCc=FALSE, fReading=TRUE;
   int      iRcv, iFd;

   //PRINTF("net_HandleSession()" CRLF);
   //
   // Read data from the socket
   //
   iFd  = pstCl->iSocket;
   while(fReading)
   { 
      iRcv = read(iFd, pstCl->pcRcvBuffer, READ_BUFFER_SIZE);
      if(iRcv < 0)
      {
         // Connection error
         PRINTF1("net_HandleSession():Connection error:FD=%d" CRLF, iFd);
         close(iFd);
         pstCl->iSocket = 0;
         fReading = FALSE;
      }
      else if(iRcv == 0)
      {
         // Connection closed by client: close this end and free up entry in connection list
         PRINTF1("net_HandleSession():Connection terminated:FD=%d" CRLF, iFd);
         close(iFd);
         pstCl->iSocket = 0;
         fReading = FALSE;
      }
      else
      {
         // Data has been read:
         //PRINTF1("net_HandleSession():Read %d bytes" CRLF, iRcv);
         pstCl->iRcvBufferGet = 0;
         pstCl->iRcvBufferPut = iRcv;
         fReading = net_Session(pstCl);
      }
   }
   return(fCc);
}

//
//  Function:   net_HandleNewSession
//  Purpose:    There is someone knocking on the door
//              
//  Parms:      Netcon struct, new connection
//              
//  Returns:    TRUE if added
//
static bool net_HandleNewSession(NETCON *pstNet)
{
   int      x;
   int      iFd;
   bool     fCc=FALSE;
   NETCL   *pstCl;

   //
   // We have a new connection coming in!  We'll try to find a spot for it in connectlist
   //
   iFd = NET_ServerAccept(pstNet);
   if (iFd < 0) 
   {
      PRINTF1("net_HandleNewSession(): Accept error FD=%d" CRLF, iFd);
      HANDLE_ERROR(errno, "NET", "net_HandleNewSession():Accept error");
      return(0);
   }
   net_setnonblocking(iFd);
   //
   for (x=0; x<pstNet->iConnections; x++)
   {
      pstCl = &pstNet->stClient[x];
      if (pstCl->iSocket == 0) 
      {
         //
         // Free slot !
         //
         PRINTF2("net_HandleNewSession(): FD=%d; Slot=%d" CRLF, iFd, x);
         if(pstCl->pcUrl       == NULL) pstCl->pcUrl       = (char *) safemalloc(MAX_URL_LEN);
         if(pstCl->pcRcvBuffer == NULL) pstCl->pcRcvBuffer = (char *) safemalloc(READ_BUFFER_SIZE);
         if(pstCl->pcXmtBuffer == NULL) pstCl->pcXmtBuffer = (char *) safemalloc(WRITE_BUFFER_SIZE);
         //
         pstCl->iSocket = iFd;
         net_SessionClear(pstCl);
         fCc = TRUE;
         break;
      }
   }
   if (fCc == FALSE) 
   {
      //
      // No room left in the queue!
      //
      PRINTF("net_HandleNewSession(): No room left for new client." CRLF);
      NET_WriteString(iFd,"Sorry, this server is too busy. Try again later!\r\n");
      close(iFd);
   }
   return(fCc);
}

//
//  Function:   net_ProtocolType
//  Purpose:    Return the protocol ID
//              
//  Parms:      protocol name ptr
//  Returns:    SOCK_STREAM, SOCK_DGRAM
//
static int net_ProtocolType(const char *pcProtocol) 
{
  if (strcmp(pcProtocol, "tcp") == 0) return SOCK_STREAM;
  if (strcmp(pcProtocol, "udp") == 0) return SOCK_DGRAM;
  if (strcmp(pcProtocol, "TCP") == 0) return SOCK_STREAM;
  if (strcmp(pcProtocol, "UDP") == 0) return SOCK_DGRAM;
  return(-1);
}

//
//  Function:   net_ResolveProtocol
//  Purpose:    
//              
//              
//  Parms:      
//  Returns:    
//
static int net_ResolveProtocol(const char *pcProtocol) 
{
  struct protoent *pstProtocol;

  pstProtocol = getprotobyname(pcProtocol);
  if (!pstProtocol) 
  {
    PRINTF1("net_ResolveProtocol(): Error protocol = %s" CRLF, pcProtocol);
    HANDLE_ERROR(errno, "NET", "net_ResolveProtocol():getprotobyname failed for %s", pcProtocol);
    return(-1);
  }
  return(pstProtocol->p_proto);
}

//
//  Function:   net_SocketAddressInit
//  Purpose:    Clear the socket structure
//              
//  Parms:      
//  Returns:    
//
static void net_SocketAddressInit(SOCKADDR_IN *pstSocketAddr) 
{
  bzero((char *) pstSocketAddr, sizeof(SOCKADDR_IN));
  //
  pstSocketAddr->sin_family      = AF_INET;
  pstSocketAddr->sin_addr.s_addr = INADDR_ANY;
}

//
//  Function:   net_SocketAddressHost
//  Purpose:    
//              
//              
//  Parms:      
//  Returns:    
//
static int net_SocketAddressHost(SOCKADDR_IN *pstSocketAddr, const char *pcHost) 
{
  struct hostent *pstHostAddr;

  pstHostAddr = gethostbyname(pcHost);
  if (pstHostAddr==NULL) 
  {
    PRINTF1("net_SocketAddressHost(): Error host address = %s" CRLF, pcHost);
    HANDLE_ERROR(errno, "NET", "net_SocketAddressHost():gethostbyname failed for %s", pcHost);
    return(-1);
  }
  memcpy(&pstSocketAddr->sin_addr, pstHostAddr->h_addr, pstHostAddr->h_length);
  return(0);
}

//
//  Function:   net_SocketAddressService
//  Purpose:    
//              
//        
//  getservent()     reads the next line from the file /etc/services and returns a structure servent containing 
//                   the broken out fields from the line. The /etc/services file is opened if necessary.
//  getservbyname()  returns a servent structure for the line from /etc/services that matches the service name 
//                   using protocol proto. If proto is NULL, any protocol will be matched.
//  getservbyport()  returns a servent structure for the line that matches the port given in network byte 
//                   order using protocol proto. If proto is NULL, any protocol will be matched.
//  setservent()     opens and rewinds the /etc/services file. If stayopen is true (1), then the file will not 
//                   be closed between calls to getservbyname() and getservbyport().
//  endservent()     closes /etc/services.
//
//  servent structure is defined in <netdb.h> as follows:
//    struct servent 
//    {
//       char    *s_name;        /* official service name */
//       char   **s_aliases;     /* alias list */
//       int      s_port;        /* port number */
//       char    *s_proto;       /* protocol to use */
//    }
//    The members of the servent structure are:
//       s_name
//          The official name of the service. 
//       s_aliases
//          A zero terminated list of alternative names for the service. 
//       s_port
//          The port number for the service given in network byte order. 
//       s_proto
//          The name of the protocol to use with this service. 
//        
//          RETURN VALUE
//          The getservent(), getservbyname() and getservbyport() functions return the servent structure, 
//          or a NULL pointer if an error occurs or the end of the file is reached.  
//        
//  Parms:      
//  Returns:    0=OKee, -1=ERROR
//
static int net_SocketAddressService(SOCKADDR_IN *pstSocketAddr, const char *pcService, const char *pcProtocol)
{
   struct servent *pstServiceAddr;
   int             iPort;

   /* Need to allow numeric as well as textual data. */
   /* 0: pass right through. */

   PRINTF2("net_SocketAddressService(): Service=%s, protocol=%s " CRLF, pcService, pcProtocol);

   if (strcmp(pcService, "0") == 0)
   {
      pstSocketAddr->sin_port = 0;
   }
   else 
   {                           
      /* nonzero port */
      PRINTF("net_SocketAddressService(): lookup port in services list" CRLF);
      pstServiceAddr = getservbyname(pcService, pcProtocol);
      if(pstServiceAddr) 
      {
         PRINTF1("net_SocketAddressService(): Translates to port %d" CRLF, pstServiceAddr->s_port);
         pstSocketAddr->sin_port = pstServiceAddr->s_port;
      } 
      else 
      {
         /* name did not resolve, try number */
         PRINTF("net_SocketAddressService(): no service list found" CRLF);
         if (net_HasAlphanumerics(pcService)) 
         { 
            /* and it's a text name, fail. */
            PRINTF1("net_SocketAddressService(): Error service address %s" CRLF, pcService);
            HANDLE_ERROR(errno, "NET", "net_SocketAddressService():no lookup for %s/%s", pcService, pcProtocol);
            return(-1);
         }
         //
         iPort = atoi(pcService);
         PRINTF1("net_SocketAddressService(): Port=%d" CRLF, iPort);
         //
         if ((pstSocketAddr->sin_port = htons(iPort)) == 0) 
         {
            PRINTF("net_SocketAddressService(): No such service found" CRLF);
            HANDLE_ERROR(errno, "NET", "net_SocketAddressService():numeric conversion failed");
            return(-1);
         }
      }
   }
   return(0);
}

#ifdef COMMENT
//
//  Function:   net_StripCrLf
//  Purpose:    Removes CR and LF from the end of a string.
//              
//  Parms:      string
//  Returns:    
//
static void net_StripCrLf(char *pcTemp)
{
  while( strlen(pcTemp) 
             &&
         ( (pcTemp[strlen(pcTemp)-1] == 13) || (pcTemp[strlen(pcTemp)-1] == 10)) ) 
  {
    pcTemp[strlen(pcTemp)-1] = 0;
  }
}
#endif   //COMMENT

//
//  Function:   net_setnonblocking
//  Purpose:    
//              
//  Parms:      Socket
//  Returns:    
//
static void net_setnonblocking(int iSock)
{
   int iOpts;
    
   iOpts = fcntl(iSock, F_GETFL);
   if (iOpts < 0) 
   {
      HANDLE_ERROR(errno, "NET", "net_setnonblocking():F_GETFL error");
      exit(EXIT_FAILURE);
   }
   iOpts = (iOpts | O_NONBLOCK);
   if (fcntl(iSock, F_SETFL, iOpts) < 0) 
   {
      HANDLE_ERROR(errno, "NET", "net_setnonblocking():F_SETFL error");
      exit(EXIT_FAILURE);
   }
   return;
}

//
//  Function:  net_Session
//  Purpose:   Handle the incoming data from this session
//
//  Parms:     Session
//  Returns:   TRUE if OKee
//
static bool net_Session(NETCL *pstCl)
{
   bool     fCc=FALSE;
   CNCMAP  *pstMap=GLOBAL_GetMapping();
   //
   PRINTF2("net_Session(): Get=%d,Put=%d" CRLF, pstCl->iRcvBufferGet, pstCl->iRcvBufferPut);
   NET_DUMPRECORD(NET_DUMP_ASCII, pstCl->pcRcvBuffer, pstCl->iRcvBufferPut);
   // 
   // The session Rcv buffer :
   //
   //         +------------------------------------------------------------------+
   //         |GET /index.html HTTP/1.1 ........                                 |
   //         +------------------------------------------------------------------+
   //          ^iRcvBufferGet                                               READ_BUFFER_SIZE
   //                                  ^iRcvBufferPut
   //
   //
   pstMap->G_HttpGetRequest  = FALSE;
   pstMap->G_HttpPostRequest = FALSE;
   //
   while(pstCl->iRcvBufferGet < pstCl->iRcvBufferPut)
   {
      //
      // Parse the incoming data for this client
      //
      switch( HTTP_CollectRequest(pstCl) )
      {
         case HTTP_CHAR:
            break;

         case HTTP_GET:
            PRINTF1("net_Session: Get Request: [%s]" CRLF, pstCl->pcUrl);
            pstMap->G_HttpGetRequest = TRUE;
            break;

         case HTTP_POST:
            PRINTF1("net_Session: Post Request: [%s]" CRLF, pstCl->pcUrl);
            pstMap->G_HttpPostRequest = TRUE;
            break;

         case HTTP_ERROR:
            HANDLE_ERROR(0, "NET", "net_Session():ERROR: HTTP buffer overflow");
            PRINTF("net_Session: ERROR: HTTP buffer overflow" CRLF);
            break;

         case HTTP_END:
            PRINTF("net_Session():EOF"CRLF);
            break;
      }
   }
   //
   // No more data: handle GET/POST completion
   //
   if(pstMap->G_HttpPostRequest)
   {
      fCc = net_HandlePostRequest(pstCl);
   }
   else if(pstMap->G_HttpGetRequest)
   {
      fCc = net_HandleGetRequest(pstCl);
   }
   PRINTF1("net_Session:Exit(%d)" CRLF, fCc);
   return(fCc);
}

//
//  Function:  net_SessionClear
//  Purpose:   Clear buffers for this session
//
//  Parms:     Session
//  Returns:   
//
static void net_SessionClear(NETCL *pstCl)
{
   PRINTF("net_SessionClear()" CRLF);
   pstCl->iReqState     = 0;
   pstCl->iReqLen       = 0;
   pstCl->iRcvBufferPut = 0;
   pstCl->iRcvBufferGet = 0;
   pstCl->iXmtBufferPut = 0;
   pstCl->iPathIdx      = 0;
   pstCl->iParmIdx      = 0;
   pstCl->iFileIdx      = 0;
   pstCl->iExtnIdx      = 0;
}

#ifdef  FEATURE_NET_DUMP_RECORD
//
//  Function:  net_DumpRecord
//  Purpose:   Dump the read record in hex
//             
//  Parms:     mode, Buffer, len
//  Returns:   
//
//
static void net_DumpRecord(int iMode, char *pcBuffer, int iLen)
{
   int   iRow=0, iCol=0, iIdx;
   char  pcChar[2];
   char  pcAscii[12];

   if(iMode & NET_DUMP_ASCII)
   {
      ES_printf("=======================================================" CRLF);
      iIdx = 0;
      while(iIdx<iLen)
      {
         ES_printf("%c", pcBuffer[iIdx++]);
      }
      ES_printf("=======================================================" CRLF);
   }

   if(iMode & NET_DUMP_HEX)
   {
      iIdx = 0;
      pcChar[1] = 0;
      //
      ES_printf("=======================================================" CRLF);
      while(iIdx < iLen)
      {
         if(iCol == 0) 
         {
            ES_printf("   %03d = ", iRow);
         }
         //
         pcChar[0] = pcBuffer[iIdx++];
         ES_printf("%02X ", pcChar[0]);
         //
         if( (pcChar[0]==0x0d) || (pcChar[0]==0x0a)) pcAscii[iCol] = '.';
         else                                        pcAscii[iCol] = pcChar[0];
         iCol++;
         //
         if(iCol == 10) 
         {
            pcAscii[iCol] = 0;
            ES_printf("   %s\n", pcAscii);
            iCol = 0;
         }
         iRow++;
      }
      //
      pcAscii[iCol] = 0;
      ES_printf("   %s\n", pcAscii);
      ES_printf("=======================================================" CRLF);
   }
}
#endif   //FEATURE_NET_DUMP_RECORD
