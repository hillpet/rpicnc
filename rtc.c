/*  (c) Copyright:  2005  CvB, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            This module handles the ALARM list
**
**  Entry Points:       RTC_GetDateTime()
**                      RTC_ConvertDateTime()
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       20 Aug 2005
**
 *  Revisions:
 *    $Log:   $
 *    v002:     Initial version
 *    v003:     Need to take care of DST !!!!!!!!!!! 
 *    v008:     Add TIME_FORMAT_YYYY_MM_DD_HH_MM_WW
 *    20130721: Add TIME_FORMAT_YYYY_MM_DD_HH_MM_SS
 *              Add RTC_ConvertDateTimeSize()
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>

#include "echotype.h"
#include "eos_api.h"

#include "log.h"
#include "rtc.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
//#define  FEATURE_TEST_TIME
#ifdef   FEATURE_TEST_TIME
#define  TIME_TEST()                           rtc_TestDST()
static   void    rtc_TestDST                   (void);
static   u_int32 rtc_TestDSTSkipYear           (u_int32, u_int8);

#else    //FEATURE_TEST_TIME
#define  TIME_TEST()

#endif   //FEATURE_TEST_TIME

static void    rtc_ClockFormat                  (bool, u_int32, CLOCK_DAY_TIME *);
static void    rtc_ClockMJDtoYMD               (u_int32, u_int32 *, u_int32 *, u_int32 *);
static void    rtc_ClockMJDtoDofW              (u_int32, u_int32 *);
static void    rtc_ClockUTCtoHMS               (u_int32, u_int8 *, u_int8 *, u_int8 *);
static bool    rtc_DaylightSavingTime          (u_int32);
static u_int32 rtc_FindNextDaylightSavingTime  (u_int32, u_int8);

static const char *cDaysOfTheWeek[] =
{
   "Zo", "Ma", "Di", "Wo", "Do", "Vr", "Za"
};

static const char *cMonthOfTheYear[] =
{
   "---", "Jan", "Feb", "Mrt", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"
};

static const char *cDaylight[] =
{
   "W", "Z"
};

static int    iTimeZone = 1;          // Amsterdam : GMT +1

/**
 **  Name:         RTC_GetSystemSecs
 **
 **  Description:  Get the current system time
 **
 **  Arguments:    
 **
 **  Returns:      Current time in secs since 1970
 **/
u_int32 RTC_GetSystemSecs(void)
{
  int  iSecs;

  iSecs  = (int) time(NULL);
  iSecs += (iTimeZone * ES_SECONDS_PER_HOUR);
  return( (u_int32) iSecs);
}


/**
 **  Name:         RTC_SetTimeZone
 **
 **  Description:  Get the current quarter number
 **
 **  Arguments:    Offset in hours from GMT
 **
 **  Returns:      
 **/
void RTC_SetTimeZone(int iGmtOffset)
{
  if( (iGmtOffset <=12) || (iGmtOffset >= -12) )
  { 
     iTimeZone = iGmtOffset;
  }
  else
  {
     LOG_Report(0, "RTC", "Bad GMT offset %d", iGmtOffset);
  }
}

/**
 **  Name:         RTC_GetCurrentQuarter
 **
 **  Description:  Get the current quarter number
 **
 **  Arguments:    the amount if secs, or 0 if need system secs
 **
 **  Returns:      Current quarter index  0 .. (24*4)-1
 **/
u_int32 RTC_GetCurrentQuarter(u_int32 ulSecs)
{
  CLOCK_DAY_TIME stTime;
  u_int32        ulQs;

  //
  // Get the date and time data from the global total-secs, if necessary
  //
  if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

  rtc_ClockFormat(DST_YES, ulSecs, &stTime);

  ulQs  = (((u_int32) stTime.ubHour) *  4);
  ulQs += (((u_int32) stTime.ubMin)  / 15);

  if(ulQs > (24*4)-1)
  {
     LOG_Report(0, "RTC", "Too many quarters (%d)", ulQs);
     ulQs = (24*4)-1;
  }
  return(ulQs);
}

/**
 **  Name:         RTC_GetDateTime
 **
 **  Description:  Get the system date/time in this time zone. 
 **
 **  Arguments:    Buffer for date/time string
 **
 **  Returns:      Current time in secs
 **/
u_int32 RTC_GetDateTime(char *pcDateTime)
{
  u_int32 ulSecs;

  //
  // Get the ASCII date and time string from the global total-secs including THIS timezone
  //
  ulSecs = RTC_GetSystemSecs();
  //
  // Rework total secs to date/time, include daylight saving !!!
  //
  RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM, pcDateTime, ulSecs);
  return(ulSecs);
}

/**
 **  Name:         RTC_GetDateTimeSecs
 **
 **  Description:  Get the system date/time in hh:mm:ss in this time zone. 
 **
 **  Arguments:    Buffer for date/time string
 **
 **  Returns:      Current time in secs
 **/
u_int32 RTC_GetDateTimeSecs(char *pcDateTime)
{
  u_int32 ulSecs;

  //
  // Get the ASCII date and time string from the global total-secs including THIS timezone
  //
  ulSecs = RTC_GetSystemSecs();
  //
  // Rework total secs to date/time, include daylight saving !!!
  //
  RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcDateTime, ulSecs);
  return(ulSecs);
}

/**
 **  Name:         RTC_ConvertDateTime
 **
 **  Description:  Rework a seconds count to ascii date/time string
 ** 
 **  Arguments:    Format, Buffer for date/time string, base seconds
 **
 **  Returns:      void
 **/
void RTC_ConvertDateTime(TIMEFORMAT tFormat, char *pcDateTime, u_int32 ulSecs)
{
  CLOCK_DAY_TIME stTime;
  u_int8         ubDst;

  //
  //  Convert to string
  //
  if(pcDateTime)
  {
     switch(tFormat)
     {
        default:
           break;

        case TIME_FORMAT_WW_DD_MM_YYYY_HH_MM:
           // size=21 <Mo|29-08-2005|10:23>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%s|%02u-%02u-%u|%02u:%02u|",
                                   cDaysOfTheWeek[stTime.ubWeekDay],
                                   stTime.ubDay,
                                   stTime.ubMonth,
                                   stTime.ubYear + 1900,
                                   stTime.ubHour,
                                   stTime.ubMin);
           break;

        case TIME_FORMAT_WW_DD_MMM_YYYY:
           // size=14 <Mo 29 Aug 2005>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%s %02u %s %u",
                                   cDaysOfTheWeek[stTime.ubWeekDay],
                                   stTime.ubDay,
                                   cMonthOfTheYear[stTime.ubMonth],
                                   stTime.ubYear + 1900);
           break;

        case TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM:
           // size=20 <Mo 29 Aug 2005 14:56>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%s %02u %s %u %02u:%02u",
                                   cDaysOfTheWeek[stTime.ubWeekDay],
                                   stTime.ubDay,
                                   cMonthOfTheYear[stTime.ubMonth],
                                   stTime.ubYear + 1900,
                                   stTime.ubHour,
                                   stTime.ubMin);
           break;

        case TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS:
           // size=22 <Mo|29-08-2005|14:56:34>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%s|%02u-%02u-%u|%02u:%02u:%02u",
                                   cDaysOfTheWeek[stTime.ubWeekDay],
                                   stTime.ubDay,
                                   stTime.ubMonth,
                                   stTime.ubYear + 1900,
                                   stTime.ubHour,
                                   stTime.ubMin,
                                   stTime.ubSec);
           break;

        case TIME_FORMAT_WW_DD_MM_YYYY:
           // size=13 <Mo 29-08-2005>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%s %02u-%02u-%u",
                                   cDaysOfTheWeek[stTime.ubWeekDay],
                                   stTime.ubDay,
                                   stTime.ubMonth,
                                   stTime.ubYear + 1900);
           break;

        case TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM:
           // size=21 <Mo(Z)29-08-2005 14:56>
           ubDst = rtc_DaylightSavingTime(ulSecs)?1:0;
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%s(%s)%02u-%02u-%u %02u:%02u",
                                   cDaysOfTheWeek[stTime.ubWeekDay],
                                   cDaylight[ubDst],
                                   stTime.ubDay,
                                   stTime.ubMonth,
                                   stTime.ubYear + 1900,
                                   stTime.ubHour,
                                   stTime.ubMin);
           break;

        case TIME_FORMAT_DD_MM_YYYY_HH_MM:
           // size=16 <29-08-2005 14:56>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%02u-%02u-%u %02u:%02u",
                                   stTime.ubDay,
                                   stTime.ubMonth,
                                   stTime.ubYear + 1900,
                                   stTime.ubHour,
                                   stTime.ubMin);
           break;

        case TIME_FORMAT_DD_MM_YYYY:
           // size=10 <29-08-2005>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%02u-%02u-%u",
                                   stTime.ubDay,
                                   stTime.ubMonth,
                                   stTime.ubYear + 1900);
           break;

        case TIME_FORMAT_HH_MM_SS:
           // size=8 <14:56:38>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%02u:%02u:%02u",
                                   stTime.ubHour,
                                   stTime.ubMin,
                                   stTime.ubSec);
           break;

        case TIME_FORMAT_WW_YYYY_MM_DD:
           // size=10 <Mo20050829>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%s%04u%02u%02u",
                                   cDaysOfTheWeek[stTime.ubWeekDay],
                                   stTime.ubYear + 1900,
                                   stTime.ubMonth,
                                   stTime.ubDay);
           break;

        case TIME_FORMAT_YYYY_MM_DD_WW:
           // size=11 <20050829_Mo>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%04u%02u%02u_%s",
                                   stTime.ubYear + 1900,
                                   stTime.ubMonth,
                                   stTime.ubDay,
                                   cDaysOfTheWeek[stTime.ubWeekDay]);
           break;

           case TIME_FORMAT_YYYY_MM_DD_HH_MM_WW:
           // size=16 <20050829_1255_Mo>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%04u%02u%02u_%02u%02u_%s",
                                   stTime.ubYear + 1900,
                                   stTime.ubMonth,
                                   stTime.ubDay,
                                   stTime.ubHour,
                                   stTime.ubMin,
                                   cDaysOfTheWeek[stTime.ubWeekDay]);
           break;

           case TIME_FORMAT_YYYY_MM_DD_HH_MM_SS:
           // size=15 <20050829_125559>
           rtc_ClockFormat(DST_YES, ulSecs, &stTime);
           ES_sprintf(pcDateTime, "%04u%02u%02u_%02u%02u%02u",
                                   stTime.ubYear + 1900,
                                   stTime.ubMonth,
                                   stTime.ubDay,
                                   stTime.ubHour,
                                   stTime.ubMin,
                                   stTime.ubSec);
           break;
     }
  }
}

/**
 **  Name:         RTC_ConvertDateTimeSzie
 **
 **  Description:  Return formsat size of ascii date/time string
 ** 
 **  Arguments:    Format
 **
 **  Returns:      Size in bytes (incl terminator)
 **/
int RTC_ConvertDateTimeSize(TIMEFORMAT tFormat)
{
   int iSize=0;

   switch(tFormat)
   {
      default:
         break;

      case TIME_FORMAT_WW_DD_MM_YYYY_HH_MM:
         // size=21 <Mo|29-08-2005|10:23>
         iSize = 21;
         break;

      case TIME_FORMAT_WW_DD_MMM_YYYY:
         // size=14 <Mo 29 Aug 2005>
         iSize = 14;
         break;

      case TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM:
         // size=20 <Mo 29 Aug 2005 14:56>
         iSize = 20;
         break;

      case TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS:
         // size=22 <Mo|29-08-2005|14:56:34>
         iSize = 22;
         break;

      case TIME_FORMAT_WW_DD_MM_YYYY:
         // size=13 <Mo 29-08-2005>
         iSize = 13;
         break;

      case TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM:
         // size=21 <Mo(Z)29-08-2005 14:56>
         iSize = 21;
         break;

      case TIME_FORMAT_DD_MM_YYYY_HH_MM:
         // size=16 <29-08-2005 14:56>
         iSize = 16;
         break;

      case TIME_FORMAT_DD_MM_YYYY:
         // size=10 <29-08-2005>
         iSize = 10;
         break;

      case TIME_FORMAT_HH_MM_SS:
         // size=8 <14:56:38>
         iSize = 8;
         break;

      case TIME_FORMAT_WW_YYYY_MM_DD:
         // size=10 <Mo20050829>
         iSize = 10;
         break;

      case TIME_FORMAT_YYYY_MM_DD_WW:
         // size=11 <20050829_Mo>
         iSize = 11;
         break;

         case TIME_FORMAT_YYYY_MM_DD_HH_MM_WW:
         // size=16 <20050829_1255_Mo>
         iSize = 16;
         break;

         case TIME_FORMAT_YYYY_MM_DD_HH_MM_SS:
         // size=15 <20050829_125559>
         iSize = 15;
         break;
   }
   return(iSize+1);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 ** rtc_ClockMJDtoYMD()
 *
 *  PARAMETERS:   ulMJD    - Modified Julian Date.
 *                pulYear  - Argument in which the year is set.
 *                           Argement value become related to ulMJD and is years since 1900.
 *                pulMonth - Argument in which the month is set.
 *                           Argement value becomes related to ulMJD, range: 1 - 12.
 *                pulDay   - Argument in which the day (related to ulMJD) is set.
 *                           Argement value becomes related to ulMJD, range: 1 - 31.
 *
 *  DESCRIPTION:  Converts MJD to year, month and day.
 *
 *  RETURNS:      None
 *
 */
static void rtc_ClockMJDtoYMD(u_int32 ulMJD, u_int32 *pulYear, u_int32 *pulMonth, u_int32 *pulDay)
{
   u_int32 ulV1, ulV2, ulV3;
   u_int32 ulY1, ulM1;
   u_int32 ulK = 0;

   // calculate using integer formulas
   // year calculation from MPEG specification:
   //   Y' = int([MJD - 15078.2] / 365.25)
   // method: (in 10 radix)
   //   Y' = MJD*8 - 15078.2*8
   //   Y' = Y'/ (365.25 * 8)
   //
   //
   ulY1 = (u_int32)(((ulMJD << 3)   - 120626L)/2922);


   // month calculation from MPEG specification
   //   M' = int([MJD - 14956.1 - int(Y' * 365.25)] / 30.6001)
   // method: (in 10 radix)
   //   M' = MJD*8 - 14956.1*8 - int(Y' * 365.25*8)
   //   M' = (M'* 512) / (30.6001 * 512 * 8)
   //
   ulV1 = 2922L;

   ulM1 = (u_int32)((((ulMJD << 3) - 119649L - (0xFFFFFFF8l&(ulY1 * ulV1))) << 9) / 125338L);

   if ((ulM1 == 14) || (ulM1 == 15))
   {
        ulK = 1;
   }
   *pulYear = (u_int32)(ulY1 + ulK);

   *pulMonth = (u_int32)(ulM1 - 1 - ulK*12);


   //
   // day calculation from MPEG specification:
   //   D = MJD - 14956 - int(y' * 365.25) - int(m' * 30.6001)
   // method:  (in 10 radix)
   //   D = (MJD*16 - 14956*16 - int(y1*365.25*16))*256 - int(m1*30.6001*4096)
   //   D = D / 4096
   //      integer conversions are done by masking off all bits below the
   //      shift amount:     FFFF000 if term multiplied by 4096
   //                        FFFFFF0 if term multiplied by 16
   //                        FFFFFF8 if term multiplied by 8
   //
   ulV1 = 239296L;
   ulV2 = 5844L;
   ulV3 = 125338L;

   *pulDay = (u_int32)(((int32)((   ((int32) (ulMJD << 4)) - ulV1
                                -   ((int32)((0xFFFFFFF0L)&(ulY1*ulV2)))  ) << 8)
                                -   (int32)  (0xFFFFF000L &(ulM1*ulV3))   ) >> 12);

}


/*
 ** rtc_ClockMJDtoDofW()
 *
 *  PARAMETERS:   ulMJD       - Modified Julian Date.
 *                pulWeekDay  - Argument is set to the day of the week (related to ulMJD).
 *
 *  DESCRIPTION:  Converts MJD to the day of the week.
 *                Sunday = 0, ....., Saturday = 6.
 *
 *  RETURNS:      None
 *
 */
static void rtc_ClockMJDtoDofW(u_int32 ulMJD, u_int32 *pulWeekDay)
{
   //
   // day of week calculation from MPEG specification:
   //   WD = [ (MJD + 2)mod 7]
   // method:
   //   WD = ((MJD + 2) % 7)
   //   *wd = 0 (sunday) to 6 (saturday)
   //
   *pulWeekDay = (ulMJD + 3) % 7;
}

/*
 ** rtc_ClockUTCtoHMS()
 *
 *  PARAMETERS:   ulUTC          - Universal Time Co-ordinated
 *                pubHour (out)  - Number of Hours (0 .. 23)
 *                pubMin (out)   - Number of Minutes (0 .. 59)
 *                pubSec (out)   - Number of Seconds (0 .. 59)
 *
 *  DESCRIPTION:  Convertes an UTC represented time to HMS represented time.s
 *
 *  RETURNS:      None
 *
 */
static void rtc_ClockUTCtoHMS(u_int32 ulUTC, u_int8 *pubHour, u_int8 *pubMin, u_int8 *pubSec)
{
   u_int32 ulCurrentSeconds = ulUTC;

   // calculate total seconds into usable parts
   ulCurrentSeconds  = (u_int32)(ulCurrentSeconds % ES_SECONDS_PER_DAY    );
   *pubHour          = (u_int8 )(ulCurrentSeconds / ES_SECONDS_PER_HOUR   );
   ulCurrentSeconds  = (u_int32)(ulCurrentSeconds % ES_SECONDS_PER_HOUR   );
   *pubMin           = (u_int8 )(ulCurrentSeconds / ES_SECONDS_PER_MINUTE );
   *pubSec           = (u_int8 )(ulCurrentSeconds % ES_SECONDS_PER_MINUTE );
}

/******************************************************************************
 * Name:    rtc_ClockFormat
 *
 * Purpose: This function will convert a date/time specified in seconds since
 *          1-1-1970 to a formatted date/time.
 *
 * Input:   DST YesNo, ulSeconds - Date specified in seconds since 1-1-1970
 *
 * Output:  pstDayTime - Formatted date/time
 *
 * Returns: ---
 *
 * Note:
******************************************************************************/
static void rtc_ClockFormat(bool fDST, u_int32 ulSeconds, CLOCK_DAY_TIME *pstDayTime)
{
   u_int32 ulMJD;
   u_int32 ulUTC;
   u_int32 ulYear;
   u_int32 ulMonth;
   u_int32 ulDay;
   u_int32 ulWeekDay;

   if(fDST)
   {
      //
      // Take daylight saving into account.
      //
      if( rtc_DaylightSavingTime(ulSeconds) )
      {
         ulSeconds += ES_SECONDS_PER_HOUR;
      }
   }
   ulMJD  = ulSeconds / ES_SECONDS_PER_DAY;
   ulMJD += ES_MJD_1970_OFFSET;
   ulUTC  = ulSeconds % ES_SECONDS_PER_DAY;

   rtc_ClockMJDtoYMD (ulMJD, &ulYear, &ulMonth, &ulDay);
   rtc_ClockMJDtoDofW(ulMJD, &ulWeekDay);
   rtc_ClockUTCtoHMS (ulUTC, &pstDayTime->ubHour, &pstDayTime->ubMin, &pstDayTime->ubSec);

   pstDayTime->ubMonth   = (u_int8)ulMonth;
   pstDayTime->ubDay     = (u_int8)ulDay;
   pstDayTime->ubYear    = (u_int8)ulYear;   // + 1900;
   pstDayTime->ubWeekDay = (u_int8)ulWeekDay;
}

/*
 *
 *  Function:     rtc_DaylightSavingTime
 *  Description:  Handle daylight saving time
 *
 *  Arguments:    Current secs (since 1970)
 *  Returns:      TRUE if daylight saving time
 *
 */
static bool rtc_DaylightSavingTime(u_int32 ulSecs)
{
   static u_int32 ulDSTstart = ES_SECS_DST_2004_START;
   static u_int32 ulDSTend   = ES_SECS_DST_2004_END;

   bool fCC = FALSE;

   while(ulSecs > ulDSTend)
   {
      //
      // we need a new start-end of the next Daylight saving:
      //
      //    Region         Start                         Stop
      //---------------------------------------------------------------------------------
      //    EU:            Last Sunday in March          Last Sunday in October 
      //                   at 03:00                      at 03:00
      //
      // DST-start-2004 = A (ES_SECS_DST_2004_START)
      // DST-end  -2004 = B (ES_SECS_DST_2004_END)
      // Do
      // {
      //      A += ES_SECONDS_PER_WEEK
      //      ThisMonth = MJDtoYMD(A)
      //      NextMonth = MJDtoYMD(A+ES_SECONDS_PER_WEEK)
      //      if( (ThisMonth == Mar) && (NextMonth = Apr) ) DST-Start-next = A;
      //      if( (ThisMonth == Oct) && (NextMonth = Dec) ) DST-End-next   = A;
      // }
      // While( (SecsNow > DST-Start-next) && (SecsNow < DST-End-next) )
      //
      PRINTF3("rtc:Secs=%lu, DST Start-End = %lu - %lu"CRLF, ulSecs, ulDSTstart, ulDSTend);
      
      ulDSTstart = rtc_FindNextDaylightSavingTime(ulDSTstart, 3);
      ulDSTend   = rtc_FindNextDaylightSavingTime(ulDSTend,  10);
      TIME_TEST();
   }

   //
   // Check if the current time needs DST correction
   //
   if( (ulSecs > ulDSTstart) && (ulSecs < ulDSTend) ) 
   {
      PRINTF("rtc:Daylight saving time"CRLF);
      fCC = TRUE;
   }
   return(fCC);
}

/*
 *
 *  Function:     rtc_FindNextDaylightSavingTime
 *  Description:  Find the next daylight saving time period
 *
 *  Arguments:    secs start, month 
 *  Returns:      second when the next DST period starts
 *
 */
static u_int32 rtc_FindNextDaylightSavingTime(u_int32 ulA, u_int8 ubMonth)
{
   bool           fMonthFound = FALSE;
   bool           fFound      = FALSE;
   u_int32        ulB         = ulA;
   CLOCK_DAY_TIME stTime;

   while(!fFound)
   {
      ulA += ES_SECONDS_PER_WEEK;               // Skip 1 week
      rtc_ClockFormat(DST_NO, ulA, &stTime);    // Get month (and the rest)
      if(fMonthFound)
      {                                         // Already correct month
         if(stTime.ubMonth == ubMonth)
         {                                      // Correct month found
            fFound = TRUE;                      // FOUND new time
         }
         else
         {                                      // Not right month
            ulB = ulA;                          // Save current time
         }
      }
      else                                      // Not right month
      {
         if(stTime.ubMonth == ubMonth)
         {
            ubMonth++;                          // Set for next month
            fMonthFound = TRUE;                 // MONTH found!
         }
         else
         {                                      // Not right month
            ulB = ulA;                          // Save current time
         }
      }
   }
#ifdef   FEATURE_TEST_TIME
   rtc_ClockFormat(DST_NO, ulB, &stTime);
   ES_printf("time:DST:%s %02u-%02u-%u %02u:%02u"CRLF,
                        cDaysOfTheWeek[stTime.ubWeekDay],
                        stTime.ubDay,
                        stTime.ubMonth,
                        stTime.ubYear + 1900,
                        stTime.ubHour,
                        stTime.ubMin);
#endif   //FEATURE_TEST_TIME
   return(ulB);
}

#ifdef   FEATURE_TEST_TIME
/*
 *
 *  Function:     rtc_TestDST
 *  Description:  test the DST functions
 *
 *  Arguments:    void
 *  Returns:      void
 *
 */
static void rtc_TestDST(void)
{
   u_int16        usLoop  = 10;
   u_int32        ulA, ulB;
   
   ulA = ES_SECS_DST_2004_START;
   ulB = ES_SECS_DST_2004_END;

   while(usLoop--)
   {
      ulA = rtc_FindNextDaylightSavingTime(ulA,  3);
      ulB = rtc_FindNextDaylightSavingTime(ulB, 10);
      // skip a year
      ulA = rtc_TestDSTSkipYear(ulA,  3);
      ulB = rtc_TestDSTSkipYear(ulB, 10);
   }
}
/*
 *
 *  Function:     rtc_TestDSTSkipYear
 *  Description:  Skip a year to test the DST functions
 *
 *  Arguments:    secs
 *  Returns:      void
 *
 */
static u_int32 rtc_TestDSTSkipYear(u_int32 ulSecs, u_int8 ubMonth)
{
   CLOCK_DAY_TIME stTime;
   
   do
   {
      // skip till this month ends
      ulSecs += ES_SECONDS_PER_WEEK;               // Skip 1 week
      rtc_ClockFormat(DST_NO, ulSecs, &stTime);    // Get month (and the rest)
   }
   while(stTime.ubMonth == ubMonth);
   
   ubMonth++;
   
   do
   {
      // skip until month starts
      ulSecs += ES_SECONDS_PER_WEEK;               // Skip 1 week
      rtc_ClockFormat(DST_NO, ulSecs, &stTime);    // Get month (and the rest)
   }
   while(stTime.ubMonth != ubMonth);

   return(ulSecs);
}

#endif   //FEATURE_TEST_TIME
