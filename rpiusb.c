/*  (c) Copyright:  2016  Patrn.nl, Confidential Data
**
**  $Workfile:          rpiusb
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            USB Interface functions
**
**  Entry Points:       
**
**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       1 Apr 2016
**
 *  Revisions:
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/select.h>
#include <sys/signal.h>
#include <sys/types.h>

#include "config.h"
#include "echotype.h"
#include "safecalls.h"
#include "log.h"
#include "globals.h"
#include "rpicnc.h"
#include "rpiusb.h"

#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static int     usb_Exit                ();
static int     usb_Init                ();
static char   *usb_StoreString         (void *, bool);

/*------ functions separator ------------------------------------------------
____Global_Functions()  {};
----------------------------------------------------------------------------*/

//
//  Function:  USB_Dump
//  Purpose:   Dump the buffer
//
//  Parms:     Buffer, size, mode, Header
//  Returns:   
//
void USB_Dump(COMMS *pstCom, int iSize, DUMPMODE tMode, const char *pcHdr)
{
   switch(tMode)
   {
      default:
      case DUMP_HEX:
      {
         int      i;
         u_int8  *pubBuffer=pstCom->pubUsb;

         for(i=0; i<iSize;i++)
         {
            if( (i % 16) == 0) ES_printf(CRLF);
            ES_printf("%02x ", pubBuffer[i]);
         }
         ES_printf(CRLF);
         break;
      }
   
      case DUMP_ASCII:
      {
         int      i;
         char     cChar;
         u_int8  *pubBuffer=pstCom->pubUsb;

         if(pcHdr) ES_printf("USB_Dump(%s):", pcHdr);
         else      ES_printf("USB_Dump():");
         //
         for(i=0; i<iSize;i++)
         {
            cChar = *pubBuffer++;
            if(cChar == 0) break;
            else           ES_printf("%c", cChar);
         }
         break;
      }
      case DUMP_REPLY:
         if(pcHdr) ES_printf("USB_Dump(%s):%s" CRLF, pcHdr, pstCom->pubUsb);
         else      ES_printf("USB_Dump():%s" CRLF, pstCom->pubUsb);
         break;

      case DUMP_COMMS:
      {
         if(pcHdr) ES_printf("USB_Dump(%s):" CRLF, pcHdr);
         else      ES_printf("USB_Dump():" CRLF);
         //
         ES_printf("          Status: %d"                  CRLF, pstCom->ubStatus);
         ES_printf("         Command: %d"                  CRLF, pstCom->ubCommand);
         ES_printf("            Line: %ld"                 CRLF, pstCom->lLineNr);
         ES_printf("           Reply: %ld"                 CRLF, pstCom->pubUsb);
         ES_printf(CRLF);
         break;
      }
   }
}

//
//  Function:  USB_Close
//  Purpose:   Close USB devide
//
//  Parms:     Device info struct
//  Returns:   true if OKee 
//
bool USB_Close(RPIUSB *pstUsb)
{
   bool  bCc=FALSE;

   if(pstUsb)
   {
      hid_close(pstUsb->ptHandle);
      pstUsb->ptHandle = NULL;
      bCc = TRUE;
   }
   return(bCc);
}

//
//  Function:  USB_GetInfo
//  Purpose:   Get info for all connected USB devices
//
//  Parms:     iVid, iPid 
//  Returns:   Linked list of all USB devices
//
RPIUSB *USB_GetInfo(int iVid, int iPid)
{
   int                     iNr=0;
   RPIUSB                 *pstTop=NULL;
   RPIUSB                 *pstUsb;
   struct hid_device_info *pstDevs, *pstCurDev;

   //
   // Enumerate all HID devices on the system
   //
   usb_Init();
   pstDevs   = hid_enumerate(iVid, iPid);
   pstCurDev = pstDevs; 
   //
   while(pstCurDev) 
   {
      if(pstTop == NULL)
      {
         pstTop = safemalloc(sizeof(RPIUSB));
         pstUsb = pstTop;
      }
      else
      {
         pstUsb->pstNext = safemalloc(sizeof(RPIUSB));
         pstUsb          = pstUsb->pstNext;
      }
      pstUsb->iIndex         = iNr;
      pstUsb->iVendorId      = (int) pstCurDev->vendor_id;
      pstUsb->iProductId     = (int) pstCurDev->product_id;
      pstUsb->iInterfaceNr   = (int) pstCurDev->interface_number;
      //
      pstUsb->pcDevicePath   = usb_StoreString(pstCurDev->path, FALSE);
      pstUsb->pcManufacturer = usb_StoreString(pstCurDev->manufacturer_string, TRUE);
      pstUsb->pcProduct      = usb_StoreString(pstCurDev->product_string, TRUE);
      pstUsb->pcSerialNr     = usb_StoreString(pstCurDev->serial_number, TRUE);
      //
      PRINTF ("USB_GetInfo():Device Found :" CRLF);
      PRINTF1("   Path          : %s"    CRLF, pstUsb->pcDevicePath);
      PRINTF1("   Vendor  ID    : %04x"  CRLF, pstUsb->iVendorId);
      PRINTF1("   Product ID    : %04x"  CRLF, pstUsb->iProductId);
      PRINTF1("   Manufacturer  : %s"    CRLF, pstUsb->pcManufacturer);
      PRINTF1("   Product       : %s"    CRLF, pstUsb->pcProduct);
      PRINTF1("   Serial nr     : %s"    CRLF, pstUsb->pcSerialNr);
      PRINTF1("   Interface     : %d"    CRLF, pstUsb->iInterfaceNr);
      //
      iNr++;
      pstCurDev = pstCurDev->next;
   }
   hid_free_enumeration(pstDevs);
   usb_Exit();
   return(pstTop);
}

//
//  Function:  USB_GetDeviceInfo
//  Purpose:   Get info struct of a USB device in the linked list
//
//  Parms:     Linked list
//             Vendor ID
//             Product ID
//             Interface Number
//  Returns:   Device struct or NULL
//
RPIUSB *USB_GetDeviceInfo(RPIUSB *pstUsb, int iVid, int iPid, int iIntfNr)
{
   while(pstUsb)
   {
      if( (pstUsb->iVendorId == iVid) && (pstUsb->iProductId == iPid) && (pstUsb->iInterfaceNr == iIntfNr) )
      {
         break;
      }
      pstUsb = pstUsb->pstNext;
   }
   return(pstUsb);
}

//
//  Function:  USB_Open
//  Purpose:   Open USB devide
//
//  Parms:     Linked list
//  Returns:   true if OKee 
//
bool USB_Open(RPIUSB *pstUsb)
{
   bool        bCc=FALSE;
   hid_device *ptHandle;

   if(pstUsb)
   {
      ptHandle = hid_open_path(pstUsb->pcDevicePath);
      if(ptHandle)
      {
         hid_set_nonblocking(ptHandle, 0);
         pstUsb->ptHandle = ptHandle;
         bCc = TRUE;
      }
   }
   return(bCc);
}

//
//  Function:  USB_Read
//  Purpose:   Read data from USB devide
//
//  Parms:     Device Info struct, Buffer, Size, timeout
//  Returns:   Number read, or -1 on error
//
int USB_Read(RPIUSB *pstUsb, u_int8 *pubBuffer, int iSize, int iTimeout)
{
   int            iNr;
   const wchar_t *pwError;

   iNr = hid_read_timeout(pstUsb->ptHandle, pubBuffer, iSize, iTimeout);
   if(iNr == -1)
   {
      pwError = hid_error(pstUsb->ptHandle);
      if(pwError)
      {
         PRINTF1("USB_Read(): ERROR hid_read(): %ls" CRLF, pwError);
      }
      else
      {
         PRINTF("USB_Read(): ERROR hid_read(): unspecified error !!" CRLF);
      }
   }
   return(iNr);
}

//
//  Function:  USB_Write
//  Purpose:   Write data to USB devide
//
//  Parms:     Device Info struct, Buffer, Size
//  Returns:   Number written, or -1 on error
//
int USB_Write(RPIUSB *pstUsb, u_int8 *pubBuffer, int iSize)
{
   int            iNr;
   const wchar_t *pwError;

   iNr = hid_write(pstUsb->ptHandle, pubBuffer, iSize);
   if(iNr == -1)
   {
      pwError = hid_error(pstUsb->ptHandle);
      if(pwError)
      {
         PRINTF1("USB_Write(): ERROR hid_read(): %ls" CRLF, pwError);
      }
      else
      {
         PRINTF("USB_Write(): ERROR hid_read(): unspecified error !!" CRLF);
      }
   }
   return(iNr);
}

//
//  Function:  USB_ReleaseInfo
//  Purpose:   Release all linked list info
//
//  Parms:     Linked list
//  Returns:    
//
void USB_ReleaseInfo(RPIUSB *pstUsb)
{
   RPIUSB  *pstCur;

   while(pstUsb)
   {
      pstCur = pstUsb;
      pstUsb = pstUsb->pstNext;
      //
      safefree(pstCur->pcDevicePath);
      safefree(pstCur->pcManufacturer);
      safefree(pstCur->pcProduct);
      safefree(pstCur->pcSerialNr);
      safefree(pstCur);
   }
}

/*------ functions separator ------------------------------------------------
____Local_usb_Functions()  {};
----------------------------------------------------------------------------*/

//
//  Function:     usb_Init
//  Description:  Init usb hid api
//
//  Arguments:    
//  Returns:      0 on success, -1 on error
//
//
static int usb_Init()
{
   return( hid_init() );
}


//
//  Function:     usb_StoreString
//  Description:  
// 
//  Arguments:    
//  Returns:      
// 
// 
static char *usb_StoreString(void *pvData, bool fLongString)
{
   int   iLen;
   char *pcDest;

   if(fLongString)
   {
      // Source is a long string
      iLen   = wcslen((wchar_t *) pvData);
      pcDest = safemalloc(iLen+1);
      ES_sprintf(pcDest, "%ls", (wchar_t *)pvData);
   }
   else
   {
      // Source is a normal string
      iLen   = strlen((char *) pvData);
      pcDest = safemalloc(iLen+1);
      ES_sprintf(pcDest, "%s", (char *)pvData);
   }
   return(pcDest);
}

//
//  Function:     usb_Exit
//  Description:  Exit usb hid api
// 
//  Arguments:    
//  Returns:      0 on success, -1 on error
// 
// 
static int usb_Exit()
{
   return( hid_exit() );
}



