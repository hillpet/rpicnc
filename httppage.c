/*  (c) Copyright:  2012  Patrn, Confidential Data 
 *
 *  $Workfile:          httppage.cpp
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Dynamic HTTP page for RPi webserver
 *
 *
 *  Entry Points:       HTTP_RequestStaticPage()
 *                      HTTP_CollectGet()
 *                      HTTP_CollectGetParameter()
 *
 *
 *  Compiler/Assembler: gnu-g++
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16Jul2012
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "echotype.h"
#include "dynpage.h"
#include "httppage.h"
#include "globals.h"

//#define USE_PRINTF
#include "printf.h"


//
// Local prototypes
//
static FTYPE   http_CollectHandleExtension   (NETCL *);
static bool    http_HandleHtml               (NETCL *);
static bool    http_HandleJs                 (NETCL *);
static bool    http_HandleJpg                (NETCL *);
static bool    http_HandleBmp                (NETCL *);
static bool    http_HandleGif                (NETCL *);
static bool    http_HandlePng                (NETCL *);
static bool    http_HandleTxt                (NETCL *);
static bool    http_HandleMp3                (NETCL *);
static bool    http_HandleHdtv               (NETCL *);
static bool    http_HandleJson               (NETCL *);
static bool    http_HandleRequest            (NETCL *);
//
static RTYPE   http_ParseRequestHeader       (NETCL *, char *);
static int     http_ReqHdrGetValue           (NETCL *, char *);
static RTYPE   http_ReqHdrAcceptEncoding     (NETCL *, char *, int);
static RTYPE   http_ReqHdrContentType        (NETCL *, char *, int);
static RTYPE   http_ReqHdrContentLength      (NETCL *, char *, int);

//
// Server Home Dir
//
static char pcHomeDir[]                    = RPI_PUBLIC_WWW;
static char pcCachedDir[]                  = "";   //RPI_CACHED_WWW;

static char pcHttpContentTypeHtml[]        = "text/html";
static char pcHttpContentTypeJs[]          = "application/js";
static char pcHttpContentTypeJpg[]         = "image/jpeg";
static char pcHttpContentTypeBmp[]         = "image/bmp";
static char pcHttpContentTypePng[]         = "image/png";
static char pcHttpContentTypeGif[]         = "image/gif";
static char pcHttpContentTypeMp3[]         = "image/mp3";
static char pcHttpContentTypeMp4[]         = "image/mp4";
static char pcHttpContentTypeH264[]        = "image/h264";
static char pcHttpContentTypeTxt[]         = "image/txt";
static char pcHttpContentTypeZip[]         = "application/zip";
static char pcHttpContentTypeJson[]        = "application/json";

static char pcHttpRequestHeaderOKee[]      = "HTTP/1.0 200 OK\r\nContent-Type:%s\r\n";
static char pcHttpRequestHeaderBad[]       = "HTTP/1.0 400 Bad Request\r\n";
static char pcHttpRequestHeaderForbidden[] = "HTTP/1.0 403 Forbidden\r\n";
static char pcHttpRequestHeaderError[]     = "HTTP/1.0 404 Not Found\r\n";
static char pcHttpRequestHeaderConnect[]   = "Connection:Closed\r\n\r\n";

static HTTPTYPE stHttpTypes[] =
{
//   pcExt           tType          pcType                  pfExt   
   { "gen",          HTTP_GEN,      pcHttpContentTypeHtml,  0                       },       // generic files
   { "shtml",        HTTP_SHTML,    0,                      0                       },       // ssi    shtml and files without ext.
   { "html",         HTTP_HTML,     pcHttpContentTypeHtml,  http_HandleHtml         },       // html   html pages
   { "htm",          HTTP_HTML,     pcHttpContentTypeHtml,  http_HandleHtml         },       // html   html pages
   { "js",           HTTP_JS,       pcHttpContentTypeJs,    http_HandleJs           },       // js     JavaScript files
   { "cgi",          HTTP_CGI,      0,                      0                       },       // cgi    cgi script files
   { "gif",          HTTP_GIF,      pcHttpContentTypeGif,   http_HandleGif          },       // gif    images
   { "jpg",          HTTP_JPG,      pcHttpContentTypeJpg,   http_HandleJpg          },       // jpg    images
   { "bmp",          HTTP_BMP,      pcHttpContentTypeBmp,   http_HandleBmp          },       // bmp    images
   { "png",          HTTP_PNG,      pcHttpContentTypePng,   http_HandlePng          },       // Png    images
   { "mp3",          HTTP_MP3,      pcHttpContentTypeMp3,   http_HandleMp3          },       // mp3    audio
   { "mp4",          HTTP_MP4,      pcHttpContentTypeMp4,   http_HandleHdtv         },       // mp4    HDTV Video
   { "h264",         HTTP_H264,     pcHttpContentTypeH264,  http_HandleHdtv         },       // h264   HDTV Video
   { "txt",          HTTP_TXT,      pcHttpContentTypeTxt,   http_HandleTxt          },       // txt    Unformatted
   { "json",         HTTP_JSON,     pcHttpContentTypeJson,  http_HandleJson         },       // json   JSON files
   { "zip",          HTTP_ZIP,      pcHttpContentTypeZip,   0                       },       // zip    files
   { "css",          HTTP_CSS,      pcHttpContentTypeHtml,  0                       },       // css    files
   { "ico",          HTTP_ICO,      pcHttpContentTypeHtml,  0                       }        // ico    files
};
static int iNumHttpTypes = sizeof(stHttpTypes)/sizeof(HTTPTYPE);

//
// HTTP request headers
//
static HTTPREQ stHttpRequestHeaders[] =
{
   { "Accept-Encoding",    http_ReqHdrAcceptEncoding        },
   { "Content-Type",       http_ReqHdrContentType           },
   { "Content-Length",     http_ReqHdrContentLength         },
   {  NULL,                NULL                             }
};

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function    : HTTP_CollectRequest
// Description : Collect all incoming HTTP data and single out the pathname/parameters
//
// Parameters  : Client
// Returns     : Request type
// Note        : To bypass www location: use the @ designator to reditect to cached location
//
//    GET /http://RPI_IP_ADDRESS:8080/cam.json?command=record&parm_1&parm_2....&parm_n
//
//    State:   0..3:    Wait for "GET " or "POST "
//             4:       Start filename collection
//             5..7:    %AB escape sequence collection
//             8:       Wait/Store Filename.ext?parameter1=x&parameter2=y (handle http escape chars)
//             9..11:   Wait for "POST " 
//             12..13:  Parse request headers
//            -1:       Abort: wait till GET/POST ends 
//
RTYPE HTTP_CollectRequest(NETCL *pstCl)
{
   char     cChar;
   char     cEscapeChar;
   RTYPE    tType       = HTTP_CHAR;
   char    *pcUrl       = pstCl->pcUrl;
   int      iIdx        = pstCl->iPathIdx;
   int      iReqState   = pstCl->iReqState;

   cChar = pstCl->pcRcvBuffer[pstCl->iRcvBufferGet];

#ifdef   FEATURE_ECHO_HTTP_IN
   //
   // Echo all HTTP request data from the client
   //
   ES_printf("%c", cChar);
#endif   //FEATURE_ECHO_HTTP_IN

   //PRINTF2("HTTP_CollectRequest():s=%d (Char=%c)" CRLF, iReqState, cChar);
   
   switch(iReqState)
   {
      case 0:
         //
         if(cChar == 'G') iReqState = 1;
         if(cChar == 'P') iReqState = 9;
         break;
         
      case 1:
         if(cChar == 'E') iReqState++;
         else             iReqState=0;
         break;
         
      case 2:
         if(cChar == 'T') 
         {
            // HTTP GET Request
            tType     = HTTP_GET;
            iReqState = 3;
         }
         else iReqState = 0;
         break;
         
      case 3:
         if(cChar == ' ') 
         {
            iReqState++;
         }
         else iReqState=0;
         break;
      
      //
      // Start URL collector: 
      //   
      case 4:
         if(cChar == '/') break;             // Skip '/' 
         if(cChar == '%')                    //
         {
            iReqState = 5;                   // %AB escape sequence
         }
         else
         if(cChar == ' ') 
         {
            pcUrl[iIdx] = 0;                 // Terminate buffer
            iReqState = 12;                  // No filename passed: Skip all next chars
         }
         else
         if(cChar == '@') 
         {
            //
            // Copy URL to pcPathname location:skip public WWW path here
            //
            strcpy(pcUrl, pcCachedDir);
            iIdx = (int) strlen(pcUrl);      // Add filename
            pstCl->iFileIdx  = iIdx;         // Filename index
            iReqState=8;                     // Start assembling the filename
         }
         else
         {
            //
            // Copy URL to pcPathname location. Insert the public WWW path first:
            //
            strcpy(pcUrl, pcHomeDir);
            iIdx = (int) strlen(pcUrl);      // Add filename
            pstCl->iFileIdx  = iIdx;         // Filename index
            pcUrl[iIdx++] = cChar;           // This is the filename start: store in the buffer
            iReqState=8;                     // Start assembling the filename
         }
         break;
         
      //
      // %AB escape sequence handler
      //   
      case 5:
      case 6:
         cEscapeChar = pcUrl[iIdx];
         //
         if( (cChar>='0') && (cChar<='9') )
         {
            cEscapeChar <<= 4;
            cEscapeChar += (cChar - 48);
         }
         else if( (cChar>='A') && (cChar<='F') )
         {
            cEscapeChar <<= 4;
            cEscapeChar += (cChar - 55);
         }
         else if( (cChar>='a') && (cChar<='f') )
         {
            cEscapeChar <<= 4;
            cEscapeChar += (cChar - 87);
         }
         pcUrl[iIdx] = cEscapeChar;
         iReqState++;
         break;
         
      //
      // %AB escape sequence collection ready: Update index
      //   
      case 7:
         iIdx++;
         iReqState++;
         //
         // Fall through after updating the index
         // Main filename collector: translate all %AB into 0xAB
         //   
      case 8:
         if(cChar == '%') 
         {
            pcUrl[iIdx] = 0;                    // clear %char accumalator
            iReqState= 5;                       // %AB escape sequence
         }
         else if(cChar == ' ') 
         {
            //
            // URL end.
            //
            pcUrl[iIdx] =  0;                   // clear %char accumalator
            iReqState   = 12;                // filename end: parse request header
            //PRINTF1("HTTP_CollectRequest():File=%s" CRLF, pcUrl);
         }
         else
         {
            if(iIdx < MAX_URL_LEN) 
            {
               //
               //  buffer : [/dir1/dir2/filename.ext?parameter1=xxx&parameter2=yyy&...&parameterX=zzz]
               //                                ^  ^^
               //                                |  ||
               //                                |  |+--- iParmIdx 
               //                                |  +---- /0 terminator
               //                                +------- iExtnIdx  
               //
               if(cChar == '/')                          // Directory separator
               {
                  //PRINTF1("HTTP_CollectRequest():s=8 (/):i=%d" CRLF, iIdx);
                  pstCl->iFileIdx = iIdx+1;              // Overwrite filename index
               }
               else if(cChar == '.')                     // Start filename extension
               {
                  //PRINTF1("HTTP_CollectRequest():s=8 (.):i=%d" CRLF, iIdx);
                  pstCl->iExtnIdx = iIdx+1;              // Overwrite index to extension
               }
               else if(cChar == '?')                     // Start parameters
               {
                  //PRINTF1("HTTP_CollectRequest():s=8 (?):i=%d" CRLF, iIdx);
                  pstCl->iParmIdx = iIdx+1;             // store index to 1st parameter
                  cChar           = 0;                  // terminate filename.ext
               }
               pcUrl[iIdx++] = cChar;
            }
            else
            {
               tType     =  HTTP_ERROR;         // buffer overflow
               iReqState = -1;                  // Skip all next chars
            }
         }
         break;

      case 9:
         if(cChar == 'O') iReqState++;
         else             iReqState=0;
         break;

      case 10:
         if(cChar == 'S') iReqState++;
         else             iReqState=0;
         break;

      case 11:
         if(cChar == 'T') 
         {
            // HTTP POST Request
            tType     = HTTP_POST;
            iReqState = 3;
         }
         else iReqState = 0;
         break;

      case 12:
         //
         // GET/POST URL done: skip to CRLF, then parse request headers
         //
         if(cChar == '\n') 
         {
            pstCl->fReqEnd = TRUE;
            pstCl->iReqHdr = pstCl->iRcvBufferGet+1;
            iReqState++; 
         }
         break;

      case 13:
         if(cChar == '\n') 
         {
            if(pstCl->fReqEnd)
            {
               //
               // End of request
               //
               PRINTF("HTTP_CollectRequest():End" CRLF);
               tType = HTTP_END;
               iReqState = -1; 
            }
            else
            {
               pstCl->fReqEnd = TRUE;
               pstCl->iReqHdr = pstCl->iRcvBufferGet+1;
            }
         }
         else if(cChar == '\r') 
         {
            pstCl->pcRcvBuffer[pstCl->iRcvBufferGet] = 0;
            tType = http_ParseRequestHeader(pstCl, &pstCl->pcRcvBuffer[pstCl->iReqHdr]);
         }
         else pstCl->fReqEnd = FALSE;
         break;

      default:                                  // Sit here until input ends
         pcUrl[iIdx] = 0;                       // Terminate buffer (again)
         break;         
   }
   pstCl->iPathIdx   = iIdx;                    // Restore index
   pstCl->iReqState  = iReqState;               //         state
   pstCl->iRcvBufferGet++;                      // Receive handled
   return(tType);
}

//
// Function    : HTTP_CollectGetParameterValue
// Description : Collect incoming HTTP parameter value
//
// Parameters  : Client
// Returns     : NULL if no command is found, 
//                else asciiz pointer to command : "command=snapshot"
//                                                          ^
//               "/dir1/dir2/filename.ext?command=snapshot&parameter1=xxx&parameter2=yyy&...&parameterX=zzz"
//                                    ^  ^^               ^
//                                    |  ||               +---- /0 terminator after call
//                                    |  |+--- iParmIdx 
//                                    |  +---- /0 terminator
//                                    +------- iExtnIdx  
//
// 
char *HTTP_CollectGetParameterValue(NETCL *pstCl)
{
   bool     fSearching=TRUE;
   char    *pcUrl    = pstCl->pcUrl;
   int      iIdx     = pstCl->iParmIdx;
   char    *pcParm   = NULL;
   
   if(iIdx)
   {
      pcParm = &pcUrl[iIdx++];
      PRINTF1("HTTP_CollectGetParameterValue():<%s>" CRLF, pcParm);
      //
      while(fSearching)
      {
         switch(pcUrl[iIdx])
         {
            default:
               iIdx++;
               break;

            case 0:
               // This is the last parameter
               pstCl->iParmIdx = 0;
               fSearching = FALSE;
               break;

            case '=':
               // This is the parameter value
               pstCl->iParmIdx = iIdx+1;
               pcParm = HTTP_CollectGetParameter(pstCl);
               fSearching = FALSE;
               break;

            case '&':
               // This is already the next parameter (no value)
               pcUrl[iIdx++] = 0;
               pstCl->iParmIdx = iIdx;
               fSearching = FALSE;
               break;
         }
      }
   }
   return(pcParm);
}

//
// Function    : HTTP_CollectGetParameter
// Description : Collect incoming HTTP parameters
//
// Parameters  : Client
// Returns     : NULL if no parameter is found, 
//                else asciiz pointer to parameter --> "parameter1=xxxx"
// 
//  Note       : Called to retrieve subsequent parameters from the pcPathname buffer in 
//               the client struct.
//
//               "/dir1/dir2/filename.ext?parameter1=xxx&parameter2=yyy&...&parameterX=zzz"
//                                    ^  ^^             ^
//                                    |  ||             +---- /0 terminator after call
//                                    |  |+--- iParmIdx 
//                                    |  +---- /0 terminator
//                                    +------- iExtnIdx  
//
// 
char *HTTP_CollectGetParameter(NETCL *pstCl)
{
   bool     fSearching=TRUE;
   char    *pcUrl    = pstCl->pcUrl;
   int      iIdx     = pstCl->iParmIdx;
   char    *pcParm   = NULL;
   
   if(iIdx)
   {
      pcParm = &pcUrl[iIdx++];
      PRINTF1("HTTP_CollectGetParameter():<%s>" CRLF, pcParm);
      //
      while(fSearching)
      {
         switch(pcUrl[iIdx])
         {
            default:
               iIdx++;
               break;

            case 0:
               // This is the last parameter
               iIdx = 0;
               fSearching = FALSE;
               break;

            case '&':
               // This is the next parameter
               pcUrl[iIdx++] = 0;
               fSearching = FALSE;
               break;
         }
      }
      pstCl->iParmIdx = iIdx;
   }
   return(pcParm);
}

//
// Function    : HTTP_RequestStaticPage
// Description : Request access to a file
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
bool HTTP_RequestStaticPage(NETCL *pstCl)
{
   PRINTF1("HTTP_RequestStaticPage():<%s>" CRLF, pstCl->pcUrl);

   http_CollectHandleExtension(pstCl);

   return(TRUE);
}
/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function    : http_CollectHandleExtension
// Description : Return the filename extension
//
// Parameters  : Client
// Returns     : FTYPE
//
static FTYPE http_CollectHandleExtension(NETCL *pstCl)
{
   char    *pcExtType;
   char    *pcUrl     = pstCl->pcUrl;
   int      iIdx      = pstCl->iExtnIdx;
   FTYPE    tFileType = HTTP_GEN;
   PFEXT    pfHandler;
   
   if(iIdx)
   {
      char *pcExt = &pcUrl[iIdx];

      for(iIdx=0; iIdx<iNumHttpTypes; iIdx++)
      {
         pcExtType = stHttpTypes[iIdx].pcExt;
         PRINTF2("http_CollectHandleExtension():<%s> --> <%s>" CRLF, pcExt, pcExtType);
         if(strcmp(pcExt, pcExtType) == 0)
         {
            PRINTF(" Match !" CRLF);
            tFileType = stHttpTypes[iIdx].tType;
            //
            // Reply the correct http header
            //
            // "HTTP/1.0 200 OK"          CRLF
            // "Content-Length:12456"     CRLF
            // "Content-Type:text/html"   CRLF
            // "Connection:Close"         CRLF CRLF
            //
            pcExtType  = stHttpTypes[iIdx].pcType;
            if(pcExtType)
            {
               HTTP_BuildGeneric(pstCl,  (char *)pcHttpRequestHeaderOKee, pcExtType);
            }
            HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderConnect);
            //
            pfHandler = (PFEXT) stHttpTypes[iIdx].pfExt;
            if(pfHandler) pfHandler(pstCl);
            return(tFileType);
         }
      }
      //
      // No valid Content type found
      //
      PRINTF("http_CollectHandleExtension():No Match !" CRLF);
      HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderForbidden);
      HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderConnect);
   }
   else
   {
      PRINTF("http_CollectHandleExtension():No ext in URL" CRLF);
      HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderBad);
      HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderConnect);
   }
   return(tFileType);
}

//
// Function    : http_HandleHtml
// Description : Handle the request for HTML type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleHtml(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleJs
// Description : Handle the request for JS files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleJs(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleJpg
// Description : Handle the request for JPG type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleJpg(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleBmp
// Description : Handle the request for BMP type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleBmp(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleGif
// Description : Handle the request for GIF type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleGif(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandlePng
// Description : Handle the request for PNG type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandlePng(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleTxt
// Description : Handle the request for TXT type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleTxt(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleMp3
// Description : Handle the request for mp3 audio files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleMp3(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleHdtv
// Description : Handle the request for HDTV type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleHdtv(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleJson
// Description : Handle the request for JSON files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleJson(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleRequest
// Description : Handle the request: 
//                pcPathname-->"/www/dir1/dir2/the_filename.ext"
//                                             ^iFileIdx    ^iFileExt
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleRequest(NETCL *pstCl)
{
   bool     fCc=FALSE;
   int      iFd, iRead, iTotal=0;

   iFd = open(pstCl->pcUrl, O_RDONLY);
   if(iFd != -1)
   {
      PRINTF1("http_HandleRequest{}: File=<%s> opended OKee"CRLF, pstCl->pcUrl);
      //
#ifdef   FEATURE_ECHO_BIN_OUT
      //
      // Echo all HTTP request data from the client
      //
      ES_printf("http_HandleRequest{}: File=<%s>"CRLF, pstCl->pcUrl);
#endif   //FEATURE_ECHO_BIN_OUT
      //
      do
      {
         iRead = read(iFd, pstCl->pcRcvBuffer, READ_BUFFER_SIZE);
         if(iRead == 0) fCc = TRUE;
         if(iRead > 0)
         {
            //PRINTF1("http_HandleRequest(): Send %d bytes" CRLF, iRead);
            HTTP_SendData(pstCl, pstCl->pcRcvBuffer, iRead);
            iTotal += iRead;
         }
      }
      while(iRead > 0);
      //
#ifdef   FEATURE_ECHO_BIN_OUT
      //
      // Echo all HTTP request data from the client
      //
      ES_printf("http_HandleRequest(): Send %d bytes" CRLF, iTotal);
#endif   //FEATURE_ECHO_BIN_OUT
      //
      close(iFd);
   }
   else
   {
      PRINTF1("http_HandleRequest{}: Error open file <%s>"CRLF, pstCl->pcUrl);
      //
      HTTP_BuildGeneric(pstCl,  (char *)pcHttpRequestHeaderError);
      HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderConnect);
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__HTTP_HEADER_PARSER_________(){};
----------------------------------------------------------------------------*/


/*
 * Function    : http_ParseRequestHeader
 * Description : Parse the received http request for header info we need
 *
 * Parameters  : Client
 * Returns     : HTTP request type
 *
 */
static RTYPE http_ParseRequestHeader(NETCL *pstCl, char *pcReqHdr)
{
   RTYPE    tType=HTTP_CHAR;
   int      iLen;
   PFHTTP   pfAddr;
   HTTPREQ *pstReqHdr=stHttpRequestHeaders;

   PRINTF1("http_ParseRequestHeader():[%s]" CRLF, pcReqHdr);
   //
   while(pstReqHdr->pcHeader)
   {
      iLen = strlen(pstReqHdr->pcHeader);
      if(strncasecmp(pstReqHdr->pcHeader, pcReqHdr, iLen) == 0)
      {
         //
         // Request header found
         //
         pfAddr = (PFHTTP) pstReqHdr->pfHandler;
         if(pfAddr)
         {
            //
            // Execute HTTP request header handler
            //
            tType = pfAddr(pstCl, pcReqHdr, iLen);
         }
         break;
      }
      else
      {
         pstReqHdr++;
      }
   }
   return(tType);
}

/*
 * Function    : http_ReqHdrGetValue
 * Description : Get a value from a request header
 *
 * Parameters  : Client, request header
 * Returns     : int
 *
 */
static int http_ReqHdrGetValue(NETCL *pstCl, char *pcBuffer)
{
   int   iVal;

   iVal = atoi(pcBuffer);
   //PRINTF1("http_ReqHdrGetValue():Val=%d" CRLF, iVal);
   return(iVal);
}

/*
 * Function    : http_ReqHdrAcceptEncoding
 * Description : RequestHeader: Accept-Encoding
 *
 * Parameters  : Client
 * Returns     : Request header type
 *
 */
static RTYPE http_ReqHdrAcceptEncoding(NETCL *pstCl, char *pcHdr, int iLen)
{
   RTYPE tType=HTTP_CHAR;

   PRINTF1("http_ReqHdrAcceptEncoding():%s" CRLF, pcHdr);
   return(tType);
}

/*
 * Function    : http_ReqHdrContentLength
 * Description : RequestHeader: content-length: xxxx
 *
 * Parameters  : Client
 * Returns     : Request header type
 *
 */
static RTYPE http_ReqHdrContentLength(NETCL *pstCl, char *pcHdr, int iLen)
{
   RTYPE tType=HTTP_CHAR;

   pstCl->iReqLen = http_ReqHdrGetValue(pstCl, &pcHdr[iLen+1]);
   PRINTF2("http_ReqHdrContentLength():%s (-->%d)" CRLF, pcHdr, pstCl->iReqLen);
   //
   return(tType);
}

/*
 * Function    : http_ReqHdrContentType
 * Description : RequestHeader: content-type
 *
 * Parameters  : Client
 * Returns     : Request header type
 *
 */
static RTYPE http_ReqHdrContentType(NETCL *pstCl, char *pcHdr, int iLen)
{
   RTYPE tType=HTTP_CHAR;

   PRINTF1("http_ReqHdrContentType():%s" CRLF, pcHdr);
   return(tType);
}
