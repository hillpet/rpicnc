/*  (c) Copyright:  2015...2016  Patrn, Confidential Data
**
**  $Workfile:   config.h
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            
**                      
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       23 Mar 2015
**
 *  Revisions:
 *    $Log:   $
 *
 **/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//=============================================================================
// Feature switches
//=============================================================================
//#define FEATURE_RESTORE_DEFAULTS_ON_REBOOT // Global restore of defaults on reboot
  #define FEATURE_USE_PRINTF                 // Global PRINTF switch
  #define FEATURE_LOG_PRINTF_TO_FILE         // Send LOG_printf's to the Log file
//#define FEATURE_UDP_CONNECTED              // UDP datagrams connected mode
//#define FEATURE_ECHO_HTTP_IN               // Echo all HTTP incoming data
//#define FEATURE_ECHO_HTTP_OUT              // Echo all HTTP outgoing data
//#define FEATURE_ECHO_BIN_OUT               // Echo all bin outgoing data (if readable)
//#define FEATURE_SHOW_COMMANDS              // Show the CMND thread command buffer
//#define FEATURE_NET_DUMP_RECORD            // Dump Network records
//#define FEATURE_VIRTUAL_LOG                // Show log entries on virtual LCD
//#define FEATURE_VIRTUAL_LOG_TS             // Show log entries on virtual LCD plus time stamps
//#define FEATURE_SHOW_BUTTON_IMAGES         // Show button images
//#define FEATURE_SHOW_BUTTON_SETUP          // Show button setup
//#define FEATURE_SHOW_PARAMETERS            // Show CNC parameters
//#define FEATURE_USB_DEBUG_TRACES           // Start USB debug thread
  #define FEATURE_DUMP_USB                   // Dump USB Comms parameters
  #define FEATURE_DUMP_DEBUG                 // Dump USB debug from slave controller

//
// IO-Options are now defined in the makefile and the actual board itself:
// Rpi-Proj root: file RPIBOARD --> decimal number 1...?
//
// IO:   select define $(INOUT):
//       FEATURE_IO_NONE                  // IO : No IO used !
//       FEATURE_IO_PATRN                 // IO : Use Patrn.nl board
//       FEATURE_IO_GERTBOARD             // IO : Use Gert Board
//       FEATURE_IO_PIFACE                // IO : Use PiFace board
//       FEATURE_IO_PIFACECAD             // IO : Use PiFace Control and Display board (IO and LCD)
//       FEATURE_IO_LCDADA                // IO : Use ADAFRUIT 4xbuttons and 2x16 LCD)
//       FEATURE_IO_TFT                   // IO : Use RAIO8870 320x240 TFT screen
//       FEATURE_IO_TFTBUTS               // IO : Use RAIO8870 320x240 TFT screen plus buttons
//
#define LCD_MODE_OFF          0
#define LCD_MODE_ASK          1
#define LCD_MODE_STBY         2
#define LCD_MODE_TEXT         3
#define LCD_MODE_BMP          4
#define LCD_MODE_FB           5
//
#define KEY_SELECT            0
#define KEY_NEXT              1
#define KEY_PREVIOUS          2
#define KEY_START_STOP        3
#define KEY_POWEROFF          4

//=============================================================================
#ifdef   FEATURE_IO_NONE      // IO : NO IO board used
//=============================================================================
#define  DIO_USE_NONE         //No IO
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiCam uses NO IO !"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  BUTTON_1             0
#define  BUTTON_2             1
//
#define  RCU_OPEN()        
#define  RCU_CLOSE()        
#define  BUT_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      0
#define  LCD_ACTUAL_COLS      0
#define  LCD_FONT_SIZE        0
//
#define  LCD_OPEN()
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_MENU(r,c,t)
#define  LCD_TEXT(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_CLOSE()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_GERTBOARD // IO : Use Gert board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiCam uses GertBoard"

//
#define  RCU_OPEN()        
#define  RCU_CLOSE()        
#define  BUT_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      0
#define  LCD_ACTUAL_COLS      0
#define  LCD_FONT_SIZE        0
//
#define  LCD_OPEN()
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_MENU(r,c,t)
#define  LCD_TEXT(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_CLOSE()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  BUTTON_1             23
#define  BUTTON_2             24
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_PATRN     // IO : Use Patrn.nl board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiCam uses PatrnBoard"
//
#define  LED_R                0
#define  LED_Y                7
#define  LED_G                3
#define  LED_W                6
//
#define  BUTTON_1             1
#define  BUTTON_2             4
//
#define  RCU_OPEN()        
#define  RCU_CLOSE()        
#define  BUT_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)            digitalWrite(x, y)
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      0
#define  LCD_ACTUAL_COLS      0
#define  LCD_FONT_SIZE        0
//
#define  LCD_OPEN()
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_MENU(r,c,t)
#define  LCD_TEXT(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_CLOSE()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_PIFACE    // IO : Use PiFace I/O board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "ToDo:RpiCam uses PiFace Board"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  BUTTON_1             0
#define  BUTTON_2             1
#define  BUTTON_3             2
#define  BUTTON_4             3
//
#define  RCU_OPEN()        
#define  RCU_CLOSE()        
#define  BUT_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)
#define  OUT(x, y)            digitalWrite(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      0
#define  LCD_ACTUAL_COLS      0
#define  LCD_FONT_SIZE        0
//
#define  LCD_OPEN()
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_MENU(r,c,t)
#define  LCD_TEXT(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_CLOSE()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         0
//
#define  LCD_ROW_VERSION      0
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           0
#define  LCD_COL_IP           0
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_PIFACECAD // IO : Use PiFaceCad I/O + LCD board
//=============================================================================
#include <wiringPi.h>
#include "rpircu.h"
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_WIRINGPI     //LCD screen used wiringPi API lbrary
//
#define  RPI_IO_BOARD         "RpiCam uses PiFace Control and Display Board"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  BUTTON_1             0
#define  BUTTON_2             1
#define  BUTTON_3             2
#define  BUTTON_4             3
#define  BUTTON_5             4
#define  BUTTON_6             5
#define  BUTTON_7             6
#define  BUTTON_8             7
//
#define  RCU_OPEN()           RCU_Open()
#define  RCU_CLOSE()          RCU_Close()
#define  BUT_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      2
#define  LCD_ACTUAL_COLS      16
#define  LCD_FONT_SIZE        0
//
#define  LCD_OPEN()           CAD_Open()
#define  LCD_SETUP()          lcdInit(LCD_ACTUAL_ROWS, LCD_ACTUAL_COLS, 4, 11, 10, 0, 1, 2, 3, 0, 0, 0, 0)
#define  LCD_BACKLIGHT(x)     CAD_Backlight(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)    CAD_Cursor(r,c,x)
#define  LCD_DISPLAY(r,c)     CAD_SetDisplay(r,c)
#define  LCD_MENU(r,c,t)      CAD_WriteLine(r,c,t)
#define  LCD_TEXT(r,c,t)      CAD_Text(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_CLOSE()          CAD_Close()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_TFT       // IO : Use RAIO8870 320x240 TFT
//=============================================================================
#define  DIO_USE_NONE         //No IO
#define  LCD_USE_FRAMEBUFFER  //TFT screen uses framebuffer deamon
//
#define  RPI_IO_BOARD         "RpiCam uses RAIO8870 board"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  BUTTON_1             0
#define  BUTTON_2             1
#define  BUTTON_3             2
#define  BUTTON_4             3
#define  BUTTON_5             4
#define  BUTTON_6             5
#define  BUTTON_7             6
#define  BUTTON_8             7
//
#define  RCU_OPEN()        
#define  RCU_CLOSE()        
#define  BUT_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      7
#define  LCD_ACTUAL_COLS      20
#define  LCD_FONT_SIZE        5
//
#define  LCD_OPEN()           CAD_Open()
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)     CAD_Backlight(x)
#define  LCD_COLOR(f,b)       CAD_Color(f,b)
#define  LCD_CURSOR(r,c,x)    CAD_Cursor(r,c,x)
#define  LCD_DISPLAY(r,c)     CAD_SetDisplay(r,c)
#define  LCD_MENU(r,c,t)      CAD_WriteLine(r,c,t)
#define  LCD_TEXT(r,c,t)      CAD_Text(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_CLOSE()          CAD_Close()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         32
//
#define  LCD_ROW_VERSION      0
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           0
#define  LCD_COL_IP           15
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_TFTBUTS   // IO : Use RAIO8870 320x240 TFT plus buttons
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_FRAMEBUFFER  //TFT screen uses framebuffer deamon
//
#define  RPI_IO_BOARD         "RpiCam uses RAIO8870 board API plus buttons"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  BUTTON_1             25
#define  BUTTON_2             27
#define  BUTTON_3             2
#define  BUTTON_4             3
#define  BUTTON_5             4
#define  BUTTON_6             5
#define  BUTTON_7             6
#define  BUTTON_8             7
//
#define  RCU_OPEN()        
#define  RCU_CLOSE()        
#define  BUT_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      7
#define  LCD_ACTUAL_COLS      20
#define  LCD_FONT_SIZE        5
//
#define  LCD_OPEN()           CAD_Open()
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)     CAD_Backlight(x)
#define  LCD_COLOR(f,b)       CAD_Color(f,b)
#define  LCD_CURSOR(r,c,x)    CAD_Cursor(r,c,x)
#define  LCD_DISPLAY(r,c)     CAD_SetDisplay(r,c)
#define  LCD_MENU(r,c,t)      CAD_WriteLine(r,c,t)
#define  LCD_TEXT(r,c,t)      CAD_Text(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_CLOSE()          CAD_Close()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         32
//
#define  LCD_ROW_VERSION      0
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           0
#define  LCD_COL_IP           15
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_LCDADA    // IO : Use Adafruit I/O + LCD board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_WIRINGPI     //LCD screen used wiringPi API lbrary
//
#define  RPI_IO_BOARD         "RpiCam uses Adafruit LCD I/O board"
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  BUTTON_1             13
#define  BUTTON_2             7
#define  BUTTON_3             4
#define  BUTTON_4             12
//
#define  RCU_OPEN()        
#define  RCU_CLOSE()        
#define  BUT_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)          {pinMode(x, INPUT);pullUpDnControl(x,PUD_UP);}
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      2
#define  LCD_ACTUAL_COLS      16
#define  LCD_FONT_SIZE        1
//
#define  LCD_OPEN()           CAD_Open()
#define  LCD_SETUP()          lcdInit(LCD_ACTUAL_ROWS, LCD_ACTUAL_COLS, 4, 11, 10, 0, 1, 2, 3, 0, 0, 0, 0)
#define  LCD_BACKLIGHT(x)     CAD_Backlight(x)
#define  LCD_COLOR(f,b)       
#define  LCD_CURSOR(r,c,x)    CAD_Cursor(r,c,x)
#define  LCD_DISPLAY(r,c)     CAD_SetDisplay(r,c)
#define  LCD_MENU(r,c,t)      CAD_WriteLine(r,c,t)
#define  LCD_TEXT(r,c,t)      CAD_Text(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_CLOSE()          CAD_Close()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
//=============================================================================
#endif
//
// RCU and Button keys
//
typedef enum RCUKEYS
{
   BUT_KEY1          = 1,
   BUT_KEY2,
   BUT_KEY3,
   BUT_KEY4,
   BUT_KEY5,
   BUT_KEY6,
   BUT_KEY7,
   BUT_KEY8,
   //
   RCU_ONOFF,
   RCU_RECORD,
   RCU_PLAY,
   RCU_OK,
   RCU_REWIND,
   RCU_FASTFORWARD,
   RCU_PAUSE,
   RCU_UPARROW,
   RCU_DOWNARROW,
   RCU_LEFTARROW,
   RCU_RIGHTARROW,
   RCU_NUM_1,
   RCU_NUM_2,
   RCU_NUM_3,
   RCU_NUM_4,
   RCU_NUM_5,
   RCU_NUM_6,
   RCU_NUM_7,
   RCU_NUM_8,
   RCU_NUM_9,
   RCU_NUM_0,
   //
   MAX_NUM_KEYS,
   RCU_NOKEY         = 255
}  RCUKEYS;

//
// Version string RpiCnc-vxxx-bbb
//
#define VERSION                     "200"
#define BUILD                       "029"
#define SWVERSION                   "v" VERSION "." BUILD
//
#define DATA_VALID_SIGNATURE        0xDEADBEEF           // Valid signature
#define DATA_VERSION_MAJOR          17
#define DATA_VERSION_MINOR          1
//
#define RPI_PUBLIC_WWW              "/all/public/www/"
#define RPI_PUBLIC_DEFAULT          "/all/public/www/index.html"
#define RPI_EXTERNAL_STORAGE        "/all/public/www/hdd/video/"
#define RPI_SHELL_FILE              "rpicnc.txt"
//
#define NUM_AXIS                    3                    // X, Y and Z axis
//
// Target RAM disk for high load file access
//
#define RPI_PUBLIC_PIX              "pix"
#define RPI_CACHED_WWW              "/mnt/rpipix/"
#define RPI_MOTION_DIR              "/mnt/rpipix/"
#define RPI_SHELL_DIR               "/mnt/rpicache/"
#define RPI_PUBLIC_LOG_FILE         "/mnt/rpicache/rpicnc.log"
#define RPI_ERROR_FILE              "/mnt/rpicache/rpicnc.err"
#define RPI_TRACE_FILE              "/mnt/rpicache/rpicnc.prn"
#define RPI_MAP_FILE                "/mnt/rpicache/rpicnc.map"
#define RPI_MAP_FILE_BACKUP         "/usr/local/share/rpi/rpicnc.map"
#define RPI_WORK_FILE               "rpicnc"
#define RPI_WORK_DIR                "/mnt/rpicache/"
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
//
#endif /* _CONFIG_H_ */
