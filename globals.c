/*  (c) Copyright:  2013  Patrn, Confidential Data  
 *
 *  $Workfile:          globals.c
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Global variables for Raspberry pi HTTP server
 *
 *
 *  Entry Points:       
 *
 *
 *  Compiler/Assembler: gnu gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       27 May 2013
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
//
#include "globals.h"
#include "dynpage.h"
#include "httppage.h"
#include "log.h"
#include "rtc.h"
#include "rpicnc.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local functions
//
static void    global_CreateMap        (void);
static void    global_InitMemory       (MAPSTATE);
static void    global_Open             (char *, int);
static void    global_SaveMapfile      ();
static void    global_RestoreMapfile   ();
//
// Static variables
//
static CNCMAP *pstCncMap         = NULL;
static int     iGlobalMapSize    = 0;
static int     iFdMap            = 0;
//
// These global settings are being initialized on EVERY RESTART of the Apps.
//
const GLOBALS stWarmBootParameters[] =
{
//    pcDefault         iChanged iChangedOffset                               iStorageOffset                         iStorageSize
   // --- Normal global parms ---------------------------------------------------------------------------------------------------------
   //
   {  "00",             0,       -1,                                          offsetof(CNCMAP, G_pcDrawMode),        MAX_PARM_LEN     },
   {  "",               0,       offsetof(CNCMAP, G_ubIpAddrChanged),         offsetof(CNCMAP, G_pcUdpAddr),         INET_ADDRSTRLEN  },
   {  "",               0,       -1,                                          offsetof(CNCMAP, G_pcTcpAddr),         INET_ADDRSTRLEN  },
   // --- Misc ------------------------------------------------------------------------------------------------------------------------
   {  "0",              0,       -1,                                          offsetof(CNCMAP, G_pcLastFileSize),    MAX_PARM_LEN     },       
   {  "0",              0,       -1,                                          offsetof(CNCMAP, G_pcNumFiles),        MAX_PARM_LEN     },
   {  SWVERSION,        0,       -1,                                          offsetof(CNCMAP, G_pcVersionSw),       MAX_PARM_LEN     },
   //
   {  NULL,             0,       -1,                                          -1,                                    0                }
};

//
// These global settings are being initialized on request
//
const GLOBALS stColdBootParameters[] =
{
//    pcDefault         iChanged iChangedOffset                               iStorageOffset                         iStorageSize
   // --- Normal global parms ---------------------------------------------------------------------------------------------------------
   //
   {  "100",            0,       -1,                                          offsetof(CNCMAP, G_pcCncDelay),        MAX_PARM_LEN     },
   {  "00",             0,       -1,                                          offsetof(CNCMAP, G_pcDrawMode),        MAX_PARM_LEN     },
   {  "800.0",          0,       -1,                                          offsetof(CNCMAP, G_pcCanvasWidth),     MAX_PARM_LEN     },
   {  "520.0",          0,       -1,                                          offsetof(CNCMAP, G_pcCanvasHeight),    MAX_PARM_LEN     },
   {  "400.0",          0,       -1,                                          offsetof(CNCMAP, G_pcDrawWidth),       MAX_PARM_LEN     },
   {  "300.0",          0,       -1,                                          offsetof(CNCMAP, G_pcDrawHeight),      MAX_PARM_LEN     },
   {  "9.8676",         0,       -1,                                          offsetof(CNCMAP, G_pcRadius),          MAX_PARM_LEN     },
   {  "800.0",          0,       -1,                                          offsetof(CNCMAP, G_pcStepRevs),        MAX_PARM_LEN     },
   {  "50",             0,       -1,                                          offsetof(CNCMAP, G_pcMoveSteps),       MAX_PARM_LEN     },
   {  "50",             0,       -1,                                          offsetof(CNCMAP, G_pcDrawSteps),       MAX_PARM_LEN     },
   //----------------------------------------------------------------------------------------------------------------------------------
   {  "10000.0",        0,       -1,                                          offsetof(CNCMAP, G_pcAccSet[0]),       MAX_PARM_LEN     },
   {  "10000.0",        0,       -1,                                          offsetof(CNCMAP, G_pcAccSet[1]),       MAX_PARM_LEN     },
   {  "10000.0",        0,       -1,                                          offsetof(CNCMAP, G_pcAccSet[2]),       MAX_PARM_LEN     },
   {  "400.0",          0,       -1,                                          offsetof(CNCMAP, G_pcSpdMin[0]),       MAX_PARM_LEN     },
   {  "400.0",          0,       -1,                                          offsetof(CNCMAP, G_pcSpdMin[1]),       MAX_PARM_LEN     },
   {  "400.0",          0,       -1,                                          offsetof(CNCMAP, G_pcSpdMin[2]),       MAX_PARM_LEN     },
   {  "800.0",          0,       -1,                                          offsetof(CNCMAP, G_pcSpdMax[0]),       MAX_PARM_LEN     },
   {  "800.0",          0,       -1,                                          offsetof(CNCMAP, G_pcSpdMax[1]),       MAX_PARM_LEN     },
   {  "800.0",          0,       -1,                                          offsetof(CNCMAP, G_pcSpdMax[2]),       MAX_PARM_LEN     },
   {  "200.0",          0,       -1,                                          offsetof(CNCMAP, G_pcStaPos[0]),       MAX_PARM_LEN     },
   {  "50.0",           0,       -1,                                          offsetof(CNCMAP, G_pcStaPos[1]),       MAX_PARM_LEN     },
   {  "0.0",            0,       -1,                                          offsetof(CNCMAP, G_pcStaPos[2]),       MAX_PARM_LEN     },
   {  "600.0",          0,       -1,                                          offsetof(CNCMAP, G_pcEndPos[0]),       MAX_PARM_LEN     },
   {  "350.0",          0,       -1,                                          offsetof(CNCMAP, G_pcEndPos[1]),       MAX_PARM_LEN     },
   {  "10.0",           0,       -1,                                          offsetof(CNCMAP, G_pcEndPos[2]),       MAX_PARM_LEN     },

   // --- Settings ----------------------------------------------------------------------------------------------------------------------
   {  "0",              0,       -1,                                          offsetof(CNCMAP, G_pcNumFiles),        MAX_PARM_LEN     },
   {  "201",            0,       -1,                                          offsetof(CNCMAP, G_pcFilter),          MAX_PARM_LEN     },           
   {  "800",            0,       -1,                                          offsetof(CNCMAP, G_pcPixWidth),        MAX_PARM_LEN     },            
   {  "600",            0,       -1,                                          offsetof(CNCMAP, G_pcPixHeight),       MAX_PARM_LEN     },           
   {  "jpg",            0,       -1,                                          offsetof(CNCMAP, G_pcPixFileExt),      MAX_PARM_LEN     },       
   {  "0",              0,       -1,                                          offsetof(CNCMAP, G_pcDetectPixel),     MAX_PARM_LEN     },      
   {  "0",              0,       -1,                                          offsetof(CNCMAP, G_pcDetectLevel),     MAX_PARM_LEN     },      
   {  "1",              0,       -1,                                          offsetof(CNCMAP, G_pcDetectSeq),       MAX_PARM_LEN     },        
   {  "100",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectRed),       MAX_PARM_LEN     },        
   {  "100",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectGreen),     MAX_PARM_LEN     },      
   {  "100",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectBlue),      MAX_PARM_LEN     },       
   {  "1.0",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectZoomX),     MAX_PARM_LEN     },      
   {  "1.0",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectZoomY),     MAX_PARM_LEN     },      
   {  "1.0",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectZoomW),     MAX_PARM_LEN     },      
   {  "1.0",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectZoomH),     MAX_PARM_LEN     },      
   {  "1.0",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectAvg1),      MAX_PARM_LEN     },       
   {  "1.0",            0,       -1,                                          offsetof(CNCMAP, G_pcDetectAvg2),      MAX_PARM_LEN     },       
   //
   {  "",               0,       -1,                                          offsetof(CNCMAP, G_pcOutputFile),      MAX_PATH_LEN     },       
   {  "",               0,       -1,                                          offsetof(CNCMAP, G_pcLastFile),        MAX_PATH_LEN     },           
   {  "0",              0,       -1,                                          offsetof(CNCMAP, G_pcLastFileSize),    MAX_PARM_LEN     },       
   {  "",               0,       -1,                                          offsetof(CNCMAP, G_pcTcpIfce),         MAX_PARM_LEN     },           
   {  "---",            0,       -1,                                          offsetof(CNCMAP, G_pcStatus),          MAX_PARM_LEN     },             
   {  "no",             0,       -1,                                          offsetof(CNCMAP, G_pcDelete),          MAX_PARM_LEN     },             
   // --- Misc ------------------------------------------------------------------------------------------------------------------------
   {  "8081",           0,       -1,                                          offsetof(CNCMAP, G_pcTcpPort),         MAX_PARM_LEN     },
   {  "9081",           0,       -1,                                          offsetof(CNCMAP, G_pcUdpPort),         MAX_PATH_LEN     },          
   {  RPI_PUBLIC_PIX,   0,       offsetof(CNCMAP, G_ubPixPathChanged),        offsetof(CNCMAP, G_pcPixPath),         MAX_PATH_LEN     },
   {  SWVERSION,        0,       -1,                                          offsetof(CNCMAP, G_pcVersionSw),       MAX_PARM_LEN     },
   {  "--:--:--",       0,       -1,                                          offsetof(CNCMAP, G_pcTimeDrawing),     MAX_PARM_LEN     },        
   {  "",               0,       offsetof(CNCMAP, G_ubIpAddrChanged),         offsetof(CNCMAP, G_pcUdpAddr),         INET_ADDRSTRLEN  },
   //
   {  NULL,             0,       -1,                                          -1,                                    0                }
};

const char *pcCncStatus[] = 
{
   "Cnc-Idle",                         //CNC_STATUS_IDLE
   "Cnc-Error",                        //CNC_STATUS_ERROR
   "Cnc-Run",                          //CNC_STATUS_RUN
   "Cnc-Motion",                       //CNC_STATUS_MOTION
   "Cnc-Stopping",                     //CNC_STATUS_STOPPING
   "Cnc-Stopped",                      //CNC_STATUS_STOPPED
   "Cnc-Rejected",                     //CNC_STATUS_REJECTED
   "Cnc-Keypress",                     //CNC_STATUS_KEYPRESS
   "Cnc-Keyrelease"                    //CNC_STATUS_KEYRELEASE
};


/*------  Local functions separator -----------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : GLOBAL_Init
 * Description : Init all global variables
 *
 * Parameters  : 
 * Returns     : MMAP ptr
 *
 */
CNCMAP *GLOBAL_Init(void)
{
   const GLOBALS *pstGlobals = stWarmBootParameters;
   char          *pcValue;
   u_int8        *pubChanged;
   GLOCMD        *pstCmd;

   //
   // (Try to) copy the mmap file from SD to RAM disk
   //
   global_RestoreMapfile();
   //
   // Create the global MMAP mapping
   //
   global_CreateMap();
   //
   // Save HOST/SRVR pids
   //
   PRINTF1("GLOBAL_Init: RpiMap=%p" CRLF, pstCncMap);
   //
   // Save Host   HOST pid
   // Save other PIDs
   //
   pstCncMap->G_tHostPid         = getpid();
   pstCncMap->G_tRootPid         = 0;
   pstCncMap->G_tSrvrPid         = 0;
   pstCncMap->G_tCncWrPid        = 0;
   pstCncMap->G_tCncDbPid        = 0;
   pstCncMap->G_tUdpPid          = 0;
   pstCncMap->G_Connected        = FALSE;
   //
   // Reset activity counter
   //
   pstCncMap->G_ulSecondsCounter = 0;
   //
   // Reset USB Comm structures
   //
   memset(&pstCncMap->G_stUsbRcv, 0, sizeof(COMMS));
   memset(&pstCncMap->G_stUsbXmt, 0, sizeof(COMMS));
   //
   // Reset global command structure 
   //
   pstCmd = &pstCncMap->G_stCmd;
   //
   pstCmd->iStatus      = 0;
   pstCmd->iCommand     = 0;
   pstCmd->iData1       = 0;
   pstCmd->iData2       = 0;
   pstCmd->iData3       = 0;
   pstCmd->pcExec[0]    = 0;
   pstCmd->pcArgs[0]    = 0;
   pstCmd->pfCallback   = NULL;
   pstCmd->pstCl        = NULL;
   //
   // Load global settings every restart :
   //
   while(pstGlobals->pcDefault)
   {
      pcValue = (char *)pstCncMap + pstGlobals->iStorageOffset;
      strncpy(pcValue, pstGlobals->pcDefault, pstGlobals->iStorageSize);
      if(pstGlobals->iChangedOffset >= 0)
      {
         pubChanged  = (u_int8 *)pstCncMap + pstGlobals->iChangedOffset;
         *pubChanged = pstGlobals->iChanged;
         //PRINTF3("GLOBAL_Init: RpiMap[%04d] Chg=%d Value = <%s>" CRLF, pstGlobals->iStorageOffset, *pubChanged, pcValue);
      }
      else
      {
         //PRINTF2("GLOBAL_Init: RpiMap[%04d] Chg=? Value = <%s>" CRLF, pstGlobals->iStorageOffset, pcValue);
      }
      pstGlobals++;
   }
#ifdef FEATURE_RESTORE_DEFAULTS_ON_REBOOT
   //
   // Restore some defaults always
   //
   GLOBAL_RestoreDefaults();
#endif   //FEATURE_RESTORE_DEFAULTS_ON_REBOOT
   PRINTF1("GLOBAL_Init: Map=%p" CRLF, pstCncMap);
   return(pstCncMap);
}

//
//  Function:   GLOBAL_AddPid
//  Purpose:    Add PID to VAR dir
//              
//  Parms:      Pathname of PID file
//  Returns:    1 = error
//
int GLOBAL_AddPid(char *pcFile)
{
   int   iCc = 1;
   FILE *pstFile;
        
   remove(pcFile);
   if( (pstFile = fopen(pcFile, "w")) != NULL )
   {
      fprintf(pstFile, "%i", getpid());
      fclose(pstFile);
      iCc = 0;
   }
   return(iCc);   
}

//
//  Function:   GLOBAL_RemovePid
//  Purpose:    Remove PID file
//              
//  Parms:      Pathname of PID file
//  Returns:    
//
int GLOBAL_RemovePid(char *pcFile)
{
  return(remove(pcFile));
}

/*
 * Function    : GLOBAL_RestoreDefaults
 * Description : Restore global variables
 *
 * Parameters  : 
 * Returns     : 
 *
 */
void GLOBAL_RestoreDefaults(void)
{
   const GLOBALS *pstGlobals = stColdBootParameters;
   char          *pcValue;
   u_int8        *pubChanged;

   //
   // Restore global settings
   //
   while(pstGlobals->pcDefault)
   {
      pcValue = (char *)pstCncMap + pstGlobals->iStorageOffset;
      strncpy(pcValue, pstGlobals->pcDefault, pstGlobals->iStorageSize);
      if(pstGlobals->iChangedOffset >= 0)
      {
         pubChanged  = (u_int8 *)pstCncMap + pstGlobals->iChangedOffset;
         *pubChanged = pstGlobals->iChanged;
         PRINTF3("GLOBAL_RestoreDefaults: CncMap[%04d] Chg=%d Value = <%s>" CRLF, pstGlobals->iStorageOffset, *pubChanged, pcValue);
      }
      else
      {
         PRINTF2("GLOBAL_RestoreDefaults: CncMap[%04d] Chg=? Value = <%s>" CRLF, pstGlobals->iStorageOffset, pcValue);
      }
      pstGlobals++;
   }
}

/*
 * Function    : GLOBAL_UpdateSecs
 * Description : Count seconds
 *
 * Parameters  : 
 * Returns     : 
 *
 */
void GLOBAL_UpdateSecs(void)
{
   pstCncMap->G_ulSecondsCounter++;
   PRINTF1("GLOBAL_UpdateSecs: %x" CRLF, (unsigned int)pstCncMap->G_ulSecondsCounter);
}

/*
 * Function    : GLOBAL_ReadSecs
 * Description : Return seconds counter
 *
 * Parameters  : 
 * Returns     : Running seconds counter
 *
 */
u_int32 GLOBAL_ReadSecs(void)
{
   PRINTF1("GLOBAL_ReadSecs: %x" CRLF, (unsigned int)pstCncMap->G_ulSecondsCounter);
   return(pstCncMap->G_ulSecondsCounter);
}

/*
 * Function    : GLOBAL_Status
 * Description : Update and/or return global status
 *
 * Parameters  : Cnc-Status or CNC_STATUS_ASK
 * Returns     : Status
 *               pcCncStatus
 */
int GLOBAL_Status(int iStatus)
{
   if(iStatus < NUM_CNC_STATUS)
   {
      pstCncMap->G_iStatus = iStatus;
      strncpy(pstCncMap->G_pcStatus, pcCncStatus[iStatus], MAX_PARM_LEN);
      pstCncMap->G_pcStatus[MAX_PARM_LEN-1] = 0;
      PRINTF1("GLOBAL_Status: %s" CRLF, pstCncMap->G_pcStatus);
   }
   return(pstCncMap->G_iStatus);
}

/**
 **  Name:         GLOBAL_GetMapping
 **
 **  Description:  Get the current mapping
 **
 **  Arguments:    
 **
 **  Returns:      Ptr to current mapping
 **/
CNCMAP *GLOBAL_GetMapping(void)
{
  return(pstCncMap);
}

/**
 **  Name:         GLOBAL_Sync
 **
 **  Description:  Sync the main file with the mapping
 **
 **  Arguments:    
 **
 **  Returns:      TRUE if OKee
 **/
bool  GLOBAL_Sync(void)
{
  msync(pstCncMap, iGlobalMapSize, MS_SYNC);
  return(TRUE);
}

/**
 **  Name:         GLOBAL_Close
 **
 **  Description:  Close the main file with the mapping
 **
 **  Arguments:    
 **
 **  Returns:      TRUE if OKee
 **/
bool  GLOBAL_Close(void)
{
   munmap(pstCncMap, iGlobalMapSize); 
   safeclose(iFdMap);
   global_SaveMapfile();
   return(TRUE);
}

//
//  Function:   GLOBAL_Share
//  Purpose:    Start sharing the mmap file
//
//  Parms:      
//  Returns:    True if OKee
//
bool GLOBAL_Share(void)
{
   bool     fCc=TRUE;

   pstCncMap = (CNCMAP *) mmap(NULL, iGlobalMapSize, PROT_READ|PROT_WRITE, MAP_SHARED, iFdMap, 0);
   if(pstCncMap == MAP_FAILED)
   {
      LOG_Report(0, "MAP", "Re-mapping failed");
      PRINTF("GLOBAL_Share(): remapping failed" CRLF);
      fCc = FALSE;
   }
   else
   {
      PRINTF1("GLOBAL_Share: Map=%p" CRLF, pstCncMap);
   }
   return(fCc);
}

#ifdef   FEATURE_SHOW_COMMANDS
//
//  Function:  GLOBAL_DumpVars
//  Purpose:   Dump the MMAP variables
//  Parms:     Owner
//
//  Returns:
//
void GLOBAL_DumpVars(char *pcSrc)
{
   ES_printf("%s(): Mapfile    : [    ] %p"     CRLF, pcSrc, pstCncMap);
   ES_printf("%s(): Version    : [%4d] v%d.%d"  CRLF, pcSrc, offsetof(CNCMAP, G_iVersionMajor),    pstCncMap->G_iVersionMajor, pstCncMap->G_iVersionMinor);
   ES_printf("%s(): Signature S: [%4d] 0x%x"    CRLF, pcSrc, offsetof(CNCMAP, G_iSignatureStart),  pstCncMap->G_iSignatureStart);
   ES_printf("%s(): Signature E: [%4d] 0x%x"    CRLF, pcSrc, offsetof(CNCMAP, G_iSignatureEnd),    pstCncMap->G_iSignatureEnd);
   //
   ES_printf("%s(): Host       : [%4d] %d"      CRLF, pcSrc, offsetof(CNCMAP, G_tHostPid),         pstCncMap->G_tHostPid);
   ES_printf("%s(): Command    : [%4d] %d"      CRLF, pcSrc, offsetof(CNCMAP, G_tCmndPid),         pstCncMap->G_tCmndPid);
   ES_printf("%s(): Exec       : [%4d] %d"      CRLF, pcSrc, offsetof(CNCMAP, G_tExecPid),         pstCncMap->G_tExecPid);
   //
   ES_printf("%s(): IP  -Me    : [%4d] <%s>"    CRLF, pcSrc, offsetof(CNCMAP, G_pcTcpAddr),        pstCncMap->G_pcTcpAddr);
   ES_printf("%s(): Port-Me    : [%4d] <%s>"    CRLF, pcSrc, offsetof(CNCMAP, G_pcTcpPort),        pstCncMap->G_pcTcpPort);
   ES_printf("%s(): IP  -Client: [%4d] <%s>"    CRLF, pcSrc, offsetof(CNCMAP, G_pcUdpAddr),        pstCncMap->G_pcUdpAddr);
   //
   ES_printf(CRLF);
}

//
//  Function:  GLOBAL_DumpCommand
//  Purpose:   Dump the MMAP command strcture
//  Parms:     Owner
//
//  Returns:
//
void GLOBAL_DumpCommand(char *pcSrc)
{
   GLOCMD  *pstCmd = &pstCncMap->G_stCmd;

   ES_printf("%s(): iStatus   : %x" CRLF, pcSrc, pstCmd->iStatus);
   ES_printf("%s(): iCommand  : %d" CRLF, pcSrc, pstCmd->iCommand);
   ES_printf("%s(): pcExec    : %s" CRLF, pcSrc, pstCmd->pcExec);
   ES_printf("%s(): pcArgs    : %s" CRLF, pcSrc, pstCmd->pcArgs);
   ES_printf("%s(): Callback  : %p" CRLF, pcSrc, pstCmd->pfCallback);
   ES_printf("%s(): Net       : %p" CRLF, pcSrc, pstCmd->pstCl);
   ES_printf(CRLF);
}
#endif   //FEATURE_SHOW_COMMANDS

/*------  Local functions separator -----------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
//  Function:   global_CreateMap
//  Purpose:    Read the mapping from the file
//  Parms:
//
//  Returns:
//
static void global_CreateMap(void)
{
   //
   // Open the mmap file
   //
   global_Open(RPI_MAP_FILE, sizeof(CNCMAP));
   if(pstCncMap == NULL ) 
   {
      ES_printf("global_CreateMap(): MMAP open error");
      exit(254);
   }
   //
   if((pstCncMap->G_iSignatureStart != DATA_VALID_SIGNATURE)
        ||
      (pstCncMap->G_iSignatureEnd   != DATA_VALID_SIGNATURE)
        ||
      (pstCncMap->G_iVersionMajor   != DATA_VERSION_MAJOR))
   {
      // Bad mapping space !
      ES_printf("global_CreateMap():Bad map: Initialize...." CRLF);
      ES_printf("global_CreateMap():Old Signature S= 0x%08x" CRLF, pstCncMap->G_iSignatureStart);
      ES_printf("global_CreateMap():Old Signature E= 0x%08x" CRLF, pstCncMap->G_iSignatureEnd);
      ES_printf("global_CreateMap():Old DB version = v%d.%d" CRLF, pstCncMap->G_iVersionMajor, pstCncMap->G_iVersionMinor);
      //
      LOG_Report(0, "MAP", "Bad signature or revision : changed :");
      LOG_Report(0, "MAP", "SigS= %08x", pstCncMap->G_iSignatureStart);
      LOG_Report(0, "MAP", "SigE= %08x", pstCncMap->G_iSignatureEnd);
      LOG_Report(0, "MAP", "Vrs = v%d.%d", pstCncMap->G_iVersionMajor, pstCncMap->G_iVersionMinor);
      //
      global_InitMemory(MAP_CLEAR);
      GLOBAL_RestoreDefaults();
      //
      LOG_Report(0, "MAP", "New Mapping :");
      LOG_Report(0, "MAP", "SigS= %08x", pstCncMap->G_iSignatureStart);
      LOG_Report(0, "MAP", "SigE= %08x", pstCncMap->G_iSignatureEnd);
      LOG_Report(0, "MAP", "Vrs = v%d.%d", pstCncMap->G_iVersionMajor, pstCncMap->G_iVersionMinor);
      //
      ES_printf("global_CreateMap():Mapping changed: Initialize...." CRLF);
      ES_printf("global_CreateMap():New signature S= 0x%08x" CRLF, pstCncMap->G_iSignatureStart);
      ES_printf("global_CreateMap():New signature E= 0x%08x" CRLF, pstCncMap->G_iSignatureEnd);
      ES_printf("global_CreateMap():New DB version = v%d.%d" CRLF, pstCncMap->G_iVersionMajor, pstCncMap->G_iVersionMinor);
   }
   else
   {
      ES_printf("global_CreateMap():Mapping OKee, v%d.%d" CRLF, pstCncMap->G_iVersionMajor, pstCncMap->G_iVersionMinor);
      global_InitMemory(MAP_RESTART);
   }
}

//
//  Function:   global_InitMemory
//  Purpose:    Init the MAP memory
//
//  Parms:      Cmd
//  Returns:
//
static void global_InitMemory(MAPSTATE tState)
{
  int      iIdx;
  u_int32  ulTime;

  switch(tState)
  {
     case MAP_CLEAR:
        ES_memset(pstCncMap, 0, sizeof(CNCMAP));
        pstCncMap->G_iSignatureStart  = DATA_VALID_SIGNATURE;
        pstCncMap->G_iSignatureEnd    = DATA_VALID_SIGNATURE;
        pstCncMap->G_iVersionMajor    = DATA_VERSION_MAJOR;
        pstCncMap->G_iVersionMinor    = DATA_VERSION_MINOR;
        pstCncMap->G_ulStartTimestamp = RTC_GetDateTime(NULL);
        break;

     case MAP_SYNC:
        pstCncMap->G_ulStartTimestamp = RTC_GetDateTime(NULL);
        break;

     case MAP_RESTART:
        ulTime = RTC_GetDateTime(NULL);
        if(ulTime > (pstCncMap->G_ulStartTimestamp+ES_SECONDS_PER_DAY))
        {
           // it's more than a day agoo:
           iIdx = 0;
           LOG_Report(0, "MAP", "InitMemory:Restart new day");
        }
        else
        {
           // Get the current quarter 0...(24*4)-1
           iIdx = RTC_GetCurrentQuarter(0);
           PRINTF1("global_InitMemory():Restart %d quarter" CRLF, iIdx);
           LOG_Report(0, "MAP", "InitMemory:Restart %d quarter", iIdx);
        }
        break;

     default:
        break;
  }
}

//
//  Function:   global_Open
//  Purpose:    Open the main file for the mapping
//
//  Parms:      Filename, size
//  Returns:    
//
static void global_Open(char *pcName, int iMapSize)
{
  pstCncMap      = NULL;
  iGlobalMapSize = 0;

  iFdMap = safeopen2(pcName, O_RDWR|O_CREAT, 0640);
  //
  // Make sure the file has the minimum size!
  //
  lseek(iFdMap, iMapSize, SEEK_SET);
  safewrite(iFdMap, "EOF", 4);
  //
  pstCncMap = (CNCMAP *) mmap(NULL, iMapSize, PROT_READ|PROT_WRITE, MAP_SHARED, iFdMap, 0);
  if(pstCncMap == MAP_FAILED)
  {
     LOG_Report(0, "MAP", "Mapping failed");
     ES_printf("global_Open():Mapping failed" CRLF);
  }
  else
  {
     iGlobalMapSize = iMapSize;
  }
}

//
//  Function:  global_SaveMapfile
//  Purpose:   Copy the MMAP file from RAM disk to SD 
//             cp /usr/local/share/rpi/rpiXXX.map /mnt/rpicache/rpiXXX.map
//  Parms:     
//  Returns:    
//
static void global_SaveMapfile()
{
   int   iCc; 
	char *pcShell="cp " RPI_WORK_DIR RPI_WORK_FILE ".* " RPI_BACKUP_DIR;

   PRINTF1("global_SaveMapfile(): backup log, map files [%s]" CRLF, pcShell);
   iCc = system(pcShell);
   if(iCc) 
   {
      PRINTF2("global_RestoreMapfile():Error-%d: Backup log, map files [%s]" CRLF, errno, pcShell);
      LOG_Report(0, "GLO", "Error: Backup log, map files [%s]", pcShell);
   }
}

//
//  Function:  global_RestoreMapfile
//  Purpose:   Copy the MMAP file from SD to RAM disk
//             cp /mnt/rpicache/rpiXXX.map /usr/local/share/rpi/rpiXXX.map
//  Parms:     
//  Returns:    
//
static void global_RestoreMapfile()
{
   int   iCc; 
	char *pcShell="cp " RPI_BACKUP_DIR RPI_WORK_FILE ".* " RPI_WORK_DIR;

   PRINTF1("global_RestoreMapfile(): restore log, map files [%s]" CRLF, pcShell);
   iCc = system(pcShell);
   if(iCc) 
   {
      PRINTF2("global_RestoreMapfile():Error-%d: Restore log, map files [%s]" CRLF, errno, pcShell);
      LOG_Report(errno, "GLO", "Error: Restore log, map files [%s]", pcShell);
   }
}

