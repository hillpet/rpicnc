/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Log handler
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
 *
**/

#ifndef _LOG_H_
#define _LOG_H_

#define LOG_MAX_RECORD_LEN      256
#define LOG_MAX_PATHNAME        32
#define FORMATTED_LONG_SIZE     24                      // 123.456.789.012
#define BUF_SPARE               32                      // some additional bufferspace for \0 etc.


//
// LOG the assert 
//
#define DEBUG_ASSERT   
#if defined(DEBUG_ASSERT)
void LOG_Assert                  (char *, bool);
#define LOG_ASSERT(a,b)          LOG_Assert(a,b)
#else
#define LOG_ASSERT(a,b)
#endif

int  LOG_AddPid                  (char *pcFile);
void LOG_SetVirtual              (bool);
int  LOG_RemovePid               (char *pcFile);
void LOG_Report                  (int iError, char *pcCaller, char *pcFormat, ...);
int  LOG_ReportVirtual           (int iIdx, char *pcText);
bool LOG_ReportOpenFile          (char *pcFilename);
void LOG_ReportFlushFile         (void);
void LOG_ReportCloseFile         (void);
int  LOG_printf                  (const char *pcFmt, ...);
int  LOG_AppendRecordCr          (char *);
 
#endif  /*_LOG_H_ */

