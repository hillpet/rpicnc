/*  (c) Copyright:  2013...2016 Patrn, Confidential Data
**
**  $Workfile:   $      raspjson.c
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Raspbian JSON functions and data
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       27 Jun 2013
**
 *  Revisions:
 *    $Log:   $
 *       cr308:   Fix JSON comma error
 *
 *
 *
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "echotype.h"
#include "dynpage.h"
#include "httppage.h"
#include "log.h"
#include "globals.h"
#include "raspjson.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static RPIJSON *json_AllocateObject       (int);
static void     json_FreeObject           (RPIJSON *);
static RPIJSON *json_Insert               (RPIJSON *, const char *);



static char pcHttpResponseHeaderJson[] = 
               "HTTP/1.1 200 OK" NEWLINE 
               "Content-Type: application/json; charset=UTF-8" NEWLINE
               "Content-Length: %d" NEWLINE
                NEWLINE;


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : JSON_RespondObject
 * Description : Raspicam Respond with the JSON objects
 *
 * Parameters  : Client, JSON object
 * Returns     : TRUE if OKee
 *
 */
bool JSON_RespondObject(NETCL *pstCl, RPIJSON *pstJsn)
{
   PRINTF2("JSON_RespondObject(): L=%d (Act=%d)" CRLF, pstJsn->iIdx, strlen(pstJsn->pcObject));
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeaderJson, pstJsn->iIdx);
   HTTP_BuildGeneric(pstCl, pstJsn->pcObject);
   return(TRUE);
}

/*
 * Function    : JSON_InsertParameter
 * Description : Insert parameter into JSON object
 *
 * Parameters  : JSON object, parameter, value, style (multi/text/int
 * Returns     : JSON object
 *
 */
RPIJSON *JSON_InsertParameter(RPIJSON *pstJsn, const char *pcParm, char *pcValue, int iStyle)
{
   if(pstJsn == NULL)
   {
      pstJsn = json_AllocateObject(JSON_DEFAULT_OBJECT_SIZE);
   }
   //
   PRINTF3("JSON_InsertParameter():parm=%s, value=%s, style=%d" CRLF, pcParm, pcValue, iStyle);
   //
   if(iStyle & JE_CURLB)   pstJsn = json_Insert(pstJsn, "{");
                           pstJsn = json_Insert(pstJsn, "\"");
                           pstJsn = json_Insert(pstJsn, pcParm);
                           pstJsn = json_Insert(pstJsn, "\"");
                           pstJsn = json_Insert(pstJsn, ":");
   if(iStyle & JE_SQRB)  { pstJsn->iLastComma = -1;              pstJsn = json_Insert(pstJsn, "[\r\n"); }
   if(iStyle & JE_TEXT)    pstJsn = json_Insert(pstJsn, "\"");
                           pstJsn = json_Insert(pstJsn, pcValue);
   if(iStyle & JE_TEXT)    pstJsn = json_Insert(pstJsn, "\"");
   if(iStyle & JE_COMMA) { pstJsn->iLastComma = pstJsn->iIdx;    pstJsn = json_Insert(pstJsn, ", ");     }
   if(iStyle & JE_SQRE)  { pstJsn->iLastComma = pstJsn->iIdx +1; pstJsn = json_Insert(pstJsn, "],\r\n"); }
   if(iStyle & JE_CURLE) { pstJsn->iLastComma = pstJsn->iIdx +1; pstJsn = json_Insert(pstJsn, "},\r\n"); }
   if(iStyle & JE_CRLF)    pstJsn = json_Insert(pstJsn, "\r\n");
   //
   return(pstJsn);
}

/*
 * Function    : JSON_InsertValue
 * Description : Insert single value into JSON object
 *
 * Parameters  : JSON object, value, style (multi/text/int
 * Returns     : JSON object
 *
 */
RPIJSON *JSON_InsertValue(RPIJSON *pstJsn, char *pcValue, int iStyle)
{
   if(pstJsn == NULL)
   {
      pstJsn = json_AllocateObject(JSON_DEFAULT_OBJECT_SIZE);
   }
   //
   PRINTF3("JSON_InsertParameter():parm=%s, value=%s, style=%d" CRLF, pcParm, pcValue, iStyle);
   //
   if(iStyle & JE_CURLB)   pstJsn = json_Insert(pstJsn, "{");
   if(iStyle & JE_SQRB)    pstJsn = json_Insert(pstJsn, "[\r\n");
   if(iStyle & JE_TEXT)    pstJsn = json_Insert(pstJsn, "\"");
                           pstJsn = json_Insert(pstJsn, (const char *)pcValue);
   if(iStyle & JE_TEXT)    pstJsn = json_Insert(pstJsn, "\"");
   if(iStyle & JE_COMMA) { pstJsn->iLastComma = pstJsn->iIdx;    pstJsn = json_Insert(pstJsn, ", ");     }
   if(iStyle & JE_SQRE)  { pstJsn->iLastComma = pstJsn->iIdx +1; pstJsn = json_Insert(pstJsn, "],\r\n"); }
   if(iStyle & JE_CURLE) { pstJsn->iLastComma = pstJsn->iIdx +1; pstJsn = json_Insert(pstJsn, "},\r\n"); }
   if(iStyle & JE_CRLF)    pstJsn = json_Insert(pstJsn, "\r\n");
   //
   return(pstJsn);
}

/*
 * Function    : JSON_TerminateObject
 * Description : Terminate JSON object
 *
 * Parameters  : JSON object
 * Returns     : JSON Object
 *
 */
RPIJSON *JSON_TerminateObject(RPIJSON *pstJsn)
{
   if(pstJsn->iLastComma > 0)
   {
      pstJsn->pcObject[pstJsn->iLastComma] = ' ';
      pstJsn->iLastComma = -1;
   }
   pstJsn = json_Insert(pstJsn, "\r\n}\r\n");

   PRINTF("JSON_TerminateObject()" CRLF);
   //PRINTF1("JSON_TerminateObject():%s" CRLF, pstJsn->pcObject);
   return(pstJsn);
}

/*
 * Function    : JSON_TerminateArray
 * Description : Build JSON array
 *
 * Parameters  : JSON object
 * Returns     : JSON Object
 *
 */
RPIJSON *JSON_TerminateArray(RPIJSON *pstJsn)
{
   if(pstJsn->iLastComma > 0)
   {
      pstJsn->pcObject[pstJsn->iLastComma] = ' ';
      pstJsn->iLastComma = -1;
   }
   pstJsn = json_Insert(pstJsn, "\r\n],\r\n");
   return(pstJsn);
}

/*
 * Function    : JSON_GetObjectSize
 * Description : return JSON object size
 *
 * Parameters  : JSON object
 * Returns     : Size
 *
 */
int JSON_GetObjectSize(RPIJSON *pstJsn)
{
   PRINTF1("JSON_GetObjectSize():%d" CRLF, pstJsn->iIdx);
   return(pstJsn->iIdx);
}

/*
 * Function    : JSON_ReleaseObject
 * Description : Release JSON objects
 *
 * Parameters  : JSON object
 * Returns     : 
 *
 */
void JSON_ReleaseObject(RPIJSON *pstJsn)
{
   PRINTF("JSON_ReleaseObject()" CRLF);
   //
   json_FreeObject(pstJsn);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : json_AllocateObject
 * Description : Build empty JSON object
 *
 * Parameters  : Initial size
 * Returns     : JSON object
 *
 */
static RPIJSON *json_AllocateObject(int iLength)
{
   RPIJSON *pstJsn = safemalloc(sizeof(RPIJSON));

   pstJsn->pcObject     = safemalloc(iLength);
   pstJsn->iIdx         = 0;
   pstJsn->iLastComma   = -1;
   pstJsn->iSize        = iLength;
   pstJsn->iFree        = iLength;
   
   PRINTF1("json_AllocateObject():%d" CRLF, iLength);
   return(pstJsn);
}

/*
 * Function    : json_FreeObject
 * Description : Free JSON object
 *
 * Parameters  : JSON Object
 * Returns     : 
 *
 */
static void json_FreeObject(RPIJSON *pstJsn)
{
   PRINTF2("json_FreeObject():%d bytes used, %d bytes free" CRLF, pstJsn->iIdx, pstJsn->iFree);
   //
   if(pstJsn)
   {
      safefree(pstJsn->pcObject);
      safefree(pstJsn);
   }
}

/*
 * Function    : json_Insert
 * Description : Insert JSON object
 *
 * Parameters  : JSON Object, data
 * Returns     : JSON Object (might have changed)
 *
 */
static RPIJSON *json_Insert(RPIJSON *pstJsn, const char *pcData)
{
   RPIJSON *pstNew = pstJsn;

   int   iIdx=pstNew->iIdx;
   int   iLen=strlen(pcData);

   //PRINTF1("json_Insert():[%s]" CRLF, pcData);
   //
   if(iLen >= pstNew->iFree)
   {
      PRINTF2("json_Insert(): Need more space: size=%d, free=%d" CRLF, iLen, pstNew->iFree);
      pstNew = json_AllocateObject(pstJsn->iSize + JSON_DEFAULT_OBJECT_SIZE);
      strncpy(pstNew->pcObject, pstJsn->pcObject, pstJsn->iSize);
      // Copy new values or necessary values from old object
      pstNew->iIdx       = iIdx;
      pstNew->iFree      = pstNew->iFree - iIdx;
      pstNew->iLastComma = pstJsn->iLastComma;
      json_FreeObject(pstJsn);
   }
   strncpy(&pstNew->pcObject[iIdx], pcData, pstNew->iFree);
   pstNew->iIdx  += iLen;
   pstNew->iFree -= iLen;
   //
   return(pstNew);
}


