/*  (c) Copyright:  2016  Patrn.nl, Confidential Data
**
**  $Workfile:          rpicnc.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            rpicnc header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       1 Apr 2016
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _RPICNC_H_
#define _RPICNC_H_

#include <hidapi/hidapi.h>
#include "netfiles.h"

//
#define  X_AXIS               0
#define  Y_AXIS               1
#define  Z_AXIS               2
//
#define  X_MASK               0x01
#define  Y_MASK               0x02
#define  Z_MASK               0x04
#define  ALL_MASK             (X_MASK|Y_MASK|Z_MASK)
//
#define  DEF_CNC_DELAY        100
#define  DEF_PULSE_WIDTH      100
#define  USB_READ_MSECS       10
//
#define  DEF_X_ACCEL          10000.0
#define  DEF_X_SPEED_MIN      400.0
#define  DEF_X_SPEED_MAX      800.0
#define  DEF_Y_ACCEL          10000.0
#define  DEF_Y_SPEED_MIN      400.0
#define  DEF_Y_SPEED_MAX      800.0
#define  DEF_Z_ACCEL          10000.0
#define  DEF_Z_SPEED_MIN      400.0
#define  DEF_Z_SPEED_MAX      400.0

//
// Gear and Cogwheels : M1, t=20, r=((22.0+18.0)/2)/2 = 10.0 mm
//
#define  CNC_COGWHEEL_RADIUS  10.3        // Radius of the steppermotor cogwheel
#define  CNC_STEPS_PER_REV    400         // Steps per X, Y steppermotor revolution
//
#define  CNC_CANVAS_W         800.0
#define  CNC_CANVAS_H         520.0
#define  CNC_DRAWING_W        400.0
#define  CNC_DRAWING_H        300.0
//
#define  CNC_DRAWING_XS       (CNC_CANVAS_W-CNC_DRAWING_W)/2
#define  CNC_DRAWING_XE       (CNC_DRAWING_XS+CNC_DRAWING_W)
#define  CNC_DRAWING_YS       50.0
#define  CNC_DRAWING_YE       (CNC_DRAWING_YS+CNC_DRAWING_H)
//
#define  min(a,b)             (a < b ? a : b)
#define  max(a,b)             (a > b ? a : b)

#define  CNC____        0x00000000     // No command
//
// HTTP Functions (callback through HTML or JSON)
//
#define  CNC_PIX        0x00000001     // List of pictures
#define  CNC_PRM        0x00000004     // List of All Parameters
#define  CNC_RMP        0x00000008     // Remove fotos
#define  CNC_DEF        0x00000010     // Defaults
#define  CNC_STP        0x00000020     // Stop
#define  CNC_DRW        0x00000040     // Draw Mode parameters
#define  CNC_DGM        0x00000080     // CNC specific vars theApp gets through unsolicited datagrams
#define  CNC_ALL        0x00000FFF     // All HTTP functions
//
// Var types
//
#define  CNC_TXT        0x00001000     // Text
#define  CNC_DBL        0x00002000     // Double prec
#define  CNC_FLT        0x00004000     // Floating point
#define  CNC_LNG        0x00008000     // Long
#define  CNC_INT        0x00010000     // Integer
//
// JSON markers for text-style-vars ("value" = "123456")
//             or number-style-vars ("value" =  123456 )
//
#define  JSN_TXT        0x80000000     // JSON Variable is text
#define  JSN_INT        0x40000000     // JSON Variable is integer
//
#define  ALWAYS_CHANGED -1
#define  NEVER_CHANGED  -2
//
// CNC states
//
#define  EXTRACT_STH(a,b,c,d,e) a,
typedef enum _CNCST_
{
   #include "cncstates.h"
   NUM_CNC_STATES
}  CNCST;
#undef   EXTRACT_STH

typedef enum _dump_mode_
{
   DUMP_COMMS    = 0,
   DUMP_PARMS,
   DUMP_ASCII,
   DUMP_REPLY,
   DUMP_HEX
}  DUMPMODE;

typedef enum _cnc_patterns_
{
   CNC_PAT_ROLB  = 0,                     // Pattern Lower right corner to Upper Left
   CNC_PAT_LORB,                          //         Lower Left            Upper Right
   CNC_PAT_RBLO,                          //         Upper Right           Lower Left
   CNC_PAT_LBRO,                          //         Upper Left            Lower Right
   //
   CNC_NUM_PATS
}  CNCPATS;
//
// CNC drawing modes
//
#define  EXTRACT_DRW(a,b,c,d,e,f,g) a,
typedef enum _cnc_draw_
{
   #include "cncdraw.h"
   NUM_CNC_DRAWS
}  CNCDRAW;
#undef   EXTRACT_DRW

//
// CNC Stoare/Vars xref list
//
#define  EXTRACT_CNC(a,b,c,d,e,f,g,h,i) a,
typedef enum _CNCX_
{
   #include "cncxref.h"
   NUM_CNC_VARS
}  CNCX;
#undef   EXTRACT_CNC

//
// CNC Status
//
typedef enum _cnc_status_
{
   CNC_STATUS_IDLE = 0,             // Cnc is idle
   CNC_STATUS_ERROR,                // Cnc is error
   CNC_STATUS_RUN,                  // Cnc is run mode
   CNC_STATUS_MOTION,               // Cnc is doing motion detect
   CNC_STATUS_STOPPING,             // Cnc is stopping
   CNC_STATUS_STOPPED,              // Cnc is stopped
   CNC_STATUS_REJECTED,             // Cnc is still busy: no new command
   CNC_STATUS_KEYPRESS,             // Cnc has key pressed
   CNC_STATUS_KEYRELEASE,           // Cnc has key released
   //
   NUM_CNC_STATUS,
   CNC_STATUS_ASK                   // Return current status
}  CNCSTA;

//
// CNC Runmodes
//
typedef enum _cnc_modes_
{
   CNC_RUNMODE_IDLE = 0,            // Cnc is idle
   CNC_RUNMODE_EXEC,                // Cnc is executing
   CNC_RUNMODE_PARM,                // Cnc has new parameters
   CNC_RUNMODE_STOP,                // Cnc should stop
   CNC_RUNMODE_EXIT,                // Cnc should exit
   //
   NUM_CNC_MODES,
}  CNCMODE;

typedef enum CNCOPT
{
   CNCOPT_NONE = 0,                 // 
   CNCOPT_SWITCH_ON,                // Option is flag and  ON
   CNCOPT_SWITCH_OFF,               //                     OFF
   CNCOPT_VALUE_ON,                 //           value and present
   CNCOPT_VALUE_OFF,                //                     not present
}  CNCOPT;

//
// CNC Reset
//
typedef enum _cnc_reset_
{
   CNC_RESET_COLD    = 0,
   CNC_RESET_WARM,
}  CNCRST;

//
// CNC Commands
//
typedef enum _cnc_gcmd_
{
   G_CMD_STOP           = -2,
   G_CMD_IDLE           = -1,
   G_CMD_MOVE           = 0,
   G_CMD_FEED_LIN       = 1,
   G_CMD_FEED_CW        = 2,
   G_CMD_FEED_CCW       = 3,
   G_CMD_EXACT_STOP     = 9,
   G_CMD_CIRCLE_CW      = 12,
   G_CMD_CIRCLE_CCW     = 13,
   G_CMD_MODE_INCH      = 20,
   G_CMD_MODE_MM        = 21,
   G_CMD_SCALING_CANCEL = 50,
   G_CMD_SCALING        = 51,
   G_CMD_ABS_POS        = 90,
   G_CMD_REL_POS        = 91,
   G_CMD_SET_POS        = 92,
   G_CMD_CONST_SPEED    = 97
}  GCMD;

typedef enum _cnc_mcmd_
{
   M_CMD_IDLE           = -1,
   M_CMD_MAN_STOP       = 0,
   M_CMD_OPT_STOP       = 1,
   M_CMD_PGM_END        = 2,
   M_CMD_ENABLE         = 17,
   M_CMD_DISABLE        = 18,
   M_CMD_PGM_END_REWIND = 30
}  MCMD;

typedef union _unvar_
{
   char       *pcVar;
   u_int16     sVar;
   int         iVar;
   float       fVar;
   double      dVar;
   long        lVar;
   long long   llVar;
}  UNVAR;

typedef struct _cnc_draw_modes_
{
   int         iState;
   int         iParm1;
   int         iParm2;
   int         iParm3;
   int         iParm4;
   const char *pcHelper;
}  CNCDRW;

typedef struct _rpipage_
{
   int      iFunction;
   char    *pcFunction;
   bool   (*pfHandler)(NETCL *, struct _rpipage_ *, FTYPE);
}  RPIPAGE;
//
typedef bool (*PFRPI)(NETCL *, RPIPAGE *, FTYPE);

typedef struct CNCPARMS
{
   int         iFunction;
   int         iPrec;
   const char *pcJson;
   const char *pcHttp;
   int         iValueOffset;
   int         iStorageOffset;
   int         iStorageSize;
   int         iChangedOffset;
}  CNCPARMS;

typedef struct _cnc_boundaries_
{
   float    fXadj;
   float    fYadj;
}  CNCBND;

//
// Global prototypes
//
void  CNC_Init          (RPICNC *);
int   CNC_Execute       (RPICNC *, volatile int *);
int   CNC_GetMotion     (char *);

#endif  /*_RPICNC_H_ */

