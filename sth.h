/*  (c) Copyright:  2016 Patrn ESS, Confidential Data
**
**  $Workfile:          sth.h
**  $Revision:  
**  $Modtime:   
**
**  Purpose:            Simple State handler
**
**
**
**
**
 *  Compiler/Assembler: Raspbian
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       3 Jun 2016
**
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
**/


typedef struct _sth_
{
   int         tCurState;
   int         tNxtState;
   int         tErrState;
   int       (*pfHandler)(const struct _sth_ *, int, void *);
   const char *pcHelper;
}  STH;

//
// STH global functs
//
int   STH_Init       (const STH *);
int   STH_Execute    (const STH *, int, void *);
