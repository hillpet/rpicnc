/*  (c) Copyright:  2016 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            rpihttp is a simple dynamic HTTP server used to control the RPi
**
**  Entry Points:       HTTP_Deamon()
**
**  Client-Server model:
**
**       SERVER                     CLIENT
**         |                           |
**         |           +-------+       |
**         |           |   0   |       |
**         +----->-----|  cmd  |--->---+ SIGUSR2
**         |           |  data |       |
**         |           +-------+       |
**         |                         Handle cmd
**         |                           |
**         |           +-------+       |
**         |           |  cc   |---<---+ SIGUSR1
**         +-----<-----|  cmd  |       |
**         |           |  data |       |
**         |           +-------+       |
**         |                           |
**         |                           |
**         |           +-------+       |
**         |           |  cc   |       |
**         +----->-----|   0   |--->---+ SIGUSR2
**         |           |   0   |       |
**         |           +-------+       |
**         |                         Cleanup 
**         |                           |
**         |           +-------+       |
**         |           |   0   |---<---+ SIGUSR1
**         +-----<-----|   0   |       |
**         |           |   0   |       |
**         |           +-------+       |
**         |                           |
**
**
**      SERVER                      BROWSER:http exit command
**         |                           |
**         +---------------<-----------+ SIGINT
**         |                           |
**         |                           
**         +---->--- SIGINT Client thread
**         |                           
**         +---->--- SIGINT Exec thread
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 May 2016
**                                   
 *  Revisions:
 *    v400  Clone for Rpi#3 rpicnc App
 *
 *
 *
 **/

#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <resolv.h>

#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "echotype.h"
#include "log.h"
#include "globals.h"
#include "rpihttp.h"

//#define USE_PRINTF
#include "printf.h"


#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

static int     http_Deamon             (void);
//
static bool    http_SignalRegister     (sigset_t *);
static void    http_ReceiveSignalUser1 (int);
static void    http_ReceiveSignalUser2 (int);
static void    http_ReceiveSignalInt   (int);
static void    http_ReceiveSignalTerm  (int);
//
volatile static bool bCncRunning = TRUE;
//
static NETCON *pstNet         = NULL;
static bool    bRpiIpOkee     = FALSE;
//

/* ======   Local Functions separator ===========================================
___Global_functions(){}
==============================================================================*/

//
//  Function:  HTTP_Deamon
//  Purpose:   Run the HTTP server deamon
//
//  Parms:     
//  Returns:   The deamon PID for the parent only
//
pid_t HTTP_Deamon(void)
{
   int      iCc;
   pid_t    tPid;
   CNCMAP  *pstMap;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent returns with the deamon PID
         PRINTF1("HTTP_Deamon(): HTTP server deamon pid=%d" CRLF, tPid);
         break;

      case 0:
         // HTTP server Deamon
         PRINTF("HTTP_Deamon(): HTTP server Deamon running" CRLF);
         iCc = http_Deamon();
         pstMap = GLOBAL_GetMapping();
         pstMap->G_tSrvrPid = 0;
         PRINTF("HTTP_Deamon(): HTTP server Deamon EXIT" CRLF);
         exit(iCc);
         break;

      case -1:
         // Error
         ES_printf("HTTP_Deamon(): HTTP server Error!"CRLF);
         exit(255);
         break;
   }
   return(tPid);
}


/* ======   Local Functions separator ===========================================
___Local_functions(){}
==============================================================================*/

//
//  Function:   http_Deamon
//  Purpose:    Main entry point for HTTP server deamon
//
//  Parms:      
//  Returns:    Exit codes
//
static int http_Deamon(void)
{
   int         iConnections, x;
   sigset_t    tBlockset;
   CNCMAP     *pstMap=GLOBAL_GetMapping();

   if(http_SignalRegister(&tBlockset) == FALSE) 
   {
      pstMap->G_tSrvrPid = 0;
      exit(254);
   }
   //
   // Set up server socket 
   //
   pstNet = NET_Init();
   if( NET_ServerConnect(pstNet, pstMap->G_pcTcpPort, RPI_PROTOCOL) == -1)
   {
      ES_printf("http_Deamon(): Server connect failed: exiting" CRLF);
      LOG_Report(errno, "SRV", "http_Deamon(): Server connect failed: exiting.");
      pstMap->G_tSrvrPid = 0;
      exit(254);
   }
   LOG_Report(0, "SRV", "http_Deamon(): Server connected OKee, port=%s", pstMap->G_pcTcpPort);
   //
   // Wait for data
   //
   while(bCncRunning)
   {
      NET_BuildConnectionList(pstNet);
      //
      // Retrieve the IP address for ETH0
      //
      if(bRpiIpOkee == FALSE) 
      {
         bRpiIpOkee = NET_GetIpInfo();
         if(bRpiIpOkee) 
         {
            LOG_Report(0, "SRV", "http_Deamon():NET_GetIpInfo:my IP=%s (Ifce=%s)", pstMap->G_pcTcpAddr, pstMap->G_pcTcpIfce);
         }
      }
      iConnections = NET_WaitForConnection(pstNet, RPI_CONNECTION_TIMEOUT_MSECS);
      if(iConnections > 0)
      {
         //
         // There is data on some of the sockets
         //
         // PRINTF1("http_Deamon():Connections=%d" CRLF, iConnections);
         NET_HandleSessions(pstNet);
      }
   }
   //
   // Exit HTTP server thread: close all sockets
   //
   if(pstNet)
   {
      for (x=0; x<NUM_CONNECTIONS; x++) 
      {
         if (pstNet->stClient[x].iSocket) 
         {
            NET_ServerDisconnect(&pstNet->stClient[x]); 
         }
      }
      shutdown(pstNet->iSocketConnect, 2);
   }
   exit(0);
}

/* ======   Local Functions separator ===========================================
___Local_functions(){}
==============================================================================*/

//
//  Function:   http_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool http_SignalRegister(sigset_t *ptBlockset)
{
  bool bCC = TRUE;

  // Local signals :
  if( signal(SIGUSR1, &http_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "SRV", "http_SignalRegister(): SIGUSR1 ERROR");
     bCC = FALSE;
  }
  if( signal(SIGUSR2, &http_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "SRV", "http_SignalRegister(): SIGUSR2 ERROR");
     bCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &http_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "SRV", "http_SignalRegister(): SIGTERM ERROR");
     bCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &http_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "SRV", "http_SignalRegister(): SIGINT ERROR");
     bCC = FALSE;
  }
  //
  if(bCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGTERM);
  }
  return(bCC);
}

//
//  Function:   http_ReceiveSignalUser1
//  Purpose:    SIGUSR1 (10) signal from a thread
//
//  Parms:
//  Returns:    
//
static void http_ReceiveSignalUser1(int iSignal)
{
   LOG_Report(0, "SRV", "http_ReceiveSignalUser1()");
}
//
//  Function:   http_ReceiveSignalUser2
//  Purpose:    SIGUSR2 (12) signal from a thread
//
//  Parms:
//  Returns:    
//
static void http_ReceiveSignalUser2(int iSignal)
{
   LOG_Report(0, "SRV", "http_ReceiveSignalUser2(): Not used");
}

//
//  Function:   http_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void http_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "SRV", "http_ReceiveSignalInt()");
   bCncRunning = FALSE;
}

//
//  Function:   http_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void http_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "SRV", "http_ReceiveSignalTerm()");
}



