/*  (c) Copyright:  2016  Patrn.nl, Confidential Data
**
**  $Workfile:          rpiusb.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            rpiusb header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       1 Apr 2016
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _RPIUSB_H_
#define _RPIUSB_H_

#include <hidapi/hidapi.h>

//
// Show/Dump USB settings
//
#ifdef FEATURE_DUMP_USB
 void    USB_Dump             (COMMS *, int, DUMPMODE, const char *);
 #define USB_DUMP(a,b,c,d)    USB_Dump(a,b,c,d)
#else    //FEATURE_DUMP_USB
 #define USB_DUMP(a,b,c,d)
#endif   //FEATURE_DUMP_USB
//
//
// Global prototypes
//
RPIUSB  *USB_GetInfo          (int, int);
RPIUSB  *USB_GetDeviceInfo    (RPIUSB *, int, int, int);
bool     USB_Open             (RPIUSB *);
int      USB_Read             (RPIUSB *, u_int8 *, int, int);
int      USB_Write            (RPIUSB *, u_int8 *, int);
bool     USB_Close            (RPIUSB *);
void     USB_ReleaseInfo      (RPIUSB *);

#endif  /*_RPIUSB_H_ */

