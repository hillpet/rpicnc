/*  (c) Copyright:  2014..2016  Patrn.nl, Confidential Data
**
**  $Workfile:          graphics.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            graphics header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       29 Apr 2014
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

//
// Define TFT size in pixels
//
#define GRF_ACTUAL_ROWS       240
#define GRF_ACTUAL_COLS       320
#define GRF_GRID_COUNT        10

typedef struct MDET
{
   int         iSeq;                // Detect sequence 0..1
   int         iGridWidth;          // Grid Width
   int         iGridHeight;         // Grid Height
   int         iZoomX;              // Zoom Column
   int         iZoomY;              // Zoom Line
   int         iZoomW;              // Zoom Width
   int         iZoomH;              // Zoom Height
   int         iAverage1;           // Graylevel average picture 1
   int         iAverage2;           // Graylevel average picture 2
   int         iThldRed;            // Pixel threshold RED
   int         iThldGreen;          // Pixel threshold GREEN
   int         iThldBlue;           // Pixel threshold BLUE
   int         iThldPixel;          // Pixel motion threshold
   int         iThldLevel;          // Motion average percentage
   //
   int         iGridMotion[GRF_GRID_COUNT][GRF_GRID_COUNT];
   int         iGridScales[GRF_GRID_COUNT][GRF_GRID_COUNT];
}  MDET;

//
// Global prototypes
//
int      GRF_Init             ();
int      GRF_MotionDelta      (char *, const char *, MDET *);
int      GRF_MotionDetect     (char *, const char *, MDET *);
void     GRF_MotionDisplay    (char *);
int      GRF_MotionNormalize  (char *, const char *, MDET *);
int      GRF_MotionZoom       (char *, const char *, MDET *);

#endif  /*_GRAPHICS_H_ */

