/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:   $     misc_api.c
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:           Misc function calls
**                     
**
**
**  Entry Points:      MISC_FindDelimiter()
**                    
**
**
**
**
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 **/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
 
#include "echotype.h"
#include "misc_api.h"

//#define USE_PRINTF
#include "printf.h"

//                                      0         1         2         3         4         5         6
//                                      0123456789012345678901234567890123456789012345678901234567890123
static const char   cMiscFunctions[] = "01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.";

// 
//  Name:         MISC_CopyToDelimiter
//  Description:  This function copies until the required delimiter
//  Arguments:    Delimiter, Dest, Src, max size
// 
//  Returns:      Updated Src ptr
// 
char *MISC_CopyToDelimiter(DELIM tDelimiter, char *pcDest, char *pcSrc, int iMaxLen)
{
  bool  fCopy;
  int   iSize = 0;

  if(pcSrc == NULL) fCopy = FALSE;
  else              fCopy = TRUE;

  while(fCopy == TRUE)
  {
     if(iMaxLen > 0)
     {
        if( (*pcSrc == '\0') || (*pcSrc == tDelimiter) ) fCopy = FALSE;
        else
        {
           *pcDest++ = *pcSrc++;
           iSize++;
        }
     }
  }
  *pcDest = '\0';
  return(pcSrc);
}

// 
//  Name:         MISC_FindDelimiter
//  Description:  This function looks for the required delimiter
//  Arguments:    Record
//                Delimiter
// 
//  Returns:      Buffer pointer on delimiter or NULL if not found
// 
char *MISC_FindDelimiter(char *pcRecord, DELIM tDelimiter)
{
  if(pcRecord == NULL) return(NULL);

  switch(tDelimiter)
  {
     case DELIM_NOSPACE:
        // Skip to next non-space char
        while(*pcRecord && *pcRecord == ' ') pcRecord++;
        break;

     case DELIM_SPACE:
        // Skip to next space char
        while(*pcRecord && *pcRecord != ' ') pcRecord++;
        break;

     case DELIM_COLON:
        // Skip to next : char
        while(*pcRecord && *pcRecord != ':') pcRecord++;
        break;

     case DELIM_NUMERIC:
        // Skip to next 0-9 char
        while(*pcRecord && (*pcRecord < '0' || *pcRecord > '9')) pcRecord++;
        break;

     case DELIM_COMMA:
        // Skip to next , char
        while(*pcRecord && *pcRecord != ',') pcRecord++;
        break;

     case DELIM_PERIOD:
        // Skip to next . char
        while(*pcRecord && *pcRecord != '.') pcRecord++;
        break;

     case DELIM_NEXTFIELD:
        // Skip to the beginning of the next word
        while(*pcRecord && *pcRecord != ' ') pcRecord++;
        while(*pcRecord && *pcRecord == ' ') pcRecord++;
        break;

     case DELIM_ASTERIX:
        // Skip to next * char
        while(*pcRecord && *pcRecord != '*') pcRecord++;
        break;

     case DELIM_EQU:
        // Skip to next = char
        while(*pcRecord && *pcRecord != '=') pcRecord++;
        break;

     case DELIM_STATIC:
        // Skip to next @ char
        while(*pcRecord && *pcRecord != '@') pcRecord++;
        break;

     case DELIM_CR:
        // Skip to next \r char
        while(*pcRecord && *pcRecord != '\r') pcRecord++;
        break;

     case DELIM_LF:
        // Skip to next \n char
        while(*pcRecord && *pcRecord != '\n') pcRecord++;
        break;

     case DELIM_TAB:
        // Skip to next \t char
        while(*pcRecord && *pcRecord != '\t') pcRecord++;
        break;

     default:
        pcRecord = NULL;
        break;
  }
  //
  // return NULL if no delimiter found
  //
  if(pcRecord)
  {
     if(*pcRecord =='\0') pcRecord = NULL;
  }
  return(pcRecord);
}

// 
//  Name:         MISC_GetPublicKey
//  Description:  
//  Arguments:    Buffer, size
// 
//  Returns:      void
// 
void MISC_GetPublicKey(u_int8 *pubKey, int iSize)
{
  int   i, j;

  for(i=0; i<iSize; i++)
  {
     j = (((i+3)<<3)+7)& 0x3f;
     *pubKey++ = cMiscFunctions[j];
  }
}

// 
//  Name:         MISC_RemoveChar
//  Description:  This function removes all chars from the record
//  Arguments:    Buffer, char to remove (and replace into space)
// 
//  Returns:      Number of removed chars
// 
int MISC_RemoveChar(char *pcDest, char cRemove)
{
   int iNr = 0;

   if(pcDest)
   {
      while(*pcDest)
      {
         if(*pcDest == cRemove)
         {
            *pcDest = ' ';
            iNr++;
         }
         pcDest++;
      }
   }
   return(iNr);
}

// 
//  Name:         MISC_TrimAll
//  Description:  This function trims leading and trailing spaces from the record
//  Arguments:    Buffer
// 
//  Returns:      Trimmed buffer ^
// 
char *MISC_TrimAll(char *pcBuffer)
{
   pcBuffer = MISC_TrimLeft(pcBuffer);
   return(MISC_TrimRight(pcBuffer));
}

// 
//  Name:         MISC_TrimLeft
//  Description:  This function trims leading spaces from the record
//  Arguments:    Buffer
// 
//  Returns:      Trimmed buffer ^
// 
char *MISC_TrimLeft(char *pcBuffer)
{
   switch(*pcBuffer)
   {
      default :
         return(pcBuffer);

      case ' ':   
         pcBuffer++;
         break;
   }
   return(pcBuffer);
}

// 
//  Name:         MISC_TrimRight
//  Description:  This function trims trailing spaces from the record
//  Arguments:    Buffer
// 
//  Returns:      Trimmed buffer ^
// 
char *MISC_TrimRight(char *pcBuffer)
{
   int   x=strlen(pcBuffer);

   while(x--)
   {
      switch(pcBuffer[x])
      {
         default :
            return(pcBuffer);

         case ' ':   
            break;
      }
   }
   return(pcBuffer);
}


/**
 **  Name:         MISC_DumpData
 **  Description:  Dump a block of data in lines/cols
 **  Arguments:    pcHeader, Nr of Columns, pubData, iSize
 **
 **  Returns:      void
 **/
void MISC_DumpData(char *pcHeader, int iCols, u_int8 *pubBuffer, int iLength)
{
  int i, x;
  
  ES_printf("%s (%d)"CRLF, pcHeader, iLength);

  for(x=0, i=0; i<iLength; i++)
  {
     ES_printf("%02X", pubBuffer[i]);
     if(i%iCols == (iCols-1)) 
     {
        ES_printf("  ");
        while(x<=i)
        {
           if( isprint((int)pubBuffer[x])) ES_printf("%c", pubBuffer[x]);
           else                            ES_printf(".");
           x++;
        }
        ES_printf("\n");
     }
     else
     {
        ES_printf(" ");
     }
  }
  ES_printf("\n");
}

/**
 **  Name:         ubyte_get
 **
 **  Description:  Gets a ubyte from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      u_int8
 **/
u_int8 ubyte_get(u_int8 *pubSrc, u_int8 ubMask, u_int8 ubShift)
{
   u_int8 ubTemp;

   ubTemp   = *pubSrc;
   ubTemp >>= ubShift;
   ubTemp  &= ubMask;

   return (ubTemp);
}

/**
 **  Name:         ulong_get
 **
 **  Description:  Gets a ulong from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      u_int32
 **/
u_int32 ulong_get(u_int8 *pubSrc, u_int32 ulMask, u_int8 ubShift)
{
   u_int32 ulTemp;

   ulTemp = ((u_int32)*pubSrc       << 24) +
            ((u_int32)*(pubSrc + 1) << 16) +
            ((u_int32)*(pubSrc + 2) <<  8) +
             (u_int32)*(pubSrc + 3);
   ulTemp >>= ubShift;
   ulTemp &= ulMask;

   return (ulTemp);
}

/**
 **  Name:         ulong_put
 **
 **  Description:  Puts a ulong into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *ulong_put(u_int8 *ubPtr, u_int32 ulValue)
{
   *ubPtr++ = (u_int8) (ulValue >> 24);
   *ubPtr++ = (u_int8) (ulValue >> 16);
   *ubPtr++ = (u_int8) (ulValue >> 8);
   *ubPtr++ = (u_int8) (ulValue);

   return(ubPtr);
}

/**
 **  Name:         ulong_to_BCD
 **
 **  Description:  Converts a u_int32 value into it's BCD representation.
 **
 **  Arguments:    Value to convert
 **
 **  Returns:      Converted value
 **/

u_int32 ulong_to_BCD(u_int32 ulValue)
{
   u_int8 ubIndex,
         ubShift = 0;

   u_int32 ulBcdDigit,
         ulRet = 0;

   for (ubIndex = 0; ubIndex < 8; ubIndex++)
   {
      ulBcdDigit   = ulValue % 10;
      ulBcdDigit <<= ubShift;
      ulRet       |= ulBcdDigit;
      ulValue     /= 10;
      ubShift     += 4;
   }

   return(ulRet);
}

/**
 **  Name:         ushort_get
 **
 **  Description:  Get's ushort value out of byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      value
 **/

u_int16 ushort_get(u_int8 *ubPtr, u_int16 usMask, u_int8 ubShift)
{
   u_int16 usTemp;

   usTemp = (*ubPtr << 8) + *(ubPtr + 1);
   usTemp >>= ubShift;
   usTemp  &= usMask;

   return (usTemp);
}

/**
 **  Name:         ushort_put
 **
 **  Description:  Puts a ushort into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *ushort_put(u_int8 *ubPtr, u_int16 usValue)
{
   *ubPtr++ = (u_int8) (usValue >> 8);
   *ubPtr++ = (u_int8) (usValue);

   return(ubPtr);
}


/**
 **  Name:         ushort_to_BCD
 **
 **  Description:  Converts a u_int16 value into it's BCD representation.
 **
 **  Arguments:    Value to convert
 **
 **  Returns:      Converted value
 **/

u_int16 ushort_to_BCD(u_int16 usValue)
{
   u_int8  ubIndex,
          ubShift = 0;

   u_int16 usBcdDigit,
          usRet = 0;

   for (ubIndex = 0; ubIndex < 4; ubIndex++)
   {
      usBcdDigit   = usValue % 10;
      usBcdDigit <<= ubShift;
      usRet       |= usBcdDigit;
      usValue     /= 10;
      ubShift     += 4;
   }

   return(usRet);
}

/**
 **  Name:         ushort_update
 **
 **  Description:  Updates a ushort in a byte array with an offset
 **
 **  Arguments:    array ^, offset
 **
 **  Returns:      None
 **/

void ushort_update(u_int8 *ubPtr, u_int16 usOffset)
{
   u_int16 usValue;

   //
   // Get value from array
   //
   usValue  = ushort_get(ubPtr, 0xFFFF, 0);

   // 
   // Update the value
   //
   usValue += usOffset;

   //
   // Put updated value back into array
   //
   ushort_put(ubPtr, usValue);
}

//
//  Function:  MISC_AsciiToInt
//  Purpose:   Ascii int to int
//  Parms:     pcBuffer
//
//  Returns:   The value
//
int MISC_AsciiToInt(char *pcBuffer)
{
   int   iTemp=0;

   iTemp = atoi(pcBuffer);
   return(iTemp);
}

//
//  Function:  MISC_AsciiToLong
//  Purpose:   Ascii long to long
//  Parms:     pcBuffer
//
//  Returns:   The number
//
long MISC_AsciiToLong(char *pcBuffer)
{
   long  lTemp=0;

   lTemp = atol(pcBuffer);
   return(lTemp);
}

//
//  Function:  MISC_AsciiToFloat
//  Purpose:   Ascii float to float
//  Parms:     pcBuffer
//
//  Returns:   The number
//
float MISC_AsciiToFloat(char *pcBuffer)
{
   float fTemp=0.0;

   fTemp = atof(pcBuffer);
   return(fTemp);
}
