/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:
**                     Offer the API as an abstraction layer for EOS calls
**
**
**  Entry Points:
**                     LOG_printf()
**                     LOG_AppendRecordCr()
**
**
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
 **/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <sys/types.h>

#include "echotype.h"
#include "config.h"
#include "globals.h"
#include "log.h"
#include "rtc.h"
#include "eos_api.h"
#include "safecalls.h"

//#define USE_PRINTF
#include "printf.h"


static FILE   *ptLogFile         = NULL;

/* ======   Local Functions separator ===========================================
void ___main_functions(){}
==============================================================================*/
//
//  Function:   LOG_Assert
//  Purpose:    Debug assert
//              
//  Parms:      Module, true = exit
//  Returns:    
//
void LOG_Assert(char *pcMod, bool fAbort)
{
  if(fAbort)
  {
     LOG_Report(0, pcMod, "Assert !!!");
     exit(254);
  }
}

//
//  Function:   LOG_AddPid
//  Purpose:    Add PID to VAR dir
//              
//  Parms:      Pathname of PID file
//  Returns:    1 = error
//
int LOG_AddPid(char *pcFile)
{
   int   iCc = 1;
   FILE *pstFile;
        
   remove(pcFile);
   if( (pstFile = fopen(pcFile, "w")) != NULL )
   {
      fprintf(pstFile, "%i", getpid());
      fclose(pstFile);
      iCc = 0;
   }
   return(iCc);   
}

//
//  Function:   LOG_RemovePid
//  Purpose:    Remove PID file
//              
//  Parms:      Pathname of PID file
//  Returns:    
//
int LOG_RemovePid(char *pcFile)
{
  return(remove(pcFile));
}

//
//  Function:   LOG_ReportOpenFile
//  Purpose:    Open a new logfile
//              
//  Parms:      Filename
//  Returns:    
//
bool LOG_ReportOpenFile(char *pcFilename) 
{
  bool  fCC = FALSE;
  FILE *ptFile;

  LOG_ReportCloseFile();

  ptFile = fopen(pcFilename, "a+");

  if(ptFile) 
  {
     ptLogFile = ptFile;
     fCC       = TRUE;
  }
  return(fCC);
}

//
//  Function:   LOG_ReportFlushFile
//  Purpose:    Flush logfile
//              
//  Parms:      
//  Returns:    
//
void LOG_ReportFlushFile(void) 
{
  if(ptLogFile)
  {
     safefflush(ptLogFile);
  }
}

//
//  Function:   LOG_ReportCloseFile
//  Purpose:    Close logfile
//              
//  Parms:      
//  Returns:    
//
void LOG_ReportCloseFile(void) 
{
  if(ptLogFile)
  {
     safefclose(ptLogFile);
     ptLogFile = NULL;
  }
}

//
//  Function:   LOG_Report
//  Purpose:    Report formatted string to the logfile
//              
//  Parms:      
//  Returns:    
//
void LOG_Report(int iError, char *pcCaller, char *pcFormat, ...) 
{
   char        cTemp[200];
   char        cLog[DATE_TIME_SIZE];
   va_list     tArgs;
   FILE       *ptFile;

   if(ptLogFile == NULL) ptFile = stderr;
   else                  ptFile = ptLogFile;
   //
   // Put out the (error) message
   //
   RTC_GetDateTimeSecs(cLog);
   //
   fprintf(ptFile, "%s[%s] ", cLog, pcCaller);
   if(iError) 
   {
      fprintf(ptFile, " %s ", strerror(iError));
   }
   va_start(tArgs, pcFormat);
   //
   // Put out data to virtual screen as well
   //
   vsnprintf(cTemp, 200, pcFormat, tArgs);
   fprintf(ptFile, cTemp);
   va_end(tArgs);
   //
   fprintf(ptFile, CRLF);
   safefflush(ptFile);
}

/**
 **  Name:         LOG_printf
 **
 **  Description:  Log with a time stamp
 **
 **  Arguments:    Formatted string
 **
 **  Returns:
 **/
int  LOG_printf(const char *pcFormat, ...)
{
#ifdef  FEATURE_LOG_PRINTF_TO_FILE

   int         iCC;
   char        cLog[DATE_TIME_SIZE];
   va_list     tArgs;
   FILE       *ptFile;

   if(ptLogFile == NULL) ptFile = stderr;
   else                  ptFile = ptLogFile;

   RTC_GetDateTimeSecs(cLog);
  
   fprintf(ptFile, "%s[LOG] ", cLog);

   va_start(tArgs, pcFormat);
   iCC = vprintf(pcFormat, tArgs);
   vfprintf(ptFile, pcFormat, tArgs);
   fprintf(ptFile, CRLF);
   safefflush(ptFile);
   return(iCC);

#else   //FEATURE_LOG_PRINTF_TO_FILE

   int         iCC;
   char        cLog[DATE_TIME_SIZE];
   va_list     tArgs;

   RTC_GetDateTimeSecs(cLog);
   ES_printf("%s %5d| ", cLog, getpid());
   va_start(tArgs, pcFormat);
   iCC = vprintf(pcFormat, tArgs);
   ES_printf(CRLF);
   return(iCC);

#endif  //FEATURE_LOG_PRINTF_TO_FILE
}

/**
 **  Name:         LOG_AppendRecordCr
 **
 **  Description:  Append the record plus CrLf to the Logfile
 **
 **  Arguments:
 **
 **  Returns:
 **/
int LOG_AppendRecordCr(char *pcRecord)
{
  return(-1);
}

/*------  Local functions separator ------------------------------------
__Local_functions_(){};
----------------------------------------------------------------------------*/


